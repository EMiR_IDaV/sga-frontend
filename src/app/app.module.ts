import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Forms
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { HttpClientModule  } from '@angular/common/http';

// Routes
import { APP_ROUTING } from './app.routes';

// Components PageWeb
import { AppComponent } from './app.component';
import { AboutComponent } from './institute/about/about.component';
import { FeatureComponent } from './institute/about/feature/feature.component';
import { HomeComponent } from './institute/home/home.component';
import { InteractionComponent } from './institute/interaction/interaction.component';
import { InvestigationComponent } from './institute/investigation/investigation.component';
import { PostgraduateComponent } from './institute/postgraduate/postgraduate.component';
import { FooterComponent } from './shared/footer/footer.component';
import { HeaderComponent } from './shared/header/header.component';
import { NavComponent } from './shared/nav/nav.component';
import { PersonComponent } from './institute/person/person.component';
import { NoticeComponent } from './institute/notices/notice/notice.component';
import { NoticesComponent } from './institute/notices/notices.component';
import { PublicationsComponent } from './institute/investigation/publications/publications.component';
import { TeachersComponent } from './institute/investigation/teachers/teachers.component';
import { EventsComponent } from './institute/interaction/events/events.component';
import { EventComponent } from './institute/interaction/event/event.component';


// Services
import { DataApiService } from './services/data-api.service';
import { SubirArchivoService } from './services/subir-archivo.service';
import { AutentificacionService } from './services/autentificacion.service';


// Modules
import { PipesModule } from './pipes/pipes.module';
import { AdminModule } from './admin/admin.module';
import { DocenteModule } from './docente/docente.module';
import { EstudianteModule } from './estudiante/estudiante.module';


// Components ADMIN

import { LoginComponent } from './login/login.component';
import { PagesComponent } from './institute/pages.component';
import { CoursesComponent } from './institute/postgraduate/courses/courses.component';
import { CourseComponent } from './institute/postgraduate/courses/course/course.component';
import { PlanComponent } from './institute/postgraduate/courses/course/plan/plan.component';
import { WebsitesInterestComponent } from './institute/elements/websites-interest/websites-interest.component';
import { ConsultationComponent } from './institute/elements/consultation/consultation.component';

import { LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeBo from '@angular/common/locales/es-BO';
import { InformationCourseComponent } from './institute/elements/information-course/information-course.component';
import { AdminService } from './admin/admin.service';
import { TermComponent } from './institute/investigation/term/term.component';
import { InvestigatorComponent } from './institute/investigation/teachers/investigator/investigator.component';
import { AssistantComponent } from './institute/investigation/teachers/assistant/assistant.component';

// the second parameter 'fr' is optional
registerLocaleData(localeBo);

@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    FeatureComponent,
    HomeComponent,
    InteractionComponent,
    InvestigationComponent,
    PostgraduateComponent,
    FooterComponent,
    HeaderComponent,
    NavComponent,
    PersonComponent,
    NoticeComponent,
    NoticesComponent,
    PublicationsComponent,
    TeachersComponent,
    EventsComponent,
    EventComponent,
    LoginComponent,
    PagesComponent,
    CoursesComponent,
    CourseComponent,
    PlanComponent,
    WebsitesInterestComponent,
    ConsultationComponent,
    InformationCourseComponent,
    TermComponent,
    InvestigatorComponent,
    AssistantComponent
  ],
  imports: [
    BrowserModule,
    APP_ROUTING,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    EstudianteModule,
    DocenteModule,
    AdminModule,
    PipesModule
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'es-BO'}, DataApiService, SubirArchivoService, AutentificacionService, AdminService],
  bootstrap: [AppComponent]
})
export class AppModule { }
