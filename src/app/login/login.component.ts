import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AutentificacionService } from '../services/autentificacion.service';
import { Router, ActivatedRoute } from '@angular/router';
import { PublicService } from '../services/public.service';
import { DataApiService } from '../services/data-api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  forma: FormGroup;
  mensajeError: string;

  constructor(private activatedRoute: ActivatedRoute, private dataApiService: DataApiService, private autentificacionService: AutentificacionService, private router: Router,
              private publicService: PublicService) {

    this.forma = new FormGroup ({
      'username': new FormControl('', Validators.required),
      'password': new FormControl('', Validators.required)
    });
  }

  ngOnInit() {
  }

  loginUser() {
    this.mensajeError = null;

    this.publicService
          .investigator(this.forma.value.username)
            .subscribe( (inves: any) => {
              console.log(inves);

              this.publicService
                    .rolem(this.forma.value.username)      // revisar antes de liberar
                      .subscribe((rolem: any) => {
                        console.log(rolem);
                        if (!inves && rolem) {
                          this.autentificacionService
                                .loginUser(this.forma.value.username, this.forma.value.password)
                                  .subscribe((data: any) => {
                                    console.log(data);
                                    this.autentificacionService.setUser(data.userId);
                                    this.autentificacionService.setToken(data.id);

                                    this.publicService
                                          .getRegistroRMPorFiltro('principalId', data.userId, data.id)
                                            .subscribe((dataRegistro: any) => {
                                              console.log(dataRegistro);
                                              if (dataRegistro.roleId === 3) {
                                                this.router.navigate(['estudiante']);
                                              } else {
                                                if (dataRegistro.roleId === 2) {
                                                  this.router.navigate(['docente']);
                                                } else {
                                                  if (dataRegistro.roleId === 1) {
                                                    this.router.navigate(['admin']);
                                                  } else {
                                                    this.mensajeError = 'El Inicio de sesión ha fallado';
                                                  }
                                                }
                                              }
                                            }, (errorRegistro) => {
                                              console.log(errorRegistro);
                                              this.router.navigate(['']);
                                            });

                                  }, errorServicio => {
                                    this.mensajeError = errorServicio.error.error.message;
                                  });

                        } else {
                          this.mensajeError = 'El inicio de sesión ha fallado';
                        }
                      });
            }, errorInvestigador => {
              this.mensajeError = 'El inicio de sesión ha fallado';
            });
  }

  irPaginaArriba() {
    this.dataApiService.irPaginaArriba(this.activatedRoute);
  }

}
