import { NgModule } from '@angular/core';

import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Rutas
import { DOCENTE_ROUTING } from './docente.routes';

// Pipes
import { PipesModule } from '../pipes/pipes.module';

// Componentes
import { DocenteComponent } from './docente.component';
import { PrincipalDocenteComponent } from './principal-docente/principal-docente.component';
import { DocenteNotasComponent } from './docente-notas/docente-notas.component';
import { DocenteDocumentosComponent } from './docente-documentos/docente-documentos.component';

@NgModule ({
    declarations: [
        DocenteComponent,
        PrincipalDocenteComponent,
        DocenteNotasComponent,
        DocenteDocumentosComponent
    ],
    exports: [],
    imports: [
        RouterModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        PipesModule,
        DOCENTE_ROUTING
    ],
    providers: []
})

export class DocenteModule { }
