import { Component, OnInit } from '@angular/core';

import { AutentificacionService } from '../services/autentificacion.service';
import { Router } from '@angular/router';
import { Curso } from '../interfaces/curso.interface';
import { DataApiService } from '../services/data-api.service';
import { PublicService } from '../services/public.service';

@Component({
  selector: 'app-docente',
  templateUrl: './docente.component.html',
  styleUrls: ['./docente.component.css']
})
export class DocenteComponent implements OnInit {

  idUsuario: number;
  cursos: Curso[] = [];
  modulosPorCurso: any[] = [];
  nombre: string;

  constructor(private publicService: PublicService, private autentificacionService: AutentificacionService, private dataApiService: DataApiService, private router: Router) { }

  ngOnInit() {
    this.idUsuario = Number(this.autentificacionService.getId());
    this.dataApiService
          .getPriNombreUsuario(this.idUsuario)
            .subscribe((dataNombre: any) => {
              this.nombre = dataNombre['pri_nombre'];
            }, errorNombre => {
              console.log('errorNombre', errorNombre);
            });
    this.dataApiService
          .getCursosDicta(this.idUsuario)
            .subscribe((dataCursos: any) => {
              this.cursos = dataCursos;
              console.log(this.cursos);

              for (let index = 0; index < this.cursos.length; index++) {
                const element = this.cursos[index];

                this.dataApiService
                      .getDictaModulosCurso(this.idUsuario, element.id)
                        .subscribe((dataModulosCurso: any) => {
                          this.modulosPorCurso[index] = dataModulosCurso;
                          console.log(this.modulosPorCurso);

                      }, (errorModulosCurso) => {
                        console.log('errorModulosCurso', errorModulosCurso);
                      });

              }

            }, (errorCursos) => {
              console.log('errorCursos', errorCursos);
            });
  }

  logout() {
    this.autentificacionService.logoutUser().subscribe();
    this.router.navigate(['']);
  }

}
