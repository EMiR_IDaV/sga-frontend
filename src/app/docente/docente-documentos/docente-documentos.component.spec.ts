import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocenteDocumentosComponent } from './docente-documentos.component';

describe('DocenteDocumentosComponent', () => {
  let component: DocenteDocumentosComponent;
  let fixture: ComponentFixture<DocenteDocumentosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocenteDocumentosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocenteDocumentosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
