import { Component, OnInit, OnDestroy } from '@angular/core';
import { Modulo } from '../../interfaces/modulo.interface';
import { Curso } from '../../interfaces/curso.interface';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PublicService } from '../../services/public.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DataApiService } from '../../services/data-api.service';
import { SubirArchivoService } from '../../services/subir-archivo.service';
import { Documento } from '../../interfaces/documento.interface';

declare var $: any;

@Component({
  selector: 'app-docente-documentos',
  templateUrl: './docente-documentos.component.html',
  styleUrls: ['./docente-documentos.component.css']
})
export class DocenteDocumentosComponent implements OnInit, OnDestroy {

  forma: FormGroup;

  mensaje = null;
  mensajeError = null;
  mensajeExito = null;

  nombreAuxiliar = 'Nuevo';
  docPathAnterior = '';

  index = null;

  sigla: string;
  numeroModulo: number;

  documentoFile: File;
  nombreDocumento = '';

  accion = 'Registrar';

  curso: Curso;
  modulo: Modulo = null;
  documentos: Documento[] = [];

  documento: Documento = {
    nombre: '',
    descripcion: '',
    docpath: ''
  };

  constructor(private dataApiService: DataApiService,  private publicService: PublicService, private activatedRoute: ActivatedRoute,
              private subirArchivoService: SubirArchivoService, private router: Router) {
    console.log('-------------------------------- docente-documento -------------------------------');

  }

  ngOnInit() {

    this.activatedRoute
          .params
            .subscribe(data => {
              this.sigla = data['siglaCurso'];
              this.numeroModulo = data['numeroModulo'];
            });

    this.cargarForma();

    this.publicService
          .getRegistroPorFiltro('cursos', 'sigla', this.sigla)
            .subscribe((dataCurso: any) => {
              this.curso = dataCurso;

              this.dataApiService
                    .getModuloPorIdCursoNumeroModulo(this.curso.id, this.numeroModulo)
                      .subscribe((dataModulo) => {
                        this.modulo = dataModulo;

                        this.obtenerDocumentos(this.modulo.id);

                        this.dataApiService
                              .getModelosRelPorId('modulos', this.modulo.id, 'documentos')
                                .subscribe((dataDocumentos: any) => {

                                  (dataDocumentos.length > 0) ? this.documentos = dataDocumentos : this.mensaje = 'Aún no se subió ningún documento';

                                }, errorDocumentos => {
                                  this.mensajeError = errorDocumentos.error.error.message;
                                });

                      }, (errorModulo) => {
                        this.mensajeError = errorModulo.error.error.message;
                      });

            }, (errorCurso) => {
              this.mensajeError = errorCurso.error.error.message;
            });
  }

  obtenerDocumentos(moduloId: number) {
    this.dataApiService
          .getModelosRelPorId('modulos', moduloId, 'documentos')
            .subscribe((dataDocumentos: any) => {
              (dataDocumentos.length > 0) ? this.documentos = dataDocumentos : this.mensaje = 'Aún no se subió ningún documento';
            }, errorDocumentos => {
              this.mensajeError = errorDocumentos.error.error.message;
            });
  }

  subirDocumento(documentoFile: File) {
    this.subirArchivoService
        .subirArchivo(documentoFile, this.nombreDocumento, 'docsPosgrado', 'b')
          .then()
          .catch(datac => console.log(datac));
  }

  editarDocumento(documento: Documento, index: number) {

    this.docPathAnterior = documento.docpath;
    this.nombreDocumento = documento.docpath;
    this.nombreAuxiliar = documento.nombre;
    this.accion = 'Actualizar';
    this.forma.setValue(documento);
  }

  registrarDocumento() {

    this.documento =  this.forma.value;

    if (this.documentoFile) {
      this.documento.nombre = this.documentoFile.name;
      this.documento.docpath = this.nombreDocumento;
    }

    this.documento.moduloId = this.modulo.id;

    this.dataApiService
          .putModelo('documentos', this.documento)
            .subscribe(dataDocumento => {
              console.log('Documento Creado', dataDocumento);


              if (this.nombreDocumento !== this.docPathAnterior) {
                this.subirDocumento(this.documentoFile);

                if (this.docPathAnterior !== '') {
                  this.subirArchivoService
                      .delImage('docsPosgrado', this.docPathAnterior)
                        .subscribe(eliDocumento => {
                          console.log('eliDocumento', eliDocumento);
                        }, errorEliDocumento => {
                          console.log('errorEliDocumento', errorEliDocumento);
                        });
                }

              }

              this.mensajeError = null;
              this.mensajeExito = `El documento ${this.documento.nombre} se registro con éxito.`;
              this.nombreDocumento = '';

              this.obtenerDocumentos(this.modulo.id);
              setTimeout(() => {
                                this.mensajeExito = '';
                                this.cargarForma();
                                $('#documentoModal').modal('hide');
                               }, 4000);



            }, errorDocumento => {
              this.mensajeError = errorDocumento.error.error.message;
              this.mensajeExito = null;
            });
  }

  limpiarBotonFile() {
    const inputFile = <HTMLInputElement>document.getElementById('exampleFormControlFileDocente1');
    inputFile.value = null;
  }

  posicionaDocumento(documento: Documento, index: number) {                    // Posesiona sobre el Noticia a eliminar
    this.mensajeError = '';
    this.documento = documento;
    this.index = index;
  }

  eliModal() {
    this.dataApiService
          .delModelo('documentos', this.documento.id)
            .subscribe(dataEliDocumento => {
              console.log('dataEliDocumento', dataEliDocumento);

              this.subirArchivoService
                    .delImage('docsPosgrado', this.documento.docpath)
                      .subscribe(dataEliDocumentoFile => {

                      }, errorEliDocumentoFile => {
                        console.log('errorEliDocumentoFile', errorEliDocumentoFile);
                      });

              delete this.documentos[this.index];
              this.documentos = this.documentos.filter(Boolean);
            }, errorEliDocumento => {
              this.mensajeError = errorEliDocumento.error.error.message;
            });
  }

  selDocumento( archivo: File ) {
    this.nombreDocumento = '';
    if ( !archivo ) {
      this.documentoFile = null;
      console.log('vacio');
      return;
    }
    this.documentoFile = archivo;
    const nombreCortado = this.documentoFile.name.split('.');
    const extensionArchivo = nombreCortado.pop();
    const name = Date.now().toString();
    this.nombreDocumento = name + '.' + extensionArchivo;
    console.log('nombre Documento', this.nombreDocumento);
  }

  ngOnDestroy(): void {
    $('.modal-backdrop').hide();
  }

  cargarForma() {
    this.limpiarBotonFile();
    this.mensajeError = null;
    this.mensajeExito = null;
    this.documentoFile = null;
    this.nombreAuxiliar = 'Nuevo';
    this.docPathAnterior = '';

    this.accion = 'Registrar';
    this.forma = new FormGroup ({
      'descripcion': new FormControl('', []), // , [Validators.required, Validators.minLength(2)]
      'docpath': new FormControl('', [Validators.required]), // , [Validators.required]
      'nombre': new FormControl(''),
      'moduloId': new FormControl(''),
      'id': new FormControl()
    });
  }

  irPaginaArriba() {
    // this.dataApiService.irPaginaArriba(this.activatedRoute);
    this.router.navigate(['../../../'], { relativeTo: this.activatedRoute });

  }
}
