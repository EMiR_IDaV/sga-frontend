import { Component, OnInit } from '@angular/core';
import { DataApiService } from '../../services/data-api.service';
import { PublicService } from '../../services/public.service';
import { Curso } from '../../interfaces/curso.interface';
import { AutentificacionService } from '../../services/autentificacion.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-principal-docente',
  templateUrl: './principal-docente.component.html',
  styleUrls: ['./principal-docente.component.css']
})
export class PrincipalDocenteComponent implements OnInit {

  mensaje = null;
  idUsuario = null;

  cursos: Curso[] = [];
  modulosPorCurso: any[] = [];

  constructor(private activatedRoute: ActivatedRoute, private dataApiService: DataApiService, private publicService: PublicService,
              private autentificacionService: AutentificacionService) {
    console.log('---------------estudiantes--------------');
  }

  ngOnInit() {
    this.idUsuario = Number(this.autentificacionService.getId());
    console.log(this.idUsuario);
    this.dataApiService
          .getCursosDicta(this.idUsuario)
            .subscribe((dataCursos: any) => {
              this.cursos = dataCursos;
              console.log(this.cursos);

              for (let index = 0; index < this.cursos.length; index++) {
                const element = this.cursos[index];

                this.dataApiService
                      .getDictaModulosCurso(this.idUsuario, element.id)
                        .subscribe((dataModulosCurso: any) => {
                          this.modulosPorCurso[index] = dataModulosCurso;
                          console.log(this.modulosPorCurso);

                      }, (errorModulosCurso) => {
                        console.log('errorModulosCurso', errorModulosCurso);
                      });

              }

            }, (errorCursos) => {
              console.log('errorCursos', errorCursos);
            });
  }

  irPaginaArriba() {
    this.dataApiService.irPaginaArriba(this.activatedRoute);
  }

}
