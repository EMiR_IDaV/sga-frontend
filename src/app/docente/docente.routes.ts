import { RouterModule, Routes } from '@angular/router';

import { DocenteComponent } from './docente.component';
import { PrincipalDocenteComponent } from './principal-docente/principal-docente.component';

// Guards
import { UsuarioGuard } from '../guards/usuario.guard';
import { ItemsGuard } from '../guards/items.guard';
import { DocenteGuard } from '../guards/docente.guard';
import { DocenteNotasComponent } from './docente-notas/docente-notas.component';
import { DocenteDocumentosComponent } from './docente-documentos/docente-documentos.component';
import { TeachGuard } from '../guards/teach.guard';

const DOCENTE_ROUTES: Routes = [
    {
        path: 'docente',
        component: DocenteComponent,
        canActivate: [UsuarioGuard, ItemsGuard, DocenteGuard],
        children: [
            { path: '', component: PrincipalDocenteComponent },
            { path: ':siglaCurso/:numeroModulo/notas', component: DocenteNotasComponent, canActivate: [TeachGuard] },
            { path: ':siglaCurso/:numeroModulo/documentos', component: DocenteDocumentosComponent, canActivate: [TeachGuard] }
        ]
    }
];

export const DOCENTE_ROUTING = RouterModule.forChild(DOCENTE_ROUTES);
