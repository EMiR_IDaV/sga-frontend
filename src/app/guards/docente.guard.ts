import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { AutentificacionService } from '../services/autentificacion.service';
import { map, catchError } from 'rxjs/operators';
import { Location } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class DocenteGuard implements CanActivate {

  constructor(private autentificacionService: AutentificacionService, private location: Location) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    // return (this.autentificacionService.isDocente());
    return this.autentificacionService
                    .idRoleUsuario()
                      .pipe(
                        map(data => {
                          if (data === 2) {
                            return true;
                          } else {
                            this.location.back();
                            return false;
                          }
                        }),
                        catchError(() => {
                          this.location.back();
                          return of(false);
                        })
                      );
  }

  // private handleError(error: Response): Observable<any> {
  //   // in a real world app, we may send the server to some remote logging infrastructure
  //   // instead of just logging it to the console
  //   console.error(error);
  //   return of(false);
  // }

  // private handleError(error: HttpErrorResponse) {
  //   if (error.error instanceof ErrorEvent) {
  //     // A client-side or network error occurred. Handle it accordingly.
  //     console.error('An error occurredAQ:', error.error.message);
  //   } else {
  //     // The backend returned an unsuccessful response code.
  //     // The response body may contain clues as to what went wrong,
  //     console.error(
  //       `Backend returned code ${error.status}, ` +
  //       `body was: ${error.error}`);
  //   }
  //   // return an observable with a user-facing error message
  //   return throwError(
  //     'Something bad happened; please try again later. NO TIENES AUTORIZACION');
  // }
}
