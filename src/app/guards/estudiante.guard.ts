import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { AutentificacionService } from '../services/autentificacion.service';
import { map, catchError } from 'rxjs/operators';
import { Location } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class EstudianteGuard implements CanActivate {
  constructor(private autentificacionService: AutentificacionService, private location: Location) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    // return (true);
    // return (this.autentificacionService.isEstudiante());
    return this.autentificacionService
                    .idRoleUsuario()
                      .pipe(
                        map(data => {
                          if (data === 3) {
                            return true;
                          } else {
                            this.location.back();
                            return false;
                          }
                        }),
                        catchError(() => {
                          this.location.back();
                          return of(false);
                        })
                      );
  }
}
