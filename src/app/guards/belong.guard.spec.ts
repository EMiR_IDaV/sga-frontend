import { TestBed, async, inject } from '@angular/core/testing';

import { BelongGuard } from './belong.guard';

describe('BelongGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BelongGuard]
    });
  });

  it('should ...', inject([BelongGuard], (guard: BelongGuard) => {
    expect(guard).toBeTruthy();
  }));
});
