import { TestBed, async, inject } from '@angular/core/testing';

import { EstudianteGuard } from './estudiante.guard';

describe('EstudianteGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EstudianteGuard]
    });
  });

  it('should ...', inject([EstudianteGuard], (guard: EstudianteGuard) => {
    expect(guard).toBeTruthy();
  }));
});
