import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { AutentificacionService } from '../services/autentificacion.service';
import { Location } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class ItemsGuard implements CanActivate {

  constructor(
    private autentificacionService: AutentificacionService, private location: Location) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      // return (true);
      // return (this.autentificacionService.itemsOK());
      return this.autentificacionService
                    .appertainItems()
                      .pipe(
                        map(data => {
                          if (data) {
                            return true;
                          }
                        }),
                        catchError(() => {
                          this.location.back();
                          return of(false);
                        })
                      );
  }
}
