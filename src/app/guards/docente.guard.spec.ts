import { TestBed, async, inject } from '@angular/core/testing';

import { DocenteGuard } from './docente.guard';

describe('DocenteGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DocenteGuard]
    });
  });

  it('should ...', inject([DocenteGuard], (guard: DocenteGuard) => {
    expect(guard).toBeTruthy();
  }));
});
