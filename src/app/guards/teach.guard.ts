import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { DataApiService } from '../services/data-api.service';
import { AutentificacionService } from '../services/autentificacion.service';
import { Location } from '@angular/common';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TeachGuard implements CanActivate {
  constructor(private dataApiService: DataApiService, private autentificacionService: AutentificacionService,
              private location: Location) {
}
canActivate(
next: ActivatedRouteSnapshot,
state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
// console.log(next.paramMap.get('siglaCurso'));
// console.log(next.paramMap.get('numeroModulo'));

return this.dataApiService
          .getIdUsuarioSiglaNumero(next.paramMap.get('siglaCurso'), Number(next.paramMap.get('numeroModulo')))
            .pipe(
              map( data => {
                const id = this.autentificacionService.getId();
                console.log(data['usuarioId']);
                console.log(id);
                if (data['usuarioId'] === Number(id)) {
                  return true;
                } else {
                  this.location.back();
                  return false;
                }
              }),

              catchError(() => {
                this.location.back();
                return of(false);
              })
            );

}
}
