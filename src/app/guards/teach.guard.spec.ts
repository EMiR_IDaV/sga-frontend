import { TestBed, async, inject } from '@angular/core/testing';

import { TeachGuard } from './teach.guard';

describe('TeachGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TeachGuard]
    });
  });

  it('should ...', inject([TeachGuard], (guard: TeachGuard) => {
    expect(guard).toBeTruthy();
  }));
});
