import { TestBed, async, inject } from '@angular/core/testing';

import { ItemsGuard } from './items.guard';

describe('ItemsGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ItemsGuard]
    });
  });

  it('should ...', inject([ItemsGuard], (guard: ItemsGuard) => {
    expect(guard).toBeTruthy();
  }));
});
