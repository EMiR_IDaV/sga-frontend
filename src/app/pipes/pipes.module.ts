import { NgModule } from '@angular/core';

import { ImagenPipe } from './imagen.pipe';
import { ReloadPipe } from './reload.pipe';
import { RempEspPipe } from './remp-esp.pipe';
import { SubtituloPipe } from './subtitulo.pipe';
import { AlinderPipe } from './alinder.pipe';
import { SeleccionaImagenPipe } from './selecciona-imagen.pipe';
import { AsistenciaPipe } from './asistencia.pipe';
import { ObtieneUnidadFechaPipe } from './obtiene-unidad-fecha.pipe';
import { NotaNumeralPipe } from './nota-numeral.pipe';
import { NotaDetallePipe } from './nota-detalle.pipe';
import { VideoPipe } from './video.pipe';
import { RecortaTituloPipe } from './recorta-titulo.pipe';
import { DocinvPipe } from './docinv.pipe';
import { VideoYoutubePipe } from './video-youtube.pipe';

@NgModule({
  declarations: [
    ImagenPipe,
    ReloadPipe,
    RempEspPipe,
    SubtituloPipe,
    AlinderPipe,
    SeleccionaImagenPipe,
    AsistenciaPipe,
    ObtieneUnidadFechaPipe,
    NotaNumeralPipe,
    NotaDetallePipe,
    VideoPipe,
    RecortaTituloPipe,
    DocinvPipe,
    VideoYoutubePipe
  ],
  imports: [
  ],
  exports: [
    ImagenPipe, ReloadPipe, RempEspPipe, SubtituloPipe, AlinderPipe, SeleccionaImagenPipe, AsistenciaPipe, ObtieneUnidadFechaPipe, NotaNumeralPipe, NotaDetallePipe, VideoPipe, RecortaTituloPipe, DocinvPipe, VideoYoutubePipe
  ]
})
export class PipesModule { }
