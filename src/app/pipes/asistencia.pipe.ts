import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'asistencia'
})
export class AsistenciaPipe implements PipeTransform {

  transform(value: any, args?: string): any {
    args = args.toLowerCase();
    args = args.trim();
    if (args === 'asistio' || args === 'asistió') {
      return `${value}check.svg`;
    } else {
      if (args === 'falto') {
        return `${value}x.svg`;
      } else {
        if (args === '?') {
          return `${value}question.svg`;
        }
      }
      return `${value}tag.svg`;
    }
  }

}
