import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'notaDetalle'
})
export class NotaDetallePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value === 0) {
      return '---';

    } else {
      if (value > 69) {
        return 'Aprobado';
      }
      return 'Reprobado';
    }
  }

}
