import { Pipe, PipeTransform } from '@angular/core';
import { URL_CON } from '../services/urls';

@Pipe({
  name: 'seleccionaImagen'
})
export class SeleccionaImagenPipe implements PipeTransform {

  transform(imagenesPath: string, container: string, posicion?: number): any {
    if (imagenesPath.includes(',')) {
      const paths = imagenesPath.split(',');
      return `${URL_CON}${container}/download/${paths[posicion]}`;
    } else {
      return `${URL_CON}${container}/download/${imagenesPath}`;
    }
  }

}
