import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'subtitulo'
})
export class SubtituloPipe implements PipeTransform {

  transform(value: string, args?: any): any {
    if ( value.startsWith('$$')) {
      return value.slice(2, value.length);
    }
    return value;
  }

}
