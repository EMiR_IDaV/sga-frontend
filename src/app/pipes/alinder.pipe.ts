import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'alinder'
})
export class AlinderPipe implements PipeTransform {

  transform(value: string, lado: string): any {
    if ( value.includes('$ad$')) {
      if ( lado === 'izq') {
        return value.slice(0, value.indexOf('$ad$'));
      }
      return value.slice(value.indexOf('$ad$') + 4, value.length);
    }
    if (value.startsWith('$#$')) {
      return value.slice(3, value.length);
    }
    return '';
  }

}
