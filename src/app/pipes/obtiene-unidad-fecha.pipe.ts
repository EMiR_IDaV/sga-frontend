import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'obtieneUnidadFecha'
})
export class ObtieneUnidadFechaPipe implements PipeTransform {

  transform(value: string, args?: string): any {
    const unidades = value.split('-');
    if (args === 'dia') {
      return unidades[2];
    } else {
      if (args === 'mes') {
        return unidades[1];
      } else {
        return unidades[0];
      }
    }
  }

}
