import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'docinv'
})
export class DocinvPipe implements PipeTransform {

  transform(value: string, args?: any): any {
    const posPunto = value.indexOf('.');
    const extension = value.slice(posPunto + 1, value.length);
    return `${this.retornaCompl(extension)}-${value.slice(0, value.indexOf('.'))}`;
  }

  retornaCompl(ext: string) {
    switch (ext) {
      case 'jpeg':
        return '1';
      case 'jpg':
        return '2';
      case 'png':
        return '3';
    }
  }
}
