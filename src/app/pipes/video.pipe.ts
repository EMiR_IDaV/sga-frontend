import { Pipe, PipeTransform } from '@angular/core';
import { URL_CON } from '../services/urls';

@Pipe({
  name: 'video'
})
export class VideoPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (args !== '') {
      console.log(`${URL_CON}instVideo/download/${args}`);
      return `${URL_CON}instVideo/download/${args}`;
    }
    console.log(value);
    return value;
  }

}
