import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'reload',
  pure: false
})
export class ReloadPipe implements PipeTransform {

  transform(value: any): any {
    return value;
  }

}
