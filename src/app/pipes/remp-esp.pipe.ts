import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'rempEsp'
})
export class RempEspPipe implements PipeTransform {

  transform(value: string, args?: any): any {
    return value.replace(/ /gi, '-');
  }

}
