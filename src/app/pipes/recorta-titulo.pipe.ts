import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'recortaTitulo'
})
export class RecortaTituloPipe implements PipeTransform {

  transform(value: string, args: number): string {
    const longitudTitulo = value.length;
    if (longitudTitulo < args) {
      return value;
    }
    return (value.slice(0, args - 1) + '...');
  }

}
