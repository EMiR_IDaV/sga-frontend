import { Component, OnInit } from '@angular/core';
import { Noticia } from '../../interfaces/noticia.interface';
import { PublicService } from '../../services/public.service';
import { Unidad } from '../../interfaces/unidad.interface';
import { URL_CON } from 'src/app/services/urls';
import { SubirArchivoService } from '../../services/subir-archivo.service';
import { Evento } from '../../interfaces/evento.interface';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  noticias: Noticia[];
  eventos: Evento[];
  uInves: Unidad = {nombre: '', descripcion: ''};
  uPosgr: Unidad = {nombre: '', descripcion: ''};
  uInter: Unidad = {nombre: '', descripcion: ''};

  videopath = 'ypfR8Bd5jFA';

  constructor(private publicService: PublicService, private subirArchivoService: SubirArchivoService) {
    console.log('------------------------------- HomeComponent-------------------------------');
  }

  ngOnInit() {
    this.publicService
          .getRegistrosTipoNoticia('noticias', 4)
            .subscribe((data: any) => {
                                        this.noticias = data;
                                      });
    this.publicService
          .getRegistrosModelo('unidades')
            .subscribe((dataUnidades: any) => {
              this.uInves = dataUnidades[0];
              this.uPosgr = dataUnidades[1];
              this.uInter = dataUnidades[2];
            });

    this.publicService
          .getRegistroByProperty('unidades', 'nombre', 'video')
            .subscribe((video: any) => {
              this.videopath = video.descripcion;
            });

    this.publicService
          .getRegistrosTipoNoticia('eventos', 2)
            .subscribe((dataEventos: any) => {
              this.eventos = dataEventos;
            });
  }

}
