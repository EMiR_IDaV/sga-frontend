import { Component, OnInit } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { PublicService } from '../../services/public.service';
import { Usuario } from '../../interfaces/usuario.interface';

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.css']
})
export class PersonComponent implements OnInit {

  carga = false;

  cargo: string;
  usuario: Usuario = null;
  descripcion: string[] = [];

  constructor(private activatedRoute: ActivatedRoute, private publicService: PublicService) {
    this.activatedRoute.params.subscribe(data => this.cargo = data['cargo']);
  }

  ngOnInit() {
    this.cargo = this.publicService.eliminaGuionesParametro(this.cargo);

    this.publicService
      .getPublicAdmin(this.cargo)
        .subscribe((data: any) => {
                                    this.usuario = data;
                                    console.log(this.usuario);
                                    this.descripcion = this.usuario.admin.descripcion.split('\n');
                                    this.carga = true;
                                  });
  }

}
