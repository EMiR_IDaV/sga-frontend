import { Component, OnInit } from '@angular/core';
import { PublicService } from '../../../services/public.service';
import { Convocatoriainves } from '../../../interfaces/convocatoriainves.interface';

@Component({
  selector: 'app-teachers',
  templateUrl: './teachers.component.html',
  styleUrls: ['./teachers.component.css']
})
export class TeachersComponent implements OnInit {

  convocatorias: Convocatoriainves[];

  constructor(private publicService: PublicService) { }

  ngOnInit() {
    this.publicService
          .getRegistrosModelo('convocatoriainves', 'gestion', 'desc')
            .subscribe((dataConvocatorias: any) => {
              this.convocatorias = dataConvocatorias;
            });
  }

}
