import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PublicService } from '../../../../services/public.service';
import { Usuario } from '../../../../interfaces/usuario.interface';

@Component({
  selector: 'app-investigator',
  templateUrl: './investigator.component.html',
  styleUrls: ['./investigator.component.css']
})
export class InvestigatorComponent implements OnInit {

  carga = false;

  term: string;
  docinv: string;
  usuario: Usuario = null;
  descripcion: string[] = [];

  constructor(private activatedRoute: ActivatedRoute, private publicService: PublicService) {
    this.activatedRoute.params.subscribe(data => {
      this.term = data['term'];
      this.docinv = this.publicService.eliminaGuionesParametro(data['docinv']);
    });
  }

  ngOnInit() {
      console.log(this.docinv);

      this.publicService
            .getPublicTipo(this.term, this.docinv, 'Investigador')
              .subscribe((dataInvestigador: any) => {
                this.usuario = dataInvestigador;
                console.log(this.usuario);
                this.descripcion = this.usuario.investigador.descripcion.split('\n');
                this.carga = true;
              });
  }

  retornaCompl(compl: string) {
    switch (compl) {
      case '1':
        return 'jpeg';
      case '2':
        return 'jpg';
      case '3':
        return 'png';
    }
  }

}
