import { Component, OnInit } from '@angular/core';
import { Usuario } from '../../../../interfaces/usuario.interface';
import { ActivatedRoute } from '@angular/router';
import { PublicService } from '../../../../services/public.service';

@Component({
  selector: 'app-assistant',
  templateUrl: './assistant.component.html',
  styleUrls: ['./assistant.component.css']
})
export class AssistantComponent implements OnInit {

  carga = false;

  term: string;
  docinv: string;
  usuario: Usuario = null;
  descripcion: string[] = [];

  constructor(private activatedRoute: ActivatedRoute, private publicService: PublicService) {
    this.activatedRoute.params.subscribe(data => {
      this.term = data['term'];
      this.docinv = this.publicService.eliminaGuionesParametro(data['docinv']);
    });
  }

  ngOnInit() {
    console.log(this.docinv);
    this.publicService
          .getPublicTipo(this.term, this.docinv, 'Auxiliar')
            .subscribe((dataAuxiliar: any) => {
              this.usuario = dataAuxiliar;
              console.log(this.usuario);
              this.descripcion = this.usuario.auxiliar.descripcion.split('\n');
              this.carga = true;
            }, error => {
              console.log('error', error);
            });
  }

  retornaCompl(compl: string) {
    switch (compl) {
      case '1':
        return 'jpeg';
      case '2':
        return 'jpg';
      case '3':
        return 'png';
    }
  }

}
