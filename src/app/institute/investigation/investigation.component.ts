import { Component, OnInit } from '@angular/core';
import { PublicService } from '../../services/public.service';
import { UInvestigacion } from '../../interfaces/uInvestigacion.interface';
import { Proyecto } from '../../interfaces/proyecto.interface';
import { Usuario } from '../../interfaces/usuario.interface';

@Component({
  selector: 'app-investigation',
  templateUrl: './investigation.component.html',
  styleUrls: ['./investigation.component.css']
})
export class InvestigationComponent implements OnInit {

  uInvestigacion: UInvestigacion;
  parrafos: string[];
  proyectosUltimos: Proyecto[];
  proyectosRecientes: Proyecto[];
  idConvocatoria: number;

  ultimaGestion: string;
  ultimosDocenteInvestigadores: Usuario[];

  investigadoresPorProyecto: any [] = [];
  auxiliaresPorProyecto: any [] = [];


  constructor(private publicService: PublicService) { }

  ngOnInit() {

    this.publicService
          .getRegistroByProperty('uInvestigacions', 'id', '1')
            .subscribe((dataUInvestigacion: any) => {
              this.uInvestigacion = dataUInvestigacion;
              this.parrafos = this.uInvestigacion.lineas.split('\n');
              console.log(this.uInvestigacion);
            });

    this.publicService
          .getRegistrosOrderLimiteSkip('proyectos', 'fecha_publicacion', 'desc', 2, 0)
            .subscribe((dataProyectosU: any) => {
              this.proyectosUltimos = dataProyectosU;

              for (let index = 0; index < this.proyectosUltimos.length; index++) {
                const element = this.proyectosUltimos[index];
                this.publicService
                      .getPublicTipoPluralPorProyecto(element.id, 'Investigadores')
                        .subscribe((dataInvesPorProyecto: any) => {
                          this.investigadoresPorProyecto[index] = dataInvesPorProyecto;
                        });

                this.publicService
                      .getPublicTipoPluralPorProyecto(element.id, 'Auxiliares')
                        .subscribe((dataAuxisPorProyecto: any) => {
                          this.auxiliaresPorProyecto[index] = dataAuxisPorProyecto;
                        });
              }
            });

    this.publicService
          .getRegistrosOrderLimiteSkip('proyectos', 'fecha_publicacion', 'desc', 4, 2)
            .subscribe((dataProyectosR: any) => {
              this.proyectosRecientes = dataProyectosR;
            });

    this.publicService
          .getRegistrosOrderLimiteSkip('convocatoriainves', 'gestion', 'desc', 1, 0)
            .subscribe((dataUltConvocatoria: any) => {
              this.ultimaGestion = dataUltConvocatoria[0].gestion;
              this.publicService
                    .getPublicTipoPluralPorConvocatoria(this.ultimaGestion, 'Investigadores')
                      .subscribe((dataDocenteUltConvocatoria: any) => {
                        this.ultimosDocenteInvestigadores = dataDocenteUltConvocatoria;
                        console.log(this.ultimosDocenteInvestigadores);
                      });
            });
  }

}
