import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Usuario } from 'src/app/interfaces/usuario.interface';
import { Convocatoriainves } from '../../../interfaces/convocatoriainves.interface';
import { PublicService } from '../../../services/public.service';

@Component({
  selector: 'app-term',
  templateUrl: './term.component.html',
  styleUrls: ['./term.component.css']
})
export class TermComponent implements OnInit {

  term: string;
  convocatoria: Convocatoriainves;
  investigadores: Usuario[];
  auxiliares: Usuario[];

  constructor(private activatedRoute: ActivatedRoute, private publicService: PublicService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(data => {
      this.term = data['term'];
      this.publicService
            .getRegistroByProperty('convocatoriainves', 'gestion', this.term)
              .subscribe((dataConvocatoria: any) => {
                this.convocatoria = dataConvocatoria;
              });
      this.publicService
            .getPublicTipoPluralPorConvocatoria(this.term, 'Investigadores')
              .subscribe((dataInvestigadores: any) => {
                console.log(dataInvestigadores);
                this.investigadores = dataInvestigadores;
              });

      this.publicService
            .getPublicTipoPluralPorConvocatoria(this.term, 'Auxiliares')
              .subscribe((dataAuxiliares: any) => {
                console.log(dataAuxiliares);
                this.auxiliares = dataAuxiliares;
              });

    });
  }

}
