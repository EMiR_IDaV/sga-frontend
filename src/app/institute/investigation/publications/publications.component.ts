import { Component, OnInit } from '@angular/core';
import { PublicService } from '../../../services/public.service';
import { Proyecto } from '../../../interfaces/proyecto.interface';

@Component({
  selector: 'app-publications',
  templateUrl: './publications.component.html',
  styleUrls: ['./publications.component.css']
})
export class PublicationsComponent implements OnInit {

  proyectos: Proyecto[];
  docentesPorProyecto: any [] = [];
  auxiliaresPorProyecto: any [] = [];

  total = 0;
  numPag: string[] = [];
  actual = 0;

  constructor(private publicService: PublicService) { }

  ngOnInit() {

    this.publicService
      .countRegistros('proyectos')
        .subscribe((data: any) => {
                                    this.total = Math.ceil(data.count / 3);
                                    this.numPag[this.total - 1] = '';
                                    this.numPag.fill('');
                                  });

    this.publicService
      .getRegistrosOrderLimiteSkip('proyectos', 'fecha_publicacion', 'desc', 3, (this.actual * 3))
        .subscribe((data: any) => {
                                    this.proyectos = data;
                                    for (let index = 0; index < this.proyectos.length; index++) {
                                      const element = this.proyectos[index];
                                      this.publicService
                                            .getPublicTipoPluralPorProyecto(element.id, 'Investigadores')
                                              .subscribe((dataInvesPorProyecto: any) => {
                                                this.docentesPorProyecto[index] = dataInvesPorProyecto;
                                              });
                                      this.publicService
                                            .getPublicTipoPluralPorProyecto(element.id, 'Auxiliares')
                                              .subscribe((dataAuxisPorProyecto: any) => {
                                                this.auxiliaresPorProyecto[index] = dataAuxisPorProyecto;
                                                console.log(this.auxiliaresPorProyecto);
                                              });
                                    }
                                  });
  }

  carga(num: number) {
    if (!(num === -1 || this.total === num || this.actual === num)) {
      this.actual = num;
      this.publicService
        .getRegistrosOrderLimiteSkip('proyectos', 'fecha_publicacion', 'desc', 3, (this.actual * 3))
          .subscribe((data: any) => {
                                      this.proyectos = data;
                                      for (let index = 0; index < this.proyectos.length; index++) {
                                        const element = this.proyectos[index];
                                        this.publicService
                                              .getPublicTipoPluralPorProyecto(element.id, 'Investigadores')
                                                .subscribe((dataInvesPorProyecto: any) => {
                                                  this.docentesPorProyecto[index] = dataInvesPorProyecto;
                                                });
                                      }
                                    });
      window.scrollTo(0, 0);
    }
  }

}
