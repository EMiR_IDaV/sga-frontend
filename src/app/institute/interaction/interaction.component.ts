import { Component, OnInit } from '@angular/core';
import { Evento } from '../../interfaces/evento.interface';
import { PublicService } from '../../services/public.service';
import { UInteraccion } from '../../interfaces/uInteraccion.interface';

@Component({
  selector: 'app-interaction',
  templateUrl: './interaction.component.html',
  styleUrls: ['./interaction.component.css']
})
export class InteractionComponent implements OnInit {

  eventosUlt: Evento[] = [];
  eventos: Evento[] = [];

  uInteraccion: UInteraccion;
  parrafos: string[];

  constructor(private publicService: PublicService) { }

  ngOnInit() {

    this.publicService
          .getRegistroByProperty('uInteraccions', 'id', '1')
            .subscribe((dataUInvestigacion: any) => {
              this.uInteraccion = dataUInvestigacion;
              this.parrafos = this.uInteraccion.lineas.split('\n');
              console.log(this.uInteraccion);
            });
    this.publicService
          .getRegistrosModelo('eventos', 'fecha', 'desc', 3)
            .subscribe((dataEventosUlt: any) => {
              this.eventosUlt = dataEventosUlt;
              console.log('dataEventosUlt', dataEventosUlt);
            }, errorEventosUlt => {
              console.log('errorEventosUlt', errorEventosUlt);
            });

    this.publicService
          .getRegistrosModelo('eventos', 'fecha', 'desc', 4)
            .subscribe((dataEventos: any) => {
              this.eventos = dataEventos;
              console.log('dataEventos', dataEventos);
            }, errorEventos => {
              console.log('errorEventos', errorEventos);
            });
  }

}

