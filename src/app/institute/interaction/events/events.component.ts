import { Component, OnInit } from '@angular/core';
import { Evento } from '../../../interfaces/evento.interface';
import { PublicService } from '../../../services/public.service';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {


  events: Evento[];
  total = 0;
  numPag: string[] = [];
  actual = 0;

  constructor(private publicService: PublicService) {
    // this.router.routeReuseStrategy.shouldReuseRoute = function() {
    //   return false;
    // };
    // this.ActRoute.params.subscribe((data: any) => this.titulo = data['notice']);
    // this.titulo = this.titulo.replace(/_/gi, ' ');
    // console.log(this.titulo);
  }

  ngOnInit() {
    console.log('Actual', this.actual);
    this.publicService
      .countRegistros('eventos')
        .subscribe((data: any) => {
                                    this.total = Math.ceil(data.count / 9);
                                    this.numPag[this.total - 1] = '';
                                    this.numPag.fill('');
                                  });
    this.publicService
      .getRegistrosTipoNoticiasPag('eventos', this.actual, 9)
        .subscribe((data: any) => {
                                    this.events = data;
                                    // this.total = this.events.length / 9;
                                    console.log(this.events);
                                    // console.log(this.total);
                                  });
  }

  carga(num: number) {
    if (!(num === -1 || this.total === num || this.actual === num)) {
      this.actual = num;
      console.log(this.actual);
      this.publicService
        .getRegistrosTipoNoticiasPag('eventos', num, 9)
          .subscribe((data: any) => {
                                      this.events = data;
                                      // this.total = this.events.length / 9;
                                      console.log(this.events);
                                      // console.log(this.total);
                                    });
    }
  }

}
