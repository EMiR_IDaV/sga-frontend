import { Component, OnInit } from '@angular/core';
import { PublicService } from '../../../services/public.service';
import { Evento } from '../../../interfaces/evento.interface';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit {

  titulo: string;

  evento: Evento;
  parrafos: string[];

  constructor(private activatedRoute: ActivatedRoute, private publicService: PublicService) { }

  ngOnInit() {

    this.activatedRoute.params.subscribe(data => this.titulo = data['event']);
    this.titulo = this.publicService.eliminaGuionesParametro(this.titulo);

    this.publicService
          .getRegistroByProperty('eventos', 'titulo', this.titulo)
            .subscribe((data: any) => {
              console.log(data);
              this.evento = data;
              this.parrafos = this.evento.descripcion.split('\n');
            }, errorEvento => {
              console.log('errorEvento', errorEvento);
            });
  }

}
