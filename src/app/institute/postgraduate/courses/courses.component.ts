import { Component, OnInit } from '@angular/core';
import { PublicService } from '../../../services/public.service';
import { Curso } from '../../../interfaces/curso.interface';
import { Location } from '@angular/common';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})
export class CoursesComponent implements OnInit {

  actual = 0;
  total = 0;
  numPag: string[] = [];
  cursos: Curso[] = [];

  constructor(private publicService: PublicService, private location: Location) {  }

  ngOnInit() {
    this.publicService
      .getCountCursos()
        .subscribe((data: any) => {
                                    this.total = Math.ceil(data.count / 3);
                                    this.numPag[this.total - 1] = '';
                                    this.numPag.fill('');
                                  });
    this.publicService
      .getCursosPaginados(this.actual, 3)
        .subscribe((data: any) => {
                                    this.cursos = data;
                                    // this.total = this.noticias.length / 9;
                                    console.log(this.cursos);
                                    // console.log(this.total);
                                  });
  }

  back() {
    this.location.back();
  }

  carga(num: number) {
    if (!(num === -1 || this.total === num || this.actual === num)) {
      this.actual = num;
      console.log(this.actual);
      this.publicService
        .getCursosPaginados(num, 3)
          .subscribe((data: any) => {
                                      this.cursos = data;
                                      // this.total = this.noticias.length / 9;
                                      console.log(this.cursos);
                                      // console.log(this.total);
                                    });
    }
  }

}
