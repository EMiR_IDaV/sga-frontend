import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Curso } from '../../../../interfaces/curso.interface';
import { PublicService } from '../../../../services/public.service';



@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.css']
})
export class CourseComponent implements OnInit {

  sigla: string;
  curso: Curso;

  carga = false;
  // curso: Curso = {
  //   nombre: '',
  //   sigla: '',
  //   acerca: '',
  //   areas: '',
  //   perfiles: '',
  //   admision: '',
  //   introduccion: '',
  //   costo: '',
  //   modalidad: '',
  //   num_plazas: '',
  //   inscripcion: new Date(),
  //   inicio_matriculacion: new Date(),
  //   fin_matriculacion: new Date(),
  //   inicio_clases: new Date(),
  //   dias_horarios: '',
  //   otra: ''
  // };
  parrafosAcerca: string[];
  parrafosAdmision: string[];
  parrafosAreas: string[];
  parrafosPerfiles: string[];
  parrafosCostos: string[];

  constructor(private activatedRoute: ActivatedRoute, private publicService: PublicService) { }

  ngOnInit() {

    this.activatedRoute.params.subscribe(data => this.sigla = data['sigla']);
    this.publicService
      .getCursoPorSigla(this.sigla)
        .subscribe(dataCurso => {
                                  this.curso =  dataCurso;
                                  console.log(this.curso);
                                  this.carga = true;
                                  this.parrafosAcerca = this.curso.acerca.split('\n');
                                  this.parrafosAdmision = this.curso.admision.split('\n');
                                  this.parrafosAreas = this.curso.areas.split('\n');
                                  this.parrafosPerfiles = this.curso.perfiles.split('\n');
                                  this.parrafosCostos = this.curso.costo.split('\n');
                                });
    console.log(this.sigla);
  }

}
