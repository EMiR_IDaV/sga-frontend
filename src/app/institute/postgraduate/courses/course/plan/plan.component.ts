import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PublicService } from '../../../../../services/public.service';
import { Curso } from '../../../../../interfaces/curso.interface';
import { Modulo } from '../../../../../interfaces/modulo.interface';

@Component({
  selector: 'app-plan',
  templateUrl: './plan.component.html',
  styleUrls: ['./plan.component.css']
})
export class PlanComponent implements OnInit {

  sigla: string;
  curso: Curso;

  // curso: Curso = {
  //   nombre: '',
  //   sigla: '',
  //   acerca: '',
  //   areas: '',
  //   perfiles: '',
  //   admision: '',
  //   introduccion: '',
  //   costo: '',
  //   modalidad: '',
  //   num_plazas: '',
  //   inscripcion: new Date(),
  //   inicio_matriculacion: new Date(),
  //   fin_matriculacion: new Date(),
  //   inicio_clases: new Date(),
  //   dias_horarios: '',
  //   otra: ''
  // };

  parrafosInMo: string[];
  parrafosInNu: string[];
  parrafosInCa: string[];
  parrafosInDh: string[];
  parrafosInOt: string[];

  modulos: Modulo[];
  tcarga_horaria_a = 0;
  tcarga_horaria_b = 0;
  thoras_academicas = 0;
  tcreditos = 0;

  constructor(private activatedRoute: ActivatedRoute, private publicService: PublicService) { }

  ngOnInit() {

    this.activatedRoute.params.subscribe(data => this.sigla = data['sigla']);
    this.publicService
      .getCursoPorSigla(this.sigla)
        .subscribe(dataCurso => {
                                  this.curso =  dataCurso;
                                  console.log(this.curso);
                                  this.parrafosInMo = this.curso.modalidad.split('\n');
                                  this.parrafosInDh = this.curso.dias_horarios.split('\n');
                                  (this.curso.num_plazas) ?  this.parrafosInNu = this.curso.num_plazas.split('\n') : this.parrafosInNu = [];
                                  (this.curso.otra) ?  this.parrafosInOt = this.curso.otra.split('\n') : this.parrafosInOt = [];
                                  // this.publicService
                                  //   .getModulosPorId(this.curso.id)
                                  //     .subscribe((dataModulos: any) => {
                                  //                                       this.modulos = dataModulos;
                                  //                                      });

                                  this.publicService
                                        .getModelosRelPorId('cursos', this.curso.id, 'modulos', 'numero')
                                          .subscribe((dataModulos: any) => {
                                            this.modulos = dataModulos;
                                            this.cortarFechasModulos();
                                            this.totalHorasCurso(this.curso.id);
                                            // console.log(this.modulos);
                                          });
                                  // this.publicService
                                  //   .getSumHrs(this.curso.id)
                                  //     .subscribe((dataTot: any) => {
                                  //                                   this.tcarga_horaria_a = dataTot.tcarga_horaria_a;
                                  //                                   this.tcarga_horaria_b = dataTot.tcarga_horaria_b;
                                  //                                   this.thoras_academicas = dataTot.thoras_academicas;
                                  //                                   this.tcreditos = dataTot.tcreditos;
                                  //     });
                                });
    console.log(this.sigla);
  }

  cortarFechasModulos() {
    for (let index = 0; index < this.modulos.length; index++) {
      const element = this.modulos[index];
      this.publicService.cortarFechasModelo(element, 'modulo');
    }
  }

  totalHorasCurso(id: number) {
    if (this.modulos.length !== 0) {
      this.publicService
            .getSumHrs(id)
              .subscribe((dataTotalHrs: any) => {
                this.tcarga_horaria_a = dataTotalHrs.tcarga_horaria_a;
                this.tcarga_horaria_b = dataTotalHrs.tcarga_horaria_b;
                this.thoras_academicas = dataTotalHrs.thoras_academicas;
                this.tcreditos = dataTotalHrs.tcreditos;
              }, (errorDataTotalHrs) => console.log(errorDataTotalHrs));
              // }, (errorDataTotalHrs) => this.mensaje = errorDataTotalHrs.error.error.messge);
    } else {
      // this.mensaje = 'No hay módulos registrados para este curso';
    }
  }

}
