import { Component, OnInit } from '@angular/core';

import { Caracteristica } from '../../../interfaces/caracteristica.interface.';
import { ActivatedRoute, Router } from '@angular/router';
import { DataApiService } from '../../../services/data-api.service';
import { Location } from '@angular/common';
import { PublicService } from '../../../services/public.service';
import { Admin } from '../../../interfaces/admin.interface';
import { Usuario } from '../../../interfaces/usuario.interface';

@Component({
  selector: 'app-feature',
  templateUrl: './feature.component.html',
  styleUrls: ['./feature.component.css']
})
export class FeatureComponent implements OnInit {

  nombre: string;
  parrafos: string[];

  caracteristicas: Caracteristica[];
  usuarios: Usuario[];
  admins: Admin[];

  // ultimaGestion: string;                  PARA QUE LOS DOCENTES INVESTIGADORES DE LA ULTIMA GESTION ESTEN AQUI PRO MEDIO PELE
  // ultimosDocenteInvestigadores: Usuario[];


  caracteristica: Caracteristica = {
    nombre: '',
    resumen: '',
    imgpath: '',
    descripcion: ''
  };

  constructor(private router: Router, private actRoute: ActivatedRoute, private publicService: PublicService, private dataApiServ: DataApiService, private location: Location) {
  }

  ngOnInit() {
    this.actRoute.params.subscribe(data => this.nombre = data['nombre']);
    this.nombre = this.dataApiServ.eliminaGuionesParametro(this.nombre);
    this.router.routeReuseStrategy.shouldReuseRoute = function() {        // Carga la pagina con distinto contenido
      return false;
    };
    this.publicService.getRegistrosModelo('caracteristicas').subscribe((data: any) => this.caracteristicas = data);

    this.dataApiServ
          .getCaracteristicaByNombre(this.nombre)
            .subscribe((data: any) => {
              // if (!data) {
              //   this.back();
              // }
              this.caracteristica = data;
              console.log(this.caracteristica);
              this.parrafos = data.descripcion.split('\n');
              if (this.nombre === 'Equipo de Trabajo') {
                this.publicService
                      .getPublicAdmins()
                        .subscribe((dataPublicAdmis: any) => {
                          this.usuarios = dataPublicAdmis;
                        });

                // this.publicService
                //       .getRegistrosOrderLimiteSkip('convocatoriainves', 'gestion', 'desc', 1, 0)
                //         .subscribe((dataUltConvocatoria: any) => {
                //           this.ultimaGestion = dataUltConvocatoria[0].gestion;
                //           this.publicService
                //                 .getPublicDocentesInvesPorConvocatoria(this.ultimaGestion)
                //                   .subscribe((dataDocenteUltConvocatoria: any) => {
                //                     this.ultimosDocenteInvestigadores = dataDocenteUltConvocatoria;
                //                   });
                //         });
              }
            }, err => console.log('Error en la obtencion de la Caracteristica', err));
  }

  back() {
    this.location.back();
  }

}
