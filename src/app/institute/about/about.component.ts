import { Component, OnInit } from '@angular/core';

import { Noticia } from 'src/app/interfaces/noticia.interface';
import { Caracteristica } from '../../interfaces/caracteristica.interface.';
import { PublicService } from '../../services/public.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  caracteristica: Caracteristica;
  caracteristicas: Caracteristica[];
  noticias: Noticia[];
  parrafos: string[];


  constructor(private publicService: PublicService) {}

  ngOnInit() {

    this.publicService
          .getRegistrosModelo('caracteristicas')
            .subscribe((data: any) => this.caracteristicas = data);

    this.publicService
          .getRegistroByProperty('caracteristicas', 'nombre', 'Nosotros')
            .subscribe((data: any) => {
              this.caracteristica = data;
              this.parrafos = this.caracteristica.descripcion.split('\n');
            });

    this.publicService
      .getRegistrosTipoNoticia('noticias', 3)
        .subscribe((data: any) => {
          this.noticias = data;
        });
  }

}
