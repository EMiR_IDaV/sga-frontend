import { Component, OnInit } from '@angular/core';
import { Noticia } from '../../interfaces/noticia.interface';
import { Router } from '@angular/router';
import { DataApiService } from '../../services/data-api.service';
import { Location } from '@angular/common';
import { PublicService } from '../../services/public.service';

@Component({
  selector: 'app-notices',
  templateUrl: './notices.component.html',
  styleUrls: ['./notices.component.css']
})
export class NoticesComponent implements OnInit {


  noticias: Noticia[];
  total = 0;
  numPag: string[] = [];
  actual = 0;

  constructor(private publicService: PublicService, private router: Router, private location: Location) {
    // this.router.routeReuseStrategy.shouldReuseRoute = function() {
    //   return false;
    // };
    // this.ActRoute.params.subscribe((data: any) => this.titulo = data['notice']);
    // this.titulo = this.titulo.replace(/_/gi, ' ');
    // console.log(this.titulo);
  }

  ngOnInit() {
    console.log('Actual', this.actual);
    this.publicService
      .countRegistros('noticias')
        .subscribe((data: any) => {
                                    this.total = Math.ceil(data.count / 9);
                                    this.numPag[this.total - 1] = '';
                                    this.numPag.fill('');
                                  });
    this.publicService
      .getRegistrosTipoNoticiasPag('noticias', this.actual, 9)
        .subscribe((data: any) => {
                                    this.noticias = data;
                                    // this.total = this.noticias.length / 9;
                                    console.log(this.noticias);
                                    // console.log(this.total);
                                  });
  }

  carga(num: number) {
    if (!(num === -1 || this.total === num || this.actual === num)) {
      this.actual = num;
      console.log(this.actual);
      this.publicService
        .getRegistrosTipoNoticiasPag('noticias', num, 9)
          .subscribe((data: any) => {
                                      this.noticias = data;
                                      // this.total = this.noticias.length / 9;
                                      console.log(this.noticias);
                                      // console.log(this.total);
                                    });
    }
  }

}
