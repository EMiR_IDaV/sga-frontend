import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Noticia } from '../../../interfaces/noticia.interface';
import { DataApiService } from '../../../services/data-api.service';
import { Location } from '@angular/common';
import { PublicService } from '../../../services/public.service';

@Component({
  selector: 'app-notice',
  templateUrl: './notice.component.html',
  styleUrls: ['./notice.component.css']
})
export class NoticeComponent implements OnInit {

  titulo = '';
  noticia: Noticia = {
    titulo: '',
    resumen: '',
    imgpath: '',
    cuerpo: '',
    fecha: ''
  };
  nomImagenes: string[];
  parrafos: string[];

  noticias: Noticia[];

  constructor(private publicService: PublicService, private router: Router, private ActRoute: ActivatedRoute, private location: Location) {

    this.router.routeReuseStrategy.shouldReuseRoute = function() {        // Carga la pagina con distinto contenido
      return false;
    };

    this.ActRoute.params.subscribe((data: any) => this.titulo = data['notice']);
    this.titulo = this.publicService.eliminaGuionesParametro(this.titulo);
  }

  ngOnInit() {
    this.publicService
          .getRegistroByProperty('noticias', 'titulo', this.titulo)
            .subscribe((data: any) => {
              if (!data) {
                this.back();
              }
              this.noticia = data;
              console.log(this.noticia);
              this.parrafos = this.noticia.cuerpo.split('\n');
              this.nomImagenes = this.noticia.imgpath.split(',');
              this.nomImagenes.shift();
            }, err => console.log('Error en la obtencion de la Noticia', err));

    this.publicService
      .getRegistrosTipoNoticia('noticias', 3, this.titulo)
        .subscribe((data: any) => {
          this.noticias = data;
        });
  }

  back() {
    this.location.back();
  }

}
