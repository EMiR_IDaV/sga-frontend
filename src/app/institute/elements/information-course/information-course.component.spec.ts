import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InformationCourseComponent } from './information-course.component';

describe('InformationCourseComponent', () => {
  let component: InformationCourseComponent;
  let fixture: ComponentFixture<InformationCourseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformationCourseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformationCourseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
