import { Component, OnInit, Input } from '@angular/core';
import { Curso } from '../../../interfaces/curso.interface';
import { PublicService } from '../../../services/public.service';

@Component({
  selector: 'app-information-course',
  templateUrl: './information-course.component.html',
  styleUrls: ['./information-course.component.css']
})
export class InformationCourseComponent implements OnInit {

  @Input() curso: Curso;

  parrafosInfoModalidad: string[];
  parrafosInfoNumeroPlazas: string[];
  parrafosInfoDiasHorarios: string[];
  parrafosInfoOtros: string[];

  constructor(private publicService: PublicService) { }

  ngOnInit() {
    console.log(this.curso);
    this.publicService.cortarFechasModelo(this.curso, 'curso');
    this.parrafosInfoModalidad = this.curso.modalidad.split('\n');
    this.parrafosInfoDiasHorarios = this.curso.dias_horarios.split('\n');
    (this.curso.num_plazas) ?  this.parrafosInfoNumeroPlazas = this.curso.num_plazas.split('\n') : this.parrafosInfoNumeroPlazas = [];
    (this.curso.otra) ?  this.parrafosInfoOtros = this.curso.otra.split('\n') : this.parrafosInfoOtros = [];
  }

}
