import { Component, OnInit, OnDestroy, DoCheck } from '@angular/core';
import { Consulta } from '../../../interfaces/consulta.interface';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PublicService } from '../../../services/public.service';

declare var $: any;

@Component({
  selector: 'app-consultation',
  templateUrl: './consultation.component.html',
  styleUrls: ['./consultation.component.css']
})
export class ConsultationComponent implements OnInit, OnDestroy {

  forma: FormGroup;
  mensajeExito = '';
  mensajeError = '';

  consulta: Consulta = {
    nombre: '',
    email: '',
    fecha: new Date(),
    celular: null,
    mensaje: '',
    leida: false
  };

  constructor(private publicService: PublicService) { }

  ngOnInit() {
    this.cargarForma();
  }

  registrarConsulta() {
    this.consulta = this.forma.value;
    console.log(this.consulta);
    this.publicService
          .postConsulta(this.consulta)
            .subscribe(() => {
                              this.mensajeExito = 'Su mensaje a sido enviado correctamente';
                              this.mensajeError = '';
                              setTimeout(() => {
                                                $('#consultationModal').modal('hide');
                                                this.mensajeExito = '';
                                                this.cargarForma();
                                               }, 4000);
                             }
                            , (errorPostConsulta) => {
                                                      this.mensajeError = errorPostConsulta.error.error.message;
                                                      this.mensajeExito = '';
                                                     });
  }

  cargarForma() {
    this.mensajeError = '';
    this.mensajeExito = '';
    this.forma = new FormGroup ({
      'nombre': new FormControl('', [Validators.required, Validators.minLength(5), Validators.pattern('[a-zA-Z ]*')]),
      'email': new FormControl('', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-z]{2,3}$')]),
      'celular': new FormControl(),
      'fecha': new FormControl(new Date()),
      'mensaje': new FormControl('', [Validators.required, Validators.minLength(25)]),
      'leida': new FormControl(false),
      'id': new FormControl()
    });
  }


  ngOnDestroy(): void {
    $('.modal-backdrop').hide();
  }

}
