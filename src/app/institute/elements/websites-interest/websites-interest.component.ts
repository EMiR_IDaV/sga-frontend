import { Component, OnInit } from '@angular/core';
import { SitioInteres } from '../../../interfaces/sitio_interes.interface';
import { PublicService } from '../../../services/public.service';

@Component({
  selector: 'app-websites-interest',
  templateUrl: './websites-interest.component.html',
  styleUrls: ['./websites-interest.component.css']
})
export class WebsitesInterestComponent implements OnInit {

  sitios: SitioInteres[];

  constructor(private publicService: PublicService) { }

  ngOnInit() {
    this.publicService
      .getRegistrosModelo('sitios_interes')
        .subscribe((dataSitios: any) => {
                                          if (dataSitios.length > 0) {
                                            this.sitios = dataSitios;
                                          }
                                        });
  }

}
