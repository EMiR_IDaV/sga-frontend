import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebsitesInterestComponent } from './websites-interest.component';

describe('WebsitesInterestComponent', () => {
  let component: WebsitesInterestComponent;
  let fixture: ComponentFixture<WebsitesInterestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebsitesInterestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebsitesInterestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
