import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PublicService } from '../../../../services/public.service';
import { Curso } from '../../../../interfaces/curso.interface';
import { Docente } from '../../../../interfaces/docente.interface';
import { Modulo } from '../../../../interfaces/modulo.interface';
import { DataApiService } from '../../../../services/data-api.service';
import { Usuario } from '../../../../interfaces/usuario.interface';
import { SubirArchivoService } from '../../../../services/subir-archivo.service';

@Component({
  selector: 'app-list-doc',
  templateUrl: './list-doc.component.html',
  styleUrls: ['./list-doc.component.css']
})
export class ListDocComponent implements OnInit {

  quitarEliminar = 'quitar';
  cargaDocRegistrados = false;
  carga = false;
  mensaje: string;

  sigla = '';
  nombreCurso = '';
  curso: Curso;

  ussDocentes: Usuario[] = [];
  modulos: Modulo[];

  index: number;
  docente: Docente;
  modulo: Modulo;

  tcarga_horaria_a = 0;
  tcarga_horaria_b = 0;
  thoras_academicas = 0;
  tcreditos = 0;

  docenteNoRegistrado: any = {
    email: '- - - - -',
    celular: '- - - - -',
    docente : {
      reg_docente: '- - - - -',
    }
  };


  constructor(private activatedRoute: ActivatedRoute, private publicService: PublicService, private dataApiService: DataApiService,
              private subirArchivoService: SubirArchivoService) {
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(data => this.sigla = data['sigla']);
    this.publicService
          .getCursoPorSigla(this.sigla)
            .subscribe(dataCurso => {
              this.curso = dataCurso;
              this.nombreCurso = this.curso.nombre;

              this.publicService
                .getModelosRelPorId('cursos', this.curso.id, 'modulos', 'numero')
                  .subscribe((dataModulos: any) => {
                    this.modulos = dataModulos;
                    console.log(this.modulos);
                    let numeroDocsRegistrados = this.contarModulosConProfNoReg(this.modulos);

                    if (this.modulos.length !== 0) {
                      for (let index = 0; index < this.modulos.length; index++) {
                        const element = this.modulos[index];

                        if (element.docenteId !== 0) {
                          this.dataApiService
                                .getUsuarioDocente(element.docenteId)
                                  .subscribe(dataUsuarioDocente => {
                                    this.ussDocentes[index] = dataUsuarioDocente;
                                    if (numeroDocsRegistrados === 1) {
                                      this.cargaDocRegistrados = true;
                                    }
                                    numeroDocsRegistrados--;

                                  }, (errorUsuarioDocente) => {
                                    this.mensaje = errorUsuarioDocente.error.error.message;
                                  });
                        } else {
                          this.ussDocentes[index] = this.docenteNoRegistrado;
                        }
                        if (this.modulos.length === index + 1) {
                          this.carga = true;
                        }
                      }
                    } else {
                          this.mensaje = 'No hay módulos registrados para este curso';
                    }

                  }, (errorModulos) => this.mensaje = errorModulos.error.error.message);

            }, (errorCurso) => this.mensaje = errorCurso.error.error.message);
  }

  elimodal(modulo: Modulo, docente: Docente, index: number) {                    // Posesiona sobre el Estudiante a eliminar
    this.docente = docente;
    this.modulo = modulo;
    this.index = index;

    this.dataApiService
          .countRegistros('modulos', 'docenteId', this.modulo.docenteId)
            .subscribe(dataNumero => {
              if (dataNumero['count'] === 1) {
                this.quitarEliminar = 'ELIMINAR';
              } else {
                this.quitarEliminar = 'quitar';
              }
            }, errorNumeroDocentesModulo => {
              this.quitarEliminar = 'quitar';
              console.log('errorNumeroDocentesModulo', errorNumeroDocentesModulo);
            });
  }

  actDocenteModulo() {

    this.modulo.docenteId = 0;
    this.dataApiService
          .putModeloRelacionado('cursos', this.curso.id, 'modulos', this.modulo, this.modulo.id)
            .subscribe( dataPutModulo => {
              console.log('Profesor y docenteId del Modulo Actualizado', dataPutModulo);

              if (this.quitarEliminar === 'ELIMINAR') {
                this.dataApiService
                    .delModeloRelacionado('usuarios', this.docente.id, 'docente')
                      .subscribe(dataDelDocente => {
                        console.log('Docente Eliminado', dataDelDocente);

                        this.subirArchivoService
                              .delImage('docentesPosgrado', this.docente.imgpath)
                                .subscribe(dataEliImagenDocente => {
                                  console.log('dataEliImagen', dataEliImagenDocente);
                                }, errorEliImagenDocente => {
                                  console.log('errorEliImagenDocente', errorEliImagenDocente);
                                });
                        this.dataApiService
                              .delModelo('usuarios', this.docente.id)
                                .subscribe(dataDelUsuario => {
                                  console.log('Usuario Eliminado', dataDelUsuario);
                                }, (errorDelUsuario) => {
                                  this.mensaje = (errorDelUsuario.error.error.message);
                                });
                      }, (errorDelDocente) => {
                        this.mensaje = (errorDelDocente.error.error.message);
                      });
              }

            }, (errorPutModeloRelacionado) => {
              this.mensaje = errorPutModeloRelacionado.error.error.message;
            });
    this.ussDocentes[this.index] = this.docenteNoRegistrado;
    this.ussDocentes = this.ussDocentes.filter(Boolean);
  }

  irPaginaArriba() {
    this.dataApiService.retrocederPagina();
  }

  contarModulosConProfNoReg( modulos: Modulo[]) {
    let numeroRegistrados = 0;
    for (let index = 0; index < modulos.length; index++) {
      if (modulos[index].docenteId !== 0) {
        numeroRegistrados++;
      }
    }
    if (numeroRegistrados === 0) {
      this.cargaDocRegistrados = true;
    }
    return numeroRegistrados;
  }
  generarPDF() {}

}
