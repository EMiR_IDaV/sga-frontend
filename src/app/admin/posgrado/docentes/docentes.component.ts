import { Component, OnInit } from '@angular/core';

import { Curso } from '../../../interfaces/curso.interface';
import { PublicService } from '../../../services/public.service';
import { DataApiService } from '../../../services/data-api.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-docentes',
  templateUrl: './docentes.component.html',
  styleUrls: ['./docentes.component.css']
})
export class DocentesComponent implements OnInit {

  mensajeCursos: string;

  cursos: Curso[];

  constructor(private activatedRoute: ActivatedRoute, private dataApiService: DataApiService, private publicService: PublicService) { }

  ngOnInit() {
    this.publicService
          .getRegistrosModelo('cursos', 'id', 'desc')
            .subscribe( (dataCursos: any) => {
              (dataCursos.lenght === 0) ? this.mensajeCursos = 'No hay cursos registrados' : this.cursos = dataCursos;
            });
  }

  irPaginaArriba() {
    this.dataApiService.irPaginaArriba(this.activatedRoute);
  }
}
