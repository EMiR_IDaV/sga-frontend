import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl } from '@angular/forms';
import { Usuario } from '../../../../interfaces/usuario.interface';
import { DataApiService } from '../../../../services/data-api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SubirArchivoService } from '../../../../services/subir-archivo.service';
import { Docente } from '../../../../interfaces/docente.interface';
import { PublicService } from '../../../../services/public.service';
import { Curso } from '../../../../interfaces/curso.interface';
import { Modulo } from '../../../../interfaces/modulo.interface';

@Component({
  selector: 'app-reg-edi-doc',
  templateUrl: './reg-edi-doc.component.html',
  styleUrls: ['./reg-edi-doc.component.css']
})
export class RegEdiDocComponent implements OnInit {

  forma: FormGroup;
  forma1: FormGroup;

  sigla: string;
  numeroModulo: any;
  username: string;

  mensajeError = null;
  mensajeExito = null;
  accion = 'Registrar';
  nacionalidad = 'Boliviano';
  cod_deptos = ['LP', 'OR', 'PT', 'CB', 'SC', 'BN', 'PA', 'TJ', 'CH'];

  cambiarFoto = 'Seleccione';

  imagen: File;
  nombreImagen = '';

  curso: Curso;
  modulo: Modulo;

  usuario: Usuario = {
    paterno: '',
    materno: '',
    pri_nombre: '',
    seg_nombre: '',
    sexo: '',
    nacionalidad: '',
    ci: null,
    extendido: '',
    fec_nacimiento: '',
    celular: null,
    telefono: null,
    email: '',
    dom_ciudad: '',
    dom_zona: '',
    dom_calle: '',
    dom_numero: null,
    dom_otro: null
  };

  docente: Docente = {
    reg_docente: null,
    resena: '',
    grado: '',
    descripcion: '',
    imgpath: ''
  };

  nombreDocente = '';

  docExistente = false;
  ussDocentes: Usuario[];
  nombreProfesor = '';
  docenteId = 0;

  tituloDescripcion = `$$Entre sus estudios pos-universitarios se encuentran:
$$En su desempeño profesional se pueden mencionar:`;

  constructor(private dataApiService: DataApiService, private publicService: PublicService, private router: Router,
              private actRoute: ActivatedRoute, private subArcService: SubirArchivoService) {
    console.log('--------------- reg-edi-docente--------------');
  }

  ngOnInit() {
    this.cargarForma();
    this.actRoute.params.subscribe((data: any) => {
                                                    this.sigla = data['sigla'];
                                                    this.numeroModulo = data['numeroModulo'];
                                                    this.username = data['username'];
                                                  });

    this.dataApiService
          .getUsuariosDocentes()
            .subscribe((dataUssDocentes: any) => {
              this.ussDocentes = dataUssDocentes;
              console.log(this.ussDocentes);
            }, (errorGetUssDocentes) => {
              this.mensajeError = errorGetUssDocentes.error.error.message;
              this.mensajeExito = null;
            });

    this.publicService
          .getCursoPorSigla(this.sigla)
            .subscribe(dataCurso => {
              this.curso = dataCurso;
            }, (errorGetCursoPorSigla) => {
              this.mensajeError = errorGetCursoPorSigla.error.error.message;
              this.mensajeExito = null;
            });

    if (this.username !== 'nuevo') {
      this.accion = 'Actualizar';
      this.cambiarFoto = 'Seleccione para cambiar la';

      this.dataApiService
            .getUsuarioIncluidoTipo(this.username, 'docente')
              .subscribe((dataUsuario: any) => {
                console.log(dataUsuario);

                if (dataUsuario.nacionalidad !== 'Boliviano') {
                this.nacionalidad = 'Extranjero';
                }
                this.docente = dataUsuario.docente;
                delete this.usuario['docente'];
                for (const key in this.usuario) {
                  if (this.usuario.hasOwnProperty(key)) {
                    this.usuario[key] = dataUsuario[key];
                  }
                }
                this.usuario.fec_nacimiento = (<string>dataUsuario.fec_nacimiento).substring(0, 10);
                this.usuario.id = dataUsuario.id;

                this.usuario['reg_docente'] = this.docente.reg_docente;
                this.usuario['resena'] = this.docente.resena;
                this.usuario['grado'] = this.docente.grado;
                this.usuario['descripcion'] = this.docente.descripcion;
                this.forma.setValue(this.usuario);
              });
    }
  }

  registrarDocente() {
    let imgAnt: string;
    this.docente.reg_docente = this.forma.value.reg_docente;
    this.docente.resena = this.forma.value.resena;
    this.docente.grado = this.forma.value.grado;
    this.docente.descripcion = this.forma.value.descripcion;
    this.usuario = this.forma.value;
    this.usuario.username = this.usuario.paterno[0] + this.usuario.materno[0] + this.usuario.pri_nombre[0] + this.usuario.ci;
    this.usuario.password = this.usuario.ci.toString();

    if (this.username === 'nuevo') {
      this.docente.imgpath = this.nombreImagen;

      this.dataApiService
            .postModelo('usuarios', this.usuario)
              .subscribe((dataUsuario: any) => {
                console.log('Usuario Creado', dataUsuario);
                // this.docente.cursoId = this.curso.id;
                this.nombreDocente = this.dataApiService.obtieneNombreProfesor(dataUsuario);

                this.dataApiService
                      .postModeloRelacionado('usuarios', dataUsuario.id, 'docente', this.docente)
                        .subscribe((dataDocente: any) => {
                          console.log('Docente Creado', dataDocente);
                          this.nombreDocente = `${dataDocente.grado} ` + this.nombreDocente;
                          this.subirImagen(this.imagen);
                          this.cargarForma();

                          if (this.numeroModulo !== 'nuevo') {
                            this.actDocenteModulo(this.curso.id, this.numeroModulo, this.nombreDocente, dataDocente.id);
                          }
                          this.mensajeError = null;
                          this.mensajeExito = this.usuario.username;
                        }, (errorDocente) => {
                          this.mensajeExito = null;
                          this.mensajeError = errorDocente.error.error.message;
                          this.dataApiService
                            .delModelo('usuarios', dataUsuario.id)
                              .subscribe(dataDelUsuario => {
                                console.log('Usuario Eliminado', dataDelUsuario);
                              });
                        });
              }, (errorUsuario) => {
                this.mensajeExito = null;
                this.mensajeError = errorUsuario.error.error.message;
              });
    } else {
      if (this.nombreImagen !== '') {  // si solo cambia el texto monbreImagen esta vacio => no cambia la imagen
        imgAnt = this.docente.imgpath;
        this.docente.imgpath = this.nombreImagen;
      }
      this.dataApiService
            .putModeloRelacionado('usuarios', this.usuario.id, 'docente', this.docente)
              .subscribe((dataPutDocente: any) => {
                console.log('Docente Actualizado', dataPutDocente);
                this.nombreDocente = dataPutDocente.grado;

                if (this.nombreImagen !== '') {  // si solo cambia el texto monbreImagen esta vacio => no cambia la imagen
                  this.subArcService.delImage('docentesPosgrado', imgAnt).subscribe( dataE => console.log('Imagen Eliminida'));
                  this.subirImagen(this.imagen);
                }
                this.actUsuario(this.usuario);
              }, (errorPutDocente) => {
                this.mensajeExito = null;
                this.mensajeError = errorPutDocente.error.error.message;
              });
    }
    window.scrollTo(0, 0);
  }

  registrarDocenteExistente() {
    this.dataApiService
          .getModuloPorIdCursoNumeroModulo(this.curso.id, this.numeroModulo)
            .subscribe((dataModulo: any) => {
              this.modulo = dataModulo;

              this.modulo.profesor = this.nombreProfesor;
              this.modulo.docenteId = this.docenteId;

              this.dataApiService
                    .putModeloRelacionado('cursos', this.curso.id, 'modulos', this.modulo, this.modulo.id)
                      .subscribe( dataPutModulo => {
                        console.log('profesor y docenteId del Modulo Actualizado', dataPutModulo);
                      }, (errorPutModeloRelacionado) => {
                        this.mensajeExito = null;
                        this.mensajeError = errorPutModeloRelacionado.error.error.message;
                      });

            }, (errorModulo) => {
              this.mensajeExito = null;
              this.mensajeError = errorModulo.error.error.message;
             });
  }

  selectElegido(id: string) {
    const valueSelect = id.split(':');
    const index = parseInt(valueSelect[0], 10);
    const docenteId = parseInt(valueSelect[1], 10);
    console.log(docenteId);
    this.docenteId = docenteId;
    const prof = `${this.ussDocentes[index].docente.grado} ${this.dataApiService.obtieneNombreProfesor(this.ussDocentes[index])}`;
    this.nombreProfesor = prof;
  }

  actUsuario(usuario: Usuario) {
    this.dataApiService
          .putModelo('usuarios', usuario)
            .subscribe((dataPutUsuario: any) => {
              console.log('Usuario Actualizado', dataPutUsuario);

              if (this.numeroModulo !== 'nuevo') {
                this.nombreDocente = `${this.nombreDocente} ${this.dataApiService.obtieneNombreProfesor(dataPutUsuario)}`;
                this.actDocenteModulo(this.curso.id, this.numeroModulo, this.nombreDocente);
              }
              this.mensajeError = null;
              this.mensajeExito = usuario.username;
            }, (errorPutUsuario) => {
              this.mensajeExito = null;
              this.mensajeError = errorPutUsuario.error.error.message;
            });
  }

  actDocenteModulo(idCurso: number, numeroModulo: number, profesor: string, idDocente?: number) {
    this.dataApiService
          .getModuloPorIdCursoNumeroModulo(idCurso, numeroModulo)
            .subscribe((dataModulo: any) => {
              this.modulo = dataModulo;
              if (idDocente) { this.modulo.docenteId = idDocente; }
              this.modulo.profesor = profesor;

              this.dataApiService
                    .putModeloRelacionado('cursos', idCurso, 'modulos', this.modulo, this.modulo.id)
                      .subscribe( dataPutModulo => {
                        console.log('profesor y docenteId del Modulo Actualizado', dataPutModulo);
                      }, (errorPutModeloRelacionado) => {
                        this.mensajeExito = null;
                        this.mensajeError = errorPutModeloRelacionado.error.error.message;
                      });

            }, (errorModulo) => {
              this.mensajeExito = null;
              this.mensajeError = errorModulo.error.error.message;
             });
  }

  subirImagen(imagen: File) {
    this.subArcService
        .subirArchivo(imagen, this.nombreImagen, 'docentesPosgrado', 'b')
          .then()
          .catch(datac => console.log(datac));
  }

  selimg( archivo: File ) {
    this.nombreImagen = '';
    if ( !archivo ) {
      this.imagen = null;
      console.log('vacio');
      return;
    }
    this.imagen = archivo;
    const nombreCortado = this.imagen.name.split('.');
    const extensionArchivo = nombreCortado.pop();
    const name = Date.now().toString();
    this.nombreImagen = name + '.' + extensionArchivo;
    console.log('nombreImagen', this.nombreImagen);
  }

  cargarForma() {
    this.nacionalidad = 'Boliviano';
    this.mensajeError = null;
    this.mensajeExito = null;
    this.forma = new FormGroup ({
      'paterno': new FormControl(''),
      'materno': new FormControl(''),
      'pri_nombre': new FormControl(''), // , [Validators.required, Validators.minLength(2)]
      'seg_nombre': new FormControl(''),
      'sexo': new FormControl(''), // , [Validators.required]
      'nacionalidad': new FormControl('Boliviano'),
      'ci': new FormControl(''), // , [Validators.required, Validators.minLength(5)]
      'extendido': new FormControl(''), // , [Validators.required]
      'fec_nacimiento': new FormControl('', []),
      'celular': new FormControl(''), // , [Validators.required, Validators.minLength(8)]
      'telefono': new FormControl(''),
      'reg_docente': new FormControl(''),
      'email': new FormControl(''), // , [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]
      'dom_ciudad': new FormControl('', []),
      'dom_zona': new FormControl('', []),
      'dom_calle': new FormControl('', []),
      'dom_numero': new FormControl('', []),
      'dom_otro': new FormControl(''),
      'grado': new FormControl(''),
      'resena': new FormControl(''),
      'descripcion': new FormControl(this.tituloDescripcion),
      'id': new FormControl()
    });
    this.forma1 = new FormGroup ({
      'control1': new FormControl('')
    });
  }

  irPaginaArriba() {
    this.dataApiService.retrocederPagina();
  }

}

