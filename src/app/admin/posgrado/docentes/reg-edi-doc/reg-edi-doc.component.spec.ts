import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegEdiDocComponent } from './reg-edi-doc.component';

describe('RegEdiDocComponent', () => {
  let component: RegEdiDocComponent;
  let fixture: ComponentFixture<RegEdiDocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegEdiDocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegEdiDocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
