import { Component, OnInit } from '@angular/core';
import { Modulo } from '../../../../interfaces/modulo.interface';
import { ActivatedRoute } from '@angular/router';
import { DataApiService } from '../../../../services/data-api.service';
import { PublicService } from '../../../../services/public.service';
import { Curso } from '../../../../interfaces/curso.interface';
import { Usuario } from '../../../../interfaces/usuario.interface';
import { FormGroup, FormControl, FormArray } from '@angular/forms';
import { Nota } from '../../../../interfaces/nota.interface';
import { TableService } from '../../../../services/table.service';

import * as jsPDF from 'jspdf';
import { logo } from '../../../../services/imagenes';


@Component({
  selector: 'app-list-notas',
  templateUrl: './list-notas.component.html',
  styleUrls: ['./list-notas.component.css']
})
export class ListNotasComponent implements OnInit {


  forma: FormGroup;

  mensaje = null;
  mensajeExito = null;
  sigla: string;
  numeroModulo: number;

  curso: Curso;
  modulo: Modulo;
  notas: Nota[] = [];

  ussEstudiantes: Usuario[] = [];

  altura = 21;
  salto = 5;
  tamtitulo = 13;
  xm = 108;
  x1 = 13;
  x2 = 20;
  x3 = 110;
  x4 = 122;
  x5 = 203;

  constructor(private activatedRoute: ActivatedRoute, private dataApiService: DataApiService, private publicService: PublicService,
              private tableService: TableService) {
    console.log('--------------------------------------- list-notas ---------------------------------------');
    this.forma = new FormGroup({
      'notas': new FormArray([
        new FormControl(this.notas),
      ])
    });
  }

  ngOnInit() {
    this.activatedRoute.params
          .subscribe(data => {
            this.sigla = data['sigla'];
            this.numeroModulo = data['numeroModulo'];
          });

    this.publicService
          .getCursoPorSigla(this.sigla)
            .subscribe((dataCurso) => {
              this.curso = dataCurso;
              console.log('curso', this.curso);

              this.dataApiService
                    .getModuloPorIdCursoNumeroModulo(this.curso.id, this.numeroModulo)
                      .subscribe((dataModulo) => {
                        this.modulo = dataModulo;
                        console.log('modulo', this.modulo);

                        this.dataApiService
                              .getEstudiantesPorSiglaNumero(this.sigla, this.numeroModulo)
                                .subscribe((dataEstudiantes: any) => {
                                  this.ussEstudiantes = dataEstudiantes;
                                  console.log('estudiantes', this.ussEstudiantes);

                                  for (let index = 0; index < this.ussEstudiantes.length; index++) {
                                    const element = this.ussEstudiantes[index];
                                    this.dataApiService
                                          .getRegistroPorFiltros('nota', 'moduloId', this.modulo.id, 'estudianteId', element.id)
                                            .subscribe((dataNota: any) => {
                                                    this.notas[index] = dataNota;
                                                    console.log('dataNotas', this.notas);
                                                  }, (errorNotas) => {
                                                    this.mensaje = errorNotas.error.error.message;
                                                  });
                                  }
                                }, (errorEstudiantes) => {
                                  this.mensaje = 'No hay Estudiantes registrados';
                                });

                      }, (errorModulo) => {
                        this.mensaje = errorModulo.error.error.message;
                      });
            }, (errorCurso) => {
              this.mensaje = errorCurso.error.error.message;
            });

  }

  registrarNotas() {
    let sw = true;
    for (let index = 0; index < this.notas.length; index++) {
      const element = this.notas[index];

      this.dataApiService
            .putModelo('nota', this.notas[index])
              .subscribe(dataNota => {

                if (sw && index === this.notas.length - 1) {
                  this.mensajeExito = 'Se registrarón las notas exitosamente';
                }
              }, (errorNotas) => {
                sw = false;
                this.mensaje = errorNotas.error.error.message;
              });
    }
    window.scrollTo(0, 0);
  }

  irPaginaArriba() {
    this.dataApiService.retrocederPagina();
  }

  llenarDatos() {
    for (let index = 0; index < this.ussEstudiantes.length; index++) {
      const element = this.ussEstudiantes[index];
      element['nota'] = this.notas[index].nota;
      element['observacion'] = this.notas[index].observacion;
    }
  }
  vaciarDatos() {
    for (let index = 0; index < this.ussEstudiantes.length; index++) {
      const element = this.ussEstudiantes[index];
      delete element['nota'];
      delete element['observacion'];
    }
  }


  generarPDF() {

    this.llenarDatos();
    console.log(this.ussEstudiantes);

    const doc: any = new jsPDF('p', 'mm', 'letter', true);
    doc.setProperties({
      title: 'Notas módulo ' + this.modulo.nombre + '.pdf',
      subject: 'Ipicom',
      author: 'Ipicom',
      keywords: 'generated, javascript, web 2.0, ajax',
      creator: 'IpiCOM'
    });

    // TÍTULO
    doc.setFontSize(this.tamtitulo);
    doc.setFontType('bold');
    doc.text('PLANILLA DE CALIFICACIONES', this.xm, this.altura, 'center');
    doc.setFontSize(this.tamtitulo - 2);
    doc.setFontType('normal');
    doc.text(this.curso.nombre.toUpperCase(), this.xm, this.saltar(), 'center');

    this.altura = this.altura + 3;

    doc.setFontSize(this.tamtitulo - 2);
    doc.text('Módulo ' + this.modulo.numero + ' ' + this.modulo.nombre, this.xm, this.saltar(), 'center');
    doc.text('Docente ' + this.modulo.profesor + '  -  Del ' + (<string>this.modulo.inicio_clases).slice(0, 10) + ' al ' + (<string>this.modulo.fin_clases).slice(0, 10), this.xm, this.saltar(), 'center');
    // doc.text('DEL ' + this.modulo.calendario.toUpperCase(), this.xm, this.saltar(), 'center');

    // doc.addImage(logoUMSA, 'PNG', 5, 21, 12, 22);
    // doc.addImage(imagenIpiCOM, 'JPEG', 140, 5, 72, 32);

    // Generar Tabla // revisar antes de liberar no esta provisto para 2 hojas
    this.tableService.tabla(doc, this.altura, this.ussEstudiantes, ['N°', 'APELLIDOS Y NOMBRES', 'nota', 'observacion'], ['', '', 'center', 'center'], [this.x1, this.x2, this.x3, this.x4, this.x5], this.tamtitulo - 3, 'times', this.salto, 1, 2);


    // GUIAS
    // doc.setFontSize(1);
    // for (let index = 0; index < 217; index++) {
    //   // if (index % 2 === 0 ) {
    //     // doc.setTextColor(255,0,0);
    //     // doc.text(index.toString(), index, 0.2);
    //     // doc.setTextColor(0,255,0);
    //     // doc.text(index.toString(), index, 0.5);
    //     doc.setTextColor(0, 0, 255);
    //     doc.text(index.toString(), index, 0.3, 'center');
    //     doc.text('|', index, 0.3);
    //   // }
    // }
    // for (let index = 0; index < 280; index++) {
    //   // if (index % 2 === 0 ) {
    //     // doc.setTextColor(255,0,0);
    //     // doc.text(index.toString(),0, index);
    //     // doc.setTextColor(0,255,0);
    //     // doc.text(index.toString(),0.5, index);
    //     doc.setTextColor(0, 0, 255);
    //     doc.text(index.toString(), 108, index);
    //     doc.text('-', 0, index);
    //   // }
    // }

    // doc.output('dataurlnewwindow'); nueva ventana
    doc.save('Notas Módulo' + this.modulo.numero + ' ' + this.modulo.nombre);
    this.altura = 15;

    this.vaciarDatos();

  }

  devuelveFecha(date: Date) {
    return `${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`;
  }

  saltar() {
    return this.altura = this.altura + this.salto;
  }

}
