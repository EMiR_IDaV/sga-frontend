import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegEdiNotasComponent } from './reg-edi-notas.component';

describe('RegEdiNotasComponent', () => {
  let component: RegEdiNotasComponent;
  let fixture: ComponentFixture<RegEdiNotasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegEdiNotasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegEdiNotasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
