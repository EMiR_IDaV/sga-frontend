import { Component, OnInit } from '@angular/core';

import { Curso } from '../../../interfaces/curso.interface';
import { PublicService } from '../../../services/public.service';
import { DataApiService } from '../../../services/data-api.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-notas',
  templateUrl: './notas.component.html',
  styleUrls: ['./notas.component.css']
})
export class NotasComponent implements OnInit {

  mensaje = null;

  cursos: Curso[] = [];
  modulosTodos: any[] = [];

  constructor(private activatedRoute: ActivatedRoute, private publicService: PublicService, private dataApiService: DataApiService) { }

  ngOnInit() {
    this.publicService
          .getRegistrosModelo('cursos', 'id', 'desc')
            .subscribe( (datagetCursos: any) => {
              this.cursos = datagetCursos;

              for (let index = 0; index < this.cursos.length; index++) {
                const curso = this.cursos[index];

                this.publicService
                      .getModelosRelPorId('cursos', curso.id, 'modulos', 'numero')
                        .subscribe((dataModulos: any) => {
                          this.modulosTodos[index] = dataModulos;

                          for (let index1 = 0; index1 < dataModulos.length; index1++) {
                            const element = dataModulos[index1];

                            this.dataApiService
                                  .getNumeroRelacionados('modulos', element.id, 'estudiantes')
                                    .subscribe( (data: any) => {
                                      element['numeroEstudiantes'] = data.count;
                                    }, errorNumEstudiantes => this.mensaje = (errorNumEstudiantes.error.error.message));
                          }
                          this.modulosTodos[index] = dataModulos;
                        }, errorGetModulos => this.mensaje = (errorGetModulos.error.error.message));
              }
            }, (errorCursos) => {
              this.mensaje = (errorCursos.error.error.message);
            });
  }

  irPaginaArriba() {
    this.dataApiService.irPaginaArriba(this.activatedRoute);
  }

}
