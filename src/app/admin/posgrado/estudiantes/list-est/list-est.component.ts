import { Component, OnInit, OnDestroy, Input } from '@angular/core';

import { DataApiService } from '../../../../services/data-api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { TableService } from '../../../../services/table.service';
import { Usuario } from 'src/app/interfaces/usuario.interface';
import { PublicService } from '../../../../services/public.service';
import { Curso } from '../../../../interfaces/curso.interface';
import { Modulo } from '../../../../interfaces/modulo.interface';

import * as jsPDF from 'jspdf';
declare var $: any;

@Component({
  selector: 'app-list-est',
  templateUrl: './list-est.component.html',
  styleUrls: ['./list-est.component.css']
})

export class ListEstComponent implements OnInit, OnDestroy {

  mensaje = null;

  usuario: Usuario = null;
  index: number = null;

  sigla: string;
  numeroModulo: number;

  curso: Curso;
  modulo: Modulo;

  nombreCurso: string;
  ussEstudiantes: Usuario[] = [];

  altura = 21;
  salto = 5;
  tamtitulo = 12;
  xm = 108;
  x1 = 13;
  x2 = 20;
  x3 = 110;
  x4 = 180;
  x5 = 203;

  constructor(private publicService: PublicService, private dataApiService: DataApiService, private router: Router,
              private activatedRoute: ActivatedRoute, private tableService: TableService) {
    console.log('---------------list-est--------------');
  }

  ngOnInit() {
    this.activatedRoute.params
          .subscribe(data => {
            this.sigla = data['sigla'];
            this.numeroModulo = data['numeroModulo'];
          });

    this.publicService
          .getCursoPorSigla(this.sigla)
            .subscribe((dataCurso) => {
              this.curso = dataCurso;
              console.log('curso', this.curso);

              this.dataApiService
                    .getModuloPorIdCursoNumeroModulo(this.curso.id, this.numeroModulo)
                      .subscribe((dataModulo) => {
                        this.modulo = dataModulo;
                        console.log('modulo', this.modulo);
                      }, (errorModulo) => {
                        this.mensaje = errorModulo.error.error.message;
                      });
            }, (errorCurso) => {
              this.mensaje = errorCurso.error.error.message;
            });

    this.dataApiService
          .getEstudiantesPorSiglaNumero(this.sigla, this.numeroModulo)
            .subscribe((dataEstudiantes: any) => {
              this.ussEstudiantes = dataEstudiantes;
              console.log('estudiantes', this.ussEstudiantes);
            }, (errorEstudiantes) => {
              this.mensaje = 'No hay Estudiantes registrados';
            });

  }

  elimodal(usuario: Usuario, index: number) {
    this.usuario = usuario;
    this.index = index;
  }

  quitarEstudiante() {

    this.dataApiService
          .delModeloRelacionado('modulos', this.modulo.id, 'estudiantes', this.ussEstudiantes[this.index].estudiante.id, true)
            .subscribe((dataDelNota) => {
              console.log('dataDelNota', dataDelNota);
              this.eliminarEstudiante(this.ussEstudiantes[this.index].estudiante.id, this.ussEstudiantes[this.index].username);
              delete this.ussEstudiantes[this.index];
              this.ussEstudiantes = this.ussEstudiantes.filter(Boolean);
            }, (errorDelNota) => {
              this.mensaje = errorDelNota.error.error.message;
            });
  }

  eliminarEstudiante(id: number, username: string) {

    this.dataApiService
          .getModelosPorFiltroOrdenar('nota', 'estudianteId', id)
            .subscribe((dataFiltrado: any) => {
              console.log('dataFiltrado', dataFiltrado);

              if (dataFiltrado.length === 0) {
                this.dataApiService
                      .getUsuarioIncluidoTipo(username, 'estudiante')
                        .subscribe( dataEstudiante => {

                          this.dataApiService
                            .delModeloRelacionado('usuarios', dataEstudiante.id, 'estudiante')
                              .subscribe(dataDelEstudiante => {
                                console.log('Estudiante Eliminado', dataDelEstudiante);

                                this.dataApiService
                                      .delModelo('usuarios', dataEstudiante.id)
                                        .subscribe(dataDelUsuario => {
                                          console.log('Usuario Eliminado', dataDelUsuario);

                                        }, (errorDelUsuario) => {
                                          this.mensaje = (errorDelUsuario.error.error.message);
                                        });
                              }, (errorDelEstudiante) => {
                                this.mensaje = (errorDelEstudiante.error.error.message);
                              });

                        }, errorEstudiante => {
                          this.mensaje = (errorEstudiante.error.error.message);
                        });
              } else {
                console.log('mas');
              }
            }, errorFiltrado => {
              this.mensaje = (errorFiltrado.error.error.message);
            });
  }

  generarPDF() {
    const doc: any = new jsPDF('p', 'mm', 'letter', true);
    doc.setProperties({
      title: 'Lista de Estudiantes ' + this.sigla + '.pdf',
      subject: 'Ipicom',
      author: 'Ipicom',
      keywords: 'generated, javascript, web 2.0, ajax',
      creator: 'IpiCOM'
    });

    // TÍTULO
    doc.setFontSize(this.tamtitulo);
    doc.setFontType('bold');
    doc.text('UNIVERSIDAD MAYOR DE SAN ANDRÉS', this.xm, this.altura, 'center');
    doc.text('FACULTAD DE CIENCIAS SOCIALES', this.xm, this.saltar(), 'center');

    doc.setFontSize(this.tamtitulo - 1);
    doc.setFontType('normal');
    doc.text('CARRERA CIENCIAS DE LA COMUNICACIÓN SOCIAL', this.xm, this.saltar(), 'center');
    doc.text('INSTITUTO DE INVESTIGACIÓN POSGRADO E INTERACCIÓN SOCIAL EN COMUNICACIÓN', this.xm, this.saltar(), 'center');
    this.altura = this.altura + 3;

    doc.setFontSize(this.tamtitulo - 2);
    doc.setFont('helvetica');

    const titulo = 'LISTA DE INSCRITOS : ' + this.curso.nombre.toLocaleUpperCase() + ' ' + this.curso.gestion;
    doc.text(titulo, 108, this.saltar(), 'center');
    const nombreObjeto = `Módulo ${this.modulo.numero} ${this.modulo.nombre}`;
    doc.text(nombreObjeto, 108, this.saltar(), 'center');

    // Generar Tabla
    this.tableService.tabla(doc, this.altura, this.ussEstudiantes, ['N°', 'APELLIDOS Y NOMBRES', 'email', 'celular'], ['', 'center', 'center', 'center'], [this.x1, this.x2, this.x3, this.x4, this.x5], this.tamtitulo - 2, 'times', this.salto, 1, 2);

    doc.save('Lista de Estudiantes ' + this.sigla);
    this.altura = 15;
  }

  saltar() {
    return this.altura = this.altura + this.salto;
  }

  ngOnDestroy(): void {
    // $('#ModalEstudiante').modal('hide');
    // $('#ModalEstudiante').modal('dispose');
    $('.modal-backdrop').hide();
  }

  irPaginaArriba() {
    this.dataApiService.retrocederPagina();
    // this.dataApiService.irPaginaArriba(this.activatedRoute);
  }

}
