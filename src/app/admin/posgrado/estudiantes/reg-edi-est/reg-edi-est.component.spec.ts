import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegEdiEstComponent } from './reg-edi-est.component';

describe('RegEdiEstComponent', () => {
  let component: RegEdiEstComponent;
  let fixture: ComponentFixture<RegEdiEstComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegEdiEstComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegEdiEstComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
