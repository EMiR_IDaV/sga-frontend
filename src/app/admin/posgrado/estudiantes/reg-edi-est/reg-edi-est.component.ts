import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Usuario } from '../../../../interfaces/usuario.interface';
import { ActivatedRoute, Router } from '@angular/router';
import { DataApiService } from '../../../../services/data-api.service';
import { AutentificacionService } from '../../../../services/autentificacion.service';
import { Estudiante } from '../../../../interfaces/estudiante.interface';
import { Location } from '@angular/common';
import { PublicService } from '../../../../services/public.service';
import { Modulo } from '../../../../interfaces/modulo.interface';
import { Curso } from '../../../../interfaces/curso.interface';
import { Nota } from 'src/app/interfaces/nota.interface';


@Component({
  selector: 'app-reg-edi-est',
  templateUrl: './reg-edi-est.component.html',
  styleUrls: ['./reg-edi-est.component.css']
})
export class RegEdiEstComponent implements OnInit {

  forma: FormGroup;
  mensajeError = null;
  mensajeExito = null;

  sigla: string;
  numeroModulo: number;
  username: string;

  modulo: Modulo;
  curso: Curso;
  accion = 'Registrar';
  nacionalidad = 'Boliviano';
  cod_deptos = ['LP', 'OR', 'PT', 'CB', 'SC', 'BN', 'PA', 'TJ', 'CH'];
  cursoId: number;

  usuario: Usuario = {
    paterno: '',
    materno: '',
    pri_nombre: '',
    seg_nombre: '',
    sexo: '',
    nacionalidad: '',
    ci: null,
    extendido: '',
    fec_nacimiento: '',
    celular: null,
    telefono: null,
    email: '',
    dom_ciudad: '',
    dom_zona: '',
    dom_calle: '',
    dom_numero: null,
    dom_otro: null
  };

  estudiante: Estudiante = {
    reg_universitario: null
  };

  nota: Nota = {
    moduloId: null,
    estudianteId: null,
    nota: 0,
    observacion: ''
  };

  // path 'estudiantes/:sigla/:username'
  constructor(private publicService: PublicService, private dataApiService: DataApiService, private router: Router,
              private actRoute: ActivatedRoute, private location: Location) {
    console.log('--------------- reg-edi-est--------------');
  }

  ngOnInit() {
    this.cargarForma();
    this.actRoute
          .params
            .subscribe((data: any) => {
              this.sigla = data['sigla'];
              this.numeroModulo = data['numeroModulo'];
              this.username = data['username'];
            });

    this.publicService
          .getCursoPorSigla(this.sigla)
            .subscribe((dataCurso: any) => {
              this.curso = dataCurso;

              this.dataApiService
                    .getModuloPorIdCursoNumeroModulo(this.curso.id, this.numeroModulo)
                      .subscribe((dataModulo) => {
                        this.modulo = dataModulo;
                        this.nota.moduloId = this.modulo.id;
                      }, (errorModulo) => {
                        this.mensajeExito = null;
                        this.mensajeError = errorModulo.error.errro.message;
                      });
            }, (errorCurso) => {
              this.mensajeExito = null;
              this.mensajeError = errorCurso.error.errro.message;
            });

    if (this.username !== 'nuevo') {
      this.accion = 'Actualizar';

      this.dataApiService
            .getUsuarioIncluidoTipo(this.username, 'estudiante')
              .subscribe((dataUsuario: any) => {
                console.log(dataUsuario);

                if (dataUsuario.nacionalidad !== 'Boliviano') {
                  this.nacionalidad = 'Extranjero';
                }
                this.estudiante = dataUsuario.estudiante;
                delete this.usuario['estudiante'];
                for (const key in this.usuario) {
                  if (this.usuario.hasOwnProperty(key)) {
                  // if (this.usuario.hasOwnProperty(key) && key !== 'fec_nacimiento') {
                    this.usuario[key] = dataUsuario[key];
                  }
                }
                this.usuario.fec_nacimiento = (<string>dataUsuario.fec_nacimiento).substring(0, 10);
                this.usuario.id = dataUsuario.id;
                this.usuario['reg_universitario'] = this.estudiante.reg_universitario;
                this.forma.setValue(this.usuario);
              });
    }
  }

  retornaFormatoOK(nombre: string) {
    if (nombre === '') { return nombre; }
    nombre = nombre.trim();
    nombre = nombre.toLowerCase();
    nombre = nombre.charAt(0).toUpperCase() + nombre.slice(1);
    return nombre;
  }

  regEstudiante() {
    this.estudiante.reg_universitario = this.forma.value.reg_universitario;
    this.usuario =  this.forma.value;
    delete this.usuario['reg_universitario'];
    this.usuario.paterno = this.retornaFormatoOK(this.usuario.paterno);
    this.usuario.materno = this.retornaFormatoOK(this.usuario.materno);
    this.usuario.pri_nombre = this.retornaFormatoOK(this.usuario.pri_nombre);
    this.usuario.seg_nombre = this.retornaFormatoOK(this.usuario.seg_nombre);
    this.usuario.username = this.usuario.paterno[0] + this.usuario.materno[0] + this.usuario.pri_nombre[0] + this.usuario.ci;
    this.usuario.password = this.usuario.ci.toString();

    if (this.username === 'nuevo') {

      this.dataApiService
            .postModelo('usuarios', this.usuario)
              .subscribe((dataUsuario: any) => {
               console.log('Usuario Creado', dataUsuario);

              this.dataApiService
                    .postModeloRelacionado('usuarios', dataUsuario.id, 'estudiante', this.estudiante)
                      .subscribe((dataEstudiante: any) => {
                          console.log('Estudiante Creado', dataEstudiante);
                          this.cargarForma();
                          this.nota.estudianteId = dataEstudiante.id;

                            this.dataApiService
                                .postModelo('nota', this.nota)
                                  .subscribe((dataNota: any) => {
                                    console.log('Nota Registrada', dataNota);
                                    this.mensajeError = null;
                                    this.mensajeExito = this.usuario.username;
                                  }, (errorNota) => {
                                    this.mensajeExito = null;
                                    this.mensajeError = errorNota.error.error.message;
                                  });
                      }, (errorPostEstudiante) => {
                        this.mensajeExito = null;
                        this.mensajeError = errorPostEstudiante.error.error.message;
                        this.dataApiService
                          .delModelo('usuarios', dataUsuario.id)
                            .subscribe(dataDelUsuario => {
                              console.log('Usuario Eliminado', dataDelUsuario);
                            }, (errorDelUsuario) => {
                              this.mensajeExito = null;
                              this.mensajeError = errorDelUsuario.error.error.message;
                            });
                      });
              }, (errorServicioUsu) => {
                this.mensajeExito = null;
                this.mensajeError = errorServicioUsu.error.error.message;
              });

    } else {
      this.dataApiService
            .putModelo('usuarios', this.usuario)
              .subscribe((dataPutUsuario: any) => {
                console.log('Usuario Actualizado', dataPutUsuario);

                this.dataApiService
                      .putModeloRelacionado('usuarios', this.usuario.id, 'estudiante', this.estudiante)
                        .subscribe((dataPutEstudiante: any) => {
                          console.log('Estudiante Actualizado', dataPutEstudiante);
                          this.mensajeError = null;
                          this.mensajeExito = this.usuario.username;
                        }, (errorPutEstudiante) => {
                          this.mensajeExito = null;
                          this.mensajeError = errorPutEstudiante.error.error.message;
                        });
              }, (errorServicioPutUsu) => {
                this.mensajeExito = null;
                this.mensajeError = errorServicioPutUsu.error.error.message;
              });
    }
    window.scrollTo(0, 0);
  }

  cargarForma() {
    this.nacionalidad = 'Boliviano';
    this.mensajeError = null;
    this.mensajeExito = null;
    this.forma = new FormGroup ({
      'paterno': new FormControl(''),
      'materno': new FormControl(''),
      'pri_nombre': new FormControl(''), // , [Validators.required, Validators.minLength(2)]
      'seg_nombre': new FormControl(''),
      'sexo': new FormControl(''), // , [Validators.required]
      'nacionalidad': new FormControl('Boliviano'),
      'ci': new FormControl(''), // , [Validators.required, Validators.minLength(5)]
      'extendido': new FormControl(''), // , [Validators.required]
      'fec_nacimiento': new FormControl('', []),
      'celular': new FormControl(''), // , [Validators.required, Validators.minLength(8)]
      'telefono': new FormControl(''),
      'reg_universitario': new FormControl(''),
      'email': new FormControl(''), // , [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]
      'dom_ciudad': new FormControl('', []),
      'dom_zona': new FormControl('', []),
      'dom_calle': new FormControl('', []),
      'dom_numero': new FormControl('', []),
      'dom_otro': new FormControl(''),
      'id': new FormControl()
    });
  }

  back() {
    this.location.back();
  }

}
