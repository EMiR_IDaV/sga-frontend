import { Component, OnInit } from '@angular/core';
import { DataApiService } from '../../../../services/data-api.service';
import { ActivatedRoute } from '@angular/router';
import { PublicService } from '../../../../services/public.service';
import { Curso } from '../../../../interfaces/curso.interface';
import { Modulo } from '../../../../interfaces/modulo.interface';
import { Nota } from '../../../../interfaces/nota.interface';
import { Location } from '@angular/common';

@Component({
  selector: 'app-agr-mul-est',
  templateUrl: './agr-mul-est.component.html',
  styleUrls: ['./agr-mul-est.component.css']
})
export class AgrMulEstComponent implements OnInit {

  mensajeEstRegistrados: string;
  mensaje = null;

  sigla: string;
  numeroModulo: number;

  curso: Curso;
  modulo: Modulo;
  cursos: Curso[];
  estudiantesPorCurso: any[] = [];
  estudiantesPorCursoModuloFiltrado: any[] = [];

  constructor(private publicService: PublicService, private dataApiService: DataApiService,
              private activatedRoute: ActivatedRoute, private location: Location) {
    console.log('--------------- agr-mul-est --------------');
  }

  async ngOnInit() {

    this.activatedRoute.params
    .subscribe(data => {
      this.sigla = data['sigla'];
      this.numeroModulo = data['numeroModulo'];
    });

    await this.publicService.getCursoPorSiglaAsync(this.sigla)
                .then(dataCurso => {
                  this.curso = dataCurso;
                })
                .catch((errorCurso) => {
                  this.mensaje = errorCurso.error.error.message;
                });

    await this.dataApiService.getModuloPorIdCursoNumeroModuloAsync(this.curso.id, this.numeroModulo)
                .then(data => {
                  this.modulo = data;
                })
                .catch((errorModulo) => {
                  this.mensaje = errorModulo.error.error.message;
                });

    await this.publicService.getRegistrosModeloAsync('cursos', 'id', 'desc')
                .then((dataCursos: any) => {
                  this.cursos = dataCursos;
                })
                .catch((errorCursos) => {
                  this.mensaje = errorCursos.error.error.message;
                });

    console.log(this.modulo);
    console.log(this.cursos);
    for (let index = 0; index < this.cursos.length; index++) {
      const element = this.cursos[index];

      this.dataApiService
            .getEstudiantesPorSigla(element.sigla)
              .subscribe((dataEstudiantesPorSigla: any) => {
                this.estudiantesPorCurso[index] = dataEstudiantesPorSigla;
                console.log(element.sigla, 'dataEstudiantesPorSigla', dataEstudiantesPorSigla);

                this.dataApiService
                      .getEstudianteIdsPorSiglaComplementoModulo(element.sigla, this.modulo.id)
                        .subscribe((dataEstudiantesIdsComplemento: any) => {
                          this.estudiantesPorCursoModuloFiltrado[index] = dataEstudiantesIdsComplemento;

                          const estudiantesPorCursoAuxiliar: any[] = [];
                          for (let index1 = 0; index1 < this.estudiantesPorCurso[index].length; index1++) {
                            const element1 = this.estudiantesPorCurso[index][index1];

                            for (let index2 = 0; index2 < this.estudiantesPorCursoModuloFiltrado[index].length; index2++) {
                              const element2 = this.estudiantesPorCursoModuloFiltrado[index][index2];

                                if (element1.id === element2.estudianteId) {
                                  estudiantesPorCursoAuxiliar.push(element1);
                                  break;
                              }
                            }
                          }

                          if (estudiantesPorCursoAuxiliar.length > 0 ) {
                            this.estudiantesPorCurso[index] = estudiantesPorCursoAuxiliar;
                          } else {
                            this.estudiantesPorCurso[index] = 'Todos los estudiantes de este curso estan registrados en este módulo';
                          }

                        }, error => {
                          console.log('error dataEstudiantesIdsComplemento', error);
                        });

              }, (errorCursos) => {
                this.estudiantesPorCurso[index] = 'No hay estudiantes en este Curso';
                console.log(index, 'nohay estudianmtes');
              });
    }

  }

  registrarEstudiantes(indice: number) {
    console.log(this.estudiantesPorCurso[indice].length);
    const nota: Nota = {
      moduloId: null,
      estudianteId: null,
      nota: 0,
      observacion: ''
    };
    let cont = 0;
    for (let index = 0; index < this.estudiantesPorCurso[indice].length; index++) {
      const tbody = 'aqcheckbox' + indice + '-' + index;
      const aqcheckbox = <HTMLInputElement>document.getElementById(tbody);
      const aqchecked = aqcheckbox.checked;

      if (aqchecked) {
        cont++;
      }
    }
    console.log(cont);

    let cont2 = 0;
    for (let index = 0; index < this.estudiantesPorCurso[indice].length; index++) {
      // const element = this.cursos[indice][index];

      const tbody = 'aqcheckbox' + indice + '-' + index;
      const aqcheckbox = <HTMLInputElement>document.getElementById(tbody);
      const aqchecked = aqcheckbox.checked;

      if (aqchecked) {
        cont2++;
        console.log('SI');
        console.log(cont2);
        nota.nota = 0;
        nota.moduloId = this.modulo.id;
        nota.estudianteId = this.estudiantesPorCurso[indice][index].id;
        console.log(nota);

        this.dataApiService
            .postModelo('nota', nota)
              .subscribe((dataNota: any) => {
                console.log('Nota Registrada', dataNota);
                if (cont === cont2) {
                  console.log('iguales');
                  // revisar Agregar un setTimeout despues de un mensaje
                  this.mensajeEstRegistrados = `Los ${cont} estudiantes seleccionados fuerón registrados exitosamente, la página se recargara en un momento.`;
                  window.scrollTo(0, 80);
                  setTimeout(() => {
                    this.mensajeEstRegistrados = null;
                    location.reload();
                   }, 5000);
                }
              }, (errorNota) => {
                console.log('Error al  registrar Nota', errorNota);
                if (cont === cont2) {
                  setTimeout(() => {
                    location.reload();
                   }, 5000);
                }
              });
      } else {
        console.log('NO');
      }
    }

  }

  irPaginaArriba() {
    // this.dataApiService.irPaginaArriba(this.activatedRoute);
    this.dataApiService.retrocederPagina();
  }

}

// registrarAsistencia() {

//   console.log(this.planillas);

//   this.dataApiService
//         .postModelo('asistencias', this.asistencia)
//           .subscribe((dataAsistencia: any) => {
//             console.log('dataAsistencia', dataAsistencia);

//             for (let index = 0; index < this.planillas.length; index++) {
//               const element = this.planillas[index];
//               element.asistenciaId = dataAsistencia.id;

//               this.dataApiService
//                     .postModelo('planillas', element)
//                       .subscribe((dataPlanilla: any) => {
//                         console.log('dataPlanilla', dataPlanilla);

//                         if (index + 1 === this.planillas.length) {
//                           location.reload();
//                         }
//                       }, (errorPlanilla) => {
//                         console.log('errorPlanilla', errorPlanilla);
//                       });
//             }

//           }, (errorAsistencia) => {
//             console.log('errorAsistencia', errorAsistencia);
//           });

// }
