import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgrMulEstComponent } from './agr-mul-est.component';

describe('AgrMulEstComponent', () => {
  let component: AgrMulEstComponent;
  let fixture: ComponentFixture<AgrMulEstComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgrMulEstComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgrMulEstComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
