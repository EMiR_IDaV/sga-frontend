import { Component, OnInit } from '@angular/core';
import { DataApiService } from '../../../services/data-api.service';
import { Curso } from '../../../interfaces/curso.interface';
import { PublicService } from '../../../services/public.service';
import { async } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-estudiantes',
  templateUrl: './estudiantes.component.html',
  styleUrls: ['./estudiantes.component.css']
})
export class EstudiantesComponent implements OnInit {

  mensaje = null;

  cursos: Curso[] = [];
  modulosTodos: any[] = [];

  constructor(private activatedRoute: ActivatedRoute, private dataApiService: DataApiService, private publicService: PublicService) {
    console.log('---------------estudiantes--------------');
  }

  ngOnInit() {
    this.publicService
          .getRegistrosModelo('cursos', 'id', 'desc')
            .subscribe( (datagetCursos: any) => {
              this.cursos = datagetCursos;

              for (let index = 0; index < this.cursos.length; index++) {
                const curso = this.cursos[index];

                this.publicService
                      .getModelosRelPorId('cursos', curso.id, 'modulos', 'numero')
                        .subscribe((dataModulos: any) => {
                          this.modulosTodos[index] = dataModulos;

                          for (let index1 = 0; index1 < dataModulos.length; index1++) {
                            const element = dataModulos[index1];

                            this.dataApiService
                                  .getNumeroRelacionados('modulos', element.id, 'estudiantes')
                                    .subscribe( (data: any) => {
                                      element['numeroEstudiantes'] = data.count;
                                    }, errorNumEstudiantes => this.mensaje = (errorNumEstudiantes.error.error.message));
                          }
                          this.modulosTodos[index] = dataModulos;
                        }, errorGetModulos => this.mensaje = (errorGetModulos.error.error.message));
              }
            }, (errorCursos) => {
              this.mensaje = (errorCursos.error.error.message);
            });
  }

  irPaginaArriba() {
    this.dataApiService.irPaginaArriba(this.activatedRoute);
  }

}
