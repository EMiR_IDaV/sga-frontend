import { Component, OnInit } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { Curso } from '../../../../interfaces/curso.interface';
import { Modulo } from '../../../../interfaces/modulo.interface';
import { PublicService } from '../../../../services/public.service';
import { DataApiService } from '../../../../services/data-api.service';
import { Usuario } from '../../../../interfaces/usuario.interface';
import { Planilla } from '../../../../interfaces/planilla.interface';
import { Asistencia } from '../../../../interfaces/asistencia.interface';

import { logoUmsa } from '../../../../services/imagenes';
import { logoIPICOM } from '../../../../services/imagenes';


import * as jsPDF from 'jspdf';
import { TableService } from '../../../../services/table.service';


@Component({
  selector: 'app-list-asi',
  templateUrl: './list-asi.component.html',
  styleUrls: ['./list-asi.component.css']
})
export class ListAsiComponent implements OnInit {

  indice: number;
  editarFlag = false;
  asistenciaEditar: Asistencia;

  mensaje = null;
  mensajeFecha = null;
  mensajeExito = null;
  carga = false;

  usuario: Usuario = null;
  index: number = null;

  sigla: string;
  numeroModulo: number;

  curso: Curso;
  modulo: Modulo;

  nombreCurso: string;
  ussEstudiantes: Usuario[] = [];
  asistencias: Asistencia[] = [];
  planillasAdicionar: Planilla[] = [];
  planillasAuxiliar: Planilla[] = [];

  asistenciasPorMes: any[] = [];
  asistenciasPorMesNumero: any[] = [];

  estados = ['Asistio', 'Falto', 'Permiso'];

  asistenciasPorEstudiantes: any[] = [];

  asistencia: Asistencia = {
    fecha: '',
    moduloId : 0
  };

  hoy = new Date();
  hoyFecha = this.hoy.getFullYear() + '-' + (this.hoy.getMonth() + 1) + '-' + this.hoy.getDate();

  altura = 29;
  margenSuperior = 29;
  salto = 4;
  tamtitulo = 11;
  xm = 140;
  xi = 7;
  xf = 272;
  xn = 13;
  xapellidosnombres = 41;


  constructor(private publicService: PublicService, private dataApiService: DataApiService, private activatedRoute: ActivatedRoute,
              private tableService: TableService) {
    console.log('-------------------------------- list-asi -------------------------------');
  }

  saltar() {
    return this.altura = this.altura + this.salto;
  }

  generarPDF() {

    const doc: any = new jsPDF('l', 'mm', 'letter', true);
    doc.setProperties({
      title: `Asistencia ${this.curso.sigla} - Módulo ${this.numeroModulo} ${this.modulo.nombre}.pdf`,
      subject: 'Ipicom',
      author: 'Ipicom',
      keywords: 'generated, javascript, web 2.0, ajax',
      creator: 'IpiCOM'
    });

    // TÍTULO
    doc.setFontSize(this.tamtitulo);
    doc.setFontType('bold');
    doc.text('UNIVERSIDAD MAYOR DE SAN ANDRES', this.xm, this.altura, 'center');
    doc.text('FACULTAD DE CIENCIAS SOCIALES - IPICOM', this.xm, this.saltar(), 'center');
    doc.text(`${this.curso.nombre}`, this.xm, this.saltar(), 'center');
    this.saltar();
    doc.setFontType('normal');
    doc.text('Módulo ' + this.numeroModulo + ': ' + `${this.modulo.nombre}`, 20, this.saltar());
    doc.text('Docente: ' + this.modulo.profesor, 20, this.saltar());
    // doc.text('Fechas: ' + this.modulo.calendario, 259, this.altura, 'right');

    doc.addImage(logoUmsa, 'PNG', 5, 21, 12, 22);
    doc.addImage(logoIPICOM, 'PNG', 255.5, 21, 17, 16);
    this.saltar();

    // Generar Tabla
    this.tableService.tablaAsistencia(doc, this.altura, this.ussEstudiantes, this.asistencias, this.asistenciasPorEstudiantes, [this.xi, this.xn, this.xapellidosnombres, this.xf], 8, 'normal', 3, 1, 1);

    // GUIAS
    // doc.setFontSize(1);
    // for (let index = 0; index < 280; index++) {
    //   // if (index % 2 === 0 ) {
    //     // doc.setTextColor(255,0,0);
    //     // doc.text(index.toString(), index, 0.2);
    //     // doc.setTextColor(0,255,0);
    //     // doc.text(index.toString(), index, 0.5);
    //     doc.setTextColor(0, 0, 255);
    //     doc.text(index.toString(), index, 0.3, 'center');
    //     doc.text('|', index, 0.3);
    //   // }
    // }
    // for (let index = 0; index < 217; index++) {
    //   // if (index % 2 === 0 ) {
    //     // doc.setTextColor(255,0,0);
    //     // doc.text(index.toString(),0, index);
    //     // doc.setTextColor(0,255,0);
    //     // doc.text(index.toString(),0.5, index);
    //     doc.setTextColor(0, 0, 255);
    //     doc.text(index.toString(), 140, index);
    //     doc.text('-', 0, index);
    //   // }
    // }

    // doc.output('dataurlnewwindow'); nueva ventana
    doc.save(`Asistencia. ${this.curso.sigla} - Módulo ${this.numeroModulo} ${this.modulo.nombre}.pdf`);
    this.altura = 15;

  }

  ngOnInit() {

    this.hoyFecha = this.ajustaFecha(this.hoyFecha);

    this.activatedRoute.params
          .subscribe(data => {
            this.sigla = data['sigla'];
            this.numeroModulo = data['numeroModulo'];
          });

          this.publicService
          .getCursoPorSigla(this.sigla)
            .subscribe((dataCurso) => {
              this.curso = dataCurso;
              console.log('curso', this.curso);

              this.dataApiService
                    .getModuloPorIdCursoNumeroModulo(this.curso.id, this.numeroModulo)
                      .subscribe((dataModulo) => {
                        this.modulo = dataModulo;
                        this.publicService.cortarFechasModelo(this.modulo, 'modulo');
                        console.log('modulo', this.modulo);

                        this.asistencia.fecha = this.hoyFecha;
                        this.asistencia.moduloId = this.modulo.id;

                        this.dataApiService
                              .getEstudiantesPorSiglaNumero(this.sigla, this.numeroModulo)
                                .subscribe((dataEstudiantes: any) => {
                                  this.ussEstudiantes = dataEstudiantes;
                                  console.log('estudiantes', this.ussEstudiantes);

                                  this.obtenerAsistencias();

                                  for (let index = 0; index < this.ussEstudiantes.length; index++) {
                                    const element = this.ussEstudiantes[index];
                                    const planilla: Planilla = {
                                      estudianteId: element.id,
                                      asistenciaId: 0,
                                      estado: 'Asistio'
                                    };
                                    this.planillasAdicionar[index] = planilla;
                                  }

                                }, (errorEstudiantes) => {
                                  this.mensaje = 'No hay Estudiantes registrados';
                                });

                      }, (errorModulo) => {
                        this.mensaje = errorModulo.error.error.message;
                      });
            }, (errorCurso) => {
              this.mensaje = errorCurso.error.error.message;
            });
  }

  obtenerAsistencias() {
    this.dataApiService
          .getModelosPorFiltroOrdenar('asistencias', 'moduloId', this.modulo.id, 'fecha', 'asc')
            .subscribe((dataAsistencias: any) => {
              this.asistencias = dataAsistencias;
              console.log('asistencias', this.asistencias);

              if (this.asistencias.length === 0) {
                this.carga = true;
              }

              for (let index1 = 0; index1 < this.asistencias.length; index1++) {
                const asist = this.asistencias[index1];

                const asistenciaPorEstudiante: any[] = [];

                for (let index = 0; index < this.ussEstudiantes.length; index++) {
                  const estud = this.ussEstudiantes[index];

                  this.dataApiService
                      .getRegistroPorFiltros('planillas', 'asistenciaId', asist.id, 'estudianteId', estud.id)
                        .subscribe((dataplanilla: any) => {
                                asistenciaPorEstudiante[index] = dataplanilla;

                                if (index + 1 === this.ussEstudiantes.length) {
                                  this.asistenciasPorEstudiantes[index1] = asistenciaPorEstudiante;
                                }
                                if (index1 + 1 === this.asistencias.length && index + 1 === this.ussEstudiantes.length) {
                                  this.limpiarCampos();
                                  this.carga = true;
                                  this.llenarAsistenciasPorMes(this.asistencias);
                                }
                              }, (errorNotas) => {
                                this.mensaje = errorNotas.error.error.message + 'Aqui';
                                this.completarAsistenciaEstudianteNuevo( asist.id, estud.id);
                                asistenciaPorEstudiante[index] = errorNotas.error.error.message;
                                if (index + 1 === this.ussEstudiantes.length) {
                                  this.asistenciasPorEstudiantes[index1] = asistenciaPorEstudiante;
                                }
                                if (index1 + 1 === this.asistencias.length && index + 1 === this.ussEstudiantes.length) {
                                  this.limpiarCampos();
                                  this.carga = true;
                                }
                              });
                }

              }

            }, (errorAsistencias) => {
              this.mensaje = errorAsistencias.error.error.message;
            });
  }

  completarAsistenciaEstudianteNuevo(idAsistencia: number, idEstudiante: number) {

    const planilla: Planilla = {
      asistenciaId: idAsistencia,
      estudianteId: idEstudiante,
      estado: '?'
    };
    this.dataApiService
          .postModelo('planillas', planilla)
            .subscribe((dataPlanilla: any) => {
              console.log('dataPlanilla', dataPlanilla);
              location.reload();
            }, (errorPlanilla) => {
              console.log('errorPlanillaCompletar', errorPlanilla);
            });
  }

  llenarAsistenciasPorMes(asistencias: any[]) {
    this.asistenciasPorMes = [];
    this.asistenciasPorMesNumero = [];
    let numeroMes = 0;
    for (let index = 0; index < asistencias.length; index++) {
      const element = asistencias[index];
      const elementAux = element['fecha'].split('-');

      if (this.asistenciasPorMes[numeroMes + 1] === Number(elementAux[1])) {
        this.asistenciasPorMes[numeroMes + 2]++;

      } else {
        numeroMes = numeroMes + 3;
        this.asistenciasPorMes[numeroMes] = Number(elementAux[0]);
        this.asistenciasPorMes[numeroMes + 1] = Number(elementAux[1]);
        this.asistenciasPorMes[numeroMes + 2] = 1;
        this.asistenciasPorMesNumero[numeroMes] = 1;
      }

    }
    this.asistenciasPorMes = this.asistenciasPorMes.filter(Boolean);
    this.asistenciasPorMesNumero = this.asistenciasPorMesNumero.filter(Boolean);
    for (let index = 0; index < this.asistenciasPorMes.length; index = index + 3) {
      this.asistenciasPorMes[index + 1] =  this.tableService.obtieneMes(Number(this.asistenciasPorMes[index + 1]));
    }
    console.log(this.asistenciasPorMes);
    console.log(this.asistenciasPorMesNumero);
  }



  registrarAsistencia() {
    this.mensajeFecha = null;
    this.asistencia.fecha = this.ajustaFecha(this.asistencia.fecha);

    if (!this.mensajeFecha) {
      this.carga = false;
      this.dataApiService
          .postModelo('asistencias', this.asistencia)
            .subscribe((dataAsistencia: any) => {

              for (let index = 0; index < this.planillasAdicionar.length; index++) {
                const element = this.planillasAdicionar[index];
                element.asistenciaId = dataAsistencia.id;

                this.dataApiService
                      .postModelo('planillas', element)
                        .subscribe((dataPlanilla: any) => {

                          if (index + 1 === this.planillasAdicionar.length) {

                            this.obtenerAsistencias();
                            this.mensajeExito = `Asistencia del ${dataAsistencia.fecha} registrada exitosamente.`;
                            // setTimeout(() => location.reload(), 6000);

                          }
                        }, (errorPlanilla) => {
                          console.log('errorPlanilla', errorPlanilla);
                        });
              }

            }, (errorAsistencia) => {
              console.log('errorAsistencia', errorAsistencia);
            });
    }

  }

  ajustaFecha(fecha: string) {

    const fechas = fecha.split('-');
    const fechaAux: any[] = [];
    console.log(fechaAux);
    fechaAux[0] = Number(fechas[0]);
    fechaAux[1] = Number(fechas[1]);
    fechaAux[2] = Number(fechas[2]);

    if (1999 < Number(fechaAux[0]) && Number(fechaAux[0]) < 2999) {
      if (0 < Number(fechaAux[1]) && Number(fechaAux[1]) < 13) {
        if (0 < Number(fechaAux[2]) && Number(fechaAux[2]) < 32) {

          if (Number(fechaAux[1]) < 10) {
            fechaAux[1] = '0' + fechaAux[1];
          }
          if (Number(fechaAux[2]) < 10) {
            fechaAux[2] = '0' + fechaAux[2];
          }

          this.mensajeFecha = null;
          return `${fechaAux[0]}-${fechaAux[1]}-${fechaAux[2]}`;

        } else {
          this.mensajeFecha = '"DÍA" inválido, rango 01 - 31, introduzca un formato de fecha válida "AAAA-MM-DD" ej. "2020-04-12", ';
          return fecha;
        }
      } else {
        this.mensajeFecha = '"MES" inválido, rango 01 - 12, introduzca un formato de fecha válida "AAAA-MM-DD" ej. "2020-08-13", ';
        return fecha;
      }
    } else {
      this.mensajeFecha = '"AÑO" inválido, rango 1999 - 2999, introduzca un formato de fecha válida "AAAA-MM-DD" ej. "2020-10-01", ';
      return fecha;
    }

  }

  editarAsistencia(ind: number) {

    // console.log(ind);

    this.indice = ind;
    this.asistenciaEditar = Object.assign({}, this.asistencias[ind]);
    this.planillasAuxiliar = this.asistenciasPorEstudiantes[ind].map(x => Object.assign({}, x));
    this.editarFlag = true;
  }

  registrarActualizacion() {

    this.mensajeFecha = null;
    this.asistenciaEditar.fecha = this.ajustaFecha(this.asistenciaEditar.fecha);

    if (!this.mensajeFecha) {
      this.carga = false;
      this.dataApiService
          .putModelo('asistencias', this.asistenciaEditar)
            .subscribe((dataPutAsistencia: any) => {
              console.log('dataPutAsistencia', dataPutAsistencia);

              for (let index = 0; index < this.planillasAuxiliar.length; index++) {
                const element = this.planillasAuxiliar[index];
                this.dataApiService
                      .putModelo('planillas', element)
                        .subscribe((dataPutPlanilla: any) => {
                          console.log('dataPutPlanilla', dataPutPlanilla);

                          if (index + 1 === this.planillasAuxiliar.length) {

                            this.obtenerAsistencias();
                            this.mensajeExito = `Asistencia del ${this.asistenciaEditar.fecha} actualizada exitosamente.`;
                            // setTimeout(() => location.reload(), 6000);

                          }
                        }, (errorPlanilla) => {
                          console.log('errorPlanilla', errorPlanilla);
                        });
              }

            }, (errorAsistencia) => {
              console.log('errorAsistencia', errorAsistencia);
            });
    }
  }

  limpiarCampos() {
    this.asistenciaEditar = null;
    this.planillasAuxiliar = null;
    this.editarFlag = false;
  }



  irPaginaArriba() {
    this.dataApiService.retrocederPagina();
    // this.dataApiService.irPaginaArriba(this.activatedRoute);
  }
}
