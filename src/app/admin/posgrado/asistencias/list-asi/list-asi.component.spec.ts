import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAsiComponent } from './list-asi.component';

describe('ListAsiComponent', () => {
  let component: ListAsiComponent;
  let fixture: ComponentFixture<ListAsiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListAsiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAsiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
