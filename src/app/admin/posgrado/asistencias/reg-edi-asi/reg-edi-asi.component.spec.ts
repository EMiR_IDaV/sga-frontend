import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegEdiAsiComponent } from './reg-edi-asi.component';

describe('RegEdiAsiComponent', () => {
  let component: RegEdiAsiComponent;
  let fixture: ComponentFixture<RegEdiAsiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegEdiAsiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegEdiAsiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
