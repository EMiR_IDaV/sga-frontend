import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegEdiCurComponent } from './reg-edi-cur.component';

describe('RegEdiCurComponent', () => {
  let component: RegEdiCurComponent;
  let fixture: ComponentFixture<RegEdiCurComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegEdiCurComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegEdiCurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
