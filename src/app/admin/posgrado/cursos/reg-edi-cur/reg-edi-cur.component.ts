import { Component, OnInit } from '@angular/core';

import { FormControl, FormGroup } from '@angular/forms';
import { Curso } from '../../../../interfaces/curso.interface';
import { ActivatedRoute } from '@angular/router';
import { DataApiService } from '../../../../services/data-api.service';
import { PublicService } from '../../../../services/public.service';


@Component({
  selector: 'app-reg-edi-cur',
  templateUrl: './reg-edi-cur.component.html',
  styleUrls: ['./reg-edi-cur.component.css']
})
export class RegEdiCurComponent implements OnInit {

  sigla: string;
  forma: FormGroup;
  accion = 'Registrar';
  mensajeExito: string;
  mensajeError: string;

  curso: Curso = {
    nombre: '',
    sigla: '',
    acerca: '',
    areas: '',
    perfiles: '',
    admision: '',
    gestion: '',
    nro_resolucion: '',
    costo: '',
    modalidad: '',
    num_plazas: '',
    inicio_clases: '',
    inscripcion: '',
    inicio_matriculacion: '',
    fin_matriculacion: '',
    dias_horarios: '',
    otra: ''
  };

  constructor(private publicService: PublicService, private dataApiService: DataApiService,
                private activatedRoute: ActivatedRoute) {
    console.log('------------------------------------- reg-edi-cur -----------------------------------');
    console.log(this.curso);
  }

  ngOnInit() {

    this.cargarForma();
    this.activatedRoute.params.subscribe(data => this.sigla = data['sigla']);

    if (this.sigla !== 'nuevo') {
      this.accion = 'Editar';
      this.publicService
            .getCursoPorSigla(this.sigla)
              .subscribe((dataCurso: any) => {
                console.log(dataCurso);
                this.curso = dataCurso;
                this.publicService.cortarFechasModelo(this.curso, 'curso');
                this.forma.setValue(this.curso);
              });
    }
  }

  registrarCurso() {
    this.curso = this.forma.value;
    this.curso.nombre = this.curso.nombre.trim();

    if (this.sigla === 'nuevo') {
      this.dataApiService
            .postModelo('cursos', this.curso)
              .subscribe((dataPosCurso: any) => {
                this.curso = dataPosCurso;
                console.log('Curso Creado');
                console.log(this.curso);
                this.cargarForma();
                this.mensajeError = null;
                this.mensajeExito = this.curso.nombre;
              }, (errorServicioEst) => {
                this.mensajeError = errorServicioEst.error.error.message;
                this.mensajeExito = null;
              });
    } else {
      this.dataApiService
            .putModelo('cursos', this.curso)
              .subscribe((dataPutCur: any) => {
                console.log('Curso Actualizado');
                console.log(dataPutCur);
                this.mensajeExito = this.curso.nombre;
                this.mensajeError = null;
              }, (errorServicioPutCur) => {
                this.mensajeError = errorServicioPutCur.error.error.message;
                this.mensajeExito = null;
              });
    }
    window.scrollTo(0, 0);
  }

  cargarForma() {
    // this.nacionalidad = 'Boliviano';
    this.mensajeError = null;
    this.mensajeExito = null;
    this.forma = new FormGroup ({
      'nombre': new FormControl(''),
      'sigla': new FormControl(''),
      'acerca': new FormControl(''), // , [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]
      'areas': new FormControl(''),
      'perfiles': new FormControl(''),
      'admision': new FormControl(''),
      'gestion': new FormControl(''),
      'nro_resolucion': new FormControl(''),
      'costo': new FormControl(''),
      'modalidad': new FormControl(''),
      'num_plazas': new FormControl(''),
      'inicio_clases': new FormControl(''),
      'inscripcion': new FormControl(''),
      'inicio_matriculacion': new FormControl(''),
      'fin_matriculacion': new FormControl(''),
      'dias_horarios': new FormControl(''),
      'otra': new FormControl(''),
      'id': new FormControl()
    });
  }

  irPaginaArriba() {
    this.dataApiService.irPaginaArriba(this.activatedRoute);
  }

  irArriba() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  }

}
