import { Component, OnInit } from '@angular/core';
import { DataApiService } from '../../../services/data-api.service';
import { Curso } from '../../../interfaces/curso.interface';
import { PublicService } from '../../../services/public.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cursos',
  templateUrl: './cursos.component.html',
  styleUrls: ['./cursos.component.css']
})
export class CursosComponent implements OnInit {

  mensaje = '';
  index: number;
  curso: Curso;
  cursos: Curso[] = [];

  constructor(private activatedRoute: ActivatedRoute, private publicService: PublicService, private dataApiService: DataApiService) { }

  ngOnInit() {
    this.publicService
      .getRegistrosModelo('cursos', 'id', 'desc')
            .subscribe( (data: any) => {
                                        this.cursos = data;
                                       });
  }

  eliModal(curso: Curso, index: number) {                    // Posesiona sobre el Admin a eliminar
    this.curso = curso;
    this.index = index;
  }

  eliCurso() {              // no es necesario el est e i, ya que estan en this.es y this.in PARA VER
    this.dataApiService
          .delModelo('cursos', this.curso.id)
            .subscribe(dataDelCurso => {
              console.log('Curso Eliminado');
              console.log(dataDelCurso);
              delete this.cursos[this.index];
              this.cursos = this.cursos.filter(Boolean);
            }, (errorDelCurso) => this.mensaje = errorDelCurso.error.error.message );
  }

  irPaginaArriba() {
    this.dataApiService.irPaginaArriba(this.activatedRoute);
  }

}
