import { Component, OnInit } from '@angular/core';
import { Modulo } from '../../../../interfaces/modulo.interface';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DataApiService } from '../../../../services/data-api.service';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { PublicService } from '../../../../services/public.service';
import { Usuario } from 'src/app/interfaces/usuario.interface';

@Component({
  selector: 'app-reg-edi-mod',
  templateUrl: './reg-edi-mod.component.html',
  styleUrls: ['./reg-edi-mod.component.css']
})
export class RegEdiModComponent implements OnInit {

  numeroModulo: any;
  siglaCurso: string;
  idCurso: number;

  mensajeExito: string;
  mensajeError: string;
  forma: FormGroup;
  accion = 'Registrar';

  nombreProfesor = '';
  ussDocentes: Usuario[];

  modulo: Modulo = {
    nombre: '',
    numero: 0,
    profesor: '',
    carga_horaria_a: null,
    carga_horaria_b: null,
    horas_academicas: null,
    creditos: null,
    inicio_clases: '',
    fin_clases: '',
    descripcion: '',
    docenteId: null
  };

  constructor(private dataApiService: DataApiService, private location: Location, private activatedRoute: ActivatedRoute,
              private publicService: PublicService) { }

  ngOnInit() {
    this.cargarForma();

    this.activatedRoute.params.subscribe(data => {
                                                  this.siglaCurso = data['sigla'];
                                                  this.numeroModulo = data['numeroModulo'];
                                                 });
    this.publicService
          .getCursoPorSigla(this.siglaCurso)
            .subscribe((dataCurso: any) => {
              this.idCurso = dataCurso.id;
              console.log(this.idCurso, this.numeroModulo);
              this.dataApiService
                    .getUsuariosDocentes()
                      .subscribe((dataUssDocentes: any) => {
                        this.ussDocentes = dataUssDocentes;
                        console.log(this.ussDocentes);
                        if (this.numeroModulo !== 'nuevo') {
                          this.accion = 'Actualizar';

                          this.dataApiService
                                .getModuloPorIdCursoNumeroModulo(dataCurso.id, this.numeroModulo)
                                  .subscribe((dataModulo: any) => {
                                    console.log(dataModulo);
                                    this.modulo = dataModulo;
                                    this.publicService.cortarFechasModelo(this.modulo, 'modulo');

                                    if (this.modulo.docenteId !== 0) {
                                      this.selectElegidoInt(this.modulo.docenteId);
                                      this.nombreProfesor = this.modulo.profesor;
                                    }
                                    // delete this.modulo['id'];
                                    this.forma.setValue(this.modulo);
                                  }, (errorGetModulo) => {
                                    this.mensajeError = errorGetModulo.error.error.message;
                                    this.mensajeExito = null;
                                  });
                        }
                      }, (errorGetUssDocentes) => {
                        this.mensajeError = errorGetUssDocentes.error.error.message;
                        this.mensajeExito = null;
                      });

            }, (errorGetCursoPorSigla) => {
              this.mensajeError = errorGetCursoPorSigla.error.error.message;
              this.mensajeExito = null;
            });
  }

  regModulo() {
    this.modulo = this.forma.value;
    this.modulo.nombre = this.modulo.nombre.trim();
    if (this.numeroModulo === 'nuevo') {
      this.dataApiService
            .postModeloRelacionado('cursos', this.idCurso, 'modulos', this.modulo)
              .subscribe((dataPostModulo: any) => {
                console.log('Módulo Creado', dataPostModulo);
                this.cargarForma();
                this.mensajeExito = dataPostModulo.nombre;
                this.mensajeError = null;
              }, (errorServicioMod) => {
                this.mensajeError = errorServicioMod.error.error.message;
                this.mensajeExito = null;
              });
    } else {
      console.log(this.modulo);
      this.dataApiService
            .putModeloRelacionado('cursos', this.idCurso, 'modulos', this.modulo, this.modulo.id)
              .subscribe((dataPutModulo: any) => {
                console.log('Módulo Actualizado', dataPutModulo);
                this.mensajeExito = dataPutModulo.nombre;
                this.mensajeError = null;
              }, (errorServicioPutMod) => {
                this.mensajeError = errorServicioPutMod.error.error.message;
                this.mensajeExito = null;
              });
    }
    window.scrollTo(0, 0);
  }

  selectElegidoInt(id: any) {

    for (let index = 0; index < this.ussDocentes.length; index++) {
      const element = this.ussDocentes[index];
      if (id === element.id) {
        const prof = `${element.docente.grado} ${this.dataApiService.obtieneNombreProfesor(element)}`;
        this.forma.patchValue({profesor: prof});
        this.nombreProfesor = prof;
      }
    }
  }

  selectElegido(id: any) {
    console.log('antes', id);
    id = parseInt(id, 10);
    console.log('despues', id);
    if ( id !== 0) {
      id--;
      const prof = `${this.ussDocentes[id].docente.grado} ${this.dataApiService.obtieneNombreProfesor(this.ussDocentes[id])}`;
      this.forma.patchValue({profesor: prof});
      this.nombreProfesor = prof;
    } else {
      this.forma.patchValue({profesor: ''});
      this.nombreProfesor = '';
    }
  }

  cargarForma() {
    this.mensajeError = null;
    this.mensajeExito = null;
    this.forma = new FormGroup ({
      'nombre': new FormControl(''),
      'numero': new FormControl(),
      'profesor': new FormControl('', [Validators.required]),
      'carga_horaria_a': new FormControl(), // , [Validators.required, Validators.minLength(2)]
      'carga_horaria_b': new FormControl(),
      'horas_academicas': new FormControl(), // , [Validators.required]
      'creditos': new FormControl(),
      'inicio_clases': new FormControl(''), // , [Validators.required, Validators.minLength(5)]
      'fin_clases': new FormControl(''), // , [Validators.required, Validators.minLength(5)]
      'observacion': new FormControl(''), // , [Validators.required, Validators.minLength(5)]
      'descripcion': new FormControl(''), // , [Validators.required]
      'docenteId': new FormControl(0),
      'cursoId': new FormControl(),
      'id': new FormControl()
    });
  }

  irPaginaArriba() {
    // console.log(this.forma.value);
    this.dataApiService.irPaginaArriba(this.activatedRoute);
  }
}
