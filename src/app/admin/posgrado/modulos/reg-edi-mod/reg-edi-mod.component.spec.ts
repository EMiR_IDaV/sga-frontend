import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegEdiModComponent } from './reg-edi-mod.component';

describe('RegEdiModComponent', () => {
  let component: RegEdiModComponent;
  let fixture: ComponentFixture<RegEdiModComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegEdiModComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegEdiModComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
