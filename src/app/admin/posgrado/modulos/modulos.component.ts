import { Component, OnInit } from '@angular/core';
import { PublicService } from '../../../services/public.service';
import { Curso } from '../../../interfaces/curso.interface';
import { ActivatedRoute } from '@angular/router';
import { DataApiService } from '../../../services/data-api.service';

@Component({
  selector: 'app-modulos',
  templateUrl: './modulos.component.html',
  styleUrls: ['./modulos.component.css']
})
export class ModulosComponent implements OnInit {

  cursos: Curso[];

  constructor(private activatedRoute: ActivatedRoute, private dataApiService: DataApiService, private publicService: PublicService) { }

  ngOnInit() {
    this.publicService
          .getRegistrosModelo('cursos', 'id', 'desc')
            .subscribe( (data: any) => {
                                        this.cursos = data;
                                       });
  }

  irPaginaArriba() {
    this.dataApiService.irPaginaArriba(this.activatedRoute);
  }
}
