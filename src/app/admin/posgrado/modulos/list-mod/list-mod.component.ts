import { Component, OnInit } from '@angular/core';

import { Curso } from '../../../../interfaces/curso.interface';
import { Modulo } from '../../../../interfaces/modulo.interface';
import { PublicService } from '../../../../services/public.service';
import { DataApiService } from '../../../../services/data-api.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-list-mod',
  templateUrl: './list-mod.component.html',
  styleUrls: ['./list-mod.component.css']
})
export class ListModComponent implements OnInit {

  siglaCurso = '';
  idCurso: number;
  mensaje: string;

  index: number;
  nombreCurso = '';
  modulo: Modulo;

  tcarga_horaria_a = 0;
  tcarga_horaria_b = 0;
  thoras_academicas = 0;
  tcreditos = 0;

  curso: Curso;
  modulos: Modulo[];

  constructor(private publicService: PublicService, private dataApiService: DataApiService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(data => this.siglaCurso = data['sigla']);

    this.publicService
          .getCursoPorSigla(this.siglaCurso)
            .subscribe(dataCurso => {
              this.curso = dataCurso;
              this.idCurso = this.curso.id;
              this.nombreCurso = this.curso.nombre;

              this.publicService
                    .getModelosRelPorId('cursos', this.idCurso, 'modulos', 'numero')
                      .subscribe((dataModulos: any) => {
                        this.modulos = dataModulos;
                        this.cortarFechasModulos();
                        this.totalHorasCurso(this.idCurso);
                        console.log(this.modulos);

                      }, (errorGetModulos) => this.mensaje = errorGetModulos.error.error.message);

          }, (errorGetCurso) => this.mensaje = errorGetCurso.error.error.message);
  }

  cortarFechasModulos() {
    for (let index = 0; index < this.modulos.length; index++) {
      const element = this.modulos[index];
      this.publicService.cortarFechasModelo(element, 'modulo');
    }
  }

  totalHorasCurso(id: number) {
    if (this.modulos.length !== 0) {
      this.publicService
            .getSumHrs(id)
              .subscribe((dataTotalHrs: any) => {
                this.tcarga_horaria_a = dataTotalHrs.tcarga_horaria_a;
                this.tcarga_horaria_b = dataTotalHrs.tcarga_horaria_b;
                this.thoras_academicas = dataTotalHrs.thoras_academicas;
                this.tcreditos = dataTotalHrs.tcreditos;
              }, (errorDataTotalHrs) => this.mensaje = errorDataTotalHrs.error.error.messge);
    } else {
      this.mensaje = 'No hay módulos registrados para este curso';
    }
  }

  eliModulo() {
    this.dataApiService
          .delModeloRelacionado('cursos', this.idCurso, 'modulos', this.modulo.id)
            .subscribe(dataDelModeloRel => {
              console.log('Módulo Eliminado', dataDelModeloRel);
              delete this.modulos[this.index];
              this.modulos = this.modulos.filter(Boolean);
              this.totalHorasCurso(this.idCurso);
            }, (errorDelModeloRel) => this.mensaje = errorDelModeloRel.error.error.message);
  }

  elimodal(modulo: Modulo, index: number) {                    // Posesiona sobre el Estudiante a eliminar
    this.modulo = modulo;
    this.index = index;
  }

  irPaginaArriba() {
    // this.dataApiService.irPaginaArriba(this.activatedRoute);
    this.dataApiService.retrocederPagina();
  }

  generarPDF() {}

}
