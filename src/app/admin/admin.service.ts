import { Injectable, Output, EventEmitter } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor() { }

  numero: number;

  @Output() noleidos: EventEmitter<number> = new EventEmitter();

  toggle(num: number) {
    console.log('AQUIservice', num);
    this.numero = num;
    this.noleidos.emit(this.numero);
  }
}
