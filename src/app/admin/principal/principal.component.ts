import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AutentificacionService } from '../../services/autentificacion.service';
import { DataApiService } from '../../services/data-api.service';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css']
})
export class PrincipalComponent implements OnInit {

  forma: FormGroup;
  idUsuario: number;
  mensajeError: string;
  mensaje: string;

  constructor(private autentificacionService: AutentificacionService, private dataApiService: DataApiService) {
    console.log('---------------------------- principal ----------------------------');
  }

  ngOnInit() {
    this.cargarForma();
  }

  buscarUsuario() {
    this.dataApiService
          .getRegistroByProperty('usuarios', 'ci', this.forma.value.oldpass)
            .subscribe((dataUsuario: any) => {
              this.idUsuario = dataUsuario.id;
              this.mensajeError = null;
              this.mensaje = `Usuario: ${dataUsuario.paterno} ${dataUsuario.materno} ${dataUsuario.pri_nombre} ${dataUsuario.seg_nombre}, C.I.: ${dataUsuario.ci}`;
            }, errorDataUsuario => {
              this.mensaje = null;
              this.mensajeError = errorDataUsuario;
            });
  }

  cambiarContrasena() {
    this.dataApiService
          .resetPassword(this.idUsuario, this.forma.value.newpass)
            .subscribe(cc => {
              console.log('Se cambio la contraseña correctamente', cc);
            }, error => {
              console.log('error', error);
            });
    // this.mensajeError = null;

    // if (this.forma.value.newpass !== this.forma.value.confnewpass) {
    //   this.cargarForma();
    //   this.mensajeError = 'El nuevo password no coinciden';
    // } else {
    //   this.autentificacionService
    //         .confirmUser(this.forma.value.email, this.forma.value.oldpass)
    //           .subscribe((data: any) => {
    //             console.log(data);
    //             this.autentificacionService
    //                   .confirmUserOut(data['id'])
    //                     .subscribe(dataOut => {
    //                       console.log(dataOut);
    //                       this.dataApiService
    //                             .cambiarContrasena(this.forma.value.oldpass, this.forma.value.newpass)
    //                               .subscribe(dataCambiar => {
    //                                 console.log('Cambio exitoso', dataCambiar);
    //                                 this.cargarForma();
    //                                 this.mensaje = 'Se cambio la Contraseña correctamente';
    //                               }, errorCambiar => {
    //                                 this.cargarForma();
    //                                 this.mensajeError = errorCambiar.error.error.message;
    //                               });
    //                     });
    //           }, errorData => {
    //             console.log('error login');
    //             this.cargarForma();
    //             this.mensajeError = errorData.error.error.message;
    //           } );
    // }
    // console.log('hola');
    // console.log(this.forma);
  }

  cargarForma() {
    this.mensajeError = null;
    this.mensaje = null;
    this.forma = new FormGroup ({
      'email': new FormControl(''), // , [Validators.required, Validators.minLength(2)]
      'oldpass': new FormControl(''), // , [Validators.required, Validators.minLength(2)]
      'newpass': new FormControl('') // , [Validators.required, Validators.minLength(2)]
    });
  }

}
