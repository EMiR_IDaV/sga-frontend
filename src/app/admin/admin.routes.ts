import { RouterModule, Routes } from '@angular/router';

// Guards
import { UsuarioGuard } from '../guards/usuario.guard';
import { ItemsGuard } from '../guards/items.guard';
import { AdminGuard } from '../guards/admin.guard';

import { AdminComponent } from './admin.component';
import { PrincipalComponent } from './principal/principal.component';
import { PosgradoComponent } from './posgrado/posgrado.component';
import { InvestigacionComponent } from './investigacion/investigacion.component';
import { InteraccionComponent } from './interaccion/interaccion.component';

import { InstitutoComponent } from './instituto/instituto.component';
import { CaracteristicasComponent } from './instituto/caracteristicas/caracteristicas.component';
import { PersonalComponent } from './instituto/personal/personal.component';
import { RegEdiPerComponent } from './instituto/personal/reg-edi-per/reg-edi-per.component';
import { NoticiasComponent } from './instituto/noticias/noticias.component';
import { RegEdiNotComponent } from './instituto/noticias/reg-edi-not/reg-edi-not.component';

import { EstudiantesComponent } from './posgrado/estudiantes/estudiantes.component';
import { ListEstComponent } from './posgrado/estudiantes/list-est/list-est.component';
import { RegEdiEstComponent } from './posgrado/estudiantes/reg-edi-est/reg-edi-est.component';

import { DocentesComponent } from './posgrado/docentes/docentes.component';
import { ListDocComponent } from './posgrado/docentes/list-doc/list-doc.component';
import { RegEdiDocComponent } from './posgrado/docentes/reg-edi-doc/reg-edi-doc.component';
import { RegEdiCarComponent } from './instituto/caracteristicas/reg-edi-car/reg-edi-car.component';
import { CursosComponent } from './posgrado/cursos/cursos.component';
import { RegEdiCurComponent } from './posgrado/cursos/reg-edi-cur/reg-edi-cur.component';
import { ModulosComponent } from './posgrado/modulos/modulos.component';
import { RegEdiModComponent } from './posgrado/modulos/reg-edi-mod/reg-edi-mod.component';
import { SitiosInteresComponent } from './instituto/sitios-interes/sitios-interes.component';
import { NotasComponent } from './posgrado/notas/notas.component';
import { ListNotasComponent } from './posgrado/notas/list-notas/list-notas.component';
import { RegEdiNotasComponent } from './posgrado/notas/reg-edi-notas/reg-edi-notas.component';
import { AsistenciasComponent } from './posgrado/asistencias/asistencias.component';
import { ListAsiComponent } from './posgrado/asistencias/list-asi/list-asi.component';
import { RegEdiAsiComponent } from './posgrado/asistencias/reg-edi-asi/reg-edi-asi.component';
import { AvisosComponent } from './posgrado/avisos/avisos.component';
import { ListModComponent } from './posgrado/modulos/list-mod/list-mod.component';
import { AgrMulEstComponent } from './posgrado/estudiantes/agr-mul-est/agr-mul-est.component';

import { ConsultasComponent } from './instituto/consultas/consultas.component';
import { EventosComponent } from './interaccion/eventos/eventos.component';
import { RegEdiEveComponent } from './interaccion/eventos/reg-edi-eve/reg-edi-eve.component';
import { InvestigacionesComponent } from './investigacion/investigaciones/investigaciones.component';
import { RegEdiInvComponent } from './investigacion/investigaciones/reg-edi-inv/reg-edi-inv.component';
import { DocentesInvestigadoresComponent } from './investigacion/docentes-investigadores/docentes-investigadores.component';
import { RegEdiDocinvComponent } from './investigacion/docentes-investigadores/reg-edi-docinv/reg-edi-docinv.component';
import { AuxiliaresInvestigadoresComponent } from './investigacion/auxiliares-investigadores/auxiliares-investigadores.component';
import { RegEdiAuxinvComponent } from './investigacion/auxiliares-investigadores/reg-edi-auxinv/reg-edi-auxinv.component';
import { ConvocatoriasComponent } from './investigacion/convocatorias/convocatorias.component';
// import { RegEdiConComponent } from './investigacion/convocatorias/reg-edi-con/reg-edi-con.component';
import { ListInvComponent } from './investigacion/investigaciones/list-inv/list-inv.component';
import { ListDocinvComponent } from './investigacion/docentes-investigadores/list-docinv/list-docinv.component';
import { ParticipantesComponent } from './interaccion/participantes/participantes.component';
import { RegEdiParComponent } from './interaccion/participantes/reg-edi-par/reg-edi-par.component';
import { ListParComponent } from './interaccion/participantes/list-par/list-par.component';
import { ListAuxinvComponent } from './investigacion/auxiliares-investigadores/list-auxinv/list-auxinv.component';

const ADMIN_ROUTES: Routes = [
    {
        path: 'admin',
        component: AdminComponent,
        canActivate: [UsuarioGuard, ItemsGuard, AdminGuard],
        children: [
            { path: '', component: PrincipalComponent },
            { path: 'instituto', component: InstitutoComponent },
            { path: 'instituto/caracteristicas', component: CaracteristicasComponent },
            { path: 'instituto/caracteristicas/:caracteristica', component: RegEdiCarComponent },
            { path: 'instituto/equipo-de-trabajo', component: PersonalComponent},
            { path: 'instituto/equipo-de-trabajo/:username', component: RegEdiPerComponent},
            { path: 'instituto/noticias', component: NoticiasComponent },
            { path: 'instituto/noticias/:noticia', component: RegEdiNotComponent },
            { path: 'instituto/sitios-interes', component: SitiosInteresComponent },
            { path: 'instituto/consultas', component: ConsultasComponent },
            { path: 'posgrado', component: PosgradoComponent },

            { path: 'posgrado/modulos', component: ModulosComponent },
            { path: 'posgrado/docentes', component: DocentesComponent },
            { path: 'posgrado/estudiantes', component: EstudiantesComponent },
            { path: 'posgrado/notas', component: NotasComponent },
            { path: 'posgrado/asistencia', component: AsistenciasComponent },

            { path: 'posgrado/cursos', component: CursosComponent },
            { path: 'posgrado/cursos/:sigla', component: RegEdiCurComponent },
            { path: 'posgrado/cursos/:sigla/modulos', component: ListModComponent },
            { path: 'posgrado/cursos/:sigla/modulos/:numeroModulo', component: RegEdiModComponent },
            { path: 'posgrado/cursos/:sigla/modulos/:numeroModulo/docentes/:username', component: RegEdiDocComponent },
            { path: 'posgrado/cursos/:sigla/modulos/:numeroModulo/estudiantes', component: ListEstComponent },
            { path: 'posgrado/cursos/:sigla/modulos/:numeroModulo/estudiantes/Agregar-Estudiantes', component: AgrMulEstComponent },
            { path: 'posgrado/cursos/:sigla/modulos/:numeroModulo/estudiantes/:username', component: RegEdiEstComponent },
            { path: 'posgrado/cursos/:sigla/modulos/:numeroModulo/notas', component: ListNotasComponent },

            { path: 'posgrado/cursos/:sigla/modulos/:numeroModulo/asistencia', component: ListAsiComponent },
            { path: 'posgrado/cursos/:sigla/docentes', component: ListDocComponent },

            { path: 'posgrado/cursos/:sigla/modulos/:idModulo', component: RegEdiModComponent },

            { path: 'posgrado/notas/:sigla/:username', component: RegEdiNotasComponent },
            { path: 'posgrado/asistencia/:sigla/:username', component: RegEdiAsiComponent },
            { path: 'posgrado/avisos', component: AvisosComponent },

            { path: 'investigacion', component: InvestigacionComponent },
            { path: 'investigacion/docentes-investigadores', component: DocentesInvestigadoresComponent },
            { path: 'investigacion/investigaciones', component: InvestigacionesComponent },
            { path: 'investigacion/auxiliares-investigadores', component: AuxiliaresInvestigadoresComponent },

            { path: 'investigacion/convocatorias', component: ConvocatoriasComponent },
            // { path: 'investigacion/convocatorias/:gestion', component: RegEdiConComponent }, //// revisar parece que ya no se usa
            { path: 'investigacion/convocatorias/:gestion/docentes-investigadores', component: ListDocinvComponent },
            { path: 'investigacion/convocatorias/:gestion/docentes-investigadores/:username', component: RegEdiDocinvComponent },
            { path: 'investigacion/convocatorias/:gestion/docentes-investigadores/:username/proyectos-investigacion/:titulo', component: RegEdiInvComponent },
            { path: 'investigacion/convocatorias/:gestion/proyectos-investigacion', component: ListInvComponent },
            { path: 'investigacion/convocatorias/:gestion/proyectos-investigacion/:titulo', component: RegEdiInvComponent },
            { path: 'investigacion/convocatorias/:gestion/auxiliares-investigadores', component: ListAuxinvComponent },
            { path: 'investigacion/convocatorias/:gestion/auxiliares-investigadores/:username', component: RegEdiAuxinvComponent },
            // { path: 'investigacion/convocatorias/:gestion/docentes-investigadores/:username/proyectos-investigacion', component: ListInvComponent },
            // { path: 'investigacion/convocatorias/:gestion/proyectos-investigacion', component: InvestigacionesComponent },
            // { path: 'investigacion/investigaciones', component: InvestigacionesComponent },
            // { path: 'investigacion/docentes-investigadores/:username', component: RegEdiDocinvComponent },
            { path: 'investigacion/auxiliares-investigacion/:auxiliarInvestigador', component: RegEdiAuxinvComponent },
            // { path: 'investigacion/investigaciones/:proyecto', component: RegEdiInvComponent },
            { path: 'interaccion', component: InteraccionComponent },
            { path: 'interaccion/eventos', component: EventosComponent },
            { path: 'interaccion/participantes', component: ParticipantesComponent },
            { path: 'interaccion/eventos/:idEvento', component: RegEdiEveComponent },
            { path: 'interaccion/eventos/:idEvento/participantes', component: ListParComponent },
            { path: 'interaccion/eventos/:idEvento/participantes/:participante', component: RegEdiParComponent }
        ]
    }
];

export const ADMIN_ROUTING = RouterModule.forChild(ADMIN_ROUTES);
