import { Component, OnInit } from '@angular/core';
import { AutentificacionService } from '../services/autentificacion.service';
import { Router } from '@angular/router';
import { DataApiService } from '../services/data-api.service';
import { AdminService } from './admin.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  // @HostBinding('class.consulta-noleidas') preguntar joss da igual con esto o sin esto
  consultasNoleidas = 0;
  idUsuario: number;
  nombre: string;

  constructor(private authService: AutentificacionService, private router: Router, private dataApiService: DataApiService,
              private adminService: AdminService) { }

  ngOnInit() {

    this.idUsuario = Number(this.authService.getId());
    this.dataApiService
          .getPriNombreUsuario(this.idUsuario)
            .subscribe((dataNombre: any) => {
              this.nombre = dataNombre['pri_nombre'];
            }, errorNombre => {
              console.log('errorNombre', errorNombre);
            });

    this.adminService.noleidos.subscribe(consultasNoleidas => {
      console.log('AQUIADMIN', consultasNoleidas);
      this.consultasNoleidas = consultasNoleidas;
    });

    this.dataApiService
          .getCountRegistrosFiltro('consultas', 'leida', false)
            .subscribe((dataNumeroNoLeidos: any) => {
              this.consultasNoleidas = dataNumeroNoLeidos['count'];
            }, errorNumeroNoLeidos => {
              console.log('errorNumeroNoLeidos', errorNumeroNoLeidos);
            });
  }

  logout() {
    this.authService.logoutUser().subscribe();
    this.router.navigate(['']);
  }

}
