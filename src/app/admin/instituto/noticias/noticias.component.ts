import { Component, OnInit } from '@angular/core';
import { DataApiService } from '../../../services/data-api.service';
import { Noticia } from '../../../interfaces/noticia.interface';
import { SubirArchivoService } from '../../../services/subir-archivo.service';
import { PublicService } from '../../../services/public.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-noticias',
  templateUrl: './noticias.component.html',
  styleUrls: ['./noticias.component.css']
})
export class NoticiasComponent implements OnInit {

  mensaje: string;

  noticia: Noticia = null;
  index: number = null;

  noticias: Noticia[];
  parrafos: string[];

  mensajeError = '';

  // path: 'admin/instituto/noticias'
  constructor(private activatedRoute: ActivatedRoute, private publicService: PublicService, private dataApiServ: DataApiService, private subirArchServ: SubirArchivoService) { }

  ngOnInit() {
    this.publicService.getRegistrosTipoNoticia('noticias')
          .subscribe((data: any) => {
            this.noticias = data;
            if (this.noticias.length === 0) {
              this.mensaje = 'No hay Noticias registradas';
            }
          });
  }


  elimodal(noticia: Noticia, index: number) {                    // Posesiona sobre el Noticia a eliminar
    this.mensajeError = '';
    this.noticia = noticia;
    this.index = index;
  }

  eliNoticia(noticia: Noticia, index: number) {              // no es necesario el est e i, ya que estan en this.es y this.in PARA VER
    this.dataApiServ
          .delModelo('noticias', noticia.id)
            .subscribe(mensaje => {
              this.mensajeError = '';
                                  console.log('Noticia Eliminada');
                                  console.log(mensaje);
                                  const imagenes = noticia.imgpath.split(',');
                                  for (let ind = 0; ind < imagenes.length; ind++) {    // Elimina todas la imgs que se encuentren en el path
                                    const element = imagenes[ind];
                                    this.subirArchServ
                                          .delImage('notInstituto', element)
                                            .subscribe(data => console.log('Imagen Eliminada'));
                                  }
                                  console.log(this.noticias.length);
                                  delete this.noticias[index];
                                  this.noticias = this.noticias.filter(Boolean);
                                  console.log(this.noticias.length);
                                  }, (errorServicioUsu) => {
                                                            this.mensajeError = errorServicioUsu.error.error.message;
                                                            console.log(errorServicioUsu.error.error.message);
                                                           });
  }

  irPaginaArriba() {
    this.dataApiServ.irPaginaArriba(this.activatedRoute);
  }

}
