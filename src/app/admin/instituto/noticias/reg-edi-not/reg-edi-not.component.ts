import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Noticia } from '../../../../interfaces/noticia.interface';
import { SubirArchivoService } from '../../../../services/subir-archivo.service';
import { DataApiService } from '../../../../services/data-api.service';


@Component({
  selector: 'app-reg-edi-not',
  templateUrl: './reg-edi-not.component.html',
  styleUrls: ['./reg-edi-not.component.css']
})
export class RegEdiNotComponent implements OnInit {

  forma: FormGroup;
  titulo: string;
  accion = 'Registrar';
  cambiarFoto = 'Seleccione';
  error = false;
  mensajeError = '';
  exito = false;
  mensajeExito = '';
  idNoticia = null;

  imagenes: File[];
  nombreImagen = '';
  resumen = '';

  noticia: Noticia = {
    titulo: '',
    resumen: '',
    imgpath: '',
    cuerpo: '',
    fecha: ''
  };


  // path: 'instituto/noticias/:noticia'
  constructor(private location: Location, private actRoute: ActivatedRoute,
              private dataApiServ: DataApiService, private subArcService: SubirArchivoService) {
    this.cargarForma();
    this.actRoute.params.subscribe((data: any) => this.titulo = data['noticia']);
  }

  ngOnInit() {
    if (this.titulo !== 'nueva') {
      this.accion = 'Editar';
      this.cambiarFoto = 'Seleccione para cambiar las';
      this.titulo = this.dataApiServ.eliminaGuionesParametro(this.titulo);
      this.dataApiServ
            .getRegistroByProperty('noticias', 'titulo', this.titulo)
              .subscribe((data: any) => {
                                  console.log(data);
                                  this.noticia = data;
                                  this.idNoticia = data.id;
                                  this.noticia.fecha = (<string>data.fecha).substring(0, 10);
                                  // this.nombreImagen = this.noticia.imgpath;
                                  // delete this.noticia['id'];
                                  delete this.noticia['adminId'];
                                  this.forma.setValue(this.noticia);
                                });
    }
  }

  regNoticia() {
    let imgsAnt: string;
    this.noticia = this.forma.value;
    this.noticia.titulo = this.noticia.titulo.trim();
    if (this.titulo === 'nueva') {
      this.noticia.imgpath =  this.nombreImagen;
      this.dataApiServ
            .postModelo('noticias', this.noticia)
              .subscribe((data: any) => {
                console.log('Noticia Creada');
                                          console.log(this.noticia);
                                          this.mensajeExito = data.titulo;
                                          this.cargarForma();
                                          this.exito = true;
                                          this.error = false;
                                          this.subirImagenes(this.imagenes);
                                        }, (errorRegNoticia) => {
                                                                  this.mensajeError = errorRegNoticia.error.error.message;
                                                                  this.exito = false;
                                                                  this.error = true;
                                                                 });
    } else {
      if (this.nombreImagen !== '') {  // si solo cambia el texto monbreImagen esta vacio => no cambia la imagen
        imgsAnt = this.noticia.imgpath;
        this.noticia.imgpath = this.nombreImagen;
      }
      this.dataApiServ
            .putModelo('noticias', this.noticia)
              .subscribe((data: any) => {
                                            console.log('Noticia Actualizada');
                                            console.log(data);
                                            if (this.nombreImagen !== '') {  // si solo cambia el texto monbreImagen esta vacio => no cambia la imagen
                                              const nombresImagenes = imgsAnt.split(',');
                                              for (let index = 0; index < nombresImagenes.length; index++) {
                                                const element = nombresImagenes[index];
                                                this.subArcService.delImage('instNoticias', element).subscribe( dataE => console.log('Imagen Eliminada'));
                                              }
                                              this.subirImagenes(this.imagenes);
                                            }
                                            this.mensajeExito = this.noticia.titulo;
                                            this.exito = true;
                                            this.error = false;
                                            }, (errorServicioUsu) => {
                                                                      this.mensajeError = errorServicioUsu.error.error.message;
                                                                      this.exito = false;
                                                                      this.error = true;
                                                                     });
    }
    window.scrollTo(0, 0);
  }

  subirImagenes(imagenes: File[]) {
    const nomImagenes = this.nombreImagen.split(',');
    for (let index = 0; index < imagenes.length; index++) {
      const element = imagenes[index];
      this.subArcService
          .subirArchivo(element, nomImagenes[index], 'instNoticias', 'b')
            .then()
            .catch(datac => console.log(datac));
    }
  }

  selimg( archivo: File[] ) {
    this.nombreImagen = '';
    if ( !archivo ) {
      this.imagenes = null;
      return;
    }
    this.imagenes = archivo;

    console.log(archivo);
    for (let index = 0; index < archivo.length; index++) {
      const element = archivo[index];
      const nombreCortado = element.name.split('.');
      const extensionArchivo = nombreCortado[ nombreCortado.length - 1 ];
      let name = Date.now().toString();
      name = `${index}-${name}`;
      name = name + '.' + extensionArchivo;
      this.nombreImagen = this.nombreImagen + name;

      if (index !== archivo.length - 1) {
        this.nombreImagen = this.nombreImagen + ',';
      }

    }
  }

  back() {
    this.location.back();
  }

  cargarForma() {
    this.error = false;
    this.exito = false;
    this.forma = new FormGroup ({
      'titulo': new FormControl('', [Validators.required, Validators.minLength(10)]), // , [Validators.required, Validators.minLength(2)]
      'resumen': new FormControl('', [Validators.required, Validators.minLength(15)]), // , [Validators.required, Validators.minLength(5)]
      'imgpath': new FormControl('', []), // , [Validators.required]
      'fecha': new FormControl('', [Validators.required]),
      'cuerpo': new FormControl('', [Validators.required, Validators.minLength(50)]), // , [Validators.required]
      'id': new FormControl()
    });
  }

}
