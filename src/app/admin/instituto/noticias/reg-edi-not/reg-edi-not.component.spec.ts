import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegEdiNotComponent } from './reg-edi-not.component';

describe('RegEdiNotComponent', () => {
  let component: RegEdiNotComponent;
  let fixture: ComponentFixture<RegEdiNotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegEdiNotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegEdiNotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
