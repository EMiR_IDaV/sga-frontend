import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DataApiService } from '../../../../services/data-api.service';
import { SubirArchivoService } from '../../../../services/subir-archivo.service';
import { Oficina } from '../../../../interfaces/oficina.interface';

declare var $: any;


@Component({
  selector: 'app-oficinas',
  templateUrl: './oficinas.component.html',
  styleUrls: ['./oficinas.component.css']
})
export class OficinasComponent implements OnInit, OnDestroy {

  mensajeExito = '';
  mensaje = '';
  formaOficina: FormGroup;

  editar = false;
  index: number;
  oficina: Oficina = {
    direccion: ''
  };
  oficinas: Oficina[] = [];

  constructor(private dataApiService: DataApiService, private subirArchivoService: SubirArchivoService) { }

  ngOnInit() {
    this.cargarDatos();
  }

  cargarDatos() {
    this.cargarForma();
    this.dataApiService
          .getRegistros('oficinas')
            .subscribe((dataRedesS: any) => {
              this.oficinas = dataRedesS;
              (this.oficinas.length === 0) ? this.mensaje = 'No hay Oficinas registradas.' : this.mensaje = null;
            }, (errorRedesS) => {
              this.mensaje = errorRedesS.error.error.message;
            });
  }

  regOficina() {
    this.oficina = this.formaOficina.value;

    this.dataApiService
          .putModelo('oficinas', this.oficina)
            .subscribe((dataOficina: any) => {
              console.log('Oficina Creada/Actualizada', dataOficina);
              this.cargarDatos();

              setTimeout(() => this.mensajeExito = null, 5000);
            }, (errorRedesSnt) => {
              this.mensaje = errorRedesSnt.error.error.message;
            });

  }

  editarOficina(oficina: Oficina) {
    this.oficina = oficina;
    this.editar = true;
    this.formaOficina.setValue(this.oficina);
  }

  eliOficina() {
    this.dataApiService
          .delModelo('oficinas', this.oficina.id)
            .subscribe(dataEliSitInt => {
              console.log('Oficina Eliminada', dataEliSitInt);
              delete this.oficinas[this.index];
              this.oficinas = this.oficinas.filter(Boolean);
              this.cargarForma();
            }, (errorEliminarRedesSnt) => {
              this.mensaje = errorEliminarRedesSnt.error.error.message;
            });
  }

  eliModal(oficina: Oficina, i: number) {
    this.index =  i;
    this.oficina =  oficina;
  }

  cargarForma() {
    this.editar = false;
    this.mensaje = null;
    this.formaOficina = new FormGroup ({
      'direccion': new FormControl('', [Validators.required, Validators.minLength(15)]), // , [Validators.required, Validators.minLength(2)]
      'id': new FormControl()
    });
  }

  irPaginaArriba() {
    // this.dataApiService.irPaginaArriba(this.activatedRoute);
  }

  ngOnDestroy() {
    // $('#ModalEstudiante').modal('hide');
    // $('#ModalEstudiante').modal('dispose');
    $('.modal-backdrop').hide();
  }

}
