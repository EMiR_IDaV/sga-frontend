import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PublicService } from '../../../../services/public.service';
import { DataApiService } from '../../../../services/data-api.service';
import { SubirArchivoService } from '../../../../services/subir-archivo.service';
import { Recurso } from '../../../../interfaces/recurso.interface';

declare var $: any;

@Component({
  selector: 'app-recursos',
  templateUrl: './recursos.component.html',
  styleUrls: ['./recursos.component.css']
})
export class RecursosComponent implements OnInit, OnDestroy {

  mensajeExito: string;
  mensaje: string;
  formaRecurso: FormGroup;

  imagen: File;
  nombreDocumento: string;

  editar = false;
  index: number;
  recurso: Recurso = {
    nombre: '',
    docpath: ''
  };
  recursos: Recurso[] = [];

  constructor(private publicService: PublicService, private dataApiService: DataApiService, private subirArchivoService: SubirArchivoService) { }

  ngOnInit() {
    this.cargarDatos();
  }

  cargarDatos() {
    this.cargarForma();
    this.dataApiService
          .getRegistros('recursos')
            .subscribe((dataRecursos: any) => {
              this.recursos = dataRecursos;
              (this.recursos.length === 0) ? this.mensaje = 'No hay Recursos registrados.' : this.mensaje = null;
            }, (errorRecursos) => {
              this.mensaje = errorRecursos.error.error.message;
            });
  }

  regRecurso() {
    let imgAnt: string;

    // if (this.recurso.docpath !== '') {
    //   imgAnt = this.recurso.docpath;
    // }
    // console.log(this.recurso);
    this.recurso = this.formaRecurso.value;

    if (this.nombreDocumento) {
      if (this.editar) {
        imgAnt = this.recurso.docpath;
      }
      this.recurso.docpath = this.nombreDocumento;
    }

    this.dataApiService
          .putModelo('recursos', this.recurso)
            .subscribe((dataRecurso: any) => {
              console.log('Recurso Creado/Actualizado', dataRecurso);
              if (!this.editar) {
                this.subirImagen();
                this.cargarDatos();
                this.mensajeExito = dataRecurso.nombre;
              } else {
                if (this.nombreDocumento) {
                  this.subirArchivoService.delImage('instFooter', imgAnt).subscribe( dataE => console.log('Recurso Eliminada'));
                  this.subirImagen();
                  this.cargarDatos();
                  this.mensajeExito = dataRecurso.nombre;
                }
              }

              setTimeout(() => this.mensajeExito = null, 5000);
            }, (errorRecursosnt) => {
              this.mensaje = errorRecursosnt.error.error.message;
            });

  }

  quitarSeleccion() {
    this.nombreDocumento = null;
    const inputFile = <HTMLInputElement>document.getElementById('exampleFormControlFileRecurso');
    inputFile.value = null;
  }

  editarRecurso(recurso: Recurso) {
    this.recurso = recurso;
    this.quitarSeleccion();
    this.editar = true;
    this.formaRecurso.setValue(this.recurso);
  }

  eliRecurso() {
    this.dataApiService
          .delModelo('recursos', this.recurso.id)
            .subscribe(dataEliSitInt => {
              console.log('Recurso Eliminada', dataEliSitInt);
              this.subirArchivoService.delImage('instFooter', this.recurso.docpath).subscribe( dataE => console.log('Imagen Eliminada'));
              delete this.recursos[this.index];
              this.recursos = this.recursos.filter(Boolean);
              this.cargarForma();
            }, (errorEliminarRecursosnt) => {
              this.mensaje = errorEliminarRecursosnt.error.error.message;
            });
  }

  eliModal(recurso: Recurso, i: number) {
    this.index =  i;
    this.recurso =  recurso;
  }


  subirImagen() {
    this.subirArchivoService
        .subirArchivo(this.imagen, this.nombreDocumento, 'instFooter', 'b')
          .then()
          .catch(datac => console.log(datac));
  }

  selimg( archivo: File ) {
    this.nombreDocumento = null;
    if ( !archivo ) {
      this.imagen = null;
      console.log('vacio');
      return;
    }
    this.imagen = archivo;
    const nombreCortado = this.imagen.name.split('.');
    const extensionArchivo = nombreCortado.pop();
    // const name = nombreCortado.shift();
    const name = Date.now().toString();
    this.nombreDocumento = name + '.' + extensionArchivo;
    console.log('nombre Imagen', this.nombreDocumento);
  }

  cargarForma() {
    this.quitarSeleccion();
    this.editar = false;
    this.mensaje = null;
    this.formaRecurso = new FormGroup ({
      'nombre': new FormControl('', [Validators.required, Validators.minLength(3)]), // , [Validators.required, Validators.minLength(2)]
      'docpath': new FormControl(''),
      'id': new FormControl()
    });
  }

  ngOnDestroy() {
    // $('#ModalEstudiante').modal('hide');
    // $('#ModalEstudiante').modal('dispose');
    $('.modal-backdrop').hide();
  }
}
