import { Component, OnInit } from '@angular/core';
import { DataApiService } from '../../../../services/data-api.service';
import { Unidad } from '../../../../interfaces/unidad.interface';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-unidades',
  templateUrl: './unidades.component.html',
  styleUrls: ['./unidades.component.css']
})
export class UnidadesComponent implements OnInit {

  mensaje: string;
  mensajeExito: string;
  formaUnidades: FormGroup;

  uInvestigacion: Unidad;
  uPosgrado: Unidad;
  uInteracción: Unidad;

  unidades = {
    nombreInv: '',
    descripcionInv: '',
    nombrePos: '',
    descripcionPos: '',
    nombreInt: '',
    descripcionInt: '',
  };



  constructor(private dataApiService: DataApiService) { }

  ngOnInit() {

    this.cargarForma();
    this.dataApiService
          .getRegistrosDistinto('unidades', 'nombre', 'video')
            .subscribe((dataUnidades: any) => {
              // this.unidades = dataUnidades;
              console.log(dataUnidades);
              this.uInvestigacion = dataUnidades[0];
              this.uPosgrado = dataUnidades[1];
              this.uInteracción = dataUnidades[2];
              this.llenar();
              this.formaUnidades.setValue(this.unidades);
            }, errorUnidades => {
              this.mensaje = errorUnidades.error.error.message;
              console.log('errorUnidades', errorUnidades);
            });
  }

  actUnidades() {
    this.dataApiService.trimObjeto(this.formaUnidades.value);

    this.uInvestigacion.nombre = this.formaUnidades.value.nombreInv;
    this.uInvestigacion.descripcion = this.formaUnidades.value.descripcionInv;
    this.uPosgrado.nombre = this.formaUnidades.value.nombrePos;
    this.uPosgrado.descripcion = this.formaUnidades.value.descripcionPos;
    this.uInteracción.nombre = this.formaUnidades.value.nombreInt;
    this.uInteracción.descripcion = this.formaUnidades.value.descripcionInt;


    this.dataApiService
          .putModelo('unidades', this.uInvestigacion)
            .subscribe((dataUInv: any) => {
              this.uInvestigacion = dataUInv;
              console.log(this.uInvestigacion);

              this.dataApiService
                    .putModelo('unidades', this.uPosgrado)
                      .subscribe((dataUPos: any) => {
                        this.uPosgrado = dataUPos;
                        console.log(this.uPosgrado);

                        this.dataApiService
                              .putModelo('unidades', this.uInteracción)
                                .subscribe((dataUInt: any) => {
                                  this.uInteracción = dataUInt;
                                  console.log(this.uInteracción);
                                  this.mensajeExito = 'Se actualizo correctamente las unidades.!!!';

                                  setTimeout(() => {
                                    this.mensajeExito = null;
                                  }, 6000);

                                }, errroUInt => {
                                  this.mensaje = errroUInt.error.error.message;
                                  console.log('errroUInt', errroUInt);
                                });

                      }, errroUPos => {
                        this.mensaje = errroUPos.error.error.message;
                        console.log('errroUPos', errroUPos);
                      });
            }, errroUInv => {
              this.mensaje = errroUInv.error.error.message;
              console.log('errroUInv', errroUInv);
            });

  }

  llenar() {
    this.unidades.nombreInv = this.uInvestigacion.nombre;
    this.unidades.descripcionInv = this.uInvestigacion.descripcion;
    this.unidades.nombrePos = this.uPosgrado.nombre;
    this.unidades.descripcionPos = this.uPosgrado.descripcion;
    this.unidades.nombreInt = this.uInteracción.nombre;
    this.unidades.descripcionInt = this.uInteracción.descripcion;
  }

  cargarForma() {
    this.formaUnidades = new FormGroup ({
      'nombreInv': new FormControl('', [Validators.required, Validators.minLength(3)]), // , [Validators.required, Validators.minLength(2)]
      'descripcionInv': new FormControl('', [Validators.required, Validators.minLength(15)]),
      'nombrePos': new FormControl('', [Validators.required, Validators.minLength(3)]), // , [Validators.required, Validators.minLength(2)]
      'descripcionPos': new FormControl('', [Validators.required, Validators.minLength(15)]),
      'nombreInt': new FormControl('', [Validators.required, Validators.minLength(3)]), // , [Validators.required, Validators.minLength(2)]
      'descripcionInt': new FormControl('', [Validators.required, Validators.minLength(15)])
    });
  }

}
