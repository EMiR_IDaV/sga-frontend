import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DataApiService } from '../../../../services/data-api.service';
import { Caracteristica } from '../../../../interfaces/caracteristica.interface.';
import { SubirArchivoService } from '../../../../services/subir-archivo.service';

@Component({
  selector: 'app-nosotros',
  templateUrl: './nosotros.component.html',
  styleUrls: ['./nosotros.component.css']
})
export class NosotrosComponent implements OnInit {

  formaNosotros: FormGroup;
  mensaje: string;
  mensajeExito: string;

  nombre: string;

  imagen: File;
  nombreImagen = '';

  resumen = '';

  caracteristica: Caracteristica = {
    nombre: '',
    resumen: '',
    imgpath: '',
    descripcion: ''
  };


  // path: 'instituto/nosotros/:caracteristica'
  constructor(private dataApiService: DataApiService, private subArcService: SubirArchivoService) {
  }

  ngOnInit() {
    this.dataApiService
          .getRegistroByProperty('caracteristicas', 'nombre', 'Nosotros')
            .subscribe((dataNosotros: any) => {
              this.caracteristica = dataNosotros;
              console.log(this.caracteristica);
              delete this.caracteristica['fecha'];
              this.formaNosotros.setValue(this.caracteristica);
            });
    this.cargarForma();
  }

  actNosotros() {
    this.dataApiService.trimObjeto(this.formaNosotros.value);

    let imgAnt: string;
    this.caracteristica = this.formaNosotros.value;
    const fec = new Date().toISOString().slice(0, 10);
    this.caracteristica.fecha = fec;

    if (this.nombreImagen !== '') {  // si solo cambia el texto monbreImagen esta vacio => no cambia la imagen
      imgAnt = this.caracteristica.imgpath;
      this.caracteristica.imgpath = this.nombreImagen;
    }
    console.log(this.caracteristica);
    this.dataApiService
          .putModelo('caracteristicas', this.caracteristica)
            .subscribe((dataPutCaracteristica: any) => {
              console.log('Caracteristica Actualizada', dataPutCaracteristica);
              if (this.nombreImagen !== '') {  // si solo cambia el texto monbreImagen esta vacio => no cambia la imagen
                this.subArcService.delImage('instCaracteristicas', imgAnt).subscribe( dataE => console.log('Imagen Eliminada'));
                this.subirImagen(this.imagen);
              }

              this.mensajeExito = this.caracteristica.nombre;
              setTimeout(() => {
                this.mensajeExito = null;
              }, 6000);

            }, (errorServicioUsu) => {
              this.mensaje = errorServicioUsu.error.error.message;
              this.mensajeExito = null;
            });
  }

  subirImagen(imagen: File) {
    this.subArcService
        .subirArchivo(imagen, this.nombreImagen, 'instCaracteristicas', 'b')
          .then()
          .catch(datac => console.log(datac));
  }

  selimg( archivo: File ) {
    this.nombreImagen = '';
    if ( !archivo ) {
      this.imagen = null;
      console.log('vacio');
      return;
    }
    this.imagen = archivo;
    const nombreCortado = this.imagen.name.split('.');
    const extensionArchivo = nombreCortado.pop();
    const name = Date.now().toString();
    this.nombreImagen = name + '.' + extensionArchivo;
    console.log('nombre Imagen', this.nombreImagen);
  }

  cargarForma() {
    this.formaNosotros = new FormGroup ({
      'nombre': new FormControl('', [Validators.required, Validators.minLength(4)]), // , [Validators.required, Validators.minLength(2)]
      'resumen': new FormControl('', [Validators.required, Validators.minLength(15)]), // , [Validators.required, Validators.minLength(5)]
      'imgpath': new FormControl('', []), // , [Validators.required]
      'descripcion': new FormControl('', [Validators.required, Validators.minLength(50)]), // , [Validators.required] MINIMO 150 Se debe saber cual es el que hace form INVALIDA!!!
      'id': new FormControl()
    });
  }

}
