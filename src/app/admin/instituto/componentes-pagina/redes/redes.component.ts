import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataApiService } from '../../../../services/data-api.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { RedSocial } from '../../../../interfaces/red_social.interface';
import { ActivatedRoute } from '@angular/router';
import { PublicService } from '../../../../services/public.service';
import { SubirArchivoService } from '../../../../services/subir-archivo.service';

declare var $: any;

@Component({
  selector: 'app-redes',
  templateUrl: './redes.component.html',
  styleUrls: ['./redes.component.css']
})
export class RedesComponent implements OnInit, OnDestroy {

  mensajeExito = '';
  mensaje = '';
  formaRedSocial: FormGroup;

  imagen: File;
  nombreImagen: string;

  editar = false;
  index: number;
  red: RedSocial = {
    nombre: '',
    direccion_url: '',
    imgpath: ''
  };
  redes: RedSocial[] = [];

  constructor(private publicService: PublicService, private dataApiService: DataApiService, private subirArchivoService: SubirArchivoService) { }

  ngOnInit() {
    this.cargarDatos();
  }

  cargarDatos() {
    this.cargarForma();
    this.dataApiService
          .getRegistros('redes_sociales')
            .subscribe((dataRedesS: any) => {
              this.redes = dataRedesS;
              (this.redes.length === 0) ? this.mensaje = 'No hay Redes Sociales registradas.' : this.mensaje = null;
            }, (errorRedesS) => {
              this.mensaje = errorRedesS.error.error.message;
            });
  }

  regRedSocial() {
    let imgAnt: string;

    // if (this.red.imgpath !== '') {
    //   imgAnt = this.red.imgpath;
    // }
    // console.log(this.red);
    this.red = this.formaRedSocial.value;

    if (this.nombreImagen) {
      if (this.editar) {
        imgAnt = this.red.imgpath;
      }
      this.red.imgpath = this.nombreImagen;
    }

    this.dataApiService
          .putModelo('redes_sociales', this.red)
            .subscribe((dataRedSocial: any) => {
              console.log('Red Social Creada/Actualizada', dataRedSocial);
              if (!this.editar) {
                this.subirImagen();
                this.cargarDatos();
                this.mensajeExito = dataRedSocial.nombre;
              } else {
                if (this.nombreImagen) {
                  this.subirArchivoService.delImage('instFooter', imgAnt).subscribe( dataE => console.log('Imagen Eliminada'));
                  this.subirImagen();
                  this.cargarDatos();
                  this.mensajeExito = dataRedSocial.nombre;
                }
              }

              // if (imgAnt) {
              //   if (this.nombreImagen) {
              //   }
              // }

              // if (this.nombreImagen) {  // si solo cambia el texto monbreImagen esta vacio => no cambia la imagen
              //   this.subirArchivoService.delImage('instFooter', imgAnt).subscribe( dataE => console.log('Imagen Eliminada'));
              // }
              // if (this.nombreImagen) {  // si solo cambia el texto monbreImagen esta vacio => no cambia la imagen
              //   imgAnt = this.red.imgpath;
              //   this.red.imgpath = this.nombreImagen;
              //   this.red.imgpath = this.nombreImagen;
              // }

              setTimeout(() => this.mensajeExito = null, 5000);
            }, (errorRedesSnt) => {
              this.mensaje = errorRedesSnt.error.error.message;
            });

  }
  quitarSeleccion() {

    this.nombreImagen = null;
    const inputFile = <HTMLInputElement>document.getElementById('exampleFormControlFileRed');
    inputFile.value = null;
  }

  editarRedSocial(red: RedSocial) {
    this.red = red;
    this.quitarSeleccion();
    this.editar = true;
    this.formaRedSocial.setValue(this.red);
  }

  eliRedSocial() {
    this.dataApiService
          .delModelo('redes_sociales', this.red.id)
            .subscribe(dataEliSitInt => {
              console.log('Red Social Eliminada', dataEliSitInt);
              this.subirArchivoService.delImage('instFooter', this.red.imgpath).subscribe( dataE => console.log('Imagen Eliminada'));
              delete this.redes[this.index];
              this.redes = this.redes.filter(Boolean);
              this.cargarForma();
            }, (errorEliminarRedesSnt) => {
              this.mensaje = errorEliminarRedesSnt.error.error.message;
            });
  }

  eliModal(red: RedSocial, i: number) {
    this.index =  i;
    this.red =  red;
  }


  subirImagen() {
    this.subirArchivoService
        .subirArchivo(this.imagen, this.nombreImagen, 'instFooter', 'b')
          .then()
          .catch(datac => console.log(datac));
  }

  selimg( archivo: File ) {
    this.nombreImagen = null;
    if ( !archivo ) {
      this.imagen = null;
      console.log('vacio');
      return;
    }
    this.imagen = archivo;
    const nombreCortado = this.imagen.name.split('.');
    const extensionArchivo = nombreCortado.pop();
    // const name = nombreCortado.shift();
    const name = Date.now().toString();
    this.nombreImagen = name + '.' + extensionArchivo;
    console.log('nombre Imagen', this.nombreImagen);
  }

  cargarForma() {
    this.quitarSeleccion();
    this.editar = false;
    this.mensaje = null;
    this.formaRedSocial = new FormGroup ({
      'nombre': new FormControl('', [Validators.required, Validators.minLength(3)]), // , [Validators.required, Validators.minLength(2)]
      'direccion_url': new FormControl('', [Validators.required, Validators.minLength(15)]),
      'imgpath': new FormControl(''),
      'id': new FormControl()
    });
  }

  ngOnDestroy() {
    // $('#ModalEstudiante').modal('hide');
    // $('#ModalEstudiante').modal('dispose');
    $('.modal-backdrop').hide();
  }
}
