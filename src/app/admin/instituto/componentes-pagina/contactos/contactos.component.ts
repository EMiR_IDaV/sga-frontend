import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Contacto } from '../../../../interfaces/contacto.interface';
import { PublicService } from '../../../../services/public.service';
import { DataApiService } from '../../../../services/data-api.service';
import { SubirArchivoService } from '../../../../services/subir-archivo.service';

declare var $: any;

@Component({
  selector: 'app-contactos',
  templateUrl: './contactos.component.html',
  styleUrls: ['./contactos.component.css']
})
export class ContactosComponent implements OnInit, OnDestroy {

  mensajeExito = '';
  mensaje = '';
  formaContacto: FormGroup;

  imagen: File;
  nombreImagen: string;

  editar = false;
  index: number;
  contacto: Contacto = {
    contacto: '',
    imgpath: ''
  };
  contactos: Contacto[] = [];

  constructor(private dataApiService: DataApiService, private subirArchivoService: SubirArchivoService) { }

  ngOnInit() {
    this.cargarDatos();
  }

  cargarDatos() {
    this.cargarForma();
    this.dataApiService
          .getRegistros('contactos')
            .subscribe((dataRedesS: any) => {
              this.contactos = dataRedesS;
              (this.contactos.length === 0) ? this.mensaje = 'No hay Contactos registrados.' : this.mensaje = null;
            }, (errorRedesS) => {
              this.mensaje = errorRedesS.error.error.message;
            });
  }

  regContacto() {
    let imgAnt: string;
    this.contacto = this.formaContacto.value;

    if (this.nombreImagen) {
      if (this.editar) {
        imgAnt = this.contacto.imgpath;
      }
      this.contacto.imgpath = this.nombreImagen;
    }

    this.dataApiService
          .putModelo('contactos', this.contacto)
            .subscribe((dataContacto: any) => {
              console.log('Contacto Creada/Actualizada', dataContacto);
              if (!this.editar) {
                this.subirImagen();
                this.cargarDatos();
                this.mensajeExito = dataContacto.nombre;
              } else {
                if (this.nombreImagen) {
                  this.subirArchivoService.delImage('instFooter', imgAnt).subscribe( dataE => console.log('Imagen Eliminada'));
                  this.subirImagen();
                  this.cargarDatos();
                  this.mensajeExito = dataContacto.nombre;
                }
              }

              setTimeout(() => this.mensajeExito = null, 5000);
            }, (errorRedesSnt) => {
              this.mensaje = errorRedesSnt.error.error.message;
            });

  }
  quitarSeleccion() {

    this.nombreImagen = null;
    const inputFile = <HTMLInputElement>document.getElementById('exampleFormControlFileContacto');
    inputFile.value = null;
  }

  editarContacto(contacto: Contacto) {
    this.contacto = contacto;
    this.quitarSeleccion();
    this.editar = true;
    this.formaContacto.setValue(this.contacto);
  }

  eliContacto() {
    this.dataApiService
          .delModelo('contactos', this.contacto.id)
            .subscribe(dataEliSitInt => {
              console.log('Contacto Eliminada', dataEliSitInt);
              this.subirArchivoService.delImage('instFooter', this.contacto.imgpath).subscribe( dataE => console.log('Imagen Eliminada'));
              delete this.contactos[this.index];
              this.contactos = this.contactos.filter(Boolean);
              this.cargarForma();
            }, (errorEliminarRedesSnt) => {
              this.mensaje = errorEliminarRedesSnt.error.error.message;
            });
  }

  eliModal(contacto: Contacto, i: number) {
    this.index =  i;
    this.contacto =  contacto;
  }


  subirImagen() {
    this.subirArchivoService
        .subirArchivo(this.imagen, this.nombreImagen, 'instFooter', 'b')
          .then()
          .catch(datac => console.log(datac));
  }

  selimg( archivo: File ) {
    this.nombreImagen = null;
    if ( !archivo ) {
      this.imagen = null;
      console.log('vacio');
      return;
    }
    this.imagen = archivo;
    const nombreCortado = this.imagen.name.split('.');
    const extensionArchivo = nombreCortado.pop();
    // const name = nombreCortado.shift();
    const name = Date.now().toString();
    this.nombreImagen = name + '.' + extensionArchivo;
    console.log('nombre Imagen', this.nombreImagen);
  }

  cargarForma() {
    this.quitarSeleccion();
    this.editar = false;
    this.mensaje = null;
    this.formaContacto = new FormGroup ({
      'contacto': new FormControl('', [Validators.required, Validators.minLength(3)]), // , [Validators.required, Validators.minLength(2)]
      'imgpath': new FormControl(''),
      'id': new FormControl()
    });
  }

  irPaginaArriba() {
    // this.dataApiService.irPaginaArriba(this.activatedRoute);
  }

  ngOnDestroy() {
    // $('#ModalEstudiante').modal('hide');
    // $('#ModalEstudiante').modal('dispose');
    $('.modal-backdrop').hide();
  }


}
