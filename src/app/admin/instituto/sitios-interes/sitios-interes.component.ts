import { Component, OnInit } from '@angular/core';
import { DataApiService } from '../../../services/data-api.service';
import { SitioInteres } from '../../../interfaces/sitio_interes.interface';
import { PublicService } from '../../../services/public.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-sitios-interes',
  templateUrl: './sitios-interes.component.html',
  styleUrls: ['./sitios-interes.component.css']
})
export class SitiosInteresComponent implements OnInit {

  mensajeExito = '';
  mensajeError = '';
  forma: FormGroup;

  editar = false;
  index: number;
  sitio: SitioInteres = {
    nombre: '',
    direccion_url: ''
  };
  sitios: SitioInteres[] = [];

  constructor(private activatedRoute: ActivatedRoute, private publicService: PublicService, private dataApiService: DataApiService) { }

  ngOnInit() {
    this.cargarDatos();
  }

  cargarDatos() {
    this.cargarForma();
    this.publicService
          .getRegistrosModelo('sitios_interes')
            .subscribe((dataSitiosI: any) => {
              this.sitios = dataSitiosI;
              (this.sitios.length === 0) ? this.mensajeError = 'No hay sitios de interés registrados' : this.mensajeError = '';
            }, (errorSitiosI) => {
              this.mensajeError = errorSitiosI.error.error.message;
            });
  }

  registrarSitio() {
    this.sitio = this.forma.value;

    if (!this.sitio.id) {
      this.dataApiService
            .postModelo('sitios_interes', this.sitio)
              .subscribe((dataSitioInt: any) => {
                console.log('Sitio de Interes Registrado', dataSitioInt);
                this.cargarForma();
                this.sitios.push(dataSitioInt);
                this.sitios = this.sitios.filter(Boolean);
              }, (errorSitiosInt) => {
                this.mensajeError = errorSitiosInt.error.error.message;
              });
    } else {
      this.dataApiService
            .putModelo('sitios_interes', this.sitio)
              .subscribe((dataSitioInt: any) => {
                console.log('Sitio de Interés Actualizado', dataSitioInt);
                this.cargarDatos();
                this.mensajeExito = dataSitioInt.nombre;
                setTimeout(() => this.mensajeExito = '', 4000);
              }, (errorSitiosInt) => {
                this.mensajeError = errorSitiosInt.error.error.message;
              });
    }

  }

  editarSitioInteres(sitio: SitioInteres) {
    this.sitio = sitio;
    this.editar = true;
    this.forma.setValue(this.sitio);
  }

  eliSitioInteres() {
    this.dataApiService
          .delModelo('sitios_interes', this.sitio.id)
            .subscribe(dataEliSitInt => {
              console.log('Sitio Eliminado', dataEliSitInt);
              delete this.sitios[this.index];
              this.sitios = this.sitios.filter(Boolean);
              this.cargarForma();
            }, (errorEliminarSitiosInt) => {
              this.mensajeError = errorEliminarSitiosInt.error.error.message;
            });
  }

  eliModal(sitio: SitioInteres, i: number) {
    this.index =  i;
    this.sitio =  sitio;
  }

  cargarForma() {
    this.editar = false;
    this.mensajeError = '';
    this.forma = new FormGroup ({
      'nombre': new FormControl('', [Validators.required, Validators.minLength(3)]), // , [Validators.required, Validators.minLength(2)]
      'direccion_url': new FormControl('', [Validators.required, Validators.minLength(15)]),
      'id': new FormControl()
    });
  }

  irPaginaArriba() {
    this.dataApiService.irPaginaArriba(this.activatedRoute);
  }
}
