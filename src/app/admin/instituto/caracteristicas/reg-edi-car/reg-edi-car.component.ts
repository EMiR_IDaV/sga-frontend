import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { DataApiService } from '../../../../services/data-api.service';
import { SubirArchivoService } from '../../../../services/subir-archivo.service';
import { Caracteristica } from '../../../../interfaces/caracteristica.interface.';
import { PublicService } from '../../../../services/public.service';

@Component({
  selector: 'app-reg-edi-car',
  templateUrl: './reg-edi-car.component.html',
  styleUrls: ['./reg-edi-car.component.css']
})
export class RegEdiCarComponent implements OnInit {

  forma: FormGroup;
  mensajeError = '';
  mensajeExito = '';


  nombre: string;
  accion = 'Registrar';

  cambiarFoto = 'Seleccione';
  imagen: File;
  nombreImagen = '';

  resumen = '';

  caracteristica: Caracteristica = {
    nombre: '',
    resumen: '',
    imgpath: '',
    descripcion: ''
  };


  // path: 'instituto/nosotros/:caracteristica'
  constructor(private location: Location, private actRoute: ActivatedRoute,
              private dataApiServ: DataApiService, private subArcService: SubirArchivoService) {
    this.cargarForma();
    this.actRoute.params.subscribe((data: any) => this.nombre = data['caracteristica']);
  }

  ngOnInit() {
    if (this.nombre !== 'nueva') {
      this.accion = 'Editar';
      this.cambiarFoto = 'Seleccione para cambiar la';
      this.nombre = this.dataApiServ.eliminaGuionesParametro(this.nombre);
      this.dataApiServ
            .getCaracteristicaByNombre(this.nombre)
              .subscribe((data: any) => {
                                  console.log(data);
                                  this.caracteristica = data;
                                  delete this.caracteristica['fecha'];
                                  this.forma.setValue(this.caracteristica);
                                });
    }
  }

  regCaracteristica() {
    let imgAnt: string;
    this.caracteristica = this.forma.value;
    this.caracteristica.nombre = this.caracteristica.nombre.trim();
    const fec = new Date().toISOString().slice(0, 10);
    this.caracteristica.fecha = fec;
    if (this.nombre === 'nueva') {
      this.caracteristica.imgpath = this.nombreImagen;
      this.dataApiServ
            .postModelo('caracteristicas', this.caracteristica)
              .subscribe((data: any) => {
                                          console.log(this.caracteristica);
                                          this.mensajeExito = data.nombre;
                                          this.cargarForma();
                                          this.mensajeError = null;
                                          this.subirImagen(this.imagen);
                                        }, (errorRegCaracteristica) => {
                                                                  this.mensajeError = errorRegCaracteristica.error.error.message;
                                                                  this.mensajeExito = null;
                                                                 });
    } else {
      if (this.nombreImagen !== '') {  // si solo cambia el texto monbreImagen esta vacio => no cambia la imagen
        imgAnt = this.caracteristica.imgpath;
        this.caracteristica.imgpath = this.nombreImagen;
      }
      console.log(this.caracteristica);
      this.dataApiServ
            .putModelo('caracteristicas', this.caracteristica)
              .subscribe((dataPutCaracteristicas: any) => {
                console.log('Caracteristica Actualizada', dataPutCaracteristicas);
                if (this.nombreImagen !== '') {  // si solo cambia el texto monbreImagen esta vacio => no cambia la imagen
                  this.subArcService.delImage('instCaracteristicas', imgAnt).subscribe( dataE => console.log('Imagen Eliminada'));
                  this.subirImagen(this.imagen);
                }
                this.mensajeExito = this.caracteristica.nombre;
                this.mensajeError = null;
              }, (errorServicioUsu) => {
                this.mensajeError = errorServicioUsu.error.error.message;
                this.mensajeExito = null;
              });
    }
    window.scrollTo(0, 0);
  }

  subirImagen(imagen: File) {
    this.subArcService
        .subirArchivo(imagen, this.nombreImagen, 'instCaracteristicas', 'b')
          .then()
          .catch(datac => console.log(datac));
  }

  selimg( archivo: File ) {
    this.nombreImagen = '';
    if ( !archivo ) {
      this.imagen = null;
      console.log('vacio');
      return;
    }
    this.imagen = archivo;
    const nombreCortado = this.imagen.name.split('.');
    const extensionArchivo = nombreCortado.pop();
    const name = Date.now().toString();
    this.nombreImagen = name + '.' + extensionArchivo;
    console.log('nombre Imagen', this.nombreImagen);
  }

  irPaginaArriba() {
    this.dataApiServ.irPaginaArriba(this.actRoute);
  }

  cargarForma() {
    this.mensajeError = null;
    this.mensajeExito = null;
    this.forma = new FormGroup ({
      'nombre': new FormControl('', [Validators.required, Validators.minLength(4)]), // , [Validators.required, Validators.minLength(2)]
      'resumen': new FormControl('', [Validators.required, Validators.minLength(15)]), // , [Validators.required, Validators.minLength(5)]
      'imgpath': new FormControl('', []), // , [Validators.required]
      'descripcion': new FormControl('', [Validators.required, Validators.minLength(50)]), // , [Validators.required] MINIMO 150 Se debe saber cual es el que hace form INVALIDA!!!
      'id': new FormControl()
    });
  }
}
