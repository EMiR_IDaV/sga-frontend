import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegEdiCarComponent } from './reg-edi-car.component';

describe('RegEdiCarComponent', () => {
  let component: RegEdiCarComponent;
  let fixture: ComponentFixture<RegEdiCarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegEdiCarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegEdiCarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
