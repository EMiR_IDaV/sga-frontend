import { Component, OnInit } from '@angular/core';
import { DataApiService } from '../../../services/data-api.service';
import { Caracteristica } from '../../../interfaces/caracteristica.interface.';
import { SubirArchivoService } from '../../../services/subir-archivo.service';
import { PublicService } from '../../../services/public.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-caracteristicas',
  templateUrl: './caracteristicas.component.html',
  styleUrls: ['./caracteristicas.component.css']
})
export class CaracteristicasComponent implements OnInit {

  caracteristica: Caracteristica;
  index: number = null;

  caracteristicas: Caracteristica[];

  constructor(private activatedRoute: ActivatedRoute, private dataApiService: DataApiService, private publicService: PublicService, private subArcService: SubirArchivoService) { }

  ngOnInit() {
    this.dataApiService
          .getRegistrosDistinto('caracteristicas', 'nombre', 'Nosotros')
            .subscribe((data: any) => {
              this.caracteristicas = data;
              console.log(data);
            });
  }

  elimodal(caracteristica: Caracteristica, index: number) {                  // Posesiona sobre el Caracteristica a eliminar
    console.log(index);
    this.caracteristica = caracteristica;
    this.index = index;
  }

  eliCaracteristica() {              // no es necesario el est e i, ya que estan en this.es y this.in PARA VER
    this.dataApiService
          .delModelo('caracteristicas', this.caracteristica.id)
            .subscribe(mensaje => {
                                  console.log('Caracteristica Eliminada', mensaje);
                                  this.subArcService.delImage('instCaracteristicas', this.caracteristica.imgpath).subscribe(data => console.log('Imagen Eliminada'));
                                  delete this.caracteristicas[this.index];
                                  this.caracteristicas = this.caracteristicas.filter(Boolean);
                                  }, (errorServicioUsu) => {
                                                            console.log(errorServicioUsu.error.error.message);
                                                           });
  }

  irPaginaArriba() {
    this.dataApiService.irPaginaArriba(this.activatedRoute);
  }

}
