import { Component, OnInit } from '@angular/core';
import { DataApiService } from '../../services/data-api.service';
import { URL_CON } from 'src/app/services/urls';
import { SubirArchivoService } from '../../services/subir-archivo.service';
import { Unidad } from '../../interfaces/unidad.interface';

@Component({
  selector: 'app-instituto',
  templateUrl: './instituto.component.html',
  styleUrls: ['./instituto.component.css']
})
export class InstitutoComponent implements OnInit {

  mensaje: string;

  nombreVideo = '';

  numCaracteristicas: any;
  numEquipoTrabajo: any;
  numNoticias: any;
  numSitiosInteres: any;
  archivos: any[] = [];
  video: Unidad;

  consultasNoleidas: number;
  videopath = '../../../assets/video/video presentacion umsa.mp4';

  constructor(private dataApiService: DataApiService, private subirArchivoService: SubirArchivoService) { }

  ngOnInit() {
    this.dataApiService
          .getCountRegistrosFiltro('consultas', 'leida', false)
            .subscribe((dataNumeroNoLeidos: any) => {
              this.consultasNoleidas = dataNumeroNoLeidos['count'];
            }, errorNumeroNoLeidos => {
              console.log('errorNumeroNoLeidos', errorNumeroNoLeidos);
            });

    this.dataApiService
          .countRegistros('caracteristicas')
            .subscribe((numCaracteristicas: any) => {
              this.numCaracteristicas = numCaracteristicas['count'];
            }, errorCountCarac => {
              this.mensaje = errorCountCarac.error.error.message;
              console.log('errorCountCarac', errorCountCarac);
            });
    this.dataApiService
          .countRegistros('admins')
            .subscribe((numEquipo: any) => {
              this.numEquipoTrabajo = numEquipo['count'];
            }, errorNumEquipo => {
              this.mensaje = errorNumEquipo.error.error.message;
              console.log('errorNumEquipo', errorNumEquipo);
            });
    this.dataApiService
          .countRegistros('noticias')
            .subscribe((numNoticias: any) => {
              this.numNoticias = numNoticias['count'];
            }, errorNumNoticias => {
              this.mensaje = errorNumNoticias.error.error.message;
              console.log('errorNumNoticias', errorNumNoticias);
            });
    this.dataApiService
          .countRegistros('sitios_interes')
            .subscribe((numSitios: any) => {
              this.numSitiosInteres = numSitios['count'];
            }, errorNumSitios => {
              this.mensaje = errorNumSitios.error.error.message;
              console.log('errorNumSitios', errorNumSitios);
            });
    this.dataApiService
          .getRegistroByProperty('unidades', 'nombre', 'video')
            .subscribe((video: any) => {
              this.video = video;
            }, errorVideo => {
              this.mensaje = errorVideo.error.error.message;
              console.log('errorVideo', errorVideo);
            });
    // this.cargarVideos();
  }

  regVideo() {
    const inputVideo = <HTMLInputElement>document.getElementById('formControlVideo');
    console.log(typeof(inputVideo.value));
    this.video.descripcion = inputVideo.value;
    this.dataApiService
          .putModelo('unidades', this.video)
            .subscribe((dataPutVideo: any) => {
              console.log('videoActualizado', dataPutVideo);
            }, errorVideo => {
              console.log('errorVideo', errorVideo);
            });
  }

  // cargarVideos() {
  //   this.subirArchivoService
  //         .getArchivos('instVideo')
  //           .subscribe((data: any) => {
  //             this.archivos = data;
  //             console.log('data', this.archivos);
  //             this.nombreVideo = data[0].name;

  //             // if (this.archivos.length > 0) {
  //             //   this.videopath = `${URL_CON}img/download/${data[0].name}`;
  //             // }
  //           });

  // }

  selimg( archivo: File ) {
    // this.nombreVideo = '';
    // if ( !archivo ) {
    //   this.video = null;
    //   console.log('vacio');
    //   return;
    // }
    // this.video = archivo;
    // const nombreCortado = this.video.name.split('.');
    // const extensionArchivo = nombreCortado.pop();
    // const name = nombreCortado.shift();
    // // const name = nombreCortado.shift();
    // // const name = Date.now().toString();

    // this.nombreVideo = name + '.' + extensionArchivo;
    // console.log('nombre Imagen', this.nombreVideo);
  }

  // cambiarVideo() {

  //   // const container = {name: 'img'};

  //   // this.dataApiService
  //   //       .delContainer('img')
  //   //         .subscribe((dataDelContainer: any) => {
  //   //           console.log('Container Eliminado', dataDelContainer);

  //   //           this.dataApiService
  //   //                 .postModelo('containers', container)
  //   //                   .subscribe((dataContainer: any) => {
  //   //                     console.log('Container Creado', dataContainer);

  //   //                     this.subirArchivoService
  //   //                           .subirArchivo(this.video, this.nombreVideo, 'img', 'b')
  //   //                             .then(data1 => {
  //   //                               // this.videopath = `${URL_CON}img/download/${this.nombreVideo}`;
  //   //                             })
  //   //                             .catch(datac => console.log(datac));

  //   //                   }, errorContainer => {
  //   //                     console.log('errorContainer', errorContainer);
  //   //                   });

  //   //         }, errorEliContainer => {
  //   //           console.log('errorEliContainer', errorEliContainer);
  //   //         });



  //   this.subirArchivoService
  //         .getArchivos('instVideo')
  //           .subscribe((data: any) => {
  //             this.archivos = data;
  //             console.log('data', this.archivos);

  //             if (this.archivos.length > 0) {
  //               // for (let index = 0; index < this.archivos.length; index++) {
  //               //   const element = this.archivos[index];

  //                 this.subirArchivoService
  //                     .delImage('instVideo', data[0].name)
  //                         .subscribe( dataE => {
  //                           console.log('Video Eliminado');
  //                           // if (index === this.archivos.length - 1) {
  //                             this.subirArchivoService
  //                                 .subirArchivo(this.video, this.nombreVideo, 'instVideo', 'b')
  //                                   .then( data1 => {
  //                                     // this.videopath = `${URL_CON}instVideo/download/${this.nombreVideo}`;
  //                                   })
  //                                   .catch(datac => console.log(datac));
  //                           // }
  //                         });
  //               // }
  //             } else {
  //               this.subirArchivoService
  //                     .subirArchivo(this.video, this.nombreVideo, 'instVideo', 'b')
  //                       .then(data1 => {
  //                         // this.videopath = `${URL_CON}instVideo/download/${this.nombreVideo}`;
  //                       })
  //                       .catch(datac => console.log(datac));
  //             }
  //           });


  // }

}
