import { Component, OnInit } from '@angular/core';
import { DataApiService } from '../../../services/data-api.service';
import { Consulta } from '../../../interfaces/consulta.interface';
import { PublicService } from '../../../services/public.service';
import { AdminService } from '../../admin.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-consultas',
  templateUrl: './consultas.component.html',
  styleUrls: ['./consultas.component.css']
})
export class ConsultasComponent implements OnInit {

  mensajeExito: string;
  mensajeErrorNuevas: string;
  mensajeErrorLeidas: string;

  consultasNoLeidas: Consulta[] = [];
  consultasLeidas: Consulta[] = [];

  constructor(private activatedRoute: ActivatedRoute, private dataApiService: DataApiService, private publicService: PublicService, private adminService: AdminService ) {
    console.log('-------------------------------------- consultas-component --------------------------------------');
  }

  ngOnInit() {
    this.dataApiService
          .getModelosPorFiltroOrdenar('consultas', 'leida', false, 'fecha', 'asc')
            .subscribe((dataConsultasNoLeidas: any) => {
              console.log(dataConsultasNoLeidas);
              this.consultasNoLeidas = dataConsultasNoLeidas;
              if (this.consultasNoLeidas.length === 0) {
                this.mensajeErrorNuevas = 'No hay nuevas consultas';
              }
            }, errorConsultas => {
              this.mensajeErrorNuevas = errorConsultas.error.error.message;
            });
    this.dataApiService
          .getModelosPorFiltroOrdenar('consultas', 'leida', true, 'fecha', 'desc')
            .subscribe((dataConsultasLeidas: any) => {
              console.log(dataConsultasLeidas);
              this.consultasLeidas = dataConsultasLeidas;
              if (this.consultasLeidas.length === 0) {
                this.mensajeErrorLeidas = 'No hay consultas leídas';
              }
            }, errorConsultas => {
              this.mensajeErrorLeidas = errorConsultas.error.error.message;
            });
  }

  registrarConsultasNoLeidas() {

    let cont = 0;

    for (let index = 0; index < this.consultasNoLeidas.length; index++) {
      const tbody = 'aqcheckboxConsultaLeida' + index;
      const aqcheckbox = <HTMLInputElement>document.getElementById(tbody);
      const aqchecked = aqcheckbox.checked;
      if (aqchecked) {
        cont++;
      }
    }
    console.log(cont);

    let cont2 = 0;

    for (let index = 0; index < this.consultasNoLeidas.length; index++) {
      const tbody = 'aqcheckboxConsultaLeida' + index;
      const aqcheckbox = <HTMLInputElement>document.getElementById(tbody);
      console.log(aqcheckbox);

      if (aqcheckbox) {
        const aqchecked = aqcheckbox.checked;
        if (aqchecked) {
          this.consultasNoLeidas[index].leida = true;
          this.dataApiService
                .putModelo('consultas', this.consultasNoLeidas[index])
                  .subscribe(dataPutConsulta => {
                    console.log('dataPutConsulta', dataPutConsulta);
                    delete this.consultasNoLeidas[index];
                    cont2++;
                    if (cont === cont2) {
                      this.consultasNoLeidas = this.consultasNoLeidas.filter(Boolean);
                      (this.consultasNoLeidas.length === 0) ? this.mensajeErrorNuevas = 'No hay nuevas Consultas' : this.mensajeErrorNuevas = null;

                      this.dataApiService
                            .getModelosPorFiltroOrdenar('consultas', 'leida', true, 'fecha', 'desc')
                              .subscribe((dataConsultasLeidas: any) => {
                                console.log(dataConsultasLeidas);
                                this.consultasLeidas = dataConsultasLeidas;
                                this.mensajeErrorLeidas = null;
                              }, errorConsultas => {
                                this.mensajeErrorLeidas = errorConsultas.error.error.message;
                              });

                      this.numeroNoLeidos(this.consultasNoLeidas.length);
                    }
                  }, errorPutConsulta => {
                      console.log('errorPutConsulta', errorPutConsulta);
                  });
        }
      }

    }
  }

  // @HostListener('numeroNoLeidos')
  numeroNoLeidos(numero: number) {
    console.log('AQUI!!!');
    this.adminService.toggle(numero);
  }

  irPaginaArriba() {
    this.dataApiService.irPaginaArriba(this.activatedRoute);
  }

}
