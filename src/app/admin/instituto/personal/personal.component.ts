import { Component, OnInit, OnDestroy } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';

// import { TableService } from '../../../../services/table.service';
import { DataApiService } from '../../../services/data-api.service';
import { TableService } from '../../../services/table.service';
import { PublicService } from '../../../services/public.service';
import { Usuario } from '../../../interfaces/usuario.interface';
import { SubirArchivoService } from '../../../services/subir-archivo.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

declare var $: any;

@Component({
  selector: 'app-personal',
  templateUrl: './personal.component.html',
  styleUrls: ['./personal.component.css']
})

export class PersonalComponent implements OnInit, OnDestroy {

  forma: FormGroup;
  asignarQuitar: string;

  usuario: Usuario = null;
  index: number = null;
  mensajeError = '';
  sigla: string;
  nombreCurso: string;
  usuarios: Usuario[];

  constructor(private activatedRoute: ActivatedRoute, private dataApiService: DataApiService, private publicService: PublicService,
              private subArcService: SubirArchivoService, private router: Router, private tableService: TableService) {
  }

  ngOnInit() {
    this.forma = new FormGroup ({
      'username': new FormControl('', Validators.required),
      'password': new FormControl('', Validators.required)
    });

    this.dataApiService
          .getUsuariosIncludeTipo('admin')
            .subscribe(data => {
              console.log(data);
              this.usuarios = data;
              for (let index = 0; index < this.usuarios.length; index++) {
                const element = this.usuarios[index];

                this.dataApiService
                      .verificarAdmin(element.id)
                            .subscribe( () => {
                              element['super'] = true;
                            }, err => {
                              element['super'] = false;
                            });
              }
            });
  }

  loginUser() {
    console.log('hola');
  }

  posicionaModal(usuario: Usuario, index: number) {                    // Posesiona sobre el Admin a eliminar
    console.log('usuario', usuario);
    console.log('index', index);
    this.usuario = usuario;
    this.index = index;
    (this.usuario.super) ? this.asignarQuitar = 'QUITAR' : this.asignarQuitar = 'ASIGNAR';
  }

  eliAdmin() {              // no es necesario el est e i, ya que estan en this.es y this.in PARA VER
    this.dataApiService
          .delModeloRelacionado('usuarios', this.usuario.id, 'admin')
            .subscribe(mensajeEA => {
              console.log('Admin Eliminado', mensajeEA);

              this.dataApiService
                    .delModelo('usuarios', this.usuario.id)
                      .subscribe(dataDelUsuario => {
                        console.log('Usuario Eliminado', dataDelUsuario);

                        this.subArcService.delImage('instPersonal', this.usuario.admin.imgpath).subscribe(msgE => msgE);

                        console.log(this.usuarios.length);
                        delete this.usuarios[this.index];
                        this.usuarios = this.usuarios.filter(Boolean);
                        console.log(this.usuarios.length);
                      }, (errorDelUsuario) => {
                        console.log(errorDelUsuario.error.error.message);
                      });
            }, (errorDelUsuario) => {
              console.log(errorDelUsuario.error.error.message);
            });
  }

  asignarQuitarAdministrador() {
    console.log(this.usuario);

    if (!this.usuario.super) {
      this.dataApiService.asignarAdministrador(this.usuario.id)
            .subscribe((dataAsignarAdmin) => {
              console.log('admin', this.usuario.pri_nombre);
              this.usuarios[this.index]['super'] = true;
              this.usuarios = this.usuarios.filter(Boolean);
              console.log(this.usuarios);
            }, err => {
              this.usuarios[this.index]['super'] = false;
              this.usuarios = this.usuarios.filter(Boolean);
              console.log(this.usuarios);
              console.log(err.error.error.message);
            });
    } else {
      this.dataApiService
            .getRegistroByProperty('RoleMappings', 'principalId', this.usuario.id.toString())
              .subscribe((dataRM: any) => {
                console.log('RM', dataRM);
                this.dataApiService
                      .quitarAdministrador(dataRM.id)
                        .subscribe((dataERM) => {
                          console.log('ADM anulado', dataERM);
                          this.usuarios[this.index]['super'] = false;
                        }, errorDataERM => {
                          console.log('errorDataERM', errorDataERM);
                        });
              }, errorDataRM => {
                console.log('no se pudo quitar Admin', errorDataRM.error.error.message);
              });
    }
  }

  irPaginaArriba() {
    this.dataApiService.irPaginaArriba(this.activatedRoute);
    // this.che = !this.che;
  }

  ngOnDestroy(): void {
    $('.modal-backdrop').hide();
  }

}
