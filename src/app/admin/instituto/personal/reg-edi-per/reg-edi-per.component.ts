import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Usuario } from '../../../../interfaces/usuario.interface';
import { ActivatedRoute, Router } from '@angular/router';
import { DataApiService } from '../../../../services/data-api.service';
import { AutentificacionService } from '../../../../services/autentificacion.service';
import { Location } from '@angular/common';
import { Admin } from '../../../../interfaces/admin.interface';
import { SubirArchivoService } from '../../../../services/subir-archivo.service';

@Component({
  selector: 'app-reg-edi-per',
  templateUrl: './reg-edi-per.component.html',
  styleUrls: ['./reg-edi-per.component.css']
})
export class RegEdiPerComponent implements OnInit {

  forma: FormGroup;
  username: string;
  accion = 'Registrar';
  mensajeError = null;
  mensajeExito = null;
  nacionalidad = 'Boliviano';
  cod_deptos = ['LP', 'OR', 'PT', 'CB', 'SC', 'BN', 'PA', 'TJ', 'CH'];

  cambiarFoto = 'Seleccione';

  imagen: File;
  nombreImagen = '';

  usuario: Usuario = {
    paterno: '',
    materno: '',
    pri_nombre: '',
    seg_nombre: '',
    sexo: '',
    nacionalidad: '',
    ci: null,
    extendido: '',
    fec_nacimiento: '',
    celular: null,
    telefono: null,
    email: '',
    dom_ciudad: '',
    dom_zona: '',
    dom_calle: '',
    dom_numero: null,
    dom_otro: null
  };

  admin: Admin = {
    cargo: '',
    resena: '',
    grado: '',
    descripcion: '',
    imgpath: '',
    oficina: 'C. Federico Zuazo esq. Zapata Edificio. René Zavaleta, piso 5',
    vista_posicion: null
  };

  tituloDescripcion = `$$Entre sus estudios pos-universitarios se encuentran:
$$En su desempeño profesional se pueden mencionar:`;

  // path 'instituto/equipo-de-trabajo/:username'
  constructor(private dataApiServ: DataApiService, private router: Router,
              private autService: AutentificacionService, private actRoute: ActivatedRoute,
              private location: Location, private subArcService: SubirArchivoService) {
  }

  ngOnInit() {
    this.cargarForma();
    this.actRoute.params.subscribe((data: any) => this.username = data['username']);

    if (this.username !== 'nuevo') {
      this.accion = 'Actualizar';
      this.cambiarFoto = 'Seleccione para cambiar la';
      this.dataApiServ
            .getUsuarioIncluidoTipo(this.username, 'admin')
              .subscribe((dataUsuario: any) => {
                                          console.log(dataUsuario);
                                          if (dataUsuario.nacionalidad !== 'Boliviano') {
                                            this.nacionalidad = 'Extranjero';
                                          }
                                          this.admin = dataUsuario.admin;
                                          delete this.usuario['admin'];
                                          for (const key in this.usuario) {
                                            if (this.usuario.hasOwnProperty(key)) {
                                            // if (this.usuario.hasOwnProperty(key) && key !== 'fec_nacimiento') {
                                              this.usuario[key] = dataUsuario[key];
                                            }
                                          }
                                          this.usuario.fec_nacimiento = (<string>dataUsuario.fec_nacimiento).substring(0, 10);
                                          this.usuario.id = dataUsuario.id;
                                          this.usuario['cargo'] = this.admin.cargo;
                                          this.usuario['resena'] = this.admin.resena;
                                          this.usuario['grado'] = this.admin.grado;
                                          this.usuario['descripcion'] = this.admin.descripcion;
                                          this.usuario['oficina'] = this.admin.oficina;
                                          this.usuario['vista_posicion'] = this.admin.vista_posicion;
                                          this.forma.setValue(this.usuario);
                                          });
    }
  }

  registrarAdmin() {
    let imgAnt: string;
    this.admin.cargo = this.forma.value.cargo;
    this.admin.resena = this.forma.value.resena;
    this.admin.grado = this.forma.value.grado;
    this.admin.descripcion = this.forma.value.descripcion;
    this.admin.oficina = this.forma.value.oficina;
    this.admin.vista_posicion = this.forma.value.vista_posicion;
    this.usuario =  this.forma.value;
    this.usuario.username = this.usuario.paterno[0] + this.usuario.materno[0] + this.usuario.pri_nombre[0] + this.usuario.ci;
    this.usuario.password = this.usuario.ci.toString();

    if (this.username === 'nuevo') {
      this.admin.imgpath = this.nombreImagen;

      this.dataApiServ
            .postModelo('usuarios', this.usuario)
              .subscribe((dataUsuario: any) => {
                console.log('Usuario Creado', dataUsuario);

                this.dataApiServ
                      .postModeloRelacionado('usuarios', dataUsuario.id, 'admin', this.admin)
                        .subscribe(dataAdmin => {
                          console.log('Admin Creado', dataAdmin);
                          this.subirImagen(this.imagen);
                          this.cargarForma();
                          this.mensajeError = null;
                          this.mensajeExito = this.usuario.username;
                        }, (errorServicioAdm) => {
                          this.mensajeExito = null;
                          this.mensajeError = errorServicioAdm.error.error.message;

                          this.dataApiServ
                                .delModelo('usuarios', dataUsuario.id)
                                  .subscribe(dataDelUsuario => {
                                    console.log('Usuario Eliminado', dataDelUsuario);
                                  });
                        });
              }, (errorUsuario) => {
                this.mensajeExito = null;
                this.mensajeError = errorUsuario.error.error.message;
              });
    } else {
        if (this.nombreImagen !== '') {  // si solo cambia el texto monbreImagen esta vacio => no cambia la imagen
          imgAnt = this.admin.imgpath;
          this.admin.imgpath = this.nombreImagen;
        }
        this.dataApiServ
              .putModeloRelacionado('usuarios', this.usuario.id, 'admin', this.admin)
                .subscribe((dataPutAdmin: any) => {
                  console.log('Admin Actualizado', dataPutAdmin);
                  if (this.nombreImagen !== '') {  // si solo cambia el texto monbreImagen esta vacio => no cambia la imagen
                    this.subArcService.delImage('instPersonal', imgAnt).subscribe( dataE => console.log('Imagen Eliminada'));
                    this.subirImagen(this.imagen);
                  }
                  this.actUsuario(this.usuario);
                }, (errorPutUsuario) => {
                  this.mensajeError = errorPutUsuario.error.error.message;
                  this.mensajeExito = null;
                });
    }
    window.scrollTo(0, 0);
  }

  actUsuario(usuario: Usuario) {
    this.dataApiServ
          .putModelo('usuarios', usuario)
            .subscribe((dataPutUsuario) => {
              console.log('Usuario Actualizado', dataPutUsuario);
              this.mensajeError = null;
              this.mensajeExito = usuario.username;
            }, (errorPutUsuario) => {
              this.mensajeExito = null;
              this.mensajeError = errorPutUsuario.error.error.message;
            });
  }

  subirImagen(imagen: File) {
    this.subArcService
        .subirArchivo(imagen, this.nombreImagen, 'instPersonal', 'b')
          .then()
          .catch(datac => console.log(datac));
  }

  selimg( archivo: File ) {
    this.nombreImagen = '';
    if ( !archivo ) {
      this.imagen = null;
      console.log('vacio');
      return;
    }
    this.imagen = archivo;
    const nombreCortado = this.imagen.name.split('.');
    const extensionArchivo = nombreCortado.pop();
    const name = Date.now().toString();
    this.nombreImagen = name + '.' + extensionArchivo;
    console.log('nombreImagen', this.nombreImagen);
  }

  cargarForma() {
    this.nacionalidad = 'Boliviano';
    this.mensajeError = null;
    this.mensajeExito = null;
    this.forma = new FormGroup ({
      'paterno': new FormControl(''),
      'materno': new FormControl(''),
      'pri_nombre': new FormControl(''), // , [Validators.required, Validators.minLength(2)]
      'seg_nombre': new FormControl(''),
      'sexo': new FormControl(''), // , [Validators.required]
      'nacionalidad': new FormControl('Boliviano'),
      'ci': new FormControl(''), // , [Validators.required, Validators.minLength(5)]
      'extendido': new FormControl(''), // , [Validators.required]
      'fec_nacimiento': new FormControl('', []),
      'celular': new FormControl(''), // , [Validators.required, Validators.minLength(8)]
      'telefono': new FormControl(''),
      'cargo': new FormControl(''),
      'email': new FormControl(''), // , [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]
      'dom_ciudad': new FormControl('', []),
      'dom_zona': new FormControl('', []),
      'dom_calle': new FormControl('', []),
      'dom_numero': new FormControl('', []),
      'dom_otro': new FormControl(''),
      'grado': new FormControl(''),
      'resena': new FormControl(''),
      'descripcion': new FormControl(this.tituloDescripcion),
      'oficina': new FormControl(''),
      'vista_posicion': new FormControl(''),
      'id': new FormControl()
    });
  }

  cancelar() {
    this.location.back();
  }

  irPaginaArriba() {
    this.dataApiServ.irPaginaArriba(this.actRoute);
  }

}
