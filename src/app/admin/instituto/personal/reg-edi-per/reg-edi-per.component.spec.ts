import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegEdiPerComponent } from './reg-edi-per.component';

describe('RegEdiPerComponent', () => {
  let component: RegEdiPerComponent;
  let fixture: ComponentFixture<RegEdiPerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegEdiPerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegEdiPerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
