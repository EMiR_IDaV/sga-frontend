import { Component, OnInit } from '@angular/core';
import { DataApiService } from '../../../services/data-api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Convocatoriainves } from '../../../interfaces/convocatoriainves.interface';
import { FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-convocatorias',
  templateUrl: './convocatorias.component.html',
  styleUrls: ['./convocatorias.component.css']
})
export class ConvocatoriasComponent implements OnInit {

  mensajeExito: string;
  mensajeError: string;
  forma: FormGroup;

  editar = false;
  index: number;
  convocatoria: Convocatoriainves;

  mensaje: string;
  convocatorias: Convocatoriainves[] = [];

  numeroInvssAsociados: number;

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private dataApiService: DataApiService) { }

  ngOnInit() {
    this.cargarDatos();
  }

  cargarDatos() {
    this.cargarForma();
    this.dataApiService
          .getRegistros('convocatoriainves')
            .subscribe((dataConvocatorias: any) => {
              this.convocatorias = dataConvocatorias;
              (this.convocatorias.length === 0) ? this.mensajeError = 'No hay convocatorias de investigación registradas' : this.mensajeError = null;
            }, (errorConvocatorias) => {
              this.mensajeError = errorConvocatorias.error.error.message;
            });
  }

  registrarConvocatoria() {
    this.convocatoria = this.forma.value;

    if (!this.convocatoria.id) {
      this.dataApiService
            .postModelo('convocatoriainves', this.convocatoria)
              .subscribe((dataConvocatoriaInt: any) => {
                console.log('Convocatoria de Invenstigación Registrada', dataConvocatoriaInt);
                this.cargarForma();
                this.convocatorias.push(dataConvocatoriaInt);
                this.convocatorias = this.convocatorias.filter(Boolean);
              }, (errorConvocatoriasInt) => {
                this.mensajeError = errorConvocatoriasInt.error.error.message;
              });
    } else {
      this.dataApiService
            .putModelo('convocatoriainves', this.convocatoria)
              .subscribe((dataConvocatoriaInt: any) => {
                console.log('Convocatoria de Invenstigación Actualizada', dataConvocatoriaInt);
                this.cargarDatos();
                this.mensajeExito = dataConvocatoriaInt.nombre;
                setTimeout(() => this.mensajeExito = '', 4000);
              }, (errorConvocatoriasInt) => {
                this.mensajeError = errorConvocatoriasInt.error.error.message;
              });
    }

  }

  editarConvocatoria(convocatoria: Convocatoriainves) {
    this.convocatoria = convocatoria;
    this.editar = true;
    this.forma.setValue(this.convocatoria);
  }

  eliConvocatoria() {
    this.dataApiService
          .delModelo('convocatoriainves', this.convocatoria.id)
            .subscribe(dataEliSitInt => {
              console.log('Convocatoria Eliminada', dataEliSitInt);
              delete this.convocatorias[this.index];
              this.convocatorias = this.convocatorias.filter(Boolean);
              this.cargarForma();
            }, (errorEliminarConvocatoriasInt) => {
              this.mensajeError = errorEliminarConvocatoriasInt.error.error.message;
            });
  }

  eliModal(convocatoria: Convocatoriainves, i: number) {
    this.index =  i;
    this.convocatoria =  convocatoria;

    this.dataApiService
          .countRegistros('investigas', 'convocatoriainvesId', this.convocatoria.id)
            .subscribe(dataNumero => {
              this.numeroInvssAsociados = dataNumero['count'] ;
            }, errorNumeroDocentesModulo => {
              console.log('errorNumeroDocentesModulo', errorNumeroDocentesModulo);
            });
  }

  cargarForma() {
    this.editar = false;
    this.mensajeError = '';
    this.forma = new FormGroup ({
      'gestion': new FormControl('', [Validators.required, Validators.minLength(3)]), // , [Validators.required, Validators.minLength(2)]
      'ejes_tematicos': new FormControl('', [Validators.required, Validators.minLength(15)]),
      'id': new FormControl()
    });
  }

  irPaginaArriba() {
    this.dataApiService.irPaginaArriba(this.activatedRoute);
    // this.router.navigate(['../../'], { relativeTo: this.activatedRoute });
  }
}
