import { Component, OnInit } from '@angular/core';
import { DataApiService } from '../../../../services/data-api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PublicService } from '../../../../services/public.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Convocatoriainves } from '../../../../interfaces/convocatoriainves.interface';
import { Usuario } from '../../../../interfaces/usuario.interface';
import { Docente } from '../../../../interfaces/docente.interface';
import { of } from 'rxjs';
import { distinct, map } from 'rxjs/operators';

@Component({
  selector: 'app-reg-edi-con',
  templateUrl: './reg-edi-con.component.html',
  styleUrls: ['./reg-edi-con.component.css']
})
export class RegEdiConComponent implements OnInit {

  gestion: string;

  boton = false;

  mensajeExito: string;
  mensajeError: string;
  forma: FormGroup;
  forma1: FormGroup;
  accion = 'Registrar';

  docenteId: number;

  ussDocentes: Usuario[];
  ussDocentesEstaGestion: Usuario[] = [];
  invesPorDocente: any[] = [];
  proyectosPorDocente: any[] = [];

  convocatoria: Convocatoriainves = {
    gestion: '',
    ejes_tematicos: ''
  };

  constructor(private dataApiService: DataApiService, private activatedRoute: ActivatedRoute, private router: Router,
    private publicService: PublicService) { }

  ngOnInit() {
    // this.activatedRoute
    //       .params
    //         .subscribe(data => {
    //           this.gestion = data['gestion'];
    //           this.cargarForma();
    //           this.dataApiService
    //                 .getDocentesInvestigadores()
    //                   .subscribe((dataInvesDocentes: any) => {
    //                     this.ussDocentes = dataInvesDocentes;
    //                   }, errorInvesDocentes => {
    //                     this.mensajeError = errorInvesDocentes.error.error.message;
    //                   });
    //           if (this.gestion !== 'nueva') {
    //             this.accion = 'Actualizar';
    //             this.dataApiService
    //                   .getRegistroByProperty('convocatoriainves', 'gestion', this.gestion)
    //                     .subscribe((dataConvocatoria: any) => {
    //                       console.log(dataConvocatoria);
    //                       this.convocatoria = dataConvocatoria;
    //                       this.forma.setValue(this.convocatoria);

    //                       this.cargarDatosDocentes();

    //                     }, (errorGetModulo) => {
    //                       this.mensajeExito = null;
    //                       this.mensajeError = errorGetModulo.error.error.message;
    //                     });
    //                   }
    //         });
  }

  cargarDatosDocentes() {
    // this.dataApiService
    //       .getInvestigadoresPorConvocatoria(this.convocatoria.id)
    //         .subscribe((dataDocentesEstaGestion: any) => {
    //           console.log(dataDocentesEstaGestion);
    //           this.ussDocentesEstaGestion = dataDocentesEstaGestion;

    //           for (let index = 0; index < dataDocentesEstaGestion.length; index++) {
    //             const element = dataDocentesEstaGestion[index];

    //             this.dataApiService
    //               .getRegistrosPorFiltros('investigas', 'convocatoriainvesId', this.convocatoria.id, 'docenteId', element.docente.id)
    //                 .subscribe((dataInvestiga: any) => {
    //                   this.invesPorDocente[index] = dataInvestiga;
    //                   console.log(this.invesPorDocente[index]);
    //                   const proyectosDocente: any [] = [];

    //                   for (let index1 = 0; index1 < this.invesPorDocente[index].length; index1++) {
    //                     const element1 = this.invesPorDocente[index][index1];

    //                     if (element1.proyectoId) {
    //                       this.dataApiService
    //                           .getRegistro('proyectos', element1.proyectoId)
    //                             .subscribe(dataProyecto => {
    //                               proyectosDocente[index1] = dataProyecto;
    //                               console.log('dataProyecto', dataProyecto);
    //                             }, errorProyecto => {
    //                               console.log('errorProyecto', errorProyecto);
    //                             });
    //                     }
    //                   }

    //                   this.proyectosPorDocente[index] = proyectosDocente;
    //                 }, errorInvestiga => {
    //                   console.log('errorInvestiga', errorInvestiga);
    //                 });
    //           }
    //         }, errorDocentesEstaGestion => {
    //           this.mensajeExito = null;
    //           this.mensajeError = errorDocentesEstaGestion.error.error.message;
    //         });

  }

  regConvocatoria() {
    // this.convocatoria = this.forma.value;

    // if (this.gestion === 'nueva') {
    //   this.dataApiService
    //         .postModelo('convocatoriainves', this.convocatoria)
    //           .subscribe((dataConvocatoriaInt: any) => {
    //             console.log('Convocatoria de Invenstigación Registrada', dataConvocatoriaInt);
    //             this.convocatoria = dataConvocatoriaInt;
    //             this.router.navigate(['/admin', 'investigacion', 'convocatorias', this.convocatoria.gestion]);
    //           }, (errorConvocatoriasInt) => {
    //             this.mensajeExito = null;
    //             this.mensajeError = errorConvocatoriasInt.error.error.message;
    //           });
    // } else {
    //   this.dataApiService
    //         .putModelo('convocatoriainves', this.convocatoria)
    //           .subscribe((dataPutConvocatoriaInt: any) => {
    //             console.log('Convocatoria de Invenstigación Actualizada', dataPutConvocatoriaInt);
    //             this.mensajeError = null;
    //             this.mensajeExito = dataPutConvocatoriaInt.nombre;
    //             // setTimeout(() => this.mensajeExito = '', 4000);
    //           }, (errorConvocatoriasInt) => {
    //             this.mensajeError = errorConvocatoriasInt.error.error.message;
    //           });
    // }
  }

  selectElegidoInt(id: any) {

    // for (let index = 0; index < this.ussDocentes.length; index++) {
    // const element = this.ussDocentes[index];
    // if (id === element.id) {
    // const prof = `${element.docente.grado} ${this.dataApiService.obtieneNombreProfesor(element)}`;
    // this.forma.patchValue({profesor: prof});
    // this.nombreProfesor = prof;
    // }
    // }
  }

  selectElegido(id: string) {
    // const valueSelect = id.split(':');
    // const index = parseInt(valueSelect[0], 10);
    // const docenteId = parseInt(valueSelect[1], 10);
    // console.log(index, docenteId);
    // this.docenteId = docenteId;
  }

  cargarForma() {
    // this.mensajeError = '';
    // this.forma = new FormGroup ({
    //   'gestion': new FormControl('', [Validators.required, Validators.minLength(4)]), // , [Validators.required, Validators.minLength(2)]
    //   'ejes_tematicos': new FormControl('', [Validators.required, Validators.minLength(15)]),
    //   'id': new FormControl()
    // });
    // this.forma1 = new FormGroup ({
    //   'control1': new FormControl('')
    // });
  }

  registrarDocInvExistente() {
    // this.dataApiService
    //       .putRelacionModelos('convocatoriainves', this.convocatoria.id, 'docentes', this.docenteId)
    //         .subscribe((dataPutRelacion: any) => {
    //           console.log('dataPutRelacion', dataPutRelacion);
    //           this.cargarDatosDocentes();
    //         }, errorPutRelacion => {
    //           this.mensajeExito = null;
    //           this.mensajeError = errorPutRelacion.error.error.message;
    //         });
  }

  irPaginaArriba() {
    // console.log(this.forma.value);
    this.dataApiService.irPaginaArriba(this.activatedRoute);
  }

}

