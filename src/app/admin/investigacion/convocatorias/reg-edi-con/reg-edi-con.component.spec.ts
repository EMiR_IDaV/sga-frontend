import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegEdiConComponent } from './reg-edi-con.component';

describe('RegEdiConComponent', () => {
  let component: RegEdiConComponent;
  let fixture: ComponentFixture<RegEdiConComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegEdiConComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegEdiConComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
