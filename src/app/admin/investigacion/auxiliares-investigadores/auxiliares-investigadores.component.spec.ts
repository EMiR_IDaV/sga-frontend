import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuxiliaresInvestigadoresComponent } from './auxiliares-investigadores.component';

describe('AuxiliaresInvestigadoresComponent', () => {
  let component: AuxiliaresInvestigadoresComponent;
  let fixture: ComponentFixture<AuxiliaresInvestigadoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuxiliaresInvestigadoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuxiliaresInvestigadoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
