import { Component, OnInit, OnDestroy } from '@angular/core';
import { Convocatoriainves } from '../../../../interfaces/convocatoriainves.interface';
import { Usuario } from '../../../../interfaces/usuario.interface';
import { Proyecto } from '../../../../interfaces/proyecto.interface';
import { DataApiService } from '../../../../services/data-api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SubirArchivoService } from '../../../../services/subir-archivo.service';
import { FormGroup, FormControl } from '@angular/forms';

declare let $: any;

@Component({
  selector: 'app-list-auxinv',
  templateUrl: './list-auxinv.component.html',
  styleUrls: ['./list-auxinv.component.css']
})
export class ListAuxinvComponent implements OnInit, OnDestroy {

  forma1: FormGroup;

  gestion: string;
  convocatoria: Convocatoriainves;

  mensaje: string;
  mensajeError: string;
  quitarEliminar = 'quitar';
  quitarEliminarProyecto = 'quitar';

  carga = false;

  usuario: Usuario;
  index: number;

  auxsInvestigadores: Usuario[] = [];
  proyectos: Proyecto[] = [];
  titulosProyectosAuxiliares: string[] = [];

  proyectoId: number;

  constructor(private dataApiService: DataApiService, private router: Router,
              private activatedRoute: ActivatedRoute, private subirArchivoService: SubirArchivoService) {
    console.log('---------------- list-auxinv ---------------');
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(data => { this.gestion = data['gestion']; });

    this.dataApiService
          .getRegistroByProperty('convocatoriainves', 'gestion', this.gestion)
            .subscribe((dataConvocatoria: any) => {
              this.convocatoria = dataConvocatoria;

              this.dataApiService
                    .getProyectosInvesPorConvocatoria(this.convocatoria.id)
                      .subscribe((dataProyectos: any) => {
                        this.proyectos = dataProyectos;

                        this.cargarAuxiliares();

                      }, errorProyectos => {
                        console.log('errorProyectos', errorProyectos);
                      });


            }, errorConvocatoria => {
              this.mensaje = errorConvocatoria.error.error.message;
            });

    this.cargarForma();
  }

  cargarAuxiliares() {
    this.dataApiService
          .getAuxiliaresPorConvocatoria(this.convocatoria.id)
            .subscribe((dataAuxiliares: any) => {
              this.auxsInvestigadores = dataAuxiliares;
              console.log(this.auxsInvestigadores);
              this.llenarProysAuxiliares();
            }, errorInvestigadores => {
              this.auxsInvestigadores = [];
              this.mensaje = errorInvestigadores.error.error.message;
              this.mensajeError = 'No hay Auxiliares en esta convocatoria';
            });
  }

  llenarProysAuxiliares() {
    console.log(this.proyectos);
    for (let index = 0; index < this.auxsInvestigadores.length; index++) {
      const element = this.auxsInvestigadores[index];

      if (element.auxiliar.proyectoId) {

        for (let index1 = 0; index1 < this.proyectos.length; index1++) {
          const element1 = this.proyectos[index1];
          if (element.auxiliar.proyectoId === element1.id) {
            this.titulosProyectosAuxiliares[index] = element1.titulo;
          }
        }
      } else {
        this.titulosProyectosAuxiliares[index] = null;
      }
    }
    console.log(this.titulosProyectosAuxiliares);
  }

  elimodal(usuario: Usuario, index: number) {
    this.cargarForma();
    this.usuario = usuario;
    this.index = index;
  }

  elimodalProyecto(usuario: any, index: number) {
    this.usuario = usuario;
    this.index = index;
    console.log(this.usuario, this.index);
  }

  eliAuxiliarInv() {
    this.dataApiService
          .getUsuarioIncluidoTipo(this.usuario.username, 'auxiliar')
            .subscribe((dataUsAuxiliar: any) => {
              this.usuario = dataUsAuxiliar;

              this.dataApiService
                    .delModeloRelacionado('usuarios', this.usuario.id, 'auxiliar')
                      .subscribe(dataDelAuxiliar => {
                        console.log('Auxiliar Eliminado', dataDelAuxiliar);

                        this.subirArchivoService
                              .delImage('inveAuxiliares', this.usuario.auxiliar.imgpath)
                                .subscribe(dataEliImagenAuxiliar => {
                                  console.log('dataEliImagen', dataEliImagenAuxiliar);

                                  this.dataApiService
                                        .delModelo('usuarios', this.usuario.id)
                                          .subscribe(dataDelUsuario => {
                                            console.log('Usuario Eliminado', dataDelUsuario);
                                            delete this.auxsInvestigadores[this.index];
                                            this.auxsInvestigadores = this.auxsInvestigadores.filter(Boolean);
                                          }, (errorDelUsuario) => {
                                            this.mensaje = (errorDelUsuario.error.error.message);
                                          });

                                }, errorEliImagenAuxiliar => {
                                  console.log('errorEliImagenAuxiliar', errorEliImagenAuxiliar);
                                });

                      }, (errorDelAuxiliar) => {
                        this.mensaje = (errorDelAuxiliar.error.error.message);
                      });

            }, errorUsAuxiliar => {
              console.log(errorUsAuxiliar);
            });
  }

  quitarProyecto() {
    this.dataApiService
          .patchRegistro('auxiliars', {id: this.usuario.auxiliar.id, proyectoId: 0})
            .subscribe((dataPutAuxiliar: any) => {
              console.log('Quitar Colaboracion', dataPutAuxiliar);
              this.cargarAuxiliares();
            }, errorPutProyaux => {
              console.log('errorPutProyaux', errorPutProyaux);
            });
  }

  agregarProyecto() {
    this.dataApiService
          .patchRegistro('auxiliars', {id: this.usuario.auxiliar.id, proyectoId: this.proyectoId})
            .subscribe((dataPutAuxiliar: any) => {
              console.log('Auxiliaagregar con Colaboracion', dataPutAuxiliar);
              this.cargarAuxiliares();
            }, errorPutProyaux => {
              console.log('errorPutProyaux', errorPutProyaux);
            });
  }

  selectElegido(id: string) {
    const valueSelect = id.split(':');
    const proyectoId = parseInt(valueSelect[1], 10);
    console.log(proyectoId);
    this.proyectoId = proyectoId;
  }

  cargarForma() {
    this.proyectoId = null;
    this.forma1 = new FormGroup ({
      'controlRegProy1': new FormControl('')
    });
  }

  irPaginaArriba() {
    this.dataApiService.retrocederPagina();
    // this.dataApiService.irPaginaArriba(this.activatedRoute);
    // this.router.navigate(['../../'], { relativeTo: this.activatedRoute });
  }

  ngOnDestroy() {
    // $('#ModalEstudiante').modal('hide');
    // $('#ModalEstudiante').modal('dispose');
    $('.modal-backdrop').hide();
  }

}
