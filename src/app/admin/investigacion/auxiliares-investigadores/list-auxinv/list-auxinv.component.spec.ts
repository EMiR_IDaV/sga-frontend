import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAuxinvComponent } from './list-auxinv.component';

describe('ListAuxinvComponent', () => {
  let component: ListAuxinvComponent;
  let fixture: ComponentFixture<ListAuxinvComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListAuxinvComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAuxinvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
