import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegEdiAuxinvComponent } from './reg-edi-auxinv.component';

describe('RegEdiAuxinvComponent', () => {
  let component: RegEdiAuxinvComponent;
  let fixture: ComponentFixture<RegEdiAuxinvComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegEdiAuxinvComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegEdiAuxinvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
