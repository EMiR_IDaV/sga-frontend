import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Usuario } from '../../../../interfaces/usuario.interface';
import { DataApiService } from '../../../../services/data-api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Convocatoriainves } from '../../../../interfaces/convocatoriainves.interface';
import { Auxiliar } from '../../../../interfaces/auxiliar.interface';
import { SubirArchivoService } from '../../../../services/subir-archivo.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-reg-edi-auxinv',
  templateUrl: './reg-edi-auxinv.component.html',
  styleUrls: ['./reg-edi-auxinv.component.css']
})
export class RegEdiAuxinvComponent implements OnInit {

  forma: FormGroup;
  mensajeError = null;
  mensajeExito = null;

  gestion: string;
  username: string;

  convocatoria: Convocatoriainves;

  cambiarFoto = 'Seleccione';
  imagen: File;
  nombreImagen: string;


  accion = 'Registrar';
  nacionalidad = 'Boliviano';
  cod_deptos = ['LP', 'OR', 'PT', 'CB', 'SC', 'BN', 'PA', 'TJ', 'CH'];
  cursoId: number;

  usuario: Usuario = {
    paterno: '',
    materno: '',
    pri_nombre: '',
    seg_nombre: '',
    sexo: '',
    nacionalidad: '',
    ci: null,
    extendido: '',
    fec_nacimiento: '',
    celular: null,
    telefono: null,
    email: '',
    dom_ciudad: '',
    dom_zona: '',
    dom_calle: '',
    dom_numero: null,
    dom_otro: null
  };

  auxiliar: Auxiliar = {
    imgpath: '',
    reg_universitario: null,
    resena: '',
    descripcion: '',
    denominacion: '',
    convocatoriainvesId: 0,
    proyectoId: 0
  };

  // path 'auxiliars/:gestion/:username'
  constructor(private subirArchivoService: SubirArchivoService, private dataApiService: DataApiService, private router: Router,
              private activatedRoute: ActivatedRoute, private location: Location) {
    console.log('--------------- reg-edi-auxinv--------------');
  }

  ngOnInit() {
    this.cargarForma();
    this.activatedRoute.params.subscribe((data: any) => {
      this.gestion = data['gestion'];
      this.username = data['username'];
    });

    this.dataApiService
          .getRegistroByProperty('convocatoriainves', 'gestion', this.gestion)
            .subscribe((dataConvocatoria: any) => {
              this.convocatoria = dataConvocatoria;
            }, (dataConvocatoria) => {
              this.mensajeExito = null;
              this.mensajeError = dataConvocatoria.error.errro.message;
            });


    if (this.username !== 'nuevo') {
      this.accion = 'Actualizar';
      this.cambiarFoto = 'Seleccione para cambiar la';

      this.dataApiService
            .getUsuarioIncluidoTipo(this.username, 'auxiliar')
              .subscribe((dataUsuario: any) => {
                console.log(dataUsuario);

                if (dataUsuario.nacionalidad !== 'Boliviano') {
                this.nacionalidad = 'Extranjero';
                }
                this.auxiliar = dataUsuario.auxiliar;
                delete this.usuario['auxiliar'];
                for (const key in this.usuario) {
                  if (this.usuario.hasOwnProperty(key)) {
                    this.usuario[key] = dataUsuario[key];
                  }
                }
                this.usuario.fec_nacimiento = (<string>dataUsuario.fec_nacimiento).substring(0, 10);
                this.usuario.id = dataUsuario.id;

                this.usuario['reg_universitario'] = this.auxiliar.reg_universitario;
                this.usuario['resena'] = this.auxiliar.resena;
                this.usuario['descripcion'] = this.auxiliar.descripcion;
                this.usuario['denominacion'] = this.auxiliar.denominacion;
                this.forma.setValue(this.usuario);
              });
    }
  }

  regAuxiliar() {
    let imgAnt: string;
    this.auxiliar.denominacion = this.forma.value.denominacion;
    this.auxiliar.reg_universitario = this.forma.value.reg_universitario;
    this.auxiliar.descripcion = this.forma.value.descripcion;
    this.auxiliar.resena = this.forma.value.resena;
    this.usuario =  this.forma.value;
    delete this.usuario['denominacion'];
    delete this.usuario['reg_universitario'];
    delete this.usuario['descripcion'];
    delete this.usuario['resena'];
    this.usuario.paterno = this.retornaFormatoOK(this.usuario.paterno);
    this.usuario.materno = this.retornaFormatoOK(this.usuario.materno);
    this.usuario.pri_nombre = this.retornaFormatoOK(this.usuario.pri_nombre);
    this.usuario.seg_nombre = this.retornaFormatoOK(this.usuario.seg_nombre);
    this.usuario.username = this.usuario.paterno[0] + this.usuario.materno[0] + this.usuario.pri_nombre[0] + this.usuario.ci;
    this.usuario.password = this.usuario.ci.toString();

    if (this.username === 'nuevo') {

    this.auxiliar.imgpath = this.nombreImagen;
      this.dataApiService
            .postModelo('usuarios', this.usuario)
              .subscribe((dataUsuario: any) => {
               console.log('Usuario Creado', dataUsuario);

              this.dataApiService
                    .postModeloRelacionado('usuarios', dataUsuario.id, 'auxiliar', this.auxiliar)
                      .subscribe((dataAuxiliar: any) => {
                          this.relacionarAuxConv(dataAuxiliar);
                          console.log('Auxiliar Creado', dataAuxiliar);
                          this.subirImagen(this.imagen);
                          this.cargarForma();
                          this.mensajeExito = `${dataUsuario.paterno} ${dataUsuario.pri_nombre}`;

                      }, (errorPostAuxiliar) => {
                        this.mensajeExito = null;
                        this.mensajeError = errorPostAuxiliar.error.error.message;
                        this.dataApiService
                          .delModelo('usuarios', dataUsuario.id)
                            .subscribe(dataDelUsuario => {
                              console.log('Usuario Eliminado', dataDelUsuario);
                            }, (errorDelUsuario) => {
                              this.mensajeExito = null;
                              this.mensajeError = errorDelUsuario.error.error.message;
                            });
                      });
              }, (errorServicioUsu) => {
                this.mensajeExito = null;
                this.mensajeError = errorServicioUsu.error.error.message;
              });

    } else {
      if (this.nombreImagen) {  // si solo cambia el texto monbreImagen esta vacio => no cambia la imagen
        imgAnt = this.auxiliar.imgpath;
        this.auxiliar.imgpath = this.nombreImagen;
      }
      this.dataApiService
            .putModelo('usuarios', this.usuario)
              .subscribe((dataPutUsuario: any) => {
                console.log('Usuario Actualizado', dataPutUsuario);

                this.dataApiService
                      .putModeloRelacionado('usuarios', this.usuario.id, 'auxiliar', this.auxiliar)
                        .subscribe((dataPutAuxiliar: any) => {
                          console.log('Auxiliar Actualizado', dataPutAuxiliar);

                          if (this.nombreImagen) {  // si solo cambia el texto monbreImagen esta vacio => no cambia la imagen
                            this.subirArchivoService.delImage('inveAuxiliares', imgAnt).subscribe( dataE => console.log('Imagen Eliminida'));
                            this.subirImagen(this.imagen);
                          }
                          this.mensajeError = null;
                          this.mensajeExito = this.usuario.username;
                        }, (errorPutAuxiliar) => {
                          this.mensajeExito = null;
                          this.mensajeError = errorPutAuxiliar.error.error.message;
                        });
              }, (errorServicioPutUsu) => {
                this.mensajeExito = null;
                this.mensajeError = errorServicioPutUsu.error.error.message;
              });

    }
    window.scrollTo(0, 0);
  }

  relacionarAuxConv(auxiliar: Auxiliar) {
    auxiliar.convocatoriainvesId = this.convocatoria.id;
    this.dataApiService
          .putModelo('auxiliars', auxiliar)
            .subscribe((dataPutAuxConv: any) => {
              console.log('AuxConv Actualizado', dataPutAuxConv);
            }, errorPutAuxConv => {
              console.log('errorPutAuxConv', errorPutAuxConv);
            });
  }


  subirImagen(imagen: File) {
    this.subirArchivoService
        .subirArchivo(imagen, this.nombreImagen, 'inveAuxiliares', 'b')
          .then()
          .catch(datac => console.log(datac));
  }

  selimg( archivo: File ) {
    if ( !archivo ) {
      this.imagen = null;
      console.log('vacio');
      return;
    }
    this.imagen = archivo;
    const nombreCortado = this.imagen.name.split('.');
    const extensionArchivo = nombreCortado.pop();
    const name = Date.now().toString();
    this.nombreImagen = name + '.' + extensionArchivo;
    console.log('nombreImagen', this.nombreImagen);
  }

  quitarSeleccionImagen() {
    this.nombreImagen = null;
    const inputFile = <HTMLInputElement>document.getElementById('exampleFormControlFileAuxiliar');
    inputFile.value = null;
  }

  retornaFormatoOK(nombre: string) {
    if (nombre === '') { return nombre; }
    nombre = nombre.trim();
    nombre = nombre.toLowerCase();
    nombre = nombre.charAt(0).toUpperCase() + nombre.slice(1);
    return nombre;
  }

  cargarForma() {
    this.quitarSeleccionImagen();
    this.nacionalidad = 'Boliviano';
    this.mensajeError = null;
    this.mensajeExito = null;
    this.forma = new FormGroup ({
      'paterno': new FormControl(''),
      'materno': new FormControl(''),
      'pri_nombre': new FormControl(''), // , [Validators.required, Validators.minLength(2)]
      'seg_nombre': new FormControl(''),
      'sexo': new FormControl(''), // , [Validators.required]
      'nacionalidad': new FormControl('Boliviano'),
      'ci': new FormControl(''), // , [Validators.required, Validators.minLength(5)]
      'extendido': new FormControl(''), // , [Validators.required]
      'fec_nacimiento': new FormControl('', []),
      'celular': new FormControl(''), // , [Validators.required, Validators.minLength(8)]
      'telefono': new FormControl(''),
      'reg_universitario': new FormControl(''),
      'resena': new FormControl(''),
      'denominacion': new FormControl(''),
      'descripcion': new FormControl(''),
      'email': new FormControl(''), // , [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]
      'dom_ciudad': new FormControl('', []),
      'dom_zona': new FormControl('', []),
      'dom_calle': new FormControl('', []),
      'dom_numero': new FormControl('', []),
      'dom_otro': new FormControl(''),
      'id': new FormControl()
    });
  }

  back() {
    this.location.back();
  }

}
