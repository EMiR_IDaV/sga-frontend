import { Component, OnInit } from '@angular/core';
import { DataApiService } from '../../../services/data-api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Convocatoriainves } from '../../../interfaces/convocatoriainves.interface';

@Component({
  selector: 'app-investigaciones',
  templateUrl: './investigaciones.component.html',
  styleUrls: ['./investigaciones.component.css']
})
export class InvestigacionesComponent implements OnInit {

  mensaje: string;
  convocatorias: Convocatoriainves[] = [];

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private dataApiService: DataApiService) { }

  ngOnInit() {
    this.dataApiService
          .getRegistros('convocatoriainves')
            .subscribe((dataConvocatorias: any) => {
              this.convocatorias = dataConvocatorias;
              console.log(this.convocatorias);
            }, errorConvocatorias => {
              this.mensaje = errorConvocatorias.error.error.message;
            });
  }

  irPaginaArriba() {
    // this.router.navigate(['../../'], { relativeTo: this.activatedRoute });
    // this.dataApiService.retrocederPagina();
    this.dataApiService.irPaginaArriba(this.activatedRoute);
  }

}
