import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Proyecto } from '../../../../interfaces/proyecto.interface';
import { ActivatedRoute, Router } from '@angular/router';
import { DataApiService } from '../../../../services/data-api.service';
import { SubirArchivoService } from '../../../../services/subir-archivo.service';
import { Usuario } from '../../../../interfaces/usuario.interface';
import { Convocatoriainves } from '../../../../interfaces/convocatoriainves.interface';
import { Investiga } from '../../../../interfaces/investiga.interface';
import { Publica } from '../../../../interfaces/publica.interface';

@Component({
  selector: 'app-reg-edi-inv',
  templateUrl: './reg-edi-inv.component.html',
  styleUrls: ['./reg-edi-inv.component.css']
})
export class RegEdiInvComponent implements OnInit {

  forma: FormGroup;
  forma1: FormGroup;

  cambiarFoto = 'Seleccione';
  accion = 'Registrar';

  gestion: string;
  username: string;
  titulo: string;

  convocatoria: Convocatoriainves;
  usuarioDocente: Usuario;
  investiga: Investiga;

  mensajeError: string;
  mensajeExito: string;

  imagenes: File[];
  nombreImagen: string;
  resumen = '';

  nombreDocumento = '';
  documentoFile: File;
  docPathAnterior = '';

  proyecto: Proyecto = {
    titulo: '',
    tipo: '',
    ambito: '',
    presentacion: '',
    abstract: '',
    fecha_publicacion: '',
    imgpath: '',
    docpath: ''
  };

  publica: Publica = {
    investigaId: 0,
    proyectoId: 0
  };

  invExistente = false;
  proyectosExistentes: Proyecto[] = [];
  proyectoId = 0;
  indexProyecto: number;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private dataApiService: DataApiService, private subArcService: SubirArchivoService) {
    console.log('-------------------------------- reg-edi-inv --------------------------------');
  }

  ngOnInit() {
    this.cargarForma();
    this.activatedRoute.params.subscribe(data => {
                                                  this.gestion = data['gestion'];
                                                  this.titulo = data['titulo'];
                                                  this.username = data['username'];
                                                  console.log(data);
                                                 });

    this.titulo = this.dataApiService.eliminaGuionesParametro(this.titulo);

    this.dataApiService
          .getRegistroByProperty('convocatoriainves', 'gestion', this.gestion)
            .subscribe((dataConvocatoria: any) => {
              this.convocatoria = dataConvocatoria;

              if (this.titulo === 'nuevo') {
                this.dataApiService
                      .getProyectosInvesPorConvocatoria(this.convocatoria.id)
                        .subscribe((dataProyectos: any) => {
                          this.proyectosExistentes = dataProyectos;
                          console.log(this.proyectosExistentes);
                        }, errorProyectos => {
                          console.log('errorProyectos', errorProyectos);
                        });

                this.dataApiService
                      .getUsuarioIncluidoTipo(this.username, 'investigador')
                        .subscribe( (dataUsDocente: any) => {
                          this.usuarioDocente = dataUsDocente;
                          console.log(this.usuarioDocente);

                          this.dataApiService
                                .getInvestiga(this.convocatoria.id, this.usuarioDocente.investigador.id)
                                  .subscribe((dataInvestiga: any) => {
                                    this.investiga = dataInvestiga;
                                    console.log(this.investiga);
                                  }, errorInvestiga => {
                                    console.log('errorInvestiga', errorInvestiga);
                                  });

                        }, errorUsDocente => {
                          this.mensajeError = errorUsDocente.error.error.message;
                        });

              } else {
                this.accion = 'Editar';
                this.cambiarFoto = 'Seleccione para cambiar la';
                this.titulo = this.dataApiService.eliminaGuionesParametro(this.titulo);

                this.dataApiService
                      .getRegistroByProperty('proyectos', 'titulo', this.titulo)
                        .subscribe((dataProyecto: any) => {
                          console.log(dataProyecto);
                          this.proyecto = dataProyecto;
                          this.proyecto.fecha_publicacion = (<string>this.proyecto.fecha_publicacion).substring(0, 10);
                          this.forma.setValue(this.proyecto);

                        }, errorProyecto => {
                          this.mensajeExito = null;
                          this.mensajeError = errorProyecto.error.error.message;
                        });
              }

            }, errorConvocatoria => {
              this.mensajeError = errorConvocatoria.error.error.message;
            });
  }

  regProyecto() {
    let imgsAnt: string;
    let docAnt: string;
    this.proyecto = this.forma.value;
    this.proyecto.titulo = this.proyecto.titulo.trim();
    if (this.titulo === 'nuevo') {
      this.proyecto.imgpath =  this.nombreImagen;
      this.proyecto.docpath =  this.nombreDocumento;
      this.dataApiService
            .postModelo('proyectos', this.proyecto)
              .subscribe((dataProyecto: any) => {
                console.log('Proyecto Creado', dataProyecto);
                this.proyecto = dataProyecto;
                this.subirDocumento(this.documentoFile);
                this.subirImagenes(this.imagenes);

                this.publica.investigaId = this.investiga.id;
                this.publica.proyectoId = this.proyecto.id;

                this.regPublica(this.proyecto.titulo);

              }, (errorProyecto) => {
                this.mensajeExito = null;
                this.mensajeError = errorProyecto.error.error.message;
              });
    } else {

      if (this.nombreImagen) {
        imgsAnt = this.proyecto.imgpath;
        this.proyecto.imgpath = this.nombreImagen;
      }
      if (this.nombreDocumento) {
        docAnt = this.proyecto.docpath;
        this.proyecto.docpath = this.nombreDocumento;
      }
      this.dataApiService
              .putModelo('proyectos', this.proyecto)
                .subscribe((dataPutProyecto: any) => {
                  console.log('Proyecto Actualizado', dataPutProyecto);
                  if (this.nombreImagen) {  // si solo cambia el texto monbreImagen esta vacio => no cambia la imagenes
                    const nombresImagenes = imgsAnt.split(',');
                    for (let index = 0; index < nombresImagenes.length; index++) {
                      const element = nombresImagenes[index];
                      this.subArcService.delImage('inveDocsImagenes', element).subscribe( dataE => console.log('Imagen Eliminada'));
                    }
                    this.subirImagenes(this.imagenes);
                  }
                  if (this.nombreDocumento) {
                    this.subirDocumento(this.documentoFile);

                      this.subArcService
                          .delImage('inveDocumentos', docAnt)
                            .subscribe(eliDocumento => {
                              console.log('eliInveDocumentos', eliDocumento);
                            }, errorEliDocumento => {
                              console.log('errorEliInveDocumentos', errorEliDocumento);
                            });
                  }

                  this.mensajeExito = this.proyecto.titulo;
                  this.mensajeError = null;
                }, (errorPutProyecto) => {
                  this.mensajeError = errorPutProyecto.error.error.message;
                  this.mensajeExito = null;
                });
    }
    window.scrollTo(0, 0);
  }

  regPublica(menExito: string) {
    this.mensajeExito = null;
    this.mensajeError = null;
    this.dataApiService
          .putModelo('publicas', this.publica)
            .subscribe((dataPutPublica: any) => {
              this.publica = dataPutPublica;
              console.log('Publica Creado', this.publica);
              this.cargarForma();
              this.mensajeExito = menExito;
              setTimeout(() => {
                this.router.navigate(['/admin', 'investigacion', 'convocatorias', this.gestion, 'docentes-investigadores']);
               }, 4000);
            }, errorPutPublica => {
              if (errorPutPublica.error.error.message.includes('Duplicate entry')) {
                this.mensajeError = 'El proyecto ya esta asociado a este Investigador';
              } else {
                console.log('errorPutPublica', errorPutPublica);
              }
            });
  }

  subirDocumento(documentoFile: File) {
    this.subArcService
        .subirArchivo(documentoFile, this.nombreDocumento, 'inveDocumentos', 'b')
          .then()
          .catch(datac => console.log(datac));
  }

  subirImagenes(imagenes: File[]) {
    const nomImagenes = this.nombreImagen.split(',');
    for (let index = 0; index < imagenes.length; index++) {
      const element = imagenes[index];
      this.subArcService
          .subirArchivo(element, nomImagenes[index], 'inveDocsImagenes', 'b')
            .then()
            .catch(datac => console.log(datac));
    }
  }

  selimg( archivo: File[] ) {
    this.nombreImagen = '';
    if ( !archivo ) {
      this.imagenes = null;
      return;
    }
    this.imagenes = archivo;

    console.log(archivo);
    for (let index = 0; index < archivo.length; index++) {
      const element = archivo[index];
      const nombreCortado = element.name.split('.');
      const extensionArchivo = nombreCortado[ nombreCortado.length - 1 ];
      let name = Date.now().toString();
      name = `${index}-${name}`;
      name = name + '.' + extensionArchivo;
      this.nombreImagen = this.nombreImagen + name;

      if (index !== archivo.length - 1) {
        this.nombreImagen = this.nombreImagen + ',';
      }

    }
    console.log('nombre Imagenes', this.nombreImagen);
  }

  selDocumento( archivo: File ) {
    this.nombreDocumento = '';
    if ( !archivo ) {
      this.documentoFile = null;
      console.log('vacio');
      return;
    }
    this.documentoFile = archivo;
    const nombreCortado = this.documentoFile.name.split('.');
    const extensionArchivo = nombreCortado.pop();
    const name = Date.now().toString();
    this.nombreDocumento = name + '.' + extensionArchivo;
    console.log('nombre Documento', this.nombreDocumento);
  }

  registrarProyectoExistente() {
    this.publica.investigaId = this.investiga.id;
    this.publica.proyectoId = this.proyectoId;
    this.regPublica(this.proyectosExistentes[this.indexProyecto].titulo);
  }

  selectElegido(id: string) {
    const valueSelect = id.split(':');
    this.indexProyecto = parseInt(valueSelect[0], 10);
    const proyectoId = parseInt(valueSelect[1], 10);
    console.log(proyectoId);
    this.proyectoId = proyectoId;
  }

  quitarSeleccionProyecto() {
    this.nombreDocumento = null;
    const inputFile = <HTMLInputElement>document.getElementById('exampleFormControlFileProyecto');
    inputFile.value = null;
  }

  quitarSeleccionImagenes() {
    this.nombreImagen = null;
    const inputFile = <HTMLInputElement>document.getElementById('exampleFormControlFileProyImagenes');
    inputFile.value = null;
  }

  cargarForma() {
    this.mensajeError = null;
    this.mensajeExito = null;
    this.forma = new FormGroup ({
      'titulo': new FormControl(''), // , [Validators.required, Validators.minLength(2)]
      'tipo': new FormControl(''), // , [Validators.required, Validators.minLength(2)]
      'ambito': new FormControl(''), // , [Validators.required, Validators.minLength(2)]
      'presentacion': new FormControl(''), // , [Validators.required, Validators.minLength(5)]
      'abstract': new FormControl('', []), // , [Validators.required]
      'fecha_publicacion': new FormControl(''),
      'imgpath': new FormControl(''),
      'docpath': new FormControl(''),
      'id': new FormControl()
    });

    this.forma1 = new FormGroup ({
      'control1': new FormControl('')
    });
  }

  irPaginaArriba() {
    this.dataApiService.retrocederPagina();
    // this.dataApiService.irPaginaArriba(this.activatedRoute, true);
  }

}

