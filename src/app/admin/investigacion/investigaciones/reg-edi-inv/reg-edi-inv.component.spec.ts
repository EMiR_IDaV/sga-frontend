import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegEdiInvComponent } from './reg-edi-inv.component';

describe('RegEdiInvComponent', () => {
  let component: RegEdiInvComponent;
  let fixture: ComponentFixture<RegEdiInvComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegEdiInvComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegEdiInvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
