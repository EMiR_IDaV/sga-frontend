import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Convocatoriainves } from '../../../../interfaces/convocatoriainves.interface';
import { Proyecto } from '../../../../interfaces/proyecto.interface';
import { DataApiService } from '../../../../services/data-api.service';

@Component({
  selector: 'app-list-inv',
  templateUrl: './list-inv.component.html',
  styleUrls: ['./list-inv.component.css']
})
export class ListInvComponent implements OnInit {

  gestion: string;
  convocatoria: Convocatoriainves;
  mensaje: string;

  proyectos: Proyecto[] = [];
  proyecto: Proyecto;
  index: number;
  ussDocsInvestigadoresPorProyecto: any[] = [];
  // invesPorProyecto: any[] = [];

  constructor(private activatedRoute: ActivatedRoute, private dataApiService: DataApiService, private router: Router) {
    console.log('--------------------------- investigaciones ----------------------------');
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(data => { this.gestion = data['gestion']; });
    this.dataApiService
          .getRegistroByProperty('convocatoriainves', 'gestion', this.gestion)
            .subscribe((dataConvocatoria: any) => {
              this.convocatoria = dataConvocatoria;

              this.dataApiService
                    .getProyectosInvesPorConvocatoria(this.convocatoria.id)
                      .subscribe((dataProyectos: any) => {
                        this.proyectos = dataProyectos;
                        console.log(this.proyectos);
                        (this.proyectos.length > 0) ? this.cargarDocsInvestigadoresPorProyecto() : this.mensaje = 'No hay proyectos registrados en esta convocatoria';
                      }, errorInvestigadores => {
                        this.mensaje = errorInvestigadores.error.error.message;
                      });

            }, errorConvocatoria => {
              this.mensaje = errorConvocatoria.error.error.message;
            });
  }
  cargarDocsInvestigadoresPorProyecto() {
    for (let index = 0; index < this.proyectos.length; index++) {
      const element = this.proyectos[index];

      this.dataApiService
            .getDocsInvestigadoresPorConvocatoriaYProyecto(this.convocatoria.id, element.id)
              .subscribe((dataUssDocsInvs: any) => {
                this.ussDocsInvestigadoresPorProyecto[index] = dataUssDocsInvs;
                console.log(this.ussDocsInvestigadoresPorProyecto[index]);
              }, errorUssDocsInvs => {
                console.log('errorUssDocsInvs', errorUssDocsInvs);
              });
    }
  }

  eliModal(proyecto: Proyecto, index: number) {                    // Posesiona sobre el Admin a eliminar
    this.proyecto = proyecto;
    this.index = index;
  }

  eliProyecto() {              // no es necesario el est e i, ya que estan en this.es y this.in PARA VER

    this.dataApiService
          .delModelo('proyectos', this.proyecto.id)
            .subscribe(dataDelProyecto => {
              console.log('Proyecto Eliminado', dataDelProyecto);

              // falta Eliminar fotos y Documento.
              this.dataApiService.eliminaArchivos('inveDocsImagenes', this.proyecto.imgpath);
              this.dataApiService.eliminaArchivos('inveDocumentos', this.proyecto.docpath);

              this.dataApiService
                    .getRegistrosPorFiltros('publicas', 'proyectoId', this.proyecto.id)
                      .subscribe((dataPublicas: any) => {
                        console.log('dataPublicas', dataPublicas);

                        for (let index = 0; index < dataPublicas.length; index++) {
                          const element = dataPublicas[index];

                          this.dataApiService
                                .delModelo('publicas', element.id)
                                  .subscribe(dataEliPublica => {
                                    console.log('dataEliPublica', dataEliPublica);
                                  }, errorDelPublica => {
                                    console.log('errorDelPublica', errorDelPublica);
                                  });
                        }

                      }, errorPublicas => {
                        console.log('errorPublicas', errorPublicas);
                      });
              delete this.proyectos[this.index];
              this.proyectos = this.proyectos.filter(Boolean);
            }, (errorDelProyecto) => this.mensaje = errorDelProyecto.error.error.message );
  }

  irPaginaArriba() {
    this.dataApiService.retrocederPagina();
    // this.dataApiService.irPaginaArriba(this.activatedRoute, true);
    // this.router.navigate(['../../'], { relativeTo: this.activatedRoute });

  }
}
