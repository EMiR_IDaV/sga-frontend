import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocentesInvestigadoresComponent } from './docentes-investigadores.component';

describe('DocentesInvestigadoresComponent', () => {
  let component: DocentesInvestigadoresComponent;
  let fixture: ComponentFixture<DocentesInvestigadoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocentesInvestigadoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocentesInvestigadoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
