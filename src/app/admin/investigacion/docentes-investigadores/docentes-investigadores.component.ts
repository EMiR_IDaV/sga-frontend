import { Component, OnInit, OnDestroy } from '@angular/core';
import { Usuario } from '../../../interfaces/usuario.interface';
import { PublicService } from '../../../services/public.service';
import { DataApiService } from '../../../services/data-api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SubirArchivoService } from '../../../services/subir-archivo.service';
import { Convocatoriainves } from '../../../interfaces/convocatoriainves.interface';
import { Proyecto } from '../../../interfaces/proyecto.interface';
import { Investiga } from '../../../interfaces/investiga.interface';

declare var $: any;

@Component({
  selector: 'app-docentes-investigadores',
  templateUrl: './docentes-investigadores.component.html',
  styleUrls: ['./docentes-investigadores.component.css']
})
export class DocentesInvestigadoresComponent implements OnInit {

  mensaje: string;
  convocatorias: Convocatoriainves[] = [];

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private dataApiService: DataApiService) { }

  ngOnInit() {
    this.dataApiService
          .getRegistros('convocatoriainves')
            .subscribe((dataConvocatorias: any) => {
              this.convocatorias = dataConvocatorias;
              console.log(this.convocatorias);
            }, errorConvocatorias => {
              this.mensaje = errorConvocatorias.error.error.message;
            });
  }

  irPaginaArriba() {
    // this.router.navigate(['../../'], { relativeTo: this.activatedRoute });
    // this.dataApiService.retrocederPagina();
    this.dataApiService.irPaginaArriba(this.activatedRoute);
  }

}
