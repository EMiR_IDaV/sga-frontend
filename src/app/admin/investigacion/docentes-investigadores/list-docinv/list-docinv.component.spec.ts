import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListDocinvComponent } from './list-docinv.component';

describe('ListDocinvComponent', () => {
  let component: ListDocinvComponent;
  let fixture: ComponentFixture<ListDocinvComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListDocinvComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListDocinvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
