import { Component, OnInit, OnDestroy } from '@angular/core';
import { Convocatoriainves } from '../../../../interfaces/convocatoriainves.interface';
import { Usuario } from '../../../../interfaces/usuario.interface';
import { Proyecto } from '../../../../interfaces/proyecto.interface';
import { Investiga } from '../../../../interfaces/investiga.interface';
import { DataApiService } from '../../../../services/data-api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SubirArchivoService } from '../../../../services/subir-archivo.service';

declare var $: any;

@Component({
  selector: 'app-list-docinv',
  templateUrl: './list-docinv.component.html',
  styleUrls: ['./list-docinv.component.css']
})
export class ListDocinvComponent implements OnInit, OnDestroy {

  gestion: string;
  convocatoria: Convocatoriainves;

  mensaje: string;
  mensajeError: string;
  quitarEliminar = 'quitar';
  quitarEliminarProyecto = 'quitar';

  invesPorDocente: any[] = [];
  proyectosPorDocente: any[] = [];
  carga = false;

  usuario: Usuario = null;
  index: number = null;
  index2: number;

  nombreCurso: string;
  ussInvestigadores: Usuario[] = [];
  proyecto: Proyecto;

  investiga: Investiga;

  constructor(private dataApiService: DataApiService, private router: Router,
              private activatedRoute: ActivatedRoute, private subirArchivoService: SubirArchivoService) {
    console.log('---------------- list-docinv-investigadores ---------------');
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(data => { this.gestion = data['gestion']; });

    this.dataApiService
          .getRegistroByProperty('convocatoriainves', 'gestion', this.gestion)
            .subscribe((dataConvocatoria: any) => {
              this.convocatoria = dataConvocatoria;

              this.cargarInvestigadores();

            }, errorConvocatoria => {
              this.mensaje = errorConvocatoria.error.error.message;
            });
  }

  cargarInvestigadores() {
    this.dataApiService
          .getInvestigadoresPorConvocatoria(this.convocatoria.id)
            .subscribe((dataInvestigadores: any) => {
              this.ussInvestigadores = dataInvestigadores;
              console.log(this.ussInvestigadores);
              this.cargarProyectosPorInvestigador();
            }, errorInvestigadores => {
              this.ussInvestigadores = [];
              this.mensaje = errorInvestigadores.error.error.message;
              this.mensajeError = 'No hay investigadores en esta convocatoria';
            });
  }

  cargarProyectosPorInvestigador() {
    for (let index = 0; index < this.ussInvestigadores.length; index++) {
      const element = this.ussInvestigadores[index];
      this.dataApiService
        .getRegistroPorFiltros('investigas', 'convocatoriainvesId', this.convocatoria.id, 'investigadorId', element.investigador.id)
          .subscribe((dataInvestiga: any) => {
            this.invesPorDocente[index] = dataInvestiga;
            // this.investiga = dataInvestiga;
            // console.log(this.investiga);

            this.dataApiService
                  .getRegistrosPorFiltros('publicas', 'investigaId', this.invesPorDocente[index].id)
                    .subscribe((dataPublicaciones: any) => {
                      // console.log(dataPublicaciones);
                      const proyectosDocente: any [] = [];
                      for (let index1 = 0; index1 < dataPublicaciones.length; index1++) {
                        const element1 = dataPublicaciones[index1];

                        this.dataApiService
                              .getRegistro('proyectos', element1.proyectoId)
                                .subscribe((dataProyectos: any) => {
                                  proyectosDocente[index1] = dataProyectos;
                                  // console.log(proyectosDocente);
                                }, errorProyecto => {
                                  console.log('errorProyecto', errorProyecto);
                                });
                      }
                      this.proyectosPorDocente[index] = proyectosDocente;
                  }, errorPublica => {
                    console.log('errorPublica', errorPublica);
                  });
          }, errorInvestiga => {
            console.log('errorInvestiga', errorInvestiga);
          });
    }
  }

  elimodal(usuario: Usuario, index: number) {
    this.usuario = usuario;
    console.log(usuario);
    this.index = index;

    this.investiga = this.invesPorDocente[this.index];

    this.quitarEliminar = 'quitar';
    this.dataApiService
          .countRegistros('investigas', 'investigadorId', this.usuario.investigador.id)
            .subscribe(dataNumero => {
              if (dataNumero['count'] === 1) {
                this.quitarEliminar = 'ELIMINAR';
              } else {
              }
            }, errorNumeroDocentesModulo => {
              console.log('errorNumeroDocentesModulo', errorNumeroDocentesModulo);
            });
  }

  quitarDocenteInv() {

    this.dataApiService
          .getInvestiga(this.convocatoria.id, this.usuario.investigador.id)
            .subscribe((dataInvestiga: any) => {
              this.investiga = dataInvestiga;
              console.log(this.investiga);
              // console.log(this.usuario);

              this.dataApiService
                    .delModelo('investigas', this.investiga.id)
                      .subscribe(dataEliInvestiga => {
                        console.log('dataEliInvestiga', dataEliInvestiga);

                        if (this.quitarEliminar === 'ELIMINAR') {
                          this.eliminarDocenteInv();
                        }

                        this.eliminarProyectosDocente();
                      }, errorEliInvestiga => {
                        console.log('errorEliInvestiga', errorEliInvestiga);
                      });
            }, errorInvestiga => {
              console.log('errorInvestiga', errorInvestiga);
            });
  }

  elimodalProyecto(usuario: Usuario, proyecto: Proyecto, index: number, index2: number) {
    console.log(index, index2);
    this.usuario = usuario;
    this.proyecto = proyecto;
    this.index = index;
    this.index2 = index2;

    this.quitarEliminarProyecto = 'quitar';
    this.dataApiService
          .countRegistros('publicas', 'proyectoId', this.proyecto.id)
            .subscribe(dataNumero => {
              if (dataNumero['count'] === 1) {
                this.quitarEliminarProyecto = 'ELIMINAR';
              } else {
              }
            }, errorNumeroDocentesModulo => {
              console.log('errorNumeroDocentesModulo', errorNumeroDocentesModulo);
            });
  }

  quitarProyecto() {
    this.dataApiService
          .getInvestiga(this.convocatoria.id, this.usuario.investigador.id)
            .subscribe((dataInvestiga: any) => {
              console.log('dataInvestiga', dataInvestiga);
              this.dataApiService
                    .getRegistroPorFiltros('publicas', 'investigaId', dataInvestiga.id, 'proyectoId', this.proyecto.id)
                      .subscribe( (dataPublica: any) => {
                        console.log('dataPublica', dataPublica);

                        this.dataApiService
                              .delModelo('publicas', dataPublica.id)
                                .subscribe(dataDelPublica => {
                                  console.log('Publica Eliminada', dataDelPublica);
                                  if (this.quitarEliminarProyecto === 'ELIMINAR') {
                                    this.eliminarProyecto(this.proyecto);
                                    // this.quitarEliminarProyecto = 'quitar';
                                  }
                                  this.cargarProyectosPorInvestigador(); // Funciona, pero se podria mejorar
                                }, errorDelPublica => {
                                  console.log('errorDelPublica', errorDelPublica);
                                });


                      }, errorPublica => {
                        console.log('errorPublica', errorPublica);
                      });
            }, errorInvestiga => {
              console.log('errorInvestiga', errorInvestiga);
            });
  }

  eliminarProyecto(proyect: Proyecto) {
    this.dataApiService
          .delModelo('proyectos', proyect.id)
            .subscribe((dataDelProyecto: any) => {
              console.log('dataDelProyecto', dataDelProyecto);

              this.dataApiService.eliminaArchivos('inveDocsImagenes', proyect.imgpath);
              this.dataApiService.eliminaArchivos('inveDocumentos', proyect.docpath);

            }, errorDelProyecto => {
              console.log('errorDelProyecto', errorDelProyecto);
            });
  }

  eliminarDocenteInv() {
    this.dataApiService
          .getUsuarioIncluidoTipo(this.usuario.username, 'investigador')
            .subscribe((dataUsDocente: any) => {
              this.usuario = dataUsDocente;

              this.dataApiService
                    .delModeloRelacionado('usuarios', this.usuario.id, 'investigador')
                      .subscribe(dataDelDocente => {
                        console.log('Docente Eliminado', dataDelDocente);

                        this.subirArchivoService
                              .delImage('inveDocentes', this.usuario.investigador.imgpath)
                                .subscribe(dataEliImagenDocente => {
                                  console.log('dataEliImagen', dataEliImagenDocente);
                                }, errorEliImagenDocente => {
                                  console.log('errorEliImagenDocente', errorEliImagenDocente);
                                });

                        this.dataApiService
                              .delModelo('usuarios', this.usuario.id)
                                .subscribe(dataDelUsuario => {
                                  console.log('Usuario Eliminado', dataDelUsuario);
                                }, (errorDelUsuario) => {
                                  this.mensaje = (errorDelUsuario.error.error.message);
                                });

                      }, (errorDelDocente) => {
                        this.mensaje = (errorDelDocente.error.error.message);
                      });

            }, errorUsDocente => {
              console.log(errorUsDocente);
            });
  }

  eliminarProyectosDocente() {

    for (let index = 0; index < this.proyectosPorDocente[this.index].length; index++) {
      const element = this.proyectosPorDocente[this.index][index];

      this.dataApiService
            .getRegistroPorFiltros('publicas', 'investigaId', this.investiga.id, 'proyectoId', element.id)
              .subscribe((dataPublica: any) => {
                console.log(dataPublica);

                this.dataApiService
                      .delModelo('publicas', dataPublica.id)
                        .subscribe((dataDelPublica: any) => {
                          console.log('dataDelPublica', dataDelPublica);

                          this.dataApiService
                                .countRegistros('publicas', 'proyectoId', element.id)
                                  .subscribe(dataNumero => {
                                    if (dataNumero['count'] === 0) {
                                      this.eliminarProyecto(element);
                                    }

                                    if (index === this.proyectosPorDocente[this.index].length - 1) {
                                      this.cargarInvestigadores();
                                    }
                                  }, errorNumeroPublicaProyectoId => {
                                    console.log('errorNumeroPublicaProyectoId', errorNumeroPublicaProyectoId);
                                  });

                        }, errorDelPublica => {
                          console.log('errorDelPublica', errorDelPublica);
                        });

              }, errorPublica => {
                console.log(errorPublica);
              });
    }

    if (this.proyectosPorDocente[this.index].length === 0) {
      this.cargarInvestigadores();
    }
  }

  irPaginaArriba() {
    this.dataApiService.retrocederPagina();
    // this.dataApiService.irPaginaArriba(this.activatedRoute);
    // this.router.navigate(['../../'], { relativeTo: this.activatedRoute });
  }

  ngOnDestroy() {
    // $('#ModalEstudiante').modal('hide');
    // $('#ModalEstudiante').modal('dispose');
    $('.modal-backdrop').hide();
  }

}
