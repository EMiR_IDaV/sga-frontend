import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegEdiDocinvComponent } from './reg-edi-docinv.component';

describe('RegEdiDocinvComponent', () => {
  let component: RegEdiDocinvComponent;
  let fixture: ComponentFixture<RegEdiDocinvComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegEdiDocinvComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegEdiDocinvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
