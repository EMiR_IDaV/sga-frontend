import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Usuario } from '../../../../interfaces/usuario.interface';
import { DataApiService } from '../../../../services/data-api.service';
import { PublicService } from '../../../../services/public.service';
import { SubirArchivoService } from '../../../../services/subir-archivo.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Convocatoriainves } from '../../../../interfaces/convocatoriainves.interface';
import { Investiga } from '../../../../interfaces/investiga.interface';
import { Investigador } from '../../../../interfaces/investigador.interface';

@Component({
  selector: 'app-reg-edi-docinv',
  templateUrl: './reg-edi-docinv.component.html',
  styleUrls: ['./reg-edi-docinv.component.css']
})
export class RegEdiDocinvComponent implements OnInit {

  forma: FormGroup;
  forma1: FormGroup;

  username: string;
  gestion: string;
  convocatoria: Convocatoriainves;

  mensajeError = null;
  mensajeExito = null;
  accion = 'Registrar';
  nacionalidad = 'Boliviano';
  cod_deptos = ['LP', 'OR', 'PT', 'CB', 'SC', 'BN', 'PA', 'TJ', 'CH'];

  cambiarFoto = 'Seleccione';

  imagen: File;
  nombreImagen = '';

  usuario: Usuario = {
    paterno: '',
    materno: '',
    pri_nombre: '',
    seg_nombre: '',
    sexo: '',
    nacionalidad: '',
    ci: null,
    extendido: '',
    fec_nacimiento: '',
    celular: null,
    telefono: null,
    email: '',
    dom_ciudad: '',
    dom_zona: '',
    dom_calle: '',
    dom_numero: null,
    dom_otro: null
  };

  investigador: Investigador = {
    denominacion: '',
    resena: '',
    grado: '',
    descripcion: '',
    imgpath: ''
  };

  investiga: Investiga = {
    investigadorId: 0,
    convocatoriainvesId: 0
  };

  investigadorExistente = false;
  investigadores: Usuario[] = [];
  investigadorId = 0;
  indexInvestigador: number;

  tituloDescripcion = `$$Entre sus estudios pos-universitarios se encuentran:
$$En su desempeño profesional se pueden mencionar:`;

  constructor(private dataApiService: DataApiService, private publicService: PublicService, private router: Router,
              private actRoute: ActivatedRoute, private subArcService: SubirArchivoService) {
    console.log('--------------- reg-edi-investigador--------------');
  }

  ngOnInit() {
    this.cargarForma();
    this.actRoute.params.subscribe((data: any) => {
                                                    this.username = data['username'];
                                                    this.gestion = data['gestion'];
                                                  });
    this.dataApiService
          .getRegistroByProperty('convocatoriainves', 'gestion', this.gestion)
            .subscribe((dataConvocatoria: any) => {
              this.convocatoria = dataConvocatoria;

              this.dataApiService
                    .getInvestigadores()
                      .subscribe((dataDocsInvestigadores: any) => {
                        this.investigadores = dataDocsInvestigadores;
                        console.log(this.investigadores);
                      }, errorDocsinvestigadores => {
                        console.log('errorDocsinvestigadores', errorDocsinvestigadores);
                      });
            }, errorConvocatoria => {
              this.mensajeError = errorConvocatoria.error.error.message;
            });

    if (this.username !== 'nuevo') {
      this.accion = 'Actualizar';
      this.cambiarFoto = 'Seleccione para cambiar la';

      this.dataApiService
            .getUsuarioIncluidoTipo(this.username, 'investigador')
              .subscribe((dataUsuario: any) => {
                console.log(dataUsuario);

                if (dataUsuario.nacionalidad !== 'Boliviano') {
                this.nacionalidad = 'Extranjero';
                }
                this.investigador = dataUsuario.investigador;
                delete this.usuario['investigador'];
                for (const key in this.usuario) {
                  if (this.usuario.hasOwnProperty(key)) {
                    this.usuario[key] = dataUsuario[key];
                  }
                }
                this.usuario.fec_nacimiento = (<string>dataUsuario.fec_nacimiento).substring(0, 10);
                this.usuario.id = dataUsuario.id;

                this.usuario['denominacion'] = this.investigador.denominacion;
                this.usuario['resena'] = this.investigador.resena;
                this.usuario['grado'] = this.investigador.grado;
                this.usuario['descripcion'] = this.investigador.descripcion;
                this.forma.setValue(this.usuario);
              });
    }

  }

  registrarInvestigador() {
    let imgAnt: string;
    this.investigador.denominacion = this.forma.value.denominacion;
    this.investigador.resena = this.forma.value.resena;
    this.investigador.grado = this.forma.value.grado;
    this.investigador.descripcion = this.forma.value.descripcion;
    this.usuario = this.forma.value;
    this.usuario.username = this.usuario.paterno[0] + this.usuario.materno[0] + this.usuario.pri_nombre[0] + this.usuario.ci;
    this.usuario.password = this.usuario.ci.toString();

    if (this.username === 'nuevo') {
      this.investigador.imgpath = this.nombreImagen;

      this.dataApiService
            .postModelo('usuarios', this.usuario)
              .subscribe((dataUsuario: any) => {
                console.log('Usuario Creado', dataUsuario);

                this.dataApiService
                      .postModeloRelacionado('usuarios', dataUsuario.id, 'investigador', this.investigador)
                        .subscribe((dataInvestigador: any) => {
                          console.log('Investigador Creado', dataInvestigador);
                          this.subirImagen(this.imagen);

                          this.dataApiService
                                .putRelacionModelos('convocatoriainves', this.convocatoria.id, 'investigadors', dataInvestigador.id)
                                  .subscribe((dataPutRelacion: any) => {
                                    console.log('dataPutRelacion', dataPutRelacion);
                                    setTimeout(() => {
                                      this.router.navigate(['/admin', 'investigacion', 'convocatorias', this.gestion, 'docentes-investigadores']);
                                     }, 4000);
                                  }, errorPutRelacion => {
                                    this.mensajeExito = null;
                                    this.mensajeError = errorPutRelacion.error.error.message;
                                  });

                          this.cargarForma();

                          this.mensajeError = null;
                          this.mensajeExito = this.usuario.username;
                        }, (errorInvestigador) => {
                          this.mensajeExito = null;
                          this.mensajeError = errorInvestigador.error.error.message;
                          this.dataApiService
                            .delModelo('usuarios', dataUsuario.id)
                              .subscribe(dataDelUsuario => {
                                console.log('Usuario Eliminado', dataDelUsuario);
                              });
                        });
              }, (errorUsuario) => {
                this.mensajeExito = null;
                this.mensajeError = errorUsuario.error.error.message;
              });
    } else {
      if (this.nombreImagen !== '') {  // si solo cambia el texto monbreImagen esta vacio => no cambia la imagen
        imgAnt = this.investigador.imgpath;
        this.investigador.imgpath = this.nombreImagen;
      }
      this.dataApiService
            .putModeloRelacionado('usuarios', this.usuario.id, 'investigador', this.investigador)
              .subscribe((dataPutInvestigador: any) => {
                console.log('Investigador Actualizado', dataPutInvestigador);

                if (this.nombreImagen !== '') {  // si solo cambia el texto monbreImagen esta vacio => no cambia la imagen
                  this.subArcService.delImage('inveDocentes', imgAnt).subscribe( dataE => console.log('Imagen Eliminida'));
                  this.subirImagen(this.imagen);
                }
                this.actUsuario(this.usuario);
              }, (errorPutInvestigador) => {
                this.mensajeExito = null;
                this.mensajeError = errorPutInvestigador.error.error.message;
              });
    }
    window.scrollTo(0, 0);
  }

  actUsuario(usuario: Usuario) {
    this.dataApiService
          .putModelo('usuarios', usuario)
            .subscribe((dataPutUsuario: any) => {
              console.log('Usuario Actualizado', dataPutUsuario);
              this.mensajeError = null;
              this.mensajeExito = usuario.username;
            }, (errorPutUsuario) => {
              this.mensajeExito = null;
              this.mensajeError = errorPutUsuario.error.error.message;
            });
  }

  subirImagen(imagen: File) {
    this.subArcService
        .subirArchivo(imagen, this.nombreImagen, 'inveDocentes', 'b')
          .then()
          .catch(datac => console.log(datac));
  }

  selimg( archivo: File ) {
    this.nombreImagen = '';
    if ( !archivo ) {
      this.imagen = null;
      console.log('vacio');
      return;
    }
    this.imagen = archivo;
    const nombreCortado = this.imagen.name.split('.');
    const extensionArchivo = nombreCortado.pop();
    const name = Date.now().toString();
    this.nombreImagen = name + '.' + extensionArchivo;
    console.log('nombreImagen', this.nombreImagen);
  }

  registrarInvestigadorExistente() {
    this.investiga.investigadorId = this.investigadorId;
    this.investiga.convocatoriainvesId = this.convocatoria.id;
    this.regInvestiga(this.investigadores[this.indexInvestigador]);
  }


  regInvestiga(investigadorInvestigador: Usuario) {
    const menExito = `${investigadorInvestigador.investigador.grado} ${investigadorInvestigador.paterno} ${investigadorInvestigador.materno} ${investigadorInvestigador.pri_nombre}`;
    this.dataApiService
          .putModelo('investigas', this.investiga)
            .subscribe((dataPutInvestiga: any) => {
              this.investiga = dataPutInvestiga;
              console.log('Investiga Creada', this.investiga);
              this.cargarForma();
              this.mensajeExito = menExito;
              setTimeout(() => {
                this.router.navigate(['/admin', 'investigacion', 'convocatorias', this.gestion, 'docentes-investigadores']);
               }, 4000);
            }, errorPutInvestiga => {
              console.log('errorPutInvestiga', errorPutInvestiga);
            });
  }

  selectElegido(id: string) {
    const valueSelect = id.split(':');
    this.indexInvestigador = parseInt(valueSelect[0], 10);
    const investigadorId = parseInt(valueSelect[1], 10);
    console.log(investigadorId);
    this.investigadorId = investigadorId;
  }

  cargarForma() {
    this.nacionalidad = 'Boliviano';
    this.mensajeError = null;
    this.mensajeExito = null;
    this.forma = new FormGroup ({
      'paterno': new FormControl(''),
      'materno': new FormControl(''),
      'pri_nombre': new FormControl(''), // , [Validators.required, Validators.minLength(2)]
      'seg_nombre': new FormControl(''),
      'sexo': new FormControl(''), // , [Validators.required]
      'nacionalidad': new FormControl('Boliviano'),
      'ci': new FormControl(''), // , [Validators.required, Validators.minLength(5)]
      'extendido': new FormControl(''), // , [Validators.required]
      'fec_nacimiento': new FormControl('', []),
      'celular': new FormControl(''), // , [Validators.required, Validators.minLength(8)]
      'telefono': new FormControl(''),
      'denominacion': new FormControl(''),
      'email': new FormControl(''), // , [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]
      'dom_ciudad': new FormControl('', []),
      'dom_zona': new FormControl('', []),
      'dom_calle': new FormControl('', []),
      'dom_numero': new FormControl('', []),
      'dom_otro': new FormControl(''),
      'grado': new FormControl(''),
      'resena': new FormControl(''),
      'descripcion': new FormControl(this.tituloDescripcion),
      'id': new FormControl()
    });

    this.forma1 = new FormGroup ({
      'control1': new FormControl('')
    });
  }

  irPaginaArriba() {
    this.dataApiService.retrocederPagina();
    // this.dataApiService.irPaginaArriba(this.actRoute);
  }

}
