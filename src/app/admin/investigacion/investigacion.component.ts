import { Component, OnInit } from '@angular/core';
import { DataApiService } from '../../services/data-api.service';
import { UInvestigacion } from '../../interfaces/uInvestigacion.interface';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { SubirArchivoService } from '../../services/subir-archivo.service';

@Component({
  selector: 'app-investigacion',
  templateUrl: './investigacion.component.html',
  styleUrls: ['./investigacion.component.css']
})
export class InvestigacionComponent implements OnInit {

  formaUInvestigacion: FormGroup;
  mensaje: string;
  mensajeExito: string;

  numConvocatorias: any;
  numInvestigadores: any;
  numProyectos: any;
  numSitiosInteres: any;

  imagen: File;
  nombreImagen = '';

  resumen = '';

  uInvestigacion: UInvestigacion = {
    nombre: '',
    descripcion: '',
    imgpath: '',
    linea: '',
    lineas: '',
    concepto: '',
    rol: ''
  };

  constructor(private dataApiService: DataApiService, private subirArchivoService: SubirArchivoService) { }

  ngOnInit() {

    this.dataApiService
          .getRegistro('uInvestigacions', 1)
            .subscribe((dataInvestigacion: any) => {
              if (dataInvestigacion) {
                this.uInvestigacion = dataInvestigacion;
                this.formaUInvestigacion.setValue(this.uInvestigacion);
              }
              console.log(dataInvestigacion);
            }, errorUInvestigacions => {
              this.mensajeExito = null;
              this.mensaje = errorUInvestigacions.error.error.message;
            }),

    this.dataApiService
          .countRegistros('convocatoriainves')
            .subscribe((numConvocatorias: any) => {
              this.numConvocatorias = numConvocatorias['count'];
            }, errorCountConvo => {
              this.mensaje = errorCountConvo.error.error.message;
              console.log('errorCountConvo', errorCountConvo);
            });
    this.dataApiService
          .getInvestigadores()
            .subscribe((dataInvestigadores: any) => {
              this.numInvestigadores = dataInvestigadores.length;
            }, errorNumEquipo => {
              this.mensaje = errorNumEquipo.error.error.message;
              console.log('errorNumEquipo', errorNumEquipo);
            });
    this.dataApiService
          .countRegistros('proyectos')
            .subscribe((numProyectos: any) => {
              this.numProyectos = numProyectos['count'];
            }, errorNumProyectos => {
              this.mensaje = errorNumProyectos.error.error.message;
              console.log('errorNumProyectos', errorNumProyectos);
            });
    // this.dataApiService
    //       .countRegistros('sitios_interes')
    //         .subscribe((numSitios: any) => {
    //           this.numSitiosInteres = numSitios['count'];
    //         }, errorNumSitios => {
    //           this.mensaje = errorNumSitios.error.error.message;
    //           console.log('errorNumSitios', errorNumSitios);
    //         });

    this.cargarForma();

  }

  actUInvestigacion() {
    this.dataApiService.trimObjeto(this.formaUInvestigacion.value);

    let imgAnt: string;
    this.uInvestigacion = this.formaUInvestigacion.value;

    if (this.nombreImagen !== '') {  // si solo cambia el texto monbreImagen esta vacio => no cambia la imagen
      imgAnt = this.uInvestigacion.imgpath;
      this.uInvestigacion.imgpath = this.nombreImagen;
    }
    this.dataApiService
          .putModelo('uInvestigacions', this.uInvestigacion)
            .subscribe((dataPutUInvestigacion: any) => {
              console.log('UInvestigacion Actualizada', dataPutUInvestigacion);
              if (this.nombreImagen !== '') {  // si solo cambia el texto monbreImagen esta vacio => no cambia la imagen
                this.subirArchivoService.delImage('invePagina', imgAnt).subscribe( dataE => console.log('Imagen Eliminada'));
                this.subirImagen(this.imagen);
              }

              this.mensajeExito = this.uInvestigacion.nombre;
              setTimeout(() => {
                this.mensajeExito = null;
              }, 6000);

            }, (errorServicioUsu) => {
              this.mensaje = errorServicioUsu.error.error.message;
              this.mensajeExito = null;
            });
  }

  subirImagen(imagen: File) {
    this.subirArchivoService
        .subirArchivo(imagen, this.nombreImagen, 'invePagina', 'b')
          .then()
          .catch(datac => console.log(datac));
  }

  selimg( archivo: File ) {
    this.nombreImagen = '';
    if ( !archivo ) {
      this.imagen = null;
      console.log('vacio');
      return;
    }
    this.imagen = archivo;
    const nombreCortado = this.imagen.name.split('.');
    const extensionArchivo = nombreCortado.pop();
    const name = Date.now().toString();
    this.nombreImagen = name + '.' + extensionArchivo;
    console.log('nombre Imagen', this.nombreImagen);
  }


  quitarSeleccion() {
    this.nombreImagen = null;
    const inputFile = <HTMLInputElement>document.getElementById('exampleFormControlFileUInvestigacion');
    inputFile.value = null;
  }

  cargarForma() {
    this.formaUInvestigacion = new FormGroup ({
      'nombre': new FormControl('', [Validators.required, Validators.minLength(4)]), // , [Validators.required, Validators.minLength(2)]
      'descripcion': new FormControl('', [Validators.required, Validators.minLength(15)]), // , [Validators.required, Validators.minLength(5)]
      'imgpath': new FormControl('', []), // , [Validators.required]
      'linea': new FormControl(''), // , [Validators.required] MINIMO 150 Se debe saber cual es el que hace form INVALIDA!!!
      'lineas': new FormControl(''), // , [Validators.required] MINIMO 150 Se debe saber cual es el que hace form INVALIDA!!!
      'concepto': new FormControl(''), // , [Validators.required] MINIMO 150 Se debe saber cual es el que hace form INVALIDA!!!
      'rol': new FormControl(''), // , [Validators.required] MINIMO 150 Se debe saber cual es el que hace form INVALIDA!!!
      'id': new FormControl()
    });
  }

}
