import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DataApiService } from '../../../../services/data-api.service';
import { SubirArchivoService } from '../../../../services/subir-archivo.service';
import { Evento } from '../../../../interfaces/evento.interface';

@Component({
  selector: 'app-reg-edi-eve',
  templateUrl: './reg-edi-eve.component.html',
  styleUrls: ['./reg-edi-eve.component.css']
})
export class RegEdiEveComponent implements OnInit {

  forma: FormGroup;
  id: any;
  accion = 'Registrar';
  cambiarFoto = 'Seleccione';

  mensajeError: string;
  mensajeExito: string;

  imagenes: File[];
  nombreImagen = '';
  resumen = '';

  evento: Evento = {
    titulo: '',
    descripcion_capacitador: '',
    tipo: '',
    descripcion: '',
    imgpath: '',
    fecha: '',
    lugar: '',
    auspiciadores: '',
    entrada: '',
    dirigido_a: '',
    contactos: ''
  };

  constructor(private activatedRoute: ActivatedRoute, private router: Router,
              private dataApiServ: DataApiService, private subArcService: SubirArchivoService) {
    this.cargarForma();
    this.activatedRoute.params.subscribe((data: any) => this.id = data['idEvento']);
  }

  ngOnInit() {
    if (this.id !== 'nuevo') {
      this.accion = 'Editar';
      this.cambiarFoto = 'Seleccione para cambiar las';

      this.dataApiServ
            .getRegistro('eventos', this.id)
              .subscribe((dataEvento: any) => {
                console.log(dataEvento);
                this.evento = dataEvento;

                const fecha = new Date(this.evento.fecha);
                const fechaLocalAbsoluta = fecha.getTime() - 240 * 60 * 1000;
                const fechaLocal = new Date(fechaLocalAbsoluta);
                console.log(fechaLocal);
                const fechaMenosCuatroHoras = fechaLocal.toISOString();
                console.log(fechaMenosCuatroHoras);
                this.evento.fecha = fechaMenosCuatroHoras;
                this.evento.fecha = this.evento.fecha.substring(0, 16);
                this.forma.setValue(this.evento);

              }, errorEvento => {
                this.mensajeExito = null;
                this.mensajeError = errorEvento.error.error.message;
              });
    }
  }

  regEvento() {
    let imgsAnt: string;
    this.evento = this.forma.value;
    this.evento.titulo = this.evento.titulo.trim();
    if (this.id === 'nuevo') {
      this.evento.imgpath =  this.nombreImagen;
      this.dataApiServ
            .postModelo('eventos', this.evento)
              .subscribe((dataEvento: any) => {
                console.log('Evento Creado', this.evento);
                this.cargarForma();
                this.mensajeExito = dataEvento.titulo;
                this.subirImagenes(this.imagenes);
              }, (errorEvento) => {
                this.mensajeExito = null;
                this.mensajeError = errorEvento.error.error.message;
              });
    } else {
      if (this.nombreImagen !== '') {  // si solo cambia el texto monbreImagen esta vacio => no cambia la imagen
        imgsAnt = this.evento.imgpath;
        this.evento.imgpath = this.nombreImagen;
      }
      this.evento.fecha = this.evento.fecha;
      this.dataApiServ
              .putModelo('eventos', this.evento)
                .subscribe((dataPutEvento: any) => {
                  console.log('Evento Actualizado', dataPutEvento);
                  if (this.nombreImagen !== '') {  // si solo cambia el texto monbreImagen esta vacio => no cambia la imagen
                    const nombresImagenes = imgsAnt.split(',');
                    for (let index = 0; index < nombresImagenes.length; index++) {
                      const element = nombresImagenes[index];
                      this.subArcService.delImage('inteEventos', element).subscribe( dataE => console.log('Imagen Eliminada'));
                    }
                    this.subirImagenes(this.imagenes);
                  }
                  this.mensajeExito = this.evento.titulo;
                  this.mensajeError = null;
                }, (errorPutEvento) => {
                  this.mensajeError = errorPutEvento.error.error.message;
                  this.mensajeExito = null;
                });
    }
    window.scrollTo(0, 50);
  }

  subirImagenes(imagenes: File[]) {
    const nomImagenes = this.nombreImagen.split(',');
    for (let index = 0; index < imagenes.length; index++) {
      const element = imagenes[index];
      this.subArcService
          .subirArchivo(element, nomImagenes[index], 'inteEventos', 'b')
            .then()
            .catch(datac => console.log(datac));
    }
  }

  selimg( archivo: File[] ) {
    this.nombreImagen = '';
    if ( !archivo ) {
      this.imagenes = null;
      return;
    }
    this.imagenes = archivo;

    console.log(archivo);
    for (let index = 0; index < archivo.length; index++) {
      const element = archivo[index];
      const nombreCortado = element.name.split('.');
      const extensionArchivo = nombreCortado[ nombreCortado.length - 1 ];
      let name = Date.now().toString();
      name = `${index}-${name}`;
      name = name + '.' + extensionArchivo;
      this.nombreImagen = this.nombreImagen + name;

      if (index !== archivo.length - 1) {
        this.nombreImagen = this.nombreImagen + ',';
      }

    }
  }

  cargarForma() {
    this.mensajeError = null;
    this.mensajeExito = null;
    this.forma = new FormGroup ({
      'titulo': new FormControl('', [Validators.required, Validators.minLength(10)]), // , [Validators.required, Validators.minLength(2)]
      'descripcion_capacitador': new FormControl('', [Validators.required, Validators.minLength(10)]), // , [Validators.required, Validators.minLength(2)]
      'tipo': new FormControl(''), // , [Validators.required, Validators.minLength(2)]
      'descripcion': new FormControl('', [Validators.required, Validators.minLength(15)]), // , [Validators.required, Validators.minLength(5)]
      'imgpath': new FormControl('', []), // , [Validators.required]
      'fecha': new FormControl('', [Validators.required]),
      'lugar': new FormControl('', [Validators.required]),
      'auspiciadores': new FormControl('', [Validators.required]),
      'entrada': new FormControl('', [Validators.required]),
      'dirigido_a': new FormControl('', [Validators.required]),
      'contactos': new FormControl('', [Validators.required]),
      'id': new FormControl()
    });
  }

  irPaginaArriba() {
    // this.dataApiServ.irPaginaArriba(this.activatedRoute);
    this.dataApiServ.irPaginaArriba(this.activatedRoute);
  }

}
