import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegEdiEveComponent } from './reg-edi-eve.component';

describe('RegEdiEveComponent', () => {
  let component: RegEdiEveComponent;
  let fixture: ComponentFixture<RegEdiEveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegEdiEveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegEdiEveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
