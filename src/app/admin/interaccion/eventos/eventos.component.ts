import { Component, OnInit } from '@angular/core';
import { DataApiService } from '../../../services/data-api.service';
import { Evento } from 'src/app/interfaces/evento.interface';
import { PublicService } from '../../../services/public.service';
import { SubirArchivoService } from '../../../services/subir-archivo.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-eventos',
  templateUrl: './eventos.component.html',
  styleUrls: ['./eventos.component.css']
})
export class EventosComponent implements OnInit {

  mensaje: string;
  mensajeError: string;

  index: number;

  eventos: Evento[] = [];
  evento: Evento;

  constructor(private activatedRoute: ActivatedRoute, private publicService: PublicService, private dataApiService: DataApiService, private subirArchivoService: SubirArchivoService) { }

  ngOnInit() {
    this.publicService
          .getRegistrosModelo('eventos', 'fecha', 'desc')
            .subscribe((dataEventos: any) => {
              this.eventos = dataEventos;
              console.log(this.eventos);
              if (this.eventos.length === 0) {
                this.mensaje = 'No hay Eventos registrados';
              }
            }, errorEventos => {
              this.mensajeError = errorEventos.error.error.message;
            });
  }

  elimodal(evento: Evento, index: number) {                    // Posesiona sobre el Noticia a eliminar
    this.mensajeError = '';
    this.evento = evento;
    this.index = index;
  }

  eliEvento() {
    this.dataApiService
          .delModelo('eventos', this.evento.id)
            .subscribe(dataEliEvento => {
              this.mensajeError = '';
              console.log('Evento Eliminado');
              console.log(dataEliEvento);
              const imagen = this.evento.imgpath;

              this.subirArchivoService
                    .delImage('inteEventos', imagen)
                      .subscribe(data => {
                        console.log('Imagen Eliminada');
                      }, errorDelImagen => {
                        console.log('errorDelImagen', errorDelImagen);
                      });

              delete this.eventos[this.index];
              this.eventos = this.eventos.filter(Boolean);

              }, (errorDelEvento) => {
                this.mensajeError = errorDelEvento.error.error.message;
                console.log(errorDelEvento.error.error.message);
              });
  }

  irPaginaArriba() {
    this.dataApiService.irPaginaArriba(this.activatedRoute);
  }

}
