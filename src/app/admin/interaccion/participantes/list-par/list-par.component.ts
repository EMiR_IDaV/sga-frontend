import { Component, OnInit, OnDestroy } from '@angular/core';
import { Evento } from '../../../../interfaces/evento.interface';
import { DataApiService } from '../../../../services/data-api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SubirArchivoService } from '../../../../services/subir-archivo.service';
import { Participante } from '../../../../interfaces/participante.interface';
import { TableService } from '../../../../services/table.service';

import * as jsPDF from 'jspdf';

declare var $: any;

@Component({
  selector: 'app-list-par',
  templateUrl: './list-par.component.html',
  styleUrls: ['./list-par.component.css']
})
export class ListParComponent implements OnInit, OnDestroy {

  idEvento: any;
  evento: Evento;

  mensaje: string;

  participante: Participante;
  participantes: Participante[];

  index: number = null;

  altura = 21;
  salto = 4;
  tamtitulo = 12;
  xm = 108;
  x1 = 13;
  x2 = 20;
  x3 = 110;
  x4 = 176;
  x5 = 203;

  constructor(private dataApiService: DataApiService, private activatedRoute: ActivatedRoute, private router: Router,
              private subirArchivoService: SubirArchivoService, private tableService: TableService) {
    console.log('---------------- list-par ---------------');
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(data => {
                                                  this.idEvento = data['idEvento'];
                                                 });

    this.dataApiService
          .getRegistroByProperty('eventos', 'id', this.idEvento)
            .subscribe((dataEvento: any) => {
              this.evento = dataEvento;
              console.log('evento', this.evento);

              this.dataApiService
                    .getModelosRelPorId('eventos', this.idEvento, 'participantes', 'paterno')
                      .subscribe((dataParticipantes: any) => {
                        console.log('dataParticipantes', dataParticipantes);
                        (dataParticipantes.length > 0) ? this.participantes = dataParticipantes : this.mensaje = 'No hay participantes inscritos en este Evento';

                      }, errorParticipantes => {
                        this.mensaje = errorParticipantes.error.error.message;
                      });


            }, errorEvento => {
              this.mensaje = errorEvento.error.error.message;
            });
  }

  elimodal(participante: Participante, index: number) {
    this.participante = participante;
    this.index = index;
  }

  quitarParticipante() {
    this.dataApiService
          .delModeloRelacionado('eventos', this.idEvento, 'participantes', this.participante.id)
            .subscribe(dataDelParticipante => {
              console.log('Participante Eliminado', dataDelParticipante);
              delete this.participantes[this.index];
              this.participantes = this.participantes.filter(Boolean);
            }, (errorDelParticipante) => {
              this.mensaje = errorDelParticipante.error.error.message;
            });
  }

  generarPDF() {

    const hoy = new Date();
    const fechaHoy = hoy.getDate() + '-' + (hoy.getMonth() + 1) + '-' + hoy.getFullYear();

    const doc: any = new jsPDF('p', 'mm', 'letter', true);
    doc.setProperties({
      title: 'Lista de Estudiantes ' + this.evento.titulo + '.pdf',
      subject: 'Ipicom',
      author: 'Ipicom',
      keywords: 'generated, javascript, web 2.0, ajax',
      creator: 'IpiCOM'
    });

    // TÍTULO
    doc.setFont('times');
    doc.setFontSize(this.tamtitulo);
    doc.setFontType('bolditalic');
    doc.text('UNIVERSIDAD MAYOR DE SAN ANDRÉS', this.xm, this.altura, 'center');
    doc.setFontSize(this.tamtitulo - 2);
    doc.text(`CARRERA CIENCIAS DE LA COMUNICACIÓN SOCIAL`, this.xm, this.saltar(), 'center');
    doc.setFontType('italic');
    doc.text('INSTITUTO DE INVESTIGACIÓN POSGRADO E INTERACCIÓN SOCIAL EN COMUNICACIÓN (IpiCOM)', this.xm, this.saltar(), 'center');
    doc.setFontType('bolditalic');

    this.saltar();

    doc.setFontType('bold');
    doc.setFontSize(this.tamtitulo);
    doc.text(`${this.evento.titulo}   ${fechaHoy}`, this.xm, this.saltar(), 'center');
    doc.setFontType('normal');

    this.altura = this.altura + 1;

    this.salto = 6;

    // Generar Tabla
    this.tableService.tListaEvento(doc, this.altura, this.participantes, ['N°', 'APELLIDOS Y NOMBRES', 'email', 'firma'], ['', 'center', 'center', 'center'], ['', 'center', 'center', 'center'], [this.x1, this.x2, this.x3, this.x4, this.x5], this.tamtitulo - 2, 'times', this.salto, 1.5, 2);

    // GUIAS
    // doc.setFontSize(1);
    // for (let index = 0; index < 217; index++) {
    //   // if (index % 2 === 0 ) {
    //     // doc.setTextColor(255,0,0);
    //     // doc.text(index.toString(), index, 0.2);
    //     // doc.setTextColor(0,255,0);
    //     // doc.text(index.toString(), index, 0.5);
    //     doc.setTextColor(0, 0, 255);
    //     doc.text(index.toString(), index, 0.3, 'center');
    //     doc.text('|', index, 0.3);
    //   // }
    // }
    // for (let index = 0; index < 280; index++) {
    //   // if (index % 2 === 0 ) {
    //     // doc.setTextColor(255,0,0);
    //     // doc.text(index.toString(),0, index);
    //     // doc.setTextColor(0,255,0);
    //     // doc.text(index.toString(),0.5, index);
    //     doc.setTextColor(0, 0, 255);
    //     doc.text(index.toString(), 108, index);
    //     doc.text('-', 0, index);
    //   // }
    // }
    doc.save('Lista de Estudiantes ' + this.evento.titulo);
    this.altura = 15;
  }

  saltar() {
    return this.altura = this.altura + this.salto;
  }

  irPaginaArriba() {
    // this.dataApiService.retrocederPagina();
    this.dataApiService.irPaginaArriba(this.activatedRoute, true);
  }

  ngOnDestroy() {
    // $('#ModalEstudiante').modal('hide');
    // $('#ModalEstudiante').modal('dispose');
    $('.modal-backdrop').hide();
  }

}
