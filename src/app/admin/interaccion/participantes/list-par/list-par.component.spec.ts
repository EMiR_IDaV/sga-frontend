import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListParComponent } from './list-par.component';

describe('ListParComponent', () => {
  let component: ListParComponent;
  let fixture: ComponentFixture<ListParComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListParComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListParComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
