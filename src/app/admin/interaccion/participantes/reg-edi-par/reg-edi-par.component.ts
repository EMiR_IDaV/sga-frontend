import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { DataApiService } from '../../../../services/data-api.service';
import { ActivatedRoute } from '@angular/router';
import { Participante } from '../../../../interfaces/participante.interface';
import { Evento } from '../../../../interfaces/evento.interface';

@Component({
  selector: 'app-reg-edi-par',
  templateUrl: './reg-edi-par.component.html',
  styleUrls: ['./reg-edi-par.component.css']
})
export class RegEdiParComponent implements OnInit {

  forma: FormGroup;
  mensajeError = null;
  mensajeExito = null;

  idEvento: any;
  idParticipante: any;
  evento: Evento;

  accion = 'Registrar';

  participante: Participante = {
    paterno: '',
    materno: '',
    pri_nombre: '',
    seg_nombre: '',
    celular: null,
    email: ''
  };

  // path 'estudiantes/:idEvento/:username'
  constructor(private dataApiService: DataApiService, private activatedRoute: ActivatedRoute) {
    console.log('--------------- reg-edi-par--------------');
  }

  ngOnInit() {
    this.cargarForma();
    this.activatedRoute.params.subscribe((data: any) => {
                                                    this.idEvento = data['idEvento'];
                                                    this.idParticipante = data['participante'];
                                                  });

    if (this.idParticipante !== 'nuevo') {
      this.accion = 'Actualizar';

      this.dataApiService
            .getRegistro('participantes', this.idParticipante)
              .subscribe((dataParticipante: any) => {
                this.participante = dataParticipante;
                console.log('participante', this.participante);
                delete this.participante.eventoId;

                this.forma.setValue(this.participante);
              });
    }
  }

  retornaFormatoOK(nombre: string) {
    if (nombre === '') { return nombre; }
    nombre = nombre.trim();
    nombre = nombre.toLowerCase();
    nombre = nombre.charAt(0).toUpperCase() + nombre.slice(1);
    return nombre;
  }

  regParticipante() {
    this.participante =  this.forma.value;
    this.participante.paterno = this.retornaFormatoOK(this.participante.paterno);
    this.participante.materno = this.retornaFormatoOK(this.participante.materno);
    this.participante.pri_nombre = this.retornaFormatoOK(this.participante.pri_nombre);
    this.participante.seg_nombre = this.retornaFormatoOK(this.participante.seg_nombre);

    if (this.idParticipante === 'nuevo') {

      this.dataApiService
            .postModeloRelacionado('eventos', this.idEvento, 'participantes', this.participante)
              .subscribe((dataParticipante: any) => {
                  console.log('Participante Creado', dataParticipante);
                  this.cargarForma();
                  this.mensajeExito = dataParticipante.paterno + ' ' + dataParticipante.pri_nombre;
                  this.mensajeError = null;
              }, (errorPostEstudiante) => {
                this.mensajeExito = null;
                this.mensajeError = errorPostEstudiante.error.error.message;
              });

    } else {
      this.dataApiService
            .putModeloRelacionado('eventos', this.idEvento, 'participantes', this.participante, this.idParticipante)
              .subscribe((dataParticipante: any) => {
                console.log('Participante Actualizado', dataParticipante);
                this.mensajeExito = dataParticipante.paterno + ' ' + dataParticipante.pri_nombre;
                this.mensajeError = null;

              }, (errorServicioPutUsu) => {
                this.mensajeExito = null;
                this.mensajeError = errorServicioPutUsu.error.error.message;
              });
    }
    window.scrollTo(0, 0);
  }

  cargarForma() {
    this.mensajeError = null;
    this.mensajeExito = null;
    this.forma = new FormGroup ({
      'paterno': new FormControl(''),
      'materno': new FormControl(''),
      'pri_nombre': new FormControl(''), // , [Validators.required, Validators.minLength(2)]
      'seg_nombre': new FormControl(''),
      'celular': new FormControl(''), // , [Validators.required, Validators.minLength(8)]
      'email': new FormControl(''), // , [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]
      'id': new FormControl()
    });
  }

  irPaginaArriba() {
    this.dataApiService.irPaginaArriba(this.activatedRoute);
  }

}
