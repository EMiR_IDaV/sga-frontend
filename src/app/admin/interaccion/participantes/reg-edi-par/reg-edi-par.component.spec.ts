import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegEdiParComponent } from './reg-edi-par.component';

describe('RegEdiParComponent', () => {
  let component: RegEdiParComponent;
  let fixture: ComponentFixture<RegEdiParComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegEdiParComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegEdiParComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
