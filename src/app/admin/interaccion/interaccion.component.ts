import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DataApiService } from '../../services/data-api.service';
import { UInteraccion } from '../../../app/interfaces/uInteraccion.interface';

@Component({
  selector: 'app-interaccion',
  templateUrl: './interaccion.component.html',
  styleUrls: ['./interaccion.component.css']
})
export class InteraccionComponent implements OnInit {

  formaUInteraccion: FormGroup;
  mensaje: string;
  mensajeExito: string;

  numEventos: any;
  numParticipantes: any;

  numInvestigadores: any;
  numSitiosInteres: any;

  uInteraccion: UInteraccion = {
    nombre: '',
    descripcion: '',
    linea: '',
    lineas: '',
    invitacion: ''
  };

  constructor(private dataApiService: DataApiService) { }

  ngOnInit() {

    this.dataApiService
          .getRegistro('uInteraccions', 1)
            .subscribe((dataInteraccion: any) => {
              if (dataInteraccion) {
                this.uInteraccion = dataInteraccion;
                this.formaUInteraccion.setValue(this.uInteraccion);
              }
              console.log(dataInteraccion);
            }, errorUInteraccions => {
              this.mensajeExito = null;
              this.mensaje = errorUInteraccions.error.error.message;
            }),

    this.dataApiService
          .countRegistros('eventos')
            .subscribe((numEventos: any) => {
              this.numEventos = numEventos['count'];
            }, errorCountEventos => {
              this.mensaje = errorCountEventos.error.error.message;
              console.log('errorCountEventos', errorCountEventos);
            });
    this.dataApiService
          .countRegistros('participantes')
            .subscribe((numParticipantes: any) => {
              this.numParticipantes = numParticipantes['count'];
            }, errorNumParticipantes => {
              this.mensaje = errorNumParticipantes.error.error.message;
              console.log('errorNumParticipantes', errorNumParticipantes);
            });
    // this.dataApiService
    //       .getDocentesInvestigadores()
    //         .subscribe((dataInvestigadores: any) => {
    //           this.numInvestigadores = dataInvestigadores.length;
    //         }, errorNumEquipo => {
    //           this.mensaje = errorNumEquipo.error.error.message;
    //           console.log('errorNumEquipo', errorNumEquipo);
    //         });

    this.cargarForma();

  }

  actUInteraccion() {
    this.dataApiService.trimObjeto(this.formaUInteraccion.value);

    this.uInteraccion = this.formaUInteraccion.value;


    this.dataApiService
          .putModelo('uInteraccions', this.uInteraccion)
            .subscribe((dataPutUInteraccion: any) => {
              console.log('UInteraccion Actualizada', dataPutUInteraccion);

              this.mensajeExito = this.uInteraccion.nombre;
              setTimeout(() => {
                this.mensajeExito = null;
              }, 6000);

            }, (errorServicioUsu) => {
              this.mensaje = errorServicioUsu.error.error.message;
              this.mensajeExito = null;
            });
  }

  cargarForma() {
    this.formaUInteraccion = new FormGroup ({
      'nombre': new FormControl('', [Validators.required, Validators.minLength(4)]), // , [Validators.required, Validators.minLength(2)]
      'descripcion': new FormControl('', [Validators.required, Validators.minLength(15)]), // , [Validators.required, Validators.minLength(5)]
      'linea': new FormControl(''), // , [Validators.required] MINIMO 150 Se debe saber cual es el que hace form INVALIDA!!!
      'lineas': new FormControl(''), // , [Validators.required] MINIMO 150 Se debe saber cual es el que hace form INVALIDA!!!
      'invitacion': new FormControl(''), // , [Validators.required] MINIMO 150 Se debe saber cual es el que hace form INVALIDA!!!
      'id': new FormControl()
    });
  }

}
