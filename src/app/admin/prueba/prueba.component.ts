import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DataApiService } from '../../services/data-api.service';
import { SubirArchivoService } from '../../services/subir-archivo.service';
import { AutentificacionService } from '../../services/autentificacion.service';
import { Persona } from '../../interfaces/persona.interface';
import { Router } from '@angular/router';


@Component({
  selector: 'app-prueba',
  templateUrl: './prueba.component.html',
  styleUrls: ['./prueba.component.css']
})
export class PruebaComponent implements OnInit {

  persona: Persona = {
    paterno: '',
    pri_nombre: '',
    sexo: '',
    nacionalidad: '',
    ci: null,
    extendido: '',
    fec_nacimiento: '',
    celular: null,
    telefono: null,
    reg_universitario: null,
    email: '',
    dom_ciudad: '',
    dom_zona: '',
    dom_calle: '',
    dom_numero: null,
    dom_otro: ''
  };

  nacionalidad = 'Boliviano';
  forma: FormGroup;
  cod_deptos = ['LP', 'OR', 'PT', 'CB', 'SC', 'BN', 'PA', 'TJ', 'CH'];
  ci = null;

  constructor(private dataApi: DataApiService, private router: Router, private autService: AutentificacionService) {
    this.cargarForma();
  }

  ngOnInit() {
  }
  cargarForma() {
    this.forma = new FormGroup ({
      'paterno': new FormControl(''),
      'materno': new FormControl(''),
      'pri_nombre': new FormControl('', [Validators.required, Validators.minLength(2)]),
      'seg_nombre': new FormControl(''),
      'sexo': new FormControl('', [Validators.required]),
      'nacionalidad': new FormControl('Boliviano'),
      'ci': new FormControl('', [Validators.required, Validators.minLength(5)]),
      'extendido': new FormControl('', [Validators.required]),
      'fec_nacimiento': new FormControl('', []),
      'celular': new FormControl('', [Validators.required, Validators.minLength(8)]),
      'telefono': new FormControl(''),
      'reg_universitario': new FormControl(''),
      'email': new FormControl('', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]),
      'dom_ciudad': new FormControl('', []),
      'dom_zona': new FormControl('', []),
      'dom_calle': new FormControl('', []),
      'dom_numero': new FormControl('', []),
      'dom_otro': new FormControl('')
    });
  }
}

  // registrarEstudiante() {

  //   this.persona =  this.forma.value;
  //   this.persona.usuario = this.persona.paterno[0] + this.persona.materno[0] + this.persona.pri_nombre[0] + this.persona.ci;
  //   this.persona.contrasena = this.persona.ci.toString();
  //   this.dataApi.postPersona(this.persona).subscribe((persona: any) =>
  //                                                     this.dataApi.postEstudiante(persona).subscribe(estudiante =>
  //                                                                                                         console.log(estudiante))
  //   );
  //   this.cargarForma();
  




  // guardarCambios( ) {
  //   this.forma.value.imagen = this.nombreImagen;
  //   this.dataApi.setNoticia(this.forma.value).subscribe();
  //   this.subirArchivoService.subirArchivo( this.imagenSubir, this.nombreImagen, 'a', 'b')
  //         .then( resp => {
  //           console.log( 'then' );
  //         })
  //         .catch( resp => {
  //           console.log( 'catch' );
  //         });
  // }

  // selimg( archivo: File ) {
  //   if ( !archivo ) {
  //     this.imagenSubir = null;
  //     return;
  //   }

  //   this.imagenSubir = archivo;
  //   const nombreCortado = archivo.name.split('.');
  //   const extensionArchivo = nombreCortado[ nombreCortado.length - 1 ];
  //   const name = Date.now().toString();
  //   console.log(name);
  //   this.nombreImagen = name + '.' + extensionArchivo;
  //   console.log(this.nombreImagen);
  // }
