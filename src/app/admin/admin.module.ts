import { NgModule } from '@angular/core';

import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Componentes
import { AdminComponent } from './admin.component';
import { PrincipalComponent } from './principal/principal.component';

import { PruebaComponent } from './prueba/prueba.component';

import { InstitutoComponent } from './instituto/instituto.component';
import { CaracteristicasComponent } from './instituto/caracteristicas/caracteristicas.component';
import { RegEdiCarComponent } from './instituto/caracteristicas/reg-edi-car/reg-edi-car.component';
import { PersonalComponent } from './instituto/personal/personal.component';
import { NoticiasComponent } from './instituto/noticias/noticias.component';
import { RegEdiNotComponent } from './instituto/noticias/reg-edi-not/reg-edi-not.component';

import { PosgradoComponent } from './posgrado/posgrado.component';
import { EstudiantesComponent } from './posgrado/estudiantes/estudiantes.component';
import { RegEdiEstComponent } from './posgrado/estudiantes/reg-edi-est/reg-edi-est.component';
import { ListEstComponent } from './posgrado/estudiantes/list-est/list-est.component';
import { DocentesComponent } from './posgrado/docentes/docentes.component';
import { ListDocComponent } from './posgrado/docentes/list-doc/list-doc.component';
import { RegEdiDocComponent } from './posgrado/docentes/reg-edi-doc/reg-edi-doc.component';

import { InvestigacionComponent } from './investigacion/investigacion.component';
import { InteraccionComponent } from './interaccion/interaccion.component';

// Pipes
import { PipesModule } from '../pipes/pipes.module';

// Rutas
import { ADMIN_ROUTING } from './admin.routes';
import { RegEdiPerComponent } from './instituto/personal/reg-edi-per/reg-edi-per.component';
import { CursosComponent } from './posgrado/cursos/cursos.component';
import { RegEdiCurComponent } from './posgrado/cursos/reg-edi-cur/reg-edi-cur.component';
import { ModulosComponent } from './posgrado/modulos/modulos.component';
import { RegEdiModComponent } from './posgrado/modulos/reg-edi-mod/reg-edi-mod.component';
import { SitiosInteresComponent } from './instituto/sitios-interes/sitios-interes.component';
import { ListModComponent } from './posgrado/modulos/list-mod/list-mod.component';
import { NotasComponent } from './posgrado/notas/notas.component';
import { ListNotasComponent } from './posgrado/notas/list-notas/list-notas.component';
import { RegEdiNotasComponent } from './posgrado/notas/reg-edi-notas/reg-edi-notas.component';
import { AsistenciasComponent } from './posgrado/asistencias/asistencias.component';
import { ListAsiComponent } from './posgrado/asistencias/list-asi/list-asi.component';
import { RegEdiAsiComponent } from './posgrado/asistencias/reg-edi-asi/reg-edi-asi.component';
import { AvisosComponent } from './posgrado/avisos/avisos.component';
import { AgrMulEstComponent } from './posgrado/estudiantes/agr-mul-est/agr-mul-est.component';
import { ConsultasComponent } from './instituto/consultas/consultas.component';
import { EventosComponent } from './interaccion/eventos/eventos.component';
import { RegEdiEveComponent } from './interaccion/eventos/reg-edi-eve/reg-edi-eve.component';
import { InvestigacionesComponent } from './investigacion/investigaciones/investigaciones.component';
import { RegEdiInvComponent } from './investigacion/investigaciones/reg-edi-inv/reg-edi-inv.component';
import { DocentesInvestigadoresComponent } from './investigacion/docentes-investigadores/docentes-investigadores.component';
import { RegEdiDocinvComponent } from './investigacion/docentes-investigadores/reg-edi-docinv/reg-edi-docinv.component';
import { AuxiliaresInvestigadoresComponent } from './investigacion/auxiliares-investigadores/auxiliares-investigadores.component';
import { RegEdiAuxinvComponent } from './investigacion/auxiliares-investigadores/reg-edi-auxinv/reg-edi-auxinv.component';
import { ConvocatoriasComponent } from './investigacion/convocatorias/convocatorias.component';
import { RegEdiConComponent } from './investigacion/convocatorias/reg-edi-con/reg-edi-con.component';
import { ListInvComponent } from './investigacion/investigaciones/list-inv/list-inv.component';
import { ListDocinvComponent } from './investigacion/docentes-investigadores/list-docinv/list-docinv.component';
import { UnidadesComponent } from './instituto/componentes-pagina/unidades/unidades.component';
import { NosotrosComponent } from './instituto/componentes-pagina/nosotros/nosotros.component';
import { RedesComponent } from './instituto/componentes-pagina/redes/redes.component';
import { ContactosComponent } from './instituto/componentes-pagina/contactos/contactos.component';
import { OficinasComponent } from './instituto/componentes-pagina/oficinas/oficinas.component';
import { RecursosComponent } from './instituto/componentes-pagina/recursos/recursos.component';
import { ParticipantesComponent } from './interaccion/participantes/participantes.component';
import { RegEdiParComponent } from './interaccion/participantes/reg-edi-par/reg-edi-par.component';
import { ListParComponent } from './interaccion/participantes/list-par/list-par.component';
import { ListAuxinvComponent } from './investigacion/auxiliares-investigadores/list-auxinv/list-auxinv.component';

@NgModule ({
    declarations: [
        AdminComponent,
        PrincipalComponent,
        PruebaComponent,
        InstitutoComponent,
        PosgradoComponent,
        InvestigacionComponent,
        InteraccionComponent,
        EstudiantesComponent,
        RegEdiEstComponent,
        ListEstComponent,
        DocentesComponent,
        RegEdiDocComponent,
        ListDocComponent,
        RegEdiNotComponent,
        CaracteristicasComponent,
        RegEdiCarComponent,
        NoticiasComponent,
        PersonalComponent,
        RegEdiPerComponent,
        CursosComponent,
        RegEdiCurComponent,
        ModulosComponent,
        RegEdiModComponent,
        SitiosInteresComponent,
        ListModComponent,
        NotasComponent,
        ListNotasComponent,
        RegEdiNotasComponent,
        AsistenciasComponent,
        ListAsiComponent,
        RegEdiAsiComponent,
        AvisosComponent,
        AgrMulEstComponent,
        ConsultasComponent,
        EventosComponent,
        RegEdiEveComponent,
        InvestigacionesComponent,
        RegEdiInvComponent,
        DocentesInvestigadoresComponent,
        RegEdiDocinvComponent,
        AuxiliaresInvestigadoresComponent,
        RegEdiAuxinvComponent,
        ConvocatoriasComponent,
        RegEdiConComponent,
        ListInvComponent,
        ListDocinvComponent,
        UnidadesComponent,
        NosotrosComponent,
        RedesComponent,
        ContactosComponent,
        OficinasComponent,
        RecursosComponent,
        ParticipantesComponent,
        RegEdiParComponent,
        ListParComponent,
        ListAuxinvComponent
    ],
    exports: [],
    imports: [
        RouterModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        PipesModule,
        ADMIN_ROUTING
    ],
    providers: []
})

export class AdminModule { }
