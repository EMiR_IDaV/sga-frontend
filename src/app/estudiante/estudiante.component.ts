import { Component, OnInit } from '@angular/core';

import { AutentificacionService } from '../services/autentificacion.service';
import { Router } from '@angular/router';
import { Curso } from '../interfaces/curso.interface';
import { DataApiService } from '../services/data-api.service';

@Component({
  selector: 'app-estudiante',
  templateUrl: './estudiante.component.html',
  styleUrls: ['./estudiante.component.css']
})
export class EstudianteComponent implements OnInit {

  cursos: Curso[] = [];
  nombre: string;
  idEstudiante: number;

  constructor(private autentificacionService: AutentificacionService, private router: Router, private dataApiService: DataApiService) {
    console.log('--------------------------------- estudiante-component -----------------------------------');
  }

  ngOnInit() {

    this.idEstudiante = Number(this.autentificacionService.getId());
    this.dataApiService
          .getPriNombreUsuario(this.idEstudiante)
            .subscribe((dataNombre: any) => {
              this.nombre = dataNombre['pri_nombre'];
            }, errorNombre => {
              console.log('errorNombre', errorNombre);
            });

    this.dataApiService
          .getCursosInscrito(this.idEstudiante)
            .subscribe((dataCursos: any) => {
              this.cursos = dataCursos;
              console.log(this.cursos);
            }, errorCursos => {
              console.log('errorCursos', errorCursos);
            });
  }

  logout() {
    this.autentificacionService.logoutUser().subscribe();
    this.router.navigate(['']);
  }


}
