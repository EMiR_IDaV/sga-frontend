import { NgModule } from '@angular/core';

import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Pipes
import { PipesModule } from '../pipes/pipes.module';

// Rutas
import { ESTUDIANTE_ROUTING } from './estudiante.routes';

// Componentes
import { EstudianteComponent } from './estudiante.component';
import { PrincipalEstudianteComponent } from './principal-estudiante/principal-estudiante.component';
import { EstudianteNotasComponent } from './estudiante-notas/estudiante-notas.component';


@NgModule ({
    declarations: [
        EstudianteComponent,
        PrincipalEstudianteComponent,
        EstudianteNotasComponent
    ],
    exports: [],
    imports: [
        RouterModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        PipesModule,
        ESTUDIANTE_ROUTING
    ],
    providers: []
})

export class EstudianteModule { }
