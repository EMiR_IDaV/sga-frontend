import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Curso } from '../../interfaces/curso.interface';
import { PublicService } from '../../services/public.service';
import { DataApiService } from '../../services/data-api.service';
import { AutentificacionService } from '../../services/autentificacion.service';
import { Nota } from '../../interfaces/nota.interface';
import { Modulo } from '../../interfaces/modulo.interface';
import { Usuario } from '../../interfaces/usuario.interface';

import * as jsPDF from 'jspdf';
import { TableService } from '../../services/table.service';
import { Documento } from '../../interfaces/documento.interface';


@Component({
  selector: 'app-estudiante-notas',
  templateUrl: './estudiante-notas.component.html',
  styleUrls: ['./estudiante-notas.component.css']
})
export class EstudianteNotasComponent implements OnInit {

  sigla: string;
  id: number;

  mensaje = '';

  curso: Curso;
  modulos: Modulo[] = [];
  usuario: Usuario;
  notas: Nota[] = [];
  documentos: Documento[] = [];

  nota: Nota = {
    moduloId: 0,
    estudianteId: 0,
    nota: 0,
    observacion: ''
  };

  aprobadas = 0;
  reprobadas = 0;
  altura = 21;

  salto = 4;
  tamtitulo = 12;
  xm = 108;
  xi = 25;
  x1 = 33;
  x2 = 137;
  x3 = 160;
  x4 = 171;
  xf = 191;

  constructor(private activatedRoute: ActivatedRoute, private publicService: PublicService,
              private dataApiService: DataApiService, private autentificacionService: AutentificacionService,
              private tableService: TableService) {
    console.log('----------------------------- estudiante-notas -----------------------------');
  }

  ngOnInit() {

    this.activatedRoute
          .params
            .subscribe(data => {
              this.sigla = data['siglaCurso'];
              this.id = Number(this.autentificacionService.getId());

              this.dataApiService
                    .getCursoDocumentos(this.sigla)
                      .subscribe((cursoDocumentos: any) => {
                        this.documentos = cursoDocumentos;
                        console.log(cursoDocumentos);
                      }, errorCursoDocumentos => {
                        console.log('errorCursoDocumentos', errorCursoDocumentos);
                      });

              this.dataApiService
                    .getUsuarioByIdIncluidoTipo(this.id, 'estudiante')
                      .subscribe((dataUsuarioEstudiante: any) => {
                        this.usuario = dataUsuarioEstudiante;
                        console.log(dataUsuarioEstudiante);
                      }, errorUsuarioEstudiante => {
                        console.log('errorUsuarioEstudiante', errorUsuarioEstudiante);
                      });

              this.publicService
                    .getCursoPorSigla(this.sigla)
                      .subscribe((dataCurso) => {
                        this.curso = dataCurso;
                        console.log(this.curso);

                        this.dataApiService
                              .getModelosRelPorId('cursos', this.curso.id, 'modulos', 'numero')
                                .subscribe((dataModulos: any) => {
                                  this.modulos = dataModulos;
                                  console.log(this.modulos);

                                  this.dataApiService
                                        .getNotasCurso(this.id, this.curso.id)
                                          .subscribe((dataNotas: any) => {
                                            this.notas =  dataNotas;
                                            this.llenarNotas();
                                            this.numAproRepro();
                                            console.log(this.notas);

                                          }, errorNotas => {
                                            console.log('errorNotas', errorNotas);
                                          });

                                }, errorModulos => {
                                  console.log('errorModulos', errorModulos);
                                });


                      }, errorCurso => {
                        console.log('errorCurso', errorCurso);
                      });

            });

  }

  llenarNotas() {
    for (let index = 0; index < this.modulos.length; index++) {
      const element = this.modulos[index];

      let sinNota = true;

      for (let index1 = 0; index1 < this.notas.length && sinNota; index1++) {
        const element1 = this.notas[index1];

        if (element.id === element1.moduloId) {
          element.nota = element1;
          // delete this.notas[index1];
          // this.notas.filter(Boolean);
          sinNota = false;
        }
      }
      if (sinNota) {
        element.nota = this.nota;
      }
    }
  }

  numAproRepro() {
    for (let index = 0; index < this.notas.length; index++) {
      const element = this.notas[index];
      if (element.nota !== 0) {
        if (element.nota > 69) {
          this.aprobadas++;
        } else {
          this.reprobadas++;
        }
      }
    }
  }

  generarPDF() {

    const doc: any = new jsPDF('p', 'mm', 'letter', true);  // Letter	216 x 279 mm - 8,5 x 11,0 pulg
    doc.setProperties({
      title: 'Reporte de Notsa ' + this.sigla + '.pdf',
      subject: 'Ipicom',
      author: 'Ipicom',
      keywords: 'generated, javascript, web 2.0, ajax',
      creator: 'IpiCOM'
    });

    // TÍTULO
    doc.setFont('times');
    doc.setFontSize(this.tamtitulo);
    doc.setFontType('bolditalic');
    doc.text('UNIVERSIDAD MAYOR DE SAN ANDRÉS', this.xm, this.altura, 'center');
    doc.setFontSize(this.tamtitulo - 2);
    doc.setFontType('italic');
    doc.text('INSTITUTO DE INVESTIGACIÓN POSGRADO E INTERACCIÓN SOCIAL', this.xm, this.saltar(), 'center');
    doc.text('EN COMUNICACIÓN (IpiCOM)', this.xm, this.saltar(), 'center');
    doc.setFontType('bolditalic');
    doc.text(`${this.curso.nombre.toLocaleUpperCase()} ${this.curso.gestion}`, this.xm, this.saltar(), 'center');

    this.saltar();
    this.saltar();

    doc.setFontType('bold');
    doc.setFontSize(this.tamtitulo);
    doc.text('REPORTE DE NOTAS', this.xm, this.altura, 'center');
    this.saltar();

    doc.setFontType('normal');
    this.datosEstudiante(doc, this.altura, 1, 2, this.usuario);

    this.saltar();

    this.tableService.tablaNotasEstudiante(doc, this.altura, ['N°', 'Módulo', 'Fecha', 'Nota', 'Detalle'], ['center', 'center', 'center', 'center', 'center'], this.modulos, ['center', 'right', 'center', 'center', 'center'], [this.xi, this.x1, this.x2, this.x3, this.x4, this.xf], this.tamtitulo - 2, 'times', this.salto + 1, 1, 2);

    doc.save(`Reporte de Notas ${this.sigla} ${this.curso.gestion}`);

    this.aprobadas = 0;
    this.reprobadas = 0;
    this.altura = 21;
  }

  datosEstudiante(doc: any, altura: number, dililearriba: number, dililederecha: number, usuario: Usuario) {
    doc.line(this.xi, this.altura - dililearriba, this.xf, this.altura - dililearriba);
    this.saltar();
    doc.setFont('times');
    doc.setFontSize(this.tamtitulo - 2 );
    doc.text(this.xi + dililederecha, this.altura, `Nombre: ${this.tableService.nomCompleto(usuario)}`);
    doc.text(this.xm + dililederecha, this.altura, `Aprobadas: ${this.aprobadas}`);
    doc.text(((this.xm + this.xf) / 2) + dililederecha, this.altura, `Matricula: ${usuario.estudiante.reg_universitario}`);
    this.saltar();
    doc.text(this.xi + dililederecha, this.altura, `Fecha de consulta: ${this.tableService.obtieneFechaddMMyyyy()}`);
    doc.text(this.xm + dililederecha, this.altura, `Reprobadas: ${this.reprobadas}`);
    this.saltar();
    doc.line(this.xi, this.altura - dililearriba, this.xf, this.altura - dililearriba);
    doc.line(this.xi, altura - dililearriba, this.xi, this.altura - dililearriba);
    doc.line(this.xf, altura - dililearriba, this.xf, this.altura - dililearriba);
  }

  saltar() {
    return this.altura = this.altura + this.salto;
  }

  irPaginaArriba() {
    this.dataApiService.irPaginaArriba(this.activatedRoute, true);
  }

}
