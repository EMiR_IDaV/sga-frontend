import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstudianteNotasComponent } from './estudiante-notas.component';

describe('EstudianteNotasComponent', () => {
  let component: EstudianteNotasComponent;
  let fixture: ComponentFixture<EstudianteNotasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstudianteNotasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstudianteNotasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
