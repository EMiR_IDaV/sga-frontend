import { RouterModule, Routes } from '@angular/router';

// Guards
import { UsuarioGuard } from '../guards/usuario.guard';


import { EstudianteComponent } from './estudiante.component';
import { PrincipalEstudianteComponent } from './principal-estudiante/principal-estudiante.component';
import { ItemsGuard } from '../guards/items.guard';
import { EstudianteGuard } from '../guards/estudiante.guard';
import { EstudianteNotasComponent } from './estudiante-notas/estudiante-notas.component';

const ESTUDIANTE_ROUTES: Routes = [
    {
        path: 'estudiante',
        component: EstudianteComponent,
        canActivate: [UsuarioGuard, ItemsGuard, EstudianteGuard],
        children: [
            { path: '', component: PrincipalEstudianteComponent },
            { path: ':siglaCurso/notas', component: EstudianteNotasComponent  }
        ]
    }
];

export const ESTUDIANTE_ROUTING = RouterModule.forChild(ESTUDIANTE_ROUTES);
