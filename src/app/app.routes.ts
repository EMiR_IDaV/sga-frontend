import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './institute/home/home.component';
import { AboutComponent } from './institute/about/about.component';
import { InvestigationComponent } from './institute/investigation/investigation.component';
import { PostgraduateComponent } from './institute/postgraduate/postgraduate.component';
import { InteractionComponent } from './institute/interaction/interaction.component';
import { FeatureComponent } from './institute/about/feature/feature.component';
import { PersonComponent } from './institute/person/person.component';
import { NoticeComponent } from './institute/notices/notice/notice.component';
import { NoticesComponent } from './institute/notices/notices.component';
import { PublicationsComponent } from './institute/investigation/publications/publications.component';
import { TeachersComponent } from './institute/investigation/teachers/teachers.component';
import { EventsComponent } from './institute/interaction/events/events.component';
import { EventComponent } from './institute/interaction/event/event.component';
import { PagesComponent } from './institute/pages.component';
import { LoginComponent } from './login/login.component';
import { CoursesComponent } from './institute/postgraduate/courses/courses.component';
import { CourseComponent } from './institute/postgraduate/courses/course/course.component';
import { PlanComponent } from './institute/postgraduate/courses/course/plan/plan.component';
import { TermComponent } from './institute/investigation/term/term.component';
import { InvestigatorComponent } from './institute/investigation/teachers/investigator/investigator.component';
import { AssistantComponent } from './institute/investigation/teachers/assistant/assistant.component';

const APP_ROUTES: Routes = [
    {
        path: '',
        component: PagesComponent,
        children: [
            { path: '', component: HomeComponent },
            { path: 'about', component: AboutComponent },
            { path: 'about/notices', component: NoticesComponent },
            { path: 'about/notices/:notice', component: NoticeComponent },
            // { path: 'about/Equipo-de-Trabajo', component: TeamWorkComponent },
            { path: 'about/:nombre', component: FeatureComponent },
            { path: 'about/Equipo-de-Trabajo/:cargo', component: PersonComponent },
            { path: 'investigation', component: InvestigationComponent },
            { path: 'investigation/publications', component: PublicationsComponent },
            { path: 'investigation/teachers', component: TeachersComponent },
            { path: 'investigation/teachers/:term', component: TermComponent },
            { path: 'investigation/teachers/:term/:docinv', component: InvestigatorComponent },
            { path: 'investigation/assistants/:term/:docinv', component: AssistantComponent },
            { path: 'postgraduate', component: PostgraduateComponent },
            { path: 'interaction', component: InteractionComponent },
            { path: 'postgraduate/courses', component: CoursesComponent },
            { path: 'postgraduate/courses/:sigla', component: CourseComponent },
            { path: 'postgraduate/courses/:sigla/Plan-de-Estudios', component: PlanComponent },
            { path: 'interaction/events', component: EventsComponent },
            { path: 'interaction/events/:event', component: EventComponent },
            { path: 'login', component: LoginComponent },
            { path: '**', pathMatch: 'full', redirectTo: '' }                 // TO DO RESOLVER QUE NO DA
        ]
     }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, {
    scrollPositionRestoration: 'enabled'
});
