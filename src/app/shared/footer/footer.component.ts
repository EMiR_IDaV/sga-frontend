import { Component, OnInit } from '@angular/core';
import { PublicService } from '../../services/public.service';
import { RedSocial } from '../../interfaces/red_social.interface';
import { Contacto } from '../../interfaces/contacto.interface';
import { Oficina } from '../../interfaces/oficina.interface';
import { Recurso } from '../../interfaces/recurso.interface';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  redes: RedSocial[] = [];
  contactos: Contacto[] = [];
  oficinas: Oficina[] = [];
  recursos: Recurso[] = [];

  constructor(private publicService: PublicService) { }

  ngOnInit() {
    this.publicService
          .getRegistrosModelo('redes_sociales')
            .subscribe((dataRedes: any) => {
              this.redes = dataRedes;
            }, errorRedes => {
              console.log('errorRedes', errorRedes);
            });

    this.publicService
          .getRegistrosModelo('contactos')
            .subscribe((dataContactos: any) => {
              this.contactos = dataContactos;
            }, errorContactos => {
              console.log('errorContactos', errorContactos);
            });

    this.publicService
          .getRegistrosModelo('oficinas')
            .subscribe((dataOficinas: any) => {
              this.oficinas = dataOficinas;
            }, errorOficinas => {
              console.log('errorOficinas', errorOficinas);
            });

    this.publicService
          .getRegistrosModelo('recursos')
            .subscribe((dataRecursos: any) => {
              this.recursos = dataRecursos;
            }, errorRecursos => {
              console.log('errorRecursos', errorRecursos);
            });

  }

}
