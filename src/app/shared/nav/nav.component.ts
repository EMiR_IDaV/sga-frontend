import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AutentificacionService } from '../../services/autentificacion.service';
import { Router } from '@angular/router';
import { DataApiService } from '../../services/data-api.service';
import { Caracteristica } from '../../interfaces/caracteristica.interface.';
import { PublicService } from '../../services/public.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  caracteristicas: Caracteristica[];
  forma: FormGroup;
  mensajeError: string;
  error = false;


  constructor(private autentificacionService: AutentificacionService, private router: Router,
              private publicService: PublicService) {

    this.publicService.getRegistrosModelo('caracteristicas').subscribe((data: any) => this.caracteristicas = data);

    this.forma = new FormGroup ({
      'username': new FormControl('', Validators.required),
      'password': new FormControl('', Validators.required)
    });
  }

  ngOnInit() {
  }

  loginUser() {
    this.autentificacionService.loginUser(this.forma.value.username, this.forma.value.password)
                                .subscribe((data: any) => {
                                  this.autentificacionService.setUser(data.user.username);
                                  this.autentificacionService.setToken(data.id);
                                  this.router.navigate(['admin']);
                                }, (errorServicio) => {this.mensajeError = errorServicio.error.error.message;
                                                       this.error = true;} );
  }
  // registrarUsuario() {
  //   this.autentificacionService.registrarUsuario().subscribe(data => console.log(data));
  // }

}
