export interface Noticia {
    imgpath: string;
    titulo: string;
    resumen: string;
    cuerpo: string;
    fecha: any;
    id?: number;
}
