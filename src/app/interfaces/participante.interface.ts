export interface Participante {
    paterno: string;
    materno?: string;
    pri_nombre: string;
    seg_nombre?: string;
    celular: number;
    email: string;
    eventoId?: number;
    id?: number;
}
