export interface Caracteristica {
    nombre: string;
    resumen: string;
    imgpath: string;
    descripcion: string;
    fecha?: any;
    id?: number;
}
