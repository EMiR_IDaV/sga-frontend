export interface Recurso {
    nombre: string;
    docpath: string;
    id?: number;
}
