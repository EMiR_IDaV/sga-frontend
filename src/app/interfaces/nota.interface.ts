export interface Nota {
    moduloId: number;
    estudianteId: number;
    nota: number;
    observacion: string;
    id?: number;
}
