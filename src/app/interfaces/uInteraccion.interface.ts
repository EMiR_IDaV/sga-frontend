export interface UInteraccion {
    nombre: string;
    descripcion: string;
    linea: string;
    lineas: string;
    invitacion: string;
    id?: number;
}
