export interface Auxiliar {
    imgpath: string;
    reg_universitario: number;
    resena: string;
    descripcion: string;
    denominacion: string;
    convocatoriainvesId?: number;
    proyectoId?: number;
    id?: number;
}
