export interface Docente {
    reg_docente: number;
    resena: string;
    grado: string;
    descripcion: string;
    imgpath: string;
    id?: number;
    usuarioId?: number;
}
