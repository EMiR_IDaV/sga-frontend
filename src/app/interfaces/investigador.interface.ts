export interface Investigador {
    resena: string;
    grado: string;
    descripcion: string;
    imgpath: string;
    denominacion: string;
    id?: number;
    usuarioId?: number;
}
