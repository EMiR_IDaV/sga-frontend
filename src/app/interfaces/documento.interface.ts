export interface Documento {
    nombre: string;
    descripcion: string;
    docpath: string;
    moduloId?: number;
    id?: number;
}
