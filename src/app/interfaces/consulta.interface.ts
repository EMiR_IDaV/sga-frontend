export interface Consulta {
    nombre: string;
    email: string;
    mensaje: string;
    fecha: any;
    leida: boolean;
    celular?: number;
    id?: number;
}
