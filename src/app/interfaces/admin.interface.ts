
export interface Admin {
    cargo: string;
    resena: string;
    grado: string;
    descripcion: string;
    imgpath: string;
    oficina: string;
    vista_posicion: number;
    id?: number;
    usuarioId?: number;
}
