import { Docente } from './docente.interface';
import { Convocatoriainves } from './convocatoriainves.interface';
export interface Proyecto {
    titulo: string;
    tipo: string;
    ambito: string;
    presentacion: string;
    abstract: string;
    fecha_publicacion: any;
    imgpath:  string;
    docpath:  string;
    convocatoriainvesId?: number;
    docente?: Docente;
    id?: number;
}

// La Comununicación es vista en este libro como un lugar desde el cual es dable trabajar la decolonización en el plano de las ideas tanto como en el de las prácticas. Con esa convicción, 12 autoras y 12 autores de Argentina, Bolivia, Colombia, Chile, Ecuador, España, México y Perú presentan en estas páginas variadas reflexiones de índole apistemológica, teórica, metodológica, política y experiencial que ponen en análisis y movimiento el pensamiento decolonial, que se yergue como un renovador horizonte de inteligibilidad y de acción liberadora para los pueblos del Sur.
