import { Nota } from './nota.interface';
export interface Modulo {
    nombre: string;
    numero: number;
    profesor?: string;
    carga_horaria_a: number;
    carga_horaria_b: number;
    horas_academicas: number;
    creditos: number;
    inicio_clases: any;
    fin_clases: any;
    observacion?: string;
    descripcion?: string;
    docenteId?: number;
    numeroEstudiantes?: number;
    id?: number;
    nota?: Nota;
}
