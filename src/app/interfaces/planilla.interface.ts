export interface Planilla {
    asistenciaId: number;
    estudianteId: number;
    estado: string;
    id?: number;
}
