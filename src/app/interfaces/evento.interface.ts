export interface Evento {
    titulo: string;
    descripcion_capacitador: string;
    tipo: string;
    descripcion: string;
    imgpath: string;
    fecha: any;
    lugar: string;
    auspiciadores: string;
    entrada: string;
    dirigido_a: string;
    contactos: string;
    id?: number;
}
