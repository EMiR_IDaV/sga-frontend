export interface UInvestigacion {
    nombre: string;
    descripcion: string;
    imgpath: string;
    linea: string;
    lineas: string;
    concepto: string;
    rol: string;
    id?: number;
}
