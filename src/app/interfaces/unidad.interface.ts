export interface Unidad {
    nombre: string;
    descripcion: string;
    id?: number;
}
