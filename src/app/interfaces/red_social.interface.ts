export interface RedSocial {
    nombre: string;
    direccion_url: string;
    imgpath: string;
    id?: number;
}
