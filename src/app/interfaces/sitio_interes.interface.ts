export interface SitioInteres {
    nombre: string;
    direccion_url: string;
    id?: number;
}
