export interface Curso {
    nombre: string;
    sigla: string;
    acerca: string;
    areas: string;
    perfiles: string;
    admision: string;
    gestion: string;
    nro_resolucion: string;
    costo: string;
    modalidad: string;
    num_plazas?: string;
    inicio_clases: any;
    inscripcion: any;
    inicio_matriculacion: any;
    fin_matriculacion: any;
    dias_horarios: string;
    otra?: string;
    id?: number;
}
