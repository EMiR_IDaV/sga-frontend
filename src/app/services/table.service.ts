import { Injectable } from '@angular/core';
import { Usuario } from '../interfaces/usuario.interface';
import { Nota } from '../interfaces/nota.interface';
import { Asistencia } from '../interfaces/asistencia.interface';


@Injectable({
  providedIn: 'root'
})
export class TableService {

  altura: number;

  constructor() { }

  tListaEvento(doc: any, altura: number, lista: any[], cabecera: string[], alineacionCabecera: string[], alineacionCuerpo: string[], xs: number[], tamanoFuente: number, tipoFuenteCuerpo: string, salto: number, dililearriba: number, dililederecha: number, notas?: Nota[]) {

    this.altura = altura;

    doc.setFontSize(tamanoFuente);
    doc.setFont(tipoFuenteCuerpo);

    const altaux = this.saltar(salto);
    let yi = altaux - (salto - dililearriba);

    // CABECERA DE LA TABLA >>center, >>right, >>justify
    doc.line(xs[0], yi, xs[xs.length - 1], yi);
    for (let index = 0; index < cabecera.length; index++) {
      let cab = cabecera[index];
      cab = cab.charAt(0).toUpperCase() + cab.slice(1);    // capitaliza
      const ali = alineacionCabecera[index];
      const x = xs[index];
      if (ali === '') {
        doc.text(cab, x + dililederecha, altaux);
      } else {

        if (ali === 'center') {
          doc.text(cab, (x + xs[index + 1]) / 2, altaux, 'center');
        } else {

          if (ali === 'right') {
            doc.text(cab, xs[index + 1] - 2, altaux, 'right');
          } else {
            // doc.text('Celular', (this.x4 + this.x5) / 2, altaux, 'justify'); TODO TODO ver como va a entrar
          }
        }
      }
    }
    doc.line(xs[0], yi + salto, xs[xs.length - 1], yi + salto);

    // CUERPO DE LA TABLA
    for (let index = 0; index < lista.length; index++) {
      const element = lista[index];
      let alturaCuerpo = this.saltar(salto);

      doc.text((index + 1).toString(), (xs[0] + xs[1]) / 2, alturaCuerpo, 'center');
      for (let inde = 1; inde < cabecera.length; inde++) {
        let cab = cabecera[inde];
        const aliCuerpo = alineacionCuerpo[inde];
        const x = xs[inde];
        if (cabecera[inde] !== 'APELLIDOS Y NOMBRES') {
          if (cabecera[inde] !== 'firma') {
            cab = cab + '';
            element[cab] = element[cab].toString();
            if (aliCuerpo === '') {
              doc.text(element[cab], x + dililederecha, alturaCuerpo);
            } else {

              if (aliCuerpo === 'center') {
                doc.text(element[cab], (x + xs[inde + 1]) / 2, alturaCuerpo, 'center');
              } else {

                if (aliCuerpo === 'right') {
                  doc.text(element[cab], xs[inde + 1] - 2, alturaCuerpo, 'right');
                } else {
                  // doc.text('Celular', (this.x4 + this.x5) / 2, altaux, 'justify'); TODO TODO ver como va a entrar
                }
              }
            }

          }
        } else {
          doc.text(this.nomCompleto(element).toString().toUpperCase(), x + dililederecha, alturaCuerpo);
        }
      }
      doc.line(xs[0], alturaCuerpo + dililearriba, xs[xs.length - 1], alturaCuerpo + dililearriba);

      if (alturaCuerpo + 13 > 279) {

        const yfint = this.altura + dililearriba;
        for (let index2 = 0; index2 < xs.length; index2++) {
          const x = xs[index2];
          doc.line(x, yi, x, yfint);
        }

        alturaCuerpo = 16;
        this.altura = 16;
        yi = alturaCuerpo + dililearriba;

        doc.addPage();

        doc.line(xs[0], yi, xs[xs.length - 1], yi);
      }
    }

    const yf = this.altura + dililearriba;
    for (let index = 0; index < xs.length; index++) {
      const x = xs[index];
      doc.line(x, yi, x, yf);
    }

  }

  tabla(doc: any, altura: number, lista: any[], cabecera: string[], alineacionCabecera: string[], xs: number[], tamanoFuente: number, tipoFuenteCuerpo: string, salto: number, dililearriba: number, dililederecha: number, notas?: Nota[]) {

    this.altura = altura;

    doc.setFontSize(tamanoFuente);
    doc.setFont(tipoFuenteCuerpo);

    const altaux = this.saltar(salto);
    const yi = altaux - (salto - dililearriba);

    // CABECERA DE LA TABLA >>center, >>right, >>justify
    doc.line(xs[0], yi, xs[xs.length - 1], yi);
    for (let index = 0; index < cabecera.length; index++) {
      let cab = cabecera[index];
      cab = cab.charAt(0).toUpperCase() + cab.slice(1);    // capitaliza
      const ali = alineacionCabecera[index];
      const x = xs[index];
      if (ali === '') {
        doc.text(cab, x + dililederecha, altaux);
      } else {

        if (ali === 'center') {
          doc.text(cab, (x + xs[index + 1]) / 2, altaux, 'center');
        } else {

          if (ali === 'right') {
            doc.text(cab, xs[index + 1] - 2, altaux, 'right');
          } else {
            // doc.text('Celular', (this.x4 + this.x5) / 2, altaux, 'justify'); TODO TODO ver como va a entrar
          }

        }

      }

    }
    doc.line(xs[0], yi + salto, xs[xs.length - 1], yi + salto);

    // CUERPO DE LA TABLA
    for (let index = 0; index < lista.length; index++) {
      const element = lista[index];
      let alturaCuerpo = this.saltar(salto);

      doc.text((index + 1).toString(), xs[0] + dililederecha, alturaCuerpo);
      for (let inde = 1; inde < cabecera.length; inde++) {
        let cab = cabecera[inde];
        const x = xs[inde];
        if (cabecera[inde] !== 'APELLIDOS Y NOMBRES') {
            cab = cab + '';
            element[cab] = element[cab].toString();
            if (element[cab].length <= 55 ) {
              doc.text(element[cab], x + dililederecha, alturaCuerpo);
            } else {
              let ultimoEspacio = 0;
              let cadenaAuxiliar = element[cab];
              while (cadenaAuxiliar.length > 55) {
                const lineaAuxiliar = cadenaAuxiliar.slice(0, 55);
                ultimoEspacio = lineaAuxiliar.lastIndexOf(' ');
                const lineaImprimir = lineaAuxiliar.slice(0, ultimoEspacio);
                doc.text(lineaImprimir, x + dililederecha, alturaCuerpo);
                alturaCuerpo = this.saltar(salto - 1.5);
                cadenaAuxiliar = cadenaAuxiliar.slice(ultimoEspacio + 1, cadenaAuxiliar.length);
              }
              doc.text(cadenaAuxiliar, x + dililederecha, alturaCuerpo);
            }
        } else {
          doc.text(this.nomCompleto(element).toString().toUpperCase(), x + dililederecha, alturaCuerpo);
        }
      }
      doc.line(xs[0], alturaCuerpo + dililearriba, xs[xs.length - 1], alturaCuerpo + dililearriba);
    }

    const yf = this.altura + dililearriba;
    for (let index = 0; index < xs.length; index++) {
      const x = xs[index];
      doc.line(x, yi, x, yf);
    }

  }

  wLineasVerticales(doc: any, yi: number, dililearriba: number, xs: any[]) {
    const yf = this.altura + dililearriba;
    for (let index = 0; index < xs.length; index++) {
      const x = xs[index];
      doc.line(x, yi, x, yf);
    }
  }

  saltar(s: number) {
    return this.altura = this.altura + s;
  }

  nomCompleto(usuario: Usuario): string {
    return usuario.paterno + ' ' + usuario.materno + ' ' + usuario.pri_nombre + ' ' + usuario.seg_nombre;
  }

  apellidos(usuario: Usuario): string {
    return usuario.paterno + ' ' + usuario.materno;
  }

  nombres(usuario: Usuario): string {
    return usuario.pri_nombre + ' ' + usuario.seg_nombre;
  }

  escribirCabecera(doc: any, asistencias: Asistencia[], verticesTabla: number[], altaux: number, salto: number, distanciaTextoLineaHorizontal: number, distanciaLineaTextoVertical: number) {
    // CABECERA DE LA TABLA >>center, >>right, >>justify

    const yiesc = altaux - ((salto + 1) - distanciaTextoLineaHorizontal);  // 57
    doc.line(verticesTabla[0], yiesc, verticesTabla[verticesTabla.length - 1], yiesc);

    doc.text('N°', verticesTabla[0] + distanciaLineaTextoVertical, altaux);
    doc.text('Apellidos y Nombres', verticesTabla[1] + distanciaLineaTextoVertical, altaux);

    const tamanoAsistencia = 231 / asistencias.length;

    for (let index = 0; index < asistencias.length; index++) {
      const x = verticesTabla[2] + (tamanoAsistencia * index);
      const cab = `Clase ${index + 1}`;
      const arrayFechas = asistencias[index].fecha.split('-');
      const mes = this.obtieneMes(Number(arrayFechas[1]));
      const fecha = `${Number(arrayFechas[2])} de`;
      doc.text(cab, (x + (tamanoAsistencia / 2)), altaux, 'center');
      doc.text(fecha, (x + (tamanoAsistencia / 2)), altaux + salto, 'center');
      doc.text(mes, (x + (tamanoAsistencia / 2)), altaux + salto + salto, 'center');
    }

    this.saltar(salto);
    this.saltar(salto);
    doc.line(verticesTabla[0], this.altura + distanciaTextoLineaHorizontal, verticesTabla[verticesTabla.length - 1], this.altura + distanciaTextoLineaHorizontal);
  }

  tablaAsistencia(doc: any, altura: number, estudiantes: any[], asistencias: Asistencia[], planillas: any[], verticesTabla: number[], tamanoFuente: number, tipoFuenteCuerpo: string, salto: number, distanciaTextoLineaHorizontal: number, distanciaLineaTextoVertical: number) {

    this.altura = altura;

    doc.setFontSize(tamanoFuente);
    doc.setFontType(tipoFuenteCuerpo);

    const altaux = this.saltar(salto);                            // 60
    let yi = altaux - ((salto + 1) - distanciaTextoLineaHorizontal);  // 57

    const tamanoAsistencia = 231 / asistencias.length;

    this.escribirCabecera(doc, asistencias, verticesTabla, altaux, salto, distanciaTextoLineaHorizontal, distanciaLineaTextoVertical);

    // CUERPO DE LA TABLA
    for (let index = 0; index < estudiantes.length; index++) {
      const element = estudiantes[index];
      this.saltar(salto);
      let alturaCuerpo = this.saltar(salto);

      doc.text((index + 1).toString(), verticesTabla[0] + distanciaLineaTextoVertical, alturaCuerpo);
      doc.text(this.apellidos(element), verticesTabla[1] + distanciaLineaTextoVertical, alturaCuerpo);
      doc.text(this.nombres(element), verticesTabla[1] + distanciaLineaTextoVertical, alturaCuerpo + salto);

      for (let index1 = 0; index1 < asistencias.length; index1++) {
        const plani = planillas[index1];

        const planilla = plani[index];
        const x = verticesTabla[2] + (tamanoAsistencia * index1);

        if (planilla.estado.includes('ermiso')) {
          doc.text(planilla.estado.slice(0, 7), (x + (tamanoAsistencia / 2)), alturaCuerpo, 'center');
          doc.text(planilla.estado.slice(7, planilla.estado.length), (x + (tamanoAsistencia / 2)), alturaCuerpo + salto, 'center');
        } else {
          doc.text(planilla.estado, (x + (tamanoAsistencia / 2)), alturaCuerpo, 'center');
        }
      }
      this.saltar(salto);

      alturaCuerpo = this.saltar(salto);
      doc.line(verticesTabla[0], alturaCuerpo + distanciaTextoLineaHorizontal, verticesTabla[verticesTabla.length - 1], alturaCuerpo + distanciaTextoLineaHorizontal);

      if (alturaCuerpo + 20 > 216) {

        const yfint = this.altura + distanciaTextoLineaHorizontal;
        for (let index2 = 0; index2 < verticesTabla.length; index2++) {
          const x = verticesTabla[index2];
          doc.line(x, yi, x, yfint);
        }
        for (let index3 = 0; index3 < asistencias.length - 1; index3++) {
          const x = verticesTabla[verticesTabla.length - 2] + (tamanoAsistencia * (index3 + 1));
          doc.line(x, yi, x, yfint);
        }

        alturaCuerpo = 28;
        this.altura = 28;
        yi = alturaCuerpo - ((salto + 1) - distanciaTextoLineaHorizontal);

        doc.addPage();
        this.escribirCabecera(doc, asistencias, verticesTabla, alturaCuerpo, salto, distanciaTextoLineaHorizontal, distanciaLineaTextoVertical);

      }
    }

    const yf = this.altura + distanciaTextoLineaHorizontal;
    for (let index = 0; index < verticesTabla.length; index++) {
      const x = verticesTabla[index];
      doc.line(x, yi, x, yf);
    }
    for (let index = 0; index < asistencias.length - 1; index++) {
      const x = verticesTabla[verticesTabla.length - 2] + (tamanoAsistencia * (index + 1));
      doc.line(x, yi, x, yf);
    }

  }

  obtieneMes(numeroMes: number) {
    switch (numeroMes) {
      case 1:
        return 'Enero';
      case 2:
        return 'Febrero';
      case 3:
        return 'Marzo';
      case 4:
        return 'Abril';
      case 5:
        return 'Mayo';
      case 6:
        return 'Junio';
      case 7:
        return 'Julio';
      case 8:
        return 'Agosto';
      case 9:
        return 'Septiembre';
      case 10:
        return 'Octubre';
      case 11:
        return 'Noviembre';
      case 12:
        return 'Diciembre';
    }
  }

  tablaNotasEstudiante(doc: any, altura: number, cabecera: string[], alineacionCabecera: string[], listaModulos: any[], alineacionCuerpo: string[], xs: number[], tamanoFuente: number, tipoFuenteCuerpo: string, salto: number, dililearriba: number, dililederecha: number) {
    this.altura = altura;
    doc.setFontSize(tamanoFuente);
    doc.setFont(tipoFuenteCuerpo);
    const yi = altura - 1;
    const altaux = this.saltar(salto);

    // CABECERA DE LA TABLA >>center, >>right, >>justify
    doc.line(xs[0], yi, xs[xs.length - 1], yi);
    for (let index = 0; index < cabecera.length; index++) {
      const cab = cabecera[index];
      const ali = alineacionCabecera[index];
      const x = xs[index];
      if (ali === '') {
        doc.text(cab, x + dililederecha, altaux);
      } else {

        if (ali === 'center') {
          doc.text(cab, (x + xs[index + 1]) / 2, altaux, 'center');
        } else {

          if (ali === 'right') {
            doc.text(cab, xs[index + 1] - 2, altaux, 'right');
          } else {
          }
        }
      }
    }

    doc.line(xs[0], yi + salto + 2, xs[xs.length - 1], yi + salto + 2);

    // CUERPO DE LA TABLA
    for (let index = 0; index < listaModulos.length; index++) {
      const element = listaModulos[index];
      const alturaCuerpo = this.saltar(salto);

      doc.text((element.numero).toString(), (xs[0] + (xs[1] - xs[0]) / 2), alturaCuerpo, 'center');
      if (element.nombre.length <= 70 ) {
        doc.text(element.nombre, xs[1] + dililederecha, alturaCuerpo);
      } else {
        let salto1 = 0;
        let ultimoEspacio = 0;
        let cadenaAuxiliar = element.nombre;
        while (cadenaAuxiliar.length > 70) {
          const lineaAuxiliar = cadenaAuxiliar.slice(0, 70);
          ultimoEspacio = lineaAuxiliar.lastIndexOf(' ');
          const lineaImprimir = lineaAuxiliar.slice(0, ultimoEspacio);
          doc.text(lineaImprimir, xs[1] + dililederecha, alturaCuerpo + salto1);
          salto1 = salto1 + salto;
          cadenaAuxiliar = cadenaAuxiliar.slice(ultimoEspacio + 1, cadenaAuxiliar.length);
        }
        doc.text(cadenaAuxiliar, xs[1] + dililederecha, alturaCuerpo + salto1 - 1);
      }
      doc.text(this.obtieneFechaddMMyyyy(element.inicio_clases), xs[2] + (xs[3] - xs[2]) / 2, alturaCuerpo, 'center');
      doc.text(this.notaNumeral(element.nota.nota), xs[3] + (xs[4] - xs[3]) / 2, alturaCuerpo, 'center');
      doc.text(this.notaDetalle(element.nota.nota), xs[4] + (xs[xs.length - 1] -  xs[4]) / 2, alturaCuerpo, 'center');
      doc.text(this.obtieneFechaddMMyyyy(element.fin_clases), xs[2] + (xs[3] - xs[2]) / 2, this.saltar(salto - 1), 'center');

      doc.line(xs[0], alturaCuerpo + salto - 1 + dililearriba, xs[xs.length - 1], alturaCuerpo + salto - 1 + dililearriba);
    }

    const yf = this.altura + dililearriba;
    for (let index = 0; index < xs.length; index++) {
      const x = xs[index];
      doc.line(x, yi, x, yf);
    }

  }

  obtieneFechaddMMyyyy(fec?: Date) {
    let d;
    if (fec) {
      d = new Date(fec);
    } else {
      d = new Date();
    }

    let fecha = '';
    const dd = d.getDate();
    const mm = d.getMonth() + 1;
    console.log(mm);
    const yyyy = d.getFullYear();
    if (dd < 10) {
      fecha = `0${dd}/`;
    } else {
      fecha = `${dd}/`;
    }
    if (mm < 10) {
      fecha = `${fecha}0${mm}/`;
    } else {
      fecha = `${fecha}${mm}/`;
    }
    return fecha = `${fecha}${yyyy}`;
  }

  notaDetalle(value: any): string {
    if (value === 0) {
      return '---';

    } else {
      if (value > 69) {
        return 'Aprobado';
      }
      return 'Reprobado';
    }
  }

  notaNumeral(value: any): string {
    if (value > 0) {
      return value.toString();
    } else {
      return '---';
    }
  }

}
