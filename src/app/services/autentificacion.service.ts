import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { URL_API } from './urls';
import { Location } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class AutentificacionService {

  httpHeaders: HttpHeaders = new HttpHeaders ({
    'Content-Type': 'application/json'
  });

  constructor(private httpClient: HttpClient, private router: Router, private location: Location) {
  }

  isAuth() {
    if (this.getToken()) {
      return true;
    }
    console.error('Unauthorized access');
    this.router.navigate(['']);
    return false;

  }


  // // Aumentar restriccion para el modelo RoleMapping
  // async idUsuario() {
  //   const VER_IDD = `${URL_API}RoleMappings/findOne?filter={"where":{"principalId":"${this.getId()}"}}`;
  //   return  await this.httpClient.get(VER_IDD).toPromise();
  // }

  appertainItems() {
    const VER_IS = `${URL_API}usuarios/${this.getId()}/accessTokens/${this.getToken()}?access_token=${this.getToken()}`;
    return this.httpClient.get(VER_IS);
  }

  // Aumentar restriccion para el modelo RoleMapping AQUI --- revisar antes de liberar
  idRoleUsuario() {
    const VER_IDD = `${URL_API}RoleMappings/findOne?filter={"where":{"principalId":"${this.getId()}"}}`;
    return this.httpClient.get(VER_IDD).pipe(map(data => data['roleId']));
  }

  loginUser(username: string, password: string): Observable<any> {
    const URL_LOG = 'http://localhost:3000/api/usuarios/login';
    return this.httpClient.post(URL_LOG,
                                {username, password, 'ttl': 10800},
                                {headers: this.httpHeaders}).
                            pipe(map(data => data));
  }

  confirmUser(email: string, password: string): Observable<any> {
    const URL_LOG = 'http://localhost:3000/api/usuarios/login';
    return this.httpClient.post(URL_LOG, {email, password, 'ttl': 10800}, {headers: this.httpHeaders});
  }

  confirmUserOut(token: string) {
    const URL = `http://localhost:3000/api/usuarios/logout?access_token=${token}`;
    return this.httpClient.post(URL, { headers: this.httpHeaders });
  }

  setUser(username: string) {
    const user_string = JSON.stringify(username);
    localStorage.setItem('username', user_string);
  }

  setToken(token: string) {
    localStorage.setItem('token', token);
  }

  getToken() {
    return localStorage.getItem('token');
  }

  getId() {
    return localStorage.getItem('username');
  }

  logoutUser() {
    const token = localStorage.getItem('token');
    const URL = `http://localhost:3000/api/usuarios/logout?access_token=${token}`;
    localStorage.removeItem('username');
    localStorage.removeItem('token');
    return this.httpClient.post(URL, { headers: this.httpHeaders });
  }


  // getCurrentUser() {
  //   const user_string = localStorage.getItem('username');
  //   if (!isNullOrUndefined(user_string)) {
  //     const user = JSON.parse(user_string);
  //     return user;
  //   } else {
  //     return null;
  //   }
  // }
}
