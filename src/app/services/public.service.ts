import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL_API } from './urls';
import { map } from 'rxjs/operators';
import { Location } from '@angular/common';
import { Consulta } from '../interfaces/consulta.interface';

@Injectable({
  providedIn: 'root'
})
export class PublicService {

  constructor(private httpClient: HttpClient, private location: Location) {}

  rolem(username: string) {
    const GET_DOC_CON = `${URL_API}usuarios/rolem/${username}`;
    return this.httpClient.get(GET_DOC_CON).pipe(map((data: any) => data.length));
  }

  investigator(username: string) {
    const GET_DOC_CON = `${URL_API}usuarios/investigator/${username}`;
    return this.httpClient.get(GET_DOC_CON).pipe(map((data: any) => data.length));
  }

  getPublicTipo(gestionConv: string, docinv: string, tipo: string) {
    const GET_DOC_CON = `${URL_API}usuarios/${gestionConv}/public${tipo}/${docinv}`;
    return this.httpClient.get(GET_DOC_CON).pipe(map(data => data[0]));
  }

  getPublicTipoPluralPorConvocatoria(gestionConv: string, tipoPlural: string) {
    const GET_DOS_CON = `${URL_API}usuarios/${gestionConv}/public${tipoPlural}PorConvocatoria`;
    return this.httpClient.get(GET_DOS_CON);
  }

  getPublicTipoPluralPorProyecto(idProyecto: number, tipoPlural: string) {
    const GET_DOS_PRO = `${URL_API}usuarios/${idProyecto}/public${tipoPlural}PorProyecto`;
    return this.httpClient.get(GET_DOS_PRO);
  }

  getRegistrosOrderLimiteSkip(modeloPlural: string, order: string, ascOdesc: string,  limit: number, skip: number) {
    const GET_RES = `${URL_API}${modeloPlural}?filter={"order":"${order} ${ascOdesc}","limit":${limit},"skip":${skip}}`;
    return this.httpClient.get(GET_RES);
  }

  countRegistros(modeloPlural: string, buscarPor?: string, valorBuscar?: any) {
    let COU_RES = `${URL_API}${modeloPlural}/count`;
    if (buscarPor) {
      COU_RES = `${COU_RES}?where={"${buscarPor}":"${valorBuscar}"}`;
    }
    return this.httpClient.get(COU_RES);
  }

  getRegistrosTipoNoticiasPag(modeloPlural: string, skip: number, limit?: number) {
    const GET_ESTS = `${URL_API}${modeloPlural}?filter={"order":"fecha desc","limit":${limit},"skip":${skip * 9}}`;
    return this.httpClient.get(GET_ESTS);
  }

  getRegistroByProperty(modeloPlural: string, property: string, value: string) {
    const GET_NOT = `${URL_API}${modeloPlural}/findOne?filter={"where":{"${property}":"${value}"}}`;
    return this.httpClient.get(GET_NOT);
  }

  retrocederPagina() {
    this.location.back();
  }

  cortarFechasModelo(modelo: any, tipoModelo: string) {
    let tipoFecha = [];
    if (tipoModelo === 'curso') {
      tipoFecha = ['inicio_clases', 'inscripcion', 'inicio_matriculacion', 'fin_matriculacion'];
    } else {
      tipoFecha = ['inicio_clases', 'fin_clases'];
    }

    for (let index = 0; index < tipoFecha.length; index++) {
      const element = tipoFecha[index];
      for (const key in modelo) {
        if (modelo.hasOwnProperty(key)) {
          if (element === key) {
            modelo[key] = modelo[key].substring(0, 10);
          }
        }
      }
    }
  }

  eliminaGuionesParametro(datoIdentificador: string) {
    return datoIdentificador.replace(/-/gi, ' ');
  }

  getRegistroPorFiltro(modelo: string, buscarPor1: string, buscarNumberOString1: any) {
    const GET_REG = `${URL_API}${modelo}/findOne?filter={"where":{"${buscarPor1}":"${buscarNumberOString1}"}}`;
    return this.httpClient.get(GET_REG);
  }

  getRegistroRMPorFiltro(buscarPor1: string, buscarNumberOString1: any, data: string) {
    const GET_REG = `${URL_API}RoleMappings/findOne?filter={"where":{"${buscarPor1}":"${buscarNumberOString1}"}}&access_token=${data}`;
    return this.httpClient.get(GET_REG);
  }

  getRegistrosModelo(pluralModelo: string, orden?: string, ascOdesc?: string, limit?: number, distinto?: string) {
    let GET_MOD = `${URL_API}${pluralModelo}`;
    if (orden) {
      GET_MOD = `${GET_MOD}?filter={"order":"${orden} ${ascOdesc}"`;
      if (limit) {
        GET_MOD = `${GET_MOD},"limit":${limit}`;
      } else {
        if (distinto) {
          GET_MOD = `${GET_MOD},"where":{"sigla":{"neq":"${distinto}"}}`;
        }
      }
      GET_MOD = `${GET_MOD}}`;
    }
    return this.httpClient.get(GET_MOD);
  }

  getRegistrosTipoNoticia(modeloPlural: string, limit?: number, neq?: string) {
    if (!limit) {
      const GET_NOT = `${URL_API}${modeloPlural}?filter={"order":"fecha desc"}`;
      return this.httpClient.get(GET_NOT);
    } else {
      const GET_NOT = `${URL_API}${modeloPlural}?filter={"order":"fecha desc","limit":${limit},"where":{"titulo":{"neq":"${neq}"}}}`;
      return this.httpClient.get(GET_NOT);
    }
  }

  getPublicAdmins() {
    const GET_PUAS = `${URL_API}usuarios/publicAdmins`;
    return this.httpClient.get(GET_PUAS);
  }

  getPublicAdmin(cargo: string) {
    const GET_PUAS = `${URL_API}usuarios/publicAdmin/'${cargo}'`;
    return this.httpClient.get(GET_PUAS).pipe(map(data => data[0]));
  }

  getAdminsDataPublics() {
    const GET_ADDAPU = `${URL_API}/admins/aqusadpudas`;
    return this.httpClient.get(GET_ADDAPU).pipe(map(data => data['aqusadpudas']));
  }

  getCountCursos() {
    const URL_COUCUS = `${URL_API}cursos/count`;
    return this.httpClient.get(URL_COUCUS);
  }

  getCursosPaginados(skip: number, limit?: number) {
    const GET_ESTS = `${URL_API}cursos?filter={"order":"id desc","limit":${limit},"skip":${skip * 3}}`;
    return this.httpClient.get(GET_ESTS);
  }

  getCursoPorSigla(sigla: string) {
    const GET_CUR = `${URL_API}cursos?filter={"where":{"sigla":"${sigla}"}}`;
    return this.httpClient.get(GET_CUR).pipe(map((data: any) => data[0] ));
  }

  async getCursoPorSiglaAsync(sigla: string) {
    const GET_CUR = `${URL_API}cursos?filter={"where":{"sigla":"${sigla}"}}`;
    const response = await this.httpClient.get(GET_CUR).pipe(map((data: any) => data[0] )).toPromise();
    return response;
    // return this.httpClient.get(GET_CUR).pipe(map((data: any) => data[0] ));
  }

  async getRegistrosModeloAsync(pluralModelo: string, orden?: string, ascOdesc?: string, distinto?: string) {
    let GET_MOD = `${URL_API}${pluralModelo}`;
    if (orden) {
      if (distinto) {
        GET_MOD = `${GET_MOD}?filter={"order":"${orden} ${ascOdesc}", "where":{"sigla":{"neq":"${distinto}"}}}`;
      } else {
        GET_MOD = `${GET_MOD}?filter={"order":"${orden} ${ascOdesc}"}`;
      }
    }
    const response = await this.httpClient.get(GET_MOD).toPromise();
    return response;
  }

  getModulosPorId(id: number) {
    const GET_MOS = `${URL_API}cursos/${id}/modulos`;
    return this.httpClient.get(GET_MOS);
  }

  getModelosRelPorId(pluralModelo: string, idModelo: number, pluralModeloRelacionado: string, orderKey?: string) {    // GENERALIZADO
    let GET_MOSREL = `${URL_API}${pluralModelo}/${idModelo}/${pluralModeloRelacionado}`;
    if (orderKey) { GET_MOSREL = `${GET_MOSREL}?filter={"order":"${orderKey} asc"}`; }
    return this.httpClient.get(GET_MOSREL);
  }

  getDocentesPorId(id: number) {
    const GET_MOS = `${URL_API}cursos/${id}/docentes`;
    return this.httpClient.get(GET_MOS);
  }

  getSumHrs(cursoId: number) {
    const GET_SUMHRS = `${URL_API}/cursos/${cursoId}/total`;
    return this.httpClient.get(GET_SUMHRS).pipe(map((data: any) => data[0]));
  }

  postConsulta(consulta: Consulta) {
    const POS_CON = `${URL_API}consultas`;
    return this.httpClient.post(POS_CON, consulta);
  }

  getModulosConDocente(id: number) {
    const GET_MOSCODO = `${URL_API}cursos/${id}/modulos?filter={"where":{"docenteId":{"regexp":"/^[1-9][0-9]*$/"}},"order":"numero asc"}`;
    return this.httpClient.get(GET_MOSCODO);
  }
}
