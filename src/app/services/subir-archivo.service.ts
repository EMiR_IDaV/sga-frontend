import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AutentificacionService } from './autentificacion.service';
import { URL_CON } from './urls';

@Injectable({
  providedIn: 'root'
})
export class SubirArchivoService {

  url = URL_CON;

  constructor(private httpClient: HttpClient, private autService: AutentificacionService) {

  }

  getArchivos(contenedor: string) {
    const GET_ARS = `${this.url}${contenedor}/files?access_token=${this.autService.getToken()}`;
    return this.httpClient.get(GET_ARS);
  }

  subirArchivo( archivo: File, nombre: string, contenedor: string, id: string) {

    return new Promise( ( resolve, reject ) => {

      const formData = new FormData();
      const xhr = new XMLHttpRequest();

      formData.append( 'imagen', archivo, 'abc.jpg' );

      xhr.onreadystatechange = function() {

        if ( xhr.readyState === 4 ) {

          if ( xhr.status === 200 ) {
            console.log( 'Imagen subida' );
            resolve( xhr.response );
          } else {
            console.log( 'Fallo la subida');
            reject( xhr.response );
          }
        }
      };

      const url = `${this.url}${contenedor}/upload?filename=${nombre}&access_token=${this.autService.getToken()}`;

      xhr.open('POST', url, true );
      xhr.send( formData );
    });
  }

  delImage(contenedor: string, nombreImagen: string) {
    const DEL_IMG = `${this.url}${contenedor}/files/${nombreImagen}?access_token=${this.autService.getToken()}`;
    return this.httpClient.delete(DEL_IMG);
  }

  bajarArchivo(carpeta: string, imgpath: string) {

    const url = 'http://localhost:3000/api/containers/' + carpeta + '/download/' + imgpath + '?access_token=' + this.autService.getToken();
    return url;
    // console.log(url);
    // this.baja(url).subscribe(data => console.log(data));

  }

  baja(url: string) {
    return this.httpClient.get(url);
  }
}
