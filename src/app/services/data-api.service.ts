import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Usuario } from '../interfaces/usuario.interface';
import { AutentificacionService } from './autentificacion.service';
import { map } from 'rxjs/operators';
import { URL_API } from './urls';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { SubirArchivoService } from './subir-archivo.service';

@Injectable({
  providedIn: 'root'
})
export class DataApiService {

  at = 'access_token=';

  httpHeaders: HttpHeaders = new HttpHeaders ({
    'Content-Type': 'application/json'
  });

  constructor(private router: Router, private httpClient: HttpClient, private autentificacionService: AutentificacionService,
              private location: Location, private subirArchivoService: SubirArchivoService ) {}

  // CRUD MODELOS ---------------------------------------------------------------------------------------------------
  // el owner hace que todo relacionado por id se lo vuelva

  getCursoDocumentos(sigla: string) {
    const GET_DOS = `${URL_API}cursos/'${sigla}'/documentos?${this.at}${this.autentificacionService.getToken()}`;
    return this.httpClient.get(GET_DOS);
  }

  resetPassword(id: number, cont: string) {
    const RES_PAS = `${URL_API}usuarios/${id}?${this.at}${this.autentificacionService.getToken()}`;
    return this.httpClient.patch(RES_PAS, {'password': cont});
  }

  cambiarContrasena(passold: string, passnew: string) {
    const URL_LOG = `${URL_API}usuarios/change-password?${this.at}${this.autentificacionService.getToken()}`;
    return this.httpClient.post(URL_LOG,
                                {oldPassword: passold, newPassword: passnew, 'ttl': 10800},
                                {headers: this.httpHeaders}).
                            pipe(map(data => data));
  }

  patchRegistro(pluralModel: string, modelo: any) {
    const PAT_MOD = `${URL_API}${pluralModel}?${this.at}${this.autentificacionService.getToken()}`;
    return this.httpClient.patch(PAT_MOD, modelo);
  }

  getAuxiliaresPorConvocatoria(idConvocatoria: number) {
    const GET_AUS_INV = `${URL_API}usuarios/${idConvocatoria}/auxiliaresPorConvocatoria?${this.at}${this.autentificacionService.getToken()}`;
    return this.httpClient.get(GET_AUS_INV);
  }

  getPriNombreUsuario(idUsuario) {
    const GET_NOM = `${URL_API}usuarios/${idUsuario}?filter={"fields": {"pri_nombre": true}}&${this.at}${this.autentificacionService.getToken()}`;
    return this.httpClient.get(GET_NOM);
  }

  delContainer(nombre: string) {
    const DEL_CONT = `${URL_API}containers/${nombre}?${this.at}${this.autentificacionService.getToken()}`;
    return this.httpClient.delete(DEL_CONT);
  }

  getRegistrosDistinto(pluralModelo: string, campoDistinto?: string, valorDistinto?: string) {
    let GET_RES = `${URL_API}${pluralModelo}`;
      if (campoDistinto) {
        GET_RES = `${GET_RES}?filter={"where":{"${campoDistinto}":{"neq":"${valorDistinto}"}}}`;
      }
      console.log(GET_RES);
    return this.httpClient.get(GET_RES);
  }

  trimObjeto(objeto: any) {
    for (const key in objeto) {
      if (objeto.hasOwnProperty(key)) {
        // let element = objeto[key];
        if (typeof(objeto[key]) === 'string') {
          objeto[key] = objeto[key].trim();
        }
      }
    }
  }

  getDocsInvestigadoresPorConvocatoriaYProyecto(idConvocatoria: number, idProyecto: number) {
    const GET_UDS_INV = `${URL_API}usuarios/${idConvocatoria}/${idProyecto}/docsInvestigadoresPorProyecto?${this.at}${this.autentificacionService.getToken()}`;
    return this.httpClient.get(GET_UDS_INV);
  }

  eliminaArchivos(contenedor: string, archivoPath: string) {
    const paths = archivoPath.split(',');
    for (let index = 0; index < paths.length; index++) {
      const element = paths[index];
      this.subirArchivoService
            .delImage(contenedor, element)
              .subscribe(data => {
                console.log(`Archivo del ${contenedor} Eliminado.`);
              }, errorEliArchivo => {
                console.log('errorEliArchivo', errorEliArchivo);
              });
    }
  }

  getInvestiga(idConvocatoria: number, idInvestigador: number) {
    const GET_INA = `${URL_API}investigas/findOne?filter={"where":{"convocatoriainvesId":"${idConvocatoria}","investigadorId":"${idInvestigador}"}}&${this.at}${this.autentificacionService.getToken()}`;
    return this.httpClient.get(GET_INA);
  }

  getProyectosInvesPorConvocatoria(idConvocatoria: number) {
    const GET_PRS_INV = `${URL_API}proyectos/${idConvocatoria}/proyectosPorConvocatoria?${this.at}${this.autentificacionService.getToken()}`;
    return this.httpClient.get(GET_PRS_INV);
  }

  getInvestigadoresPorConvocatoria(idConvocatoria: number) {
    const GET_DOS_INV = `${URL_API}usuarios/${idConvocatoria}/investigadoresPorConvocatoria?${this.at}${this.autentificacionService.getToken()}`;
    return this.httpClient.get(GET_DOS_INV);
  }

  getInvestigadores() {
    const GET_DOS_INV = `${URL_API}usuarios/investigadores?${this.at}${this.autentificacionService.getToken()}`;
    return this.httpClient.get(GET_DOS_INV);
  }

  putRelacionModelos(modeloPlural: string, idModelo: number, modeloPluralRelacionado: string, fkModelosRelacionado: number) {
    const modelo = null;
    const PUT_REL = `${URL_API}${modeloPlural}/${idModelo}/${modeloPluralRelacionado}/rel/${fkModelosRelacionado}?${this.at}${this.autentificacionService.getToken()}`;
    return this.httpClient.put(PUT_REL, modelo);
  }

  getCountRegistrosFiltro(pluralOSingularModelo: string, buscarPor?: string, buscarNumberOString?: any) {
    let GET_COU_RES = `${URL_API}${pluralOSingularModelo}/count`;

    if (buscarPor) {
      GET_COU_RES = `${GET_COU_RES}?where={"${buscarPor}":"${buscarNumberOString}"}`;
    }
    GET_COU_RES = `${GET_COU_RES}&${this.at}${this.autentificacionService.getToken()}`;
    return this.httpClient.get(GET_COU_RES);

  }

  countRegistros(modeloPlural: string, buscarPor?: string, valorBuscar?: any) {
    let COU_RES = `${URL_API}${modeloPlural}/count`;
    if (buscarPor) {
      COU_RES = `${COU_RES}?where={"${buscarPor}":"${valorBuscar}"}&${this.at}${this.autentificacionService.getToken()}`;
    } else {
      COU_RES = `${COU_RES}?${this.at}${this.autentificacionService.getToken()}`;
    }
    return this.httpClient.get(COU_RES);
  }

  irPaginaArriba(activatedRoute: ActivatedRoute, dos?: boolean) {
    let path = '../';
    if (dos) {path = '../../'; }
    this.router.navigate([path], { relativeTo: activatedRoute });
  }

  getRegistro(modeloPlural: string, idRegistro: number) {
    const GET_REG = `${URL_API}${modeloPlural}/${idRegistro}?${this.at}${this.autentificacionService.getToken()}`;
    return this.httpClient.get(GET_REG);
  }

  getRegistroByProperty(modeloPlural: string, property?: string, value?: string) {
    const GET_NOT = `${URL_API}${modeloPlural}/findOne?filter={"where":{"${property}":"${value}"}}&access_token=${this.autentificacionService.getToken()}`;
    return this.httpClient.get(GET_NOT);
  }

  getUsuarioByIdIncluidoTipo(id: number, tipo: string) {
    const GET_USTI = `${URL_API}usuarios/${id}?filter={"include": "${tipo}"}&access_token=${this.autentificacionService.getToken()}`;
    return this.httpClient.get(GET_USTI);
  }

  getNotasCurso(id: number, idCurso: number) {
    const GET_NOS_CUR = `${URL_API}usuarios/${id}/${idCurso}/notasCurso?${this.at}${this.autentificacionService.getToken()}`;
    return this.httpClient.get(GET_NOS_CUR);
  }

  getCursosInscrito(id: number) {
    const GET_CUS_INS = `${URL_API}usuarios/${id}/cursosInscrito?${this.at}${this.autentificacionService.getToken()}`;
    return this.httpClient.get(GET_CUS_INS);
  }
  getRegistros(modeloPlural: string) {
    const GET_RES = `${URL_API}${modeloPlural}?${this.at}${this.autentificacionService.getToken()}`;
    return this.httpClient.get(GET_RES);
  }

  getIdUsuarioSiglaNumero (sigla: string, numero: number) {
    const GET_IDU_SINU = `${URL_API}cursos/'${sigla}'/${numero}/idUsuario?${this.at}${this.autentificacionService.getToken()}`;
    console.log(GET_IDU_SINU);
    return this.httpClient.get(GET_IDU_SINU).pipe(map(data => data[0]));
  }

  getDictaModulosCurso(idUsuario: number, idCurso: number) {
    const GET_MOSCUR_DIC = `${URL_API}usuarios/${idUsuario}/${idCurso}/dictaModulosCurso?${this.at}${this.autentificacionService.getToken()}`;
    return this.httpClient.get(GET_MOSCUR_DIC);
  }

  getCursosDicta(idUsuario: number) {
    const GET_CUS_DIC = `${URL_API}usuarios/${idUsuario}/dictaCursos?${this.at}${this.autentificacionService.getToken()}`;
    return this.httpClient.get(GET_CUS_DIC);
  }

  postModelo(pluralModelo: string, modelo: any) {
    const POS_MOD = `${URL_API}${pluralModelo}?${this.at}${this.autentificacionService.getToken()}`;
    return this.httpClient.post(POS_MOD, modelo);
  }

  delModelo(pluralModelo: string, id: number) {
    const DEL_MOD = `${URL_API}${pluralModelo}/${id}?${this.at}${this.autentificacionService.getToken()}`;
    return this.httpClient.delete(DEL_MOD);
  }

  putModelo(pluralModelo: string, modelo: any) {
    const PUT_MOD = `${URL_API}${pluralModelo}?${this.at}${this.autentificacionService.getToken()}`;
    return this.httpClient.put(PUT_MOD, modelo);
  }

  postModeloRelacionado(pluralModelo: string, idModelo: number, singularOpluralModeloRelacionado: string, modeloRelacionado: any) {
    const POS_MOD_REL = `${URL_API}${pluralModelo}/${idModelo}/${singularOpluralModeloRelacionado}?${this.at}${this.autentificacionService.getToken()}`;
    return this.httpClient.post(POS_MOD_REL, modeloRelacionado);
  }

  delModeloRelacionado(pluralModelo: string, idModelo: number, singularOpluralModeloRelacionado: string, idModeloRelacionado?: number, rel?: boolean) {
    let DEL_MOD_REL = `${URL_API}${pluralModelo}/${idModelo}/${singularOpluralModeloRelacionado}`;
    if (idModeloRelacionado > -1) {
      if (rel) {
        DEL_MOD_REL = `${DEL_MOD_REL}/rel/${idModeloRelacionado}?${this.at}${this.autentificacionService.getToken()}`;
      } else {
        DEL_MOD_REL = `${DEL_MOD_REL}/${idModeloRelacionado}?${this.at}${this.autentificacionService.getToken()}`;
      }
    } else {
      DEL_MOD_REL = `${DEL_MOD_REL}?${this.at}${this.autentificacionService.getToken()}`;
    }
    return this.httpClient.delete(DEL_MOD_REL);
  }

  putModeloRelacionado(pluralModelo: string, idModelo: number, singularOpluralModeloRelacionado: string, modeloRelacionado: any, fkModeloRelacionado?: number) {
    let PUT_MOD_REL = `${URL_API}${pluralModelo}/${idModelo}/${singularOpluralModeloRelacionado}`;
    if (fkModeloRelacionado) {
      PUT_MOD_REL = `${PUT_MOD_REL}/${fkModeloRelacionado}?${this.at}${this.autentificacionService.getToken()}`;
    } else {
      PUT_MOD_REL = `${PUT_MOD_REL}?${this.at}${this.autentificacionService.getToken()}`;
    }
    return this.httpClient.put(PUT_MOD_REL, modeloRelacionado);
  }

  verificarAdmin(idUsuario: number) {
    const GET = `${URL_API}RoleMappings/findOne?filter={"where":{"principalId": ${idUsuario}, "roleId":1}}&${this.at}${this.autentificacionService.getToken()}`;
    return this.httpClient.get(GET);
  }

  retrocederPagina() {
    this.location.back();
  }

  obtieneNombreProfesor(usuario: Usuario) {
    let nombre = `${usuario.pri_nombre}`;
    if (usuario.seg_nombre !== '') {
      nombre = `${nombre} ${usuario.seg_nombre}`;
    }
    nombre = `${nombre} ${usuario.paterno}`;
    if (usuario.materno !== '') {
      nombre = `${nombre} ${usuario.materno}`;
    }
    return nombre;
  }

  eliminaGuionesParametro(datoIdentificador: string) {
    return datoIdentificador.replace(/-/gi, ' ');
  }

  quitarAdministrador(id: number) {          // verificar por que ingresa cualquier numero sin validar su existencia
    const REM_ADMIN = `${URL_API}RoleMappings/${id}?${this.at}${this.autentificacionService.getToken()}`;
    return this.httpClient.delete(REM_ADMIN);
  }

  asignarAdministrador(id: number) {          // verificar por que ingresa cualquier numero sin validar su existencia
    const ASIG_ADMIN = `${URL_API}usuarios/${id}/aqmaadm?${this.at}${this.autentificacionService.getToken()}`;
    return this.httpClient.put(ASIG_ADMIN, {});
  }

  getEstudiantesPorSiglaNumero(sigla: string, numero: number) {
    const GET_EST = `${URL_API}usuarios/estudiantesPrivadoSiglaNumero/'${sigla}'/${numero}?${this.at}${this.autentificacionService.getToken()}`;
    return this.httpClient.get(GET_EST);
  }

  getModuloPorIdCursoNumeroModulo(idCurso: number, numeroModulo: number) {
    const PUT_MOID = `${URL_API}cursos/${idCurso}/modulos/?filter={"where":{"numero":${numeroModulo}}}&${this.at}${this.autentificacionService.getToken()}`;
    return this.httpClient.get(PUT_MOID).pipe(map((data: any) => data[0]));
  }

  async getModuloPorIdCursoNumeroModuloAsync(idCurso: number, numeroModulo: number) {
    const PUT_MOID = `${URL_API}cursos/${idCurso}/modulos/?filter={"where":{"numero":${numeroModulo}}}&${this.at}${this.autentificacionService.getToken()}`;
    const response = await this.httpClient.get(PUT_MOID).pipe(map((data: any) => data[0])).toPromise();
    return response;
  }

  async getModelosRelPorId1(pluralModelo: string, idModelo: number, pluralModeloRelacionado: string, orderKey?: string) {    // GENERALIZADO
    let GET_MOSREL = `${URL_API}${pluralModelo}/${idModelo}/${pluralModeloRelacionado}`;
    if (orderKey) { GET_MOSREL = `${GET_MOSREL}?filter={"order":"${orderKey} asc"}`; }
    return await this.httpClient.get(GET_MOSREL).toPromise();
  }

  getModelosRelPorId(pluralModelo: string, idModelo: number, pluralModeloRelacionado: string, orderKey?: string) {    // GENERALIZADO
    let GET_MOSREL = `${URL_API}${pluralModelo}/${idModelo}/${pluralModeloRelacionado}`;
    (orderKey) ? GET_MOSREL = `${GET_MOSREL}?filter={"order":"${orderKey} asc"}&${this.at}${this.autentificacionService.getToken()}` : GET_MOSREL = `${GET_MOSREL}?${this.at}${this.autentificacionService.getToken()}`;
    return this.httpClient.get(GET_MOSREL);
  }

  getNumeroRelacionados(pluralModelo: string, idModelo: number, pluralModeloRelacionado: string) {
    const GET_ESS_MOD = `${URL_API}${pluralModelo}/${idModelo}/${pluralModeloRelacionado}/count?${this.at}${this.autentificacionService.getToken()}`;
    return this.httpClient.get(GET_ESS_MOD);
  }

  getEstudiantesPorSigla(sigla: string) {
    const GET_EST_SIG = `${URL_API}usuarios/privadoEstudiantesPorSigla/'${sigla}'?${this.at}${this.autentificacionService.getToken()}`;
    return this.httpClient.get(GET_EST_SIG);
  }

  getEstudianteIdsPorSiglaComplementoModulo(sigla: string, idModulo: number) {
    const GET_ESS_COM = `${URL_API}usuarios/estudiantesPorSiglaIdModuloComplemento/'${sigla}'/${idModulo}?${this.at}${this.autentificacionService.getToken()}`;
    return this.httpClient.get(GET_ESS_COM);
  }

  getModelosPorFiltroOrdenar(pluralOSingularModelo: string, buscarPor: string, buscarNumberOString: any, ordenarPor?: string, ascOdesc?: string) {
    let GET_MOD = `${URL_API}${pluralOSingularModelo}?filter={"where": {"${buscarPor}":"${buscarNumberOString}"}}&${this.at}${this.autentificacionService.getToken()}`;

    if (ordenarPor) {
      GET_MOD = `${URL_API}${pluralOSingularModelo}?filter={"where": {"${buscarPor}":"${buscarNumberOString}"},"order":"${ordenarPor} ${ascOdesc}"}&${this.at}${this.autentificacionService.getToken()}`;
    }
    return this.httpClient.get(GET_MOD);
  }

  getRegistrosPorFiltros(pluralOSingularModelo: string, buscarPor1: string, buscarNumberOString1: any, buscarPor2?: string, buscarNumberOString2?: any) {
    let GET_REG = `${URL_API}${pluralOSingularModelo}?filter={"where":{"${buscarPor1}":"${buscarNumberOString1}"`;
    if (buscarPor2) {
      GET_REG = `${GET_REG},"${buscarPor2}":"${buscarNumberOString2}"`;
    }
    GET_REG = `${GET_REG}}}&${this.at}${this.autentificacionService.getToken()}`;
    return this.httpClient.get(GET_REG);
  }

  getRegistroPorFiltros(pluralOSingularModelo: string, buscarPor1: string, buscarNumberOString1: any, buscarPor2?: string, buscarNumberOString2?: any) {
    let GET_REG = `${URL_API}${pluralOSingularModelo}/findOne?filter={"where":{"${buscarPor1}":"${buscarNumberOString1}"`;
    if (buscarPor2) {
      GET_REG = `${GET_REG},"${buscarPor2}":"${buscarNumberOString2}"`;
    }
    GET_REG = `${GET_REG}}}&${this.at}${this.autentificacionService.getToken()}`;
    return this.httpClient.get(GET_REG);
  }




  // ---------------------------------------------------------------------------------------------------

  getUsuarioId(id: number) {
    const GET_USID = `${URL_API}usuarios/${id}?access_token=${this.autentificacionService.getToken()}`;
    return this.httpClient.get(GET_USID);
  }

  getUsuariosIncludeTipo(tipo: string) {
    const GET_USSTI = `${URL_API}usuarios?filter={"include": "${tipo}"}&access_token=${this.autentificacionService.getToken()}`;
    return this.httpClient.get(GET_USSTI).pipe(map((data: any) => data.filter(usuario => usuario[tipo])));
  }

  getUsuarioIncluidoTipo(username: string, tipo: string) {
    const GET_USTI = `${URL_API}usuarios?filter={"include": "${tipo}","where":{"username":"${username}"}}&access_token=${this.autentificacionService.getToken()}`;
    return this.httpClient.get(GET_USTI).pipe(map((data: any) => data[0] ));
  }

  getUsuariosEstudiantes(cursoId: number) {
    const GET_USSESS = `${URL_API}usuarios/privadoEstudiantes/${cursoId}?access_token=${this.autentificacionService.getToken()}`;
    return this.httpClient.get(GET_USSESS);
  }

  getModuloById(idCurso: number, idModulo: number) {
    const PUT_MOID = `${URL_API}cursos/${idCurso}/modulos/${idModulo}?access_token=${this.autentificacionService.getToken()}`;
    return this.httpClient.get(PUT_MOID);
  }

  getUsuarioDocente(idDocente: number) {
    const GET_USUDOC = `${URL_API}usuarios/privadoDocente/${idDocente}?access_token=${this.autentificacionService.getToken()}`;
    return this.httpClient.get(GET_USUDOC).pipe(map((data: any) => data[0]));
  }

  getUsuariosDocentes() {
    const GET_USSDOS = `${URL_API}usuarios/privadoDocentes?access_token=${this.autentificacionService.getToken()}`;
    return this.httpClient.get(GET_USSDOS);
  }


  // --------------------------------------------------------------------------------------------------------------

  getEstudiantes(cursoId: number) {
    const GET_ESTS = URL_API + 'cursos/' + cursoId + '/estudiantes?filter={"include": "usuario"}&access_token=' + this.autentificacionService.getToken();
    return this.httpClient.get(GET_ESTS);
  }

  getUsuario(username: string) {
    const GET_USU = URL_API + 'usuarios?filter={"where": {"username":"' + username + '"}}&access_token=' + this.autentificacionService.getToken();
    return this.httpClient.get(GET_USU).pipe( map (data => data[0]));
  }

  getTipoUsuarioById(tipoUsuarios: string, id: number) {
    // const GET_EST = URL_API + 'estudiantes/' + id + '?filter={"include":"usuario"}&access_token=' + this.autentificacionService.getToken();
    const GET_TIP = `${URL_API}${tipoUsuarios}/${id}?filter={"include":"usuario"}&access_token=${this.autentificacionService.getToken()}`;
    return this.httpClient.get(GET_TIP);
  }

  getNoticiaByTitulo(titulo: string) {
    const GET_NOT = `${URL_API}noticias?filter={"where":{"titulo":"${titulo}"}}`;
    return this.httpClient.get(GET_NOT).pipe( map (data => data[0]));
  }

  getCaracteristicaByNombre(nombre: string) {
    const GET_CAR = `${URL_API}caracteristicas?filter={"where":{"nombre":"${nombre}"}}`;
    return this.httpClient.get(GET_CAR).pipe( map (data => data[0]));
  }

}
